% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc, patterns, matrix, backgrounds, angles}

\rivmathlib{geometry}

\usepackage{sesacompat}
\usepackage{multicol}

\persocourssetup{qrcodes=prod-scal}
%\AtEndDocument{\foreachlink{\makelink[here]{#1} }}

\begin{document}
\StudentMode

\chapter{Produit scalaire}

\section{Produit scalaire et vecteurs orthogonaux}

\subsection{Projeté orthogonal}

\begin{sidefigure}[fc]
    \begin{tikzpicture}[scale=0.7,rotate=10]
        \path (1,0) coordinate[label=below:$A$] (A) ;
        \path (4,0) coordinate[label=below:$B$] (B) ;
        \path (3,1.7) coordinate[label=above:$C$] (C) ;
        \path (A -| C) coordinate[label=below:$H$] (H) ;
        \draw ($ (A)!-1/3!(B) $)
                -- (A) pic[tick marker] {}
                -- (B) pic[tick marker] {}
                -- ($ (A)!4/3!(B) $);
        \draw[dashed] (C) -- (H) pic[perp marker] {} ;
        \draw (C) pic[point marker] {};
        %\draw[->, thick] (A) -- (B);
        %\draw[->, thick] (A) -- (C);
        %\draw[->] (A) -- (H);
    \end{tikzpicture}
\sidetext
    \begin{definition}
        Dans le plan, on considère une droite~$(AB)$ et un point~$C$. Le
        \define{projeté orthogonal} de~$C$ sur la droite~$(AB)$ est
        l'intersection~$H$ de~$(AB)$ et de la perpendiculaire à~$(AB)$ passant
        par~$C$.
    \end{definition}
\end{sidefigure}


\begin{example}
    \makelink{proj}
    On considère un carré $ABCD$~de centre~$O$. On note $I$~le milieu de~$[AB]$
    et $J$~le milieu de~$[OA]$.
    Faire la figure, puis déterminer:
    \begin{enumerate}
        \item le projeté orthogonal de~$O$ sur~$(AB)$;
        \item le projeté orthogonal de~$A$ sur~$(BC)$;
        \item les projetés orthogonaux sur~$(AC)$ de $A$, puis~$B$, et enfin~$I$.
    \end{enumerate}
    \IfStudentT{\bigfiller{5}}
\end{example}

\subsection{Formule du projeté orthogonal}

\begin{activity}
    \makelink{forces}
    Un chariot se déplace de $A$~à~$B$ sur des rails. Pendant ce déplacement, on
    lui applique une force constante représentée par un vecteur~$\vect{F}$
    partant du centre de masse de l'objet. On suppose que l'objet est trop lourd
    pour être soulevé ou sorti de ses rails.

    \NewDocumentCommand\dopic{mmO{}O{}}{%
        \ffigbox{}{\caption{}\label{fig:force-work-#1}%
        \begin{tikzpicture}[
                x=0.5cm,y=0.5cm,scale=1.1,
                #1/.try,
            ]
            \draw[blue] (0,0) coordinate[label=below:$A$] (A)
                    pic[point marker] {};
            \path (A) +(-2.5,-1.3) coordinate (SW);
            \path ($ (A)!-1!(SW) $) coordinate (NE);
            \fill ($ (SW)!1/5!(SW-|NE) $) +(0,-0.2) circle[radius=0.2];
            \fill ($ (SW)!4/5!(SW-|NE) $) +(0,-0.2) circle[radius=0.2];
            \path (A) +#2 coordinate (F);
            #4
            \draw[thick,->] (A)
                -- node[auto,#3,circle,inner sep=0.2ex] {$\vect{F}$} (F);
            \draw[->,blue]  (A) -- +(4,0) coordinate[label=below:$B$] (B);
            \path (current bounding box.south west)
                    +(-0.3,0) coordinate (BSW);
            \path (current bounding box.north east)
                    +(0.3,0.3) coordinate (BNE);
            \scoped[on background layer, shift=(A)] {
                \draw[help lines,step=1] (BSW) grid (BNE);
                \draw[fill,fill opacity=0.05] (NE) rectangle (SW);
            }
            \draw (BSW) -- (BNE |- BSW);
            \path[pattern = north east lines]
                (BSW) +(0,-0.3) rectangle (BNE |- BSW);
        \end{tikzpicture}%
        }\ignorespaces
    }%
    \thisfloatsetup{heightadjust=none}
    \begin{figure}
    \ffigbox{}{
        \begin{subfloatrow}[2]
            \dopic{neg}{(-4,0)}[swap]
            \dopic{med}{(2,0)}
        \end{subfloatrow}
        \begin{subfloatrow}[2]
            \dopic{negup}{(-4,2)}[][\path (-4,4);]
            \dopic{null}{(0,4)}
        \end{subfloatrow}
        \begin{subfloatrow}[2]
            \dopic{big}{(3,-1)}[swap][\path(-4,3);]
            \dopic{medup}{(2,3)}
        \end{subfloatrow}
        \caption{%
            Forces motrices ou résistives, avec un travail plus ou moins grand.
        }
        \label{fig:force-work}%
    }
    \end{figure}

    \begin{enumerate}
        \item \begin{itemize}
                \item Parmi tous les cas représentés dans la figure
                    \ref{fig:force-work}, le(s)quel(s) correspondent à une force
                    \emph{motrice} (qui favorise le déplacement) ?  Dans quel(s)
                    cas la force n'a-t-elle aucun effet sur le mouvement ?  Dans
                    quel(s) cas est-elle \emph{résistive} (elle s'oppose au
                    mouvement) ?

                    \IfStudentT{
                        motrice: \hfiller~
                        aucun effet: \hfiller~
                        résistive: \hfiller
                    }
                \item Entre les cas
                    \subref{fig:force-work-med}~et~\subref{fig:force-work-medup},
                    quelle force favorise le plus le déplacement ?
                    \IfStudentT{\bigfiller{1}}

                \item Sur la figure
                    \ref{fig:force-work}\subref{fig:force-work-big}
                    représenter une autre force ayant le même effet que la
                    force~$\vect{F}$ déjà représentée.
            \end{itemize}
    \end{enumerate}
    \textbf{Bilan :}
    \textit{
        L'intensité effective de la force (représentée par la longueur du
        vecteur~$\vect{F}$) n'a pas d'importance, c'est la composante
        horizontale (colinéaire au mouvement) de la force qui favorise ou freine
        le déplacement: tout l'effort fourni verticalement n'a aucun effet.
    }
    \begin{enumerate}[resume]
        \item \makelink{travail}
            Pour traduire cette efficacité les physiciens ont introduit la
            notion de \emph{travail} d'une force, qui représente la quantité
            d'énergie que la force va ajouter ou retirer au système.

            Soient~$C$ tel que $\vect{F} = \vect{AC}$ et $H$~le projeté
            orthogonal de~$C$ sur~$(AB)$. Alors le travail, en \emph{joules},
            est égal à $W = AB \times AH$ si $\vect{AB}$~et~$\vect{AH}$ sont de
            même sens, $W = AB \times AH$ si $\vect{AB}$~et~$\vect{AH}$ sont de
            sens contraire, et $W = 0$ si $\vect{AB}$~et~$\vect{AH}$ forment un
            angle droit.

            On prend une échelle d'un carreau pour $\SI{1}{\newton}$.
            La~distance entre $A$~et~$B$ est $\SI{4}{\m}$.
            Dans chaque cas, calculer le travail de la force.
            \IfStudentT{
                \foreach \x in {a,b,c,d,e,f} {%
                    \par \textsf{\textbf{(\x)}} \hfiller \par
                }
            }
    \end{enumerate}
\end{activity}

On peut généraliser la notion de travail d’une force pendant un déplacement à
deux vecteurs quelconques $\vect{AB}$~et~$\vect{AC}$:

\begin{definition}
    Soient $A$, $B$ et $C$ trois points du plan tels que $A \ne B$.
    On note~$H$ le projeté orthogonal de~$C$ sur~$(AB)$. Le \define{produit
    scalaire} de $\vect{AB}$~et~$\vect{AC}$ est le nombre réel noté
    $\vect{AB}\cdot\vect{AC}$ défini par l'une des trois configurations
    possibles:

    \begin{center}
        \leavevmode\kern-2cm
        \begin{tikzpicture}
            \matrix[matrix of nodes,
                anchor=base,
                row 2/.style={every picture/.style={
                                scale=0.7,
                                anchor=center,
                                baseline=(current bounding box),
                            }}] {
            $\vect{AH} = \vect{0}$ &
            $\vect{AB}$ et $\vect{AH}$ de même sens &
            $\vect{AB}$ et $\vect{AH}$ de sens opposé \\
        \begin{tikzpicture}[scale=0.7,rotate=55]
            \path (1,0) coordinate[label={below:$A = H$}] (A) ;
            \path (4,0) coordinate[label=above left:$B$] (B) ;
            \path (1,2) coordinate[label=above:$C$] (C) ;
            \path (A -| C) coordinate (H) ;
            \draw ($ (A)!-0.1!(B) $) -- ($ (A)!1.1!(B) $);
            \draw[dashed] (C) -- (H) pic[perp marker] {} ;
            \draw[->, thick] (A) -- (B);
            \draw[->, thick] (A) -- (C);
        \end{tikzpicture}
        &
        \begin{tikzpicture}[rotate=10]
            \path (1,0) coordinate[label=below:$A$] (A) ;
            \path (4,0) coordinate[label=below:$B$] (B) ;
            \path (3,1.7) coordinate[label=above:$C$] (C) ;
            \path (A -| C) coordinate[label=below:$H$] (H) ;
            \draw ($ (A)!-1/3!(B) $) -- ($ (A)!4/3!(B) $);
            \draw[dashed] (C) -- (H) pic[perp marker] {} ;
            \draw[->, thick] (A) -- (B);
            \draw[->, thick] (A) -- (C);
            \draw[->] (A) -- (H);
        \end{tikzpicture}
        &
        \begin{tikzpicture}[rotate=-10]
            \path (1,0) coordinate[label=above:$A$] (A) ;
            \path (4,0) coordinate[label=above:$B$] (B) ;
            \path (-0.5,-2) coordinate[label=below:$C$] (C) ;
            \path (A -| C) coordinate[label=above:$H$] (H) ;
            \draw ($ (H)!-1/5!(B) $) -- ($ (H)!6/5!(B) $);
            \draw[dashed] (C) -- (H) pic[perp marker] {} ;
            \draw[->, thick] (A) -- (B);
            \draw[->, thick] (A) -- (C);
            \draw[->] (A) -- (H);
        \end{tikzpicture}
        \\
            $\vect{AB}\cdot \vect{AC}= 0$ &
            $\vect{AB}\cdot \vect{AC} = AB\times AH$ &
            $\vect{AB}\cdot \vect{AC} = -AB\times AH$ \\
        };
        \end{tikzpicture}
        \kern-2cm
    \end{center}
\end{definition}


\begin{remark}
    Le produit scalaire de deux vecteurs n'est pas un vecteur mais \emph{un
    nombre réel}.
\end{remark}

\begin{remark}
    Si $A = B$ on définit
    $\vect{AB}\cdot\vect{AC} = \vect{0}\cdot\vect{AC} = 0$.
\end{remark}

\begin{sidefigure}[t,ft]
    \begin{tikzpicture}
        \path (0,0) coordinate[label=below:$A$] (A) ;
        \path (2,0) coordinate[label=below:$B$] (B) ;
        \path ($ (B)!1!-90:(A) $) coordinate[label=above:$C$] (C);
        \path ($ (C)!1!-90:(B) $) coordinate[label=above:$D$] (D);
        \path ($ (A)!1/2!(B) $) coordinate[label=below:$I$] (I);
        \draw (A) -- (I) pic[tick marker] {} -- (B) -- (C) -- (D) -- cycle;
    \end{tikzpicture}
\sidetext
    \begin{example}
        \makelink{ex-proj}
        On considère le carré $ABCD$ de côté $2$ et $I$ le milieu de $[AB]$.
        Déterminer les produits scalaires $\vect{AB}\cdot \vect{AC}$,
        $\vect{AB}\cdot \vect{AD}$ et $\vect{IC}\cdot \vect{BI}$.
        \IfStudentT{\bigfiller{3}}
    \end{example}
\end{sidefigure}
\IfStudentT{\bigfiller{1}}

\subsection{Formule du cosinus}

\begin{theorem}
    \label{thm:ABACcos}
    Soient $A$, $B$, $C$ trois points avec $A \ne B$ et $A \ne C$. Alors:
    \[ \vect{AB}\cdot\vect{AC} = AB \times AC \times \cos \widehat{BAC} \]
\end{theorem}

\begin{proof}
    Notons $H$~le projeté orthogonal de~$C$ sur~$(AB)$. Si l'angle
    $\hat{BAC}$~est droit, $\cos\hat{BAC} = 0$. De plus la perpendiculaire
    à~$(AB)$ passant par~$C$ est~$(AC)$, donc $H = A$. Ainsi, on a bien
    $\vect{AB}\cdot\vect{AC} = 0 = AB \times AC \times \cos \widehat{BAC}$.

    \begin{figure}[b]
    \ffigbox{}{
        \begin{subfloatrow}[2]
            \ffigbox{}{\caption{}\label{fig:proof-cos-sharp}%
                \begin{tikzpicture}[rotate=10]
                    \path (1,0) coordinate[label=below:$A$] (A) ;
                    \path (4,0) coordinate[label=below:$B$] (B) ;
                    \path (3,1.7) coordinate[label=above:$C$] (C) ;
                    \path (A -| C) coordinate[label=below:$H$] (H) ;
                    \draw ($ (A)!-1/3!(B) $) -- ($ (A)!4/3!(B) $);
                    \draw[dashed] (C) -- (H) pic[perp marker] {} ;
                    \draw[->, thick] (A) -- (B);
                    \draw[->, thick] (A) -- (C);
                    \draw[->] (A) -- (H);
                    \path pic (angle) [draw, ->, angle radius=2em,
                        angle eccentricity=1, pic text=]
                        {angle=B--A--C};
                    \node[anchor=-160] at (angle) {$\theta$};
                \end{tikzpicture}
            }%
            \ffigbox{}{\caption{}\label{fig:proof-cos-blunt}%
                \begin{tikzpicture}[rotate=-10]
                    \path (1,0) coordinate[label=above:$A$] (A) ;
                    \path (3,0) coordinate[label=above:$B$] (B) ;
                    \path (-0.5,-2) coordinate[label=below:$C$] (C) ;
                    \path (A -| C) coordinate[label=above:$H$] (H) ;
                    \draw ($ (H)!-1/5!(B) $) -- ($ (H)!6/5!(B) $);
                    \draw[dashed] (C) -- (H) pic[perp marker] {} ;
                    \draw[->, thick] (A) -- (B);
                    \draw[->, thick] (A) -- (C);
                    \draw[->] (A) -- (H);
                    \path pic (angle) [draw, <-, angle radius=2em,
                        angle eccentricity=1, pic text=]
                        {angle=C--A--B};
                    \node[below] at (angle) {$\theta$};
                    \path pic (angle) [draw, ->, angle radius=2em,
                        angle eccentricity=1, pic text=]
                        {angle=H--A--C};
                    \node[left] at (angle) {$\pi+\theta$};
                    \path pic (angle) [draw, ->, angle radius=1.5em,
                        angle eccentricity=1, pic text=]
                        {angle=H--A--B};
                    \node[anchor=-100] at (angle) {$\pi$};
                \end{tikzpicture}
            }%
        \end{subfloatrow}
        \caption{%
            \subref{fig:proof-cos-sharp}~Si $\hat{BAC}$ est aigu,
            $\hat{HAC} = \hat{BAC} = \theta$.
            \subref{fig:proof-cos-blunt}~Si $\hat{BAC}$ est obtus,
            $\hat{BAC} = \theta$ mais $\hat{HAC} = \pi+\theta$.
        }
    }
    \end{figure}

    Si $\hat{BAC}$~est un angle aigu, alors $\cos\hat{BAC} = \cos\hat{HAC} =
    \dfrac{AH}{AC}$ c'est-à-dire $AH = AC \times \cos\hat{BAC}$ (voir
    \vref{fig:proof-cos-sharp}). D'autre part, $\vect{AB}$~et~$\vect{AH}$ ont le
    même sens, donc $\vect{AB}\cdot \vect{AC} = AB\times AH =
    AB \times AC \times \cos\hat{BAC}$.

    Si $\hat{BAC}$~est obtus, on a toujours $\cos\hat{HAC} = \dfrac{AH}{AC}$,
    mais cette fois $\hat{HAC} = \pi+\theta$ avec $\theta = \hat{BAC}$ (voir
    \vref{fig:proof-cos-blunt}).
    Ainsi, $AH = AC \times \cos\hat{HAC} = AC \times \cos(\pi+\theta) =
    AC \times (-\cos\theta) = -AC\times\cos\hat{BAC}$.
    Comme $\vect{AB}$~et~$\vect{AH}$ sont de sens contraire, 
    $\vect{AB}\cdot \vect{AC} = -AB\times AH = -AB \times (-AC) \times
    \cos\hat{BAC} = AB \times AC \times \cos\hat{BAC}$, ce qu'il fallait
    démontrer.
\end{proof}

\begin{theorem}
    \label{thm:uvcos}
    Soient $\vect{u} \ne \vect{0}$ et~$\vect{v} \ne \vect{0}$ deux vecteurs non
    nuls. Alors:
    \[ \vect{u}\cdot\vect{v} = \norm{u} \times \norm{v} \times
    \cos\vectangle(u,v) \]
\end{theorem}

\begin{remark}
    Le produit scalaire de deux vecteurs
    $\vect{u}$~et~$\vect{v}$ est $\vect{u}\cdot\vect{v} =
    \vect{AB}\cdot\vect{AC}$ où $\vect{AB}$~et~$\vect{AC}$ sont des
    représentants respectifs de $\vect{u}$~et~$\vect{v}$ de même origine~$A$.
\end{remark}

\begin{recall}
    $\norm{\vect{u}}$ est appelé \emph{norme} de~$\vect{u}$ et est la longueur
    du vecteur~$\vect{u}$.
\end{recall}

\begin{example}
    \makelink{ex-cos}
    Dans le triangle~$ABC$, on a $AB=10$, $AC=2$ et $\widehat{BAC} =
    \dfrac{\pi}{3}$.\\
    Calculer $\vect{AB}\cdot\vect{AC}$.
    \IfStudentT{\bigfiller{2}}
\end{example}

\TeacherMode
\begin{remark}
    \begin{itemize}
        \item Si $\vect{u}$~et~$\vect{v}$ sont colinéaires de même sens,
            $\vect{u}\cdot\vect{v} = \UGHOST{
                \norm{\vect{u}} \times \norm{\vect{v}}
            }$.
        \item Si $\vect{u}$~et~$\vect{v}$ sont colinéaires de sens contraire,
            $\vect{u}\cdot\vect{v} = \UGHOST{
                - \norm{\vect{u}} \times \norm{\vect{v}}
            }$.
        \item En particulier, $\vect{u}\cdot\vect{u} = \norm{\vect{u}}^2$.
    \end{itemize}
\end{remark}

\begin{proof}
    \UGHOST{%
    %\newcommand\vect{\vec}
    %\newcommand\norm[1]{\left\|#1\right\|}
    %\newcommand\qedhere{}
    %\newcommand\vectangle{}
    \begin{enumerate}
        \item Si $\vect{u} = \vect{0}$ ou $\vect{v} = \vect{0}$:
                    $\vect{u}\cdot\vect{v} = \vect{v}\cdot\vect{u} = 0$. \\
            Sinon, $\vect{u}\cdot\vect{v}
                    = \norm{\vect{u}} \times \norm{\vect{v}}
                        \times \cos\vectangle(u,v)
                    = \norm{\vect{v}} \times \norm{\vect{u}}
                        \times \cos\left( -\vectangle(v,u) \right)
                        = \vect{v}\cdot\vect{u}$ car $\cos(-x) = \cos x$.
        \item $\vect{u}^2= \vect{u}\cdot\vect{u}
            = \norm{\vect{u}} \times \norm{\vect{u}} \times \cos 0
            = \norm{\vect{u}}^2$
        \qedhere
    \end{enumerate}
}
%    \IfStudentT{\bigfiller{4}}
\end{proof}
\StudentMode

\subsection{Orthogonalité}

\begin{definition}
    \begin{itemize}
        \item On dit que deux vecteurs non nuls $\vect{u}$~et~$\vect{v}$ sont
            \define[vecteurs orthogonaux]{orthogonaux}, et l'on note
            $\vect{u}\perp\vect{v}$, si et seulement si une mesure de l'angle
            entre $\vect{u}$~et~$\vect{v}$ est $\pm\dfrac{\pi}{2}$.
        \item On décide que le vecteur nul~$\vect{0}$ est orthogonal à tous les
            vecteurs.
    \end{itemize}
\end{definition}

\begin{proposition}
    Deux vecteurs $\vect{u}$~et~$\vect{v}$ sont orthogonaux si et
    seulement si $\vect{u}\cdot\vect{v}=0$.
\end{proposition}

\begin{remark}
    Si $A \ne B$ et $A \ne C$, alors $\vect{AB} \cdot \vect{AC} = 0$ si~et
    seulement si~$\hat{BAC}$ est un angle droit.
\end{remark}

\section{Propriétés du produit scalaire}

\subsection{Propriétés algébriques (symétrie et bilinéarité)}

\begin{proposition}
    Le produit scalaire est \define{commutatif}:
            $\vect{u}\cdot\vect{v}=\vect{v}\cdot\vect{u}$.
    On dit aussi que le produit scalaire est \define{symétrique}.
\end{proposition}

\begin{proposition}
    On a les propriétés suivantes:
    \begin{enumerate}
        \item Le produit scalaire est \define{distributif} par rapport
            à l'addition:
            \[\vect{u}\cdot(\vect{v}+\vect{w}) =
            \vect{u}\cdot\vect{v} + \vect{u}\cdot\vect{w}\]
        \item Pour deux réels $k$ et $k'$:
            \[ (k\vect{u})\cdot (k'\vect{v}) =
            (k\times k')\vect{u}\cdot \vect{v} \]
    \end{enumerate}
    On dit que le produit scalaire est \define{bilinéaire}.
\end{proposition}

\begin{example}
    Simplifier $\vect{u} \cdot (2\vect{u} + 5 \vect{v})
    - 5 \vect{v}\cdot\vect{u}$.\\
    $\vect{u} \cdot (2\vect{u} + 5 \vect{v})
    - 5 \vect{v}\cdot\vect{u}
    = \vect{u} \cdot (2\vect{u})
    + \vect{u} \cdot (5 \vect{v})
    - 5 \vect{v}\cdot\vect{u}
    = 2 \vect{u}\cdot\vect{u}
    + 5 \vect{u}\cdot\vect{v}
    - 5 \vect{v}\cdot\vect{u}
    = 2 \norm{\vect{u}}^2$
\end{example}

\iffalse

\begin{proposition}[Identités remarquables]
    On a:
    \begin{enumerate}
        \item $\norm{\vect{u}+\vect{v}}^2 = \left(\vect{u}+\vect{v}\right)^2 =
            \vect{u}^2+2\vect{u}\cdot \vect{v}+\vect{v}^2 =
            \norm{\vect{u}}^2+2\vect{u}\cdot \vect{v}+\norm{\vect{v}}^2$
        \item $\norm{\vect{u}-\vect{v}}^2 = \left(\vect{u}-\vect{v}\right)^2 =
            \vect{u}^2-2\vect{u}\cdot \vect{v}+\vect{v}^2 =
            \norm{\vect{u}}^2-2\vect{u}\cdot \vect{v}+\norm{\vect{v}}^2$
        \item $(\vect{u}+\vect{v})\cdot(\vect{u}-\vect{v}) =
            \vect{u}^2-\vect{v}^2 =
            \norm{\vect{u}}^2-\norm{\vect{v}}^2$
    \end{enumerate}
\end{proposition}

\StudentOnly
\begin{proof}
    \bigfiller{7}
\end{proof}
\EndStudentOnly

\fi

\subsection{Formule du produit scalaire dans une base orthonormée}

\begin{proposition}
    Soient $\vect{u}(x;y)$ et $\vect{v}(x';y')$ dans une base orthonormée.
    Alors:
    \[\vect{u}\cdot\vect{v} = xx' + yy'\]
\end{proposition}

\TeacherMode
\begin{proof}
    \UGHOST{
        $\vect{u} = x \vect{\imath} + y \vect{\jmath}$ et
        $\vect{v} = x' \vect{\imath} + y' \vect{\jmath}$, donc
        $\vect{u}\cdot\vect{v} =
        (x \vect{\imath} + y \vect{\jmath}) \cdot
        ( x' \vect{\imath} + y' \vect{\jmath})$.

        On développe en utilisant la bilinéarité:
        $\vect{u}\cdot\vect{v} = xx'\vect{\imath}\cdot\vect{\imath} +
        yx'\vect{\jmath}\cdot\vect{\imath} +
        xy'\vect{\imath}\cdot\vect{\jmath} +
        yy'\vect{\jmath}\cdot\vect{\jmath}$

        Or $\vect{\imath}\cdot\vect{\imath} = \norm{\vect{\imath}}^2 = 1$,
        $\vect{\jmath}\cdot\vect{\jmath} = \norm{\vect{\jmath}}^2 = 1$,
        et puisque $\vect{\imath}\perp\vect{\imath}$,
        $\vect{\imath}\cdot\vect{\jmath} = \vect{\jmath}\cdot\vect{\imath} = 0$.

        En définitive, $\vect{u}\cdot\vect{v} = xx' \times 1 + yx' \times 0 +
        xy' \times 0 + yy' \times 1 = x'x + y'y$, ce qu'il fallait démontrer.
    }
\end{proof}
\StudentMode

\begin{exercise}*
    \makelink{ex-orth}
    Soient $\point{A}(-1;3)$, $\point{B}(5;1)$, $\point{C}(3;5)$
    et~$\point{D}(6;14)$ dans un repère orthonormé~$(O;I;J)$. Montrer que
    $(AB)$~et~$(CD)$ sont perpendiculaires.
    \IfStudentT{\bigfiller{3}}
\end{exercise}

\begin{exercise}*
    \makelink{ex-angle}
    Dans un repère~$(O;I;J)$ orthonormé, on considère
    $\point{B}(5;1)$ et~$\point{L}(2;4)$.
    \begin{enumerate}
        \item Calculer $\vect{OB}\cdot\vect{OL}$.
            \IfStudentT{\bigfiller{1}}
        \item Calculer $OB$ et $OL$.
            \IfStudentT{\bigfiller{2}}
        \item En déduire une mesure de l'angle $\widehat{BOL}$, qu'on donnera
            en degrés arrondi à \<0.1>~près.
            %\IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{exercise}




\iffalse

\section{Produit scalaire et norme}


\subsection{Polarisation}

\begin{activity}
    \begin{enumerate}
        \item \makelink{polar-z}
            Dessiner un triangle~$ABC$ tel que $AB=3$, $AC=4$ et~$BC=5$.
            Calculer $p = AB^2 + AC^2 - BC^2$. À quelle situation géométrique
            cela correspond-il ?
            \IfStudentT{\bigfiller{6}}
        \item \makelink{polar-nz}
            Dessiner un triangle~$ABC$ tel que $AB=3$, $AC=4$ et~$BC=\<3.5>$.
            Calculer $p = AB^2 + AC^2 - BC^2$. Que remarque-t-on ?
            \IfStudentT{\bigfiller{5}}
        \item Dessiner un triangle~$ABC$ tel que $AB=3$, $AC=4$ et~$BC=\<6>$.
            Calculer~$p$. Que remarque-t-on ?
            \IfStudentT{\bigfiller{5}}
        \item \makelink{polar-dte}
            Placer trois points $A$, $B$ et~$C$ alignés dans cet ordre, tels
            que\\
            $AB = 3$~et~$AC=4$. Calculer~$p$ et le comparer avec $AB \times AC$.
            \IfStudentT{\bigfiller{3}}
        \item Même question avec $B$~et~$C$ de part et d'autre de~$A$.
            \IfStudentT{\bigfiller{3}}
        \item À quelle notion du chapitre $p$~semble-t-il correspondre ?
            Le vérifier.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{activity}

\begin{proposition}
    $\vect{u}\cdot\vect{v} = \UGHOST{\dfrac{1}{2}\left(
        \norm{\vect{u}}^2 + \norm{\vect{v}}^2 - \norm{\vect{v}-\vect{u}}^2
        \right)}$

    \Vref{fig:polarisation} montre une interprétation concrète de cette
    proposition.
\end{proposition}


\begin{proof}
    \UGHOST{
    Si $\vect{u} = \vect{0}$,
    $\norm{\vect{u}}^2 + \norm{\vect{v}}^2 - \norm{\vect{v}-\vect{u}}^2
    = \norm{\vect{v}}^2 - \norm{\vect{v}}^2 = 0 = 2 \vect{u}\cdot\vect{v}$.
    Sinon, il existe un repère orthonormé direct $(O;I;J)$
    tel que $\vect{u} = \norm{u}\vect{OI}$.

    Dans ce repère,
    $\vect{u}(\norm{u};0)$, et
    $\vect{v}(\norm{v}\cos\theta;\norm{v}\sin\theta)$ avec
    $\theta = \vectangle(u,v)$.
    Ainsi, $\vect{w} = \vect{v} - \vect{u}$ a pour coordonnées
    $\vect{w}(\norm{v}\cos\theta - \norm{u};\norm{v}\sin\theta)$ et donc\\
    $\norm{w}^2 = x_w^2 + y_w^2
    = \left( \norm{v}\cos\theta - \norm{u} \right)^2
        + \left( \norm{v}\sin\theta \right)^2\\
    = \left( \norm{v}\cos\theta \right)^2
        - 2 \norm{u} \norm{v} \cos\theta + \norm{u}^2
        + \left( \norm{v}\sin\theta \right)^2\\
    = \norm{u}^2 + \norm{v}^2 (\cos^2\theta + \sin^2\theta)
        - 2 \vect{u}\cdot\vect{v}$.

    $\norm{w}^2 = \norm{u}^2 + \norm{v}^2 - 2 \vect{u}\cdot\vect{v}$.
    On en déduit $\norm{u}^2 + \norm{v}^2 - \norm{w}^2 =
    2\vect{u}\cdot\vect{v}$.
    }
\end{proof}

\begin{example}
    \makelink{ex-polar}
    Soit $ABC$ un triangle tel que $AB=6$, $AC=5$ et $BC=8$.
    \begin{enumerate}
        \item Calculer $\vect{AB}\cdot \vect{AC}$.
            \IfStudentT{\bigfiller{3}}
        \item $\vect{AB}$~et~$\vect{AC}$ sont-ils orthogonaux ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}


\TeacherMode
\begin{proposition}
    $\vect{u}\cdot\vect{v} = \UGHOST{\dfrac{1}{2}\left(
    \norm{\vect{u}+\vect{v}}^2 - \norm{\vect{u}}^2 - \norm{\vect{v}}^2
        \right)}$
\end{proposition}
\StudentMode

\fi




\end{document}
