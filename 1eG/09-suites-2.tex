% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\def\genericform#1{<\textsl{#1}>}

\RenewDocumentCommand\BGHOST {s}    {\begingroup\color{blue}}
\RenewDocumentCommand\EGHOST {}     {\endgroup}
\RenewDocumentCommand\GHOST  {s +m} {%
    \IfBooleanTF{#1}{\BGHOST* #2\EGHOST}{\BGHOST #2\EGHOST}%
}
\let\UGHOST\GHOST

\persocourssetup{qrcodes}
\usepackage{cancel}


\begin{document}

%\StudentMode

\chapter{Suites, la suite}

\section{Suites arithmétiques}

\subsection{Définition par récurrence}

\begin{definition}
    On dit qu'une suite $u = (u_n)$ est \define{arithmétique} s'il existe un
    nombre réel~$r$ tel que pour tout entier naturel~$n$ on a $u_{n+1} = u_n +
    r$.

    Le réel $r$~est appelé \define{raison} de la suite arithmétique~$u$.
\end{definition}

\begin{remark}
  Cette définition utilise une relation de récurrence. Une suite est
  arithmétique si l'on ajoute toujours le même nombre pour passer d'un terme au
  suivant.
\end{remark}

\begin{remark}
  Pour définir complètement une suite arithmétique, il faut donner sa raison,
  mais aussi son premier terme.
\end{remark}

\begin{proposition}
    $u = (u_n)$ est arithmétique de raison~$r$ si et seulement si $\forall n,
    u_{n+1} - u_n = r$. Autrement dit, si la différence entre deux termes
    consécutifs est constante (ne dépend pas de~$n$).
\end{proposition}

\begin{remark}
    La méthode va changer selon que l'on veut montrer que la suite est
    arithmétique ou qu'elle ne l'est pas. Attention: vérifier que $u_8 - u_7 =
    u_7 - u_6 = u_3 - u_2$ ne démontre pas que $u$~est arithmétique; tout au
    plus cela permet de dire que la suite~$u$ \emph{semble} arithmétique.
\end{remark}

\begin{example}
    Indiquer si les suites ci-dessous sont arithmétiques ou non, et si oui
    déterminer leur raison:
    \begin{enumerate}
        \item $t_n = n^2$\\
            Méthode: calculer $t_1 - t_0$ puis $t_2 - t_1$, et conclure.

            \UGHOST{$t_0 = 0^2 = 0$; $t_1 = 1^2 = 1$ et $t_2 = 2^2 = 4$.
            Ainsi $t_2 - t_1 = 4 - 1 = 3$ alors que $t_1 - t_0 = 1 - 0 = 0$.
            La différence n'est pas constante, et la suite~$t$ ne peut donc pas
            être arithmétique.}
        \item $u_n = 2 + 3n$\\
            \UGHOST{
                $u_0 = 2 + 3 \times 0 = 2$; $u_1 = 2 + 3 \times 1 = 5$ et
                $u_2 = 2 + 3 \times 2 = 8$.  Ainsi $t_2 - t_1 = 8 - 5 = 3$ alors
                que $t_1 - t_0 = 5 - 2 = 3$. La différence \emph{semble}
                constante, et la suite~$u$ \emph{semble} donc arithmétique.
            }

            Méthode: déterminer une formule pour $t_{n+1}$ puis pour
            $t_{n+1} - t_n$. Conclure.

            \UGHOST{
                Démontrons-le: $u_{n+1} = 2 + 3(n+1) = 2 + 3n + 3 = 5 + 3n$.
                Mais alors $u_{n+1} - u_n = 5 + 3n - (2 + 3n) = 5 + 3n - 2 - 3n
                = 5 - 2 = 3$. La différence \emph{est} constante, et la
                suite~$u$ est bien arithmétique, de raison~$3$.
            }
        \item $\left\{
                \begin{aligned}
                    &v_0 = 0\\
                    &v_{n + 1} = v_n - 5
                \end{aligned} \right.$\\
            Méthode: déterminer $v_{n+1} - v_n$ et conclure.

            \UGHOST{$v_{n+1} - v_n = (v_n - 5) - v_n = -5$. La différence est
            constante, et la suite~$v$ est arithmétique, de raison~$5$.}
        \item $\left\{
                \begin{aligned}
                    &w_0 = 0\\
                    &w_{n + 1} = w_n + n + 1
                \end{aligned} \right.$\\
            Méthode: déterminer quelques termes, puis quelques différences, et
            conclure.

            \UGHOST{$w_0 = 0$; $w_1 = w_0 + 0 + 1 = 1$ et $w_2 = w_1 + 1 + 1 = 3$.
            Ainsi $w_2 - w_1 = 4 - 1 = 3$ alors que $w_1 - w_0 = 1 - 0 = 0$.
            La différence n'est pas constante, et la suite~$w$ ne peut donc pas
            être arithmétique.}
    \end{enumerate}
\end{example}

\begin{remark}
    \makelink{https://qrgo.page.link/PzbdD}
    Un exemple en vidéo « Montrer qu'une suite est arithmétique »:
    \url{https://qrgo.page.link/PzbdD}
\end{remark}

\subsection{Forme explicite d'une suite arithmétique}

\begin{remark}
    On parle aussi de \emph{terme général}.
\end{remark}

\begin{theorem}
    Si $u = (u_n)$ est une suite arithmétique de premier terme $u_0$ et de
    raison~$r$ alors $\forall n \in \mdN, u_n = u_0 + n \times r$.
\end{theorem}

\begin{proof}
    Cette démonstration est au programme, elle est à bien comprendre.

    Par définition, on sait que pour tout entier naturel $n$,
    on a ${u_{n+1}} = {u_n} + r$. 

    $\left.\begin{gathered}
        {u_1} = {u_0} + r\\
        {u_2} = {u_1} + r\\
        {u_3} = {u_2} + r\\
        ...\\
        {u_n} = {u_{n - 1}} + r
    \end{gathered} \right\}
    \begin{gathered}
        \text{On ajoute membre à membre:}\\
        u_1 + u_2 + \dots + u_{n - 1} + u_n =
        u_0 + u_1 + \dots + u_{n - 1} + nr
    \end{gathered}$

    On retranche ${u_1} + {u_2} + \dots + {u_{n - 1}}$ de chaque côté:
    \[
        \cancel{u_1} + \cancel{u_2} + \dots + \cancel{u_{n - 1}} + {u_n} =
        {u_0} + \cancel{u_1} + \dots + \cancel{u_{n - 1}} + nr
    \]
    et l'on obtient ${u_n} = {u_0} + nr$.
\end{proof}

\begin{remark}
    \makelink{https://www.youtube.com/watch?v=Jn4_xM_ZJD0}
    Une vidéo de cette démonstration est disponible:\\
    \url{https://www.youtube.com/watch?v=Jn4_xM_ZJD0}
\end{remark}


\begin{corollary}
    Si $u = (u_n)$ est une suite arithmétique de
    raison~$r$ alors pour tous entiers $p$~et~$q$, $u_q = u_p + (q-p) \times r$.
\end{corollary}

\begin{proof}
    D'après le théorème précédent, $u_q = u_0 + q r$. De même, $u_p = u_0 +
    pr$. Mais alors $u_0 = u_p - pr$ et ainsi $u_q = u_p - pr + qr$. En
    factorisant par~$r$ on obtient le résultat annoncé.
\end{proof}

\begin{example}
    Dans les cas suivants, déterminer une forme explicite de la suite, puis
    calculer son terme d'indice~$100$.
    \begin{enumerate}
        \item $\left\{\begin{aligned}
                &u_0 = 11\\
                &u_{n + 1} = u_n - 3
            \end{aligned} \right.$
            \GHOST{$u_{n+1} - u_n = -3$: la suite~$u$ est arithmétique de
            raison~$-3$ et de premier terme~$u_0 = 11$. Ainsi, pour
            tout~$n\in\mdN$, $u_n = u_0 + n \times (-3) = -3n + 11$.
            On en déduit $u_{100} = -300 + 11 = -289$.}
        \item $\left\{ \begin{aligned}
                &v_1 = -30\\
                &v_{n + 1} = v_n + 5
            \end{aligned} \right.$
            \GHOST{$v_{n+1} - v_n = 5$: la suite~$v$ est arithmétique de
            raison~$5$ et de premier terme~$v_1 = -30$. Ainsi, pour
            tout~$n\in\mdN$, $v_n = v_1 + (n-1) \times 5 = 5n - 5 - 30$.
            On en déduit $v_{100} = 500 - 35 = 465$.}
    \end{enumerate}
\end{example}

\subsection{Sens de variation}

\begin{proposition}
    Soit $u = (u_n)$ une suite arithmétique de raison~$r$.
    \begin{itemize}
        \item Si $r>0$ alors la suite arithmétique~$u$ est (strictement)
            \emph{croissante}.
        \item Si $r=0$ alors la suite arithmétique~$u$ est \emph{constante}.
        \item Si $r<0$ alors la suite arithmétique~$u$ est (strictement)
            \emph{décroissante}.
    \end{itemize}
\end{proposition}

\begin{example}
    \makelink{https://qrgo.page.link/tvPmL}
    Un exemple en vidéo: \url{https://qrgo.page.link/tvPmL}
\end{example}

\subsection{Somme des termes d'une suite arithmétique}

\begin{proposition}
    La somme des entiers naturels de $1$~à~$n$ inclus vaut $\dfrac{n(n+1)}{2}$
\end{proposition}

\begin{proof}
  On peut exprimer la somme des $n$ premiers entiers naturels de deux
  mani\`{e}res :

  $\cellprops{array td {padding: 0.5ex} }
  \begin{array}[t]{r*6c}
    S  &=&    1    &+&    2    &+ \cdots +& n \\
    S  &=&    n    &+& (n - 1) &+ \cdots +& 1 \\
    \hline
    2S &=& (n + 1) &+& (n + 1) &+ \cdots +& (n + 1) \\
  \end{array}$ On  additionne les deux égalités.

  Puis on obtient $2S = n\left(n + 1\right)$ d'o\`{u} le résultat
  $S = \dfrac{n\left( n + 1 \right)}{2}$.
\end{proof}

\begin{remark}
    \makelink{https://qrgo.page.link/61vbQ} On peut voir une vidéo de la
    démonstration à l'adresse\\
    \url{https://qrgo.page.link/61vbQ}
\end{remark}

\begin{remark}[Notation]
  La somme de $k = 0$ \`{a} $n$ des ${u_k}$ se note
  \[ S = \sum_{k = 0}^n u_k = {u_0} + {u_1} + \dots + {u_n} \]

  Ainsi, la propriété précédente peut s'écrire
  $\sum_{k = 0}^n k = \frac{{n(n + 1)}}{2} $.
\end{remark}

\begin{example}
    \makelink{https://qrgo.page.link/g24Kr}
    $1 + 2 + 3 + \dots + 100 = \dfrac{100(100+1)}{2} = 5050$.

    Un autre exemple est présenté en vidéo:\\
    \url{https://qrgo.page.link/g24Kr}
\end{example}

\begin{corollary}
    Si $u = (u_n)$~est une suite arithmétique, on a
    \[ \sum_{k=0}^n u_k = u_0 + u_1 + \dots + u_n = \frac{(n+1)(u_0 + u_n)}{2} \]
    et plus généralement
    \[
        \text{somme de termes consécutifs} =
        \frac{(\text{nombre de termes})(\text{premier} + \text{dernier})}{2}
    \]
\end{corollary}

\begin{example}
    On considère la suite arithmétique~$u = (u_n)$ de premier terme~$u_0 = -5$
    et de raison~$r = 2$. Donner l'expression de~$u_n$ en fonction de~$n$, puis
    calculer les sommes suivantes:
    \begin{enumerate}[gathered, label=, labelsep=0pt]
        \item $S = u_0 + u_1 + \dots + u_8$
        \item $T = u_1 + u_2 + \dots + u_{11}$
        \item $U = u_2 + u_3 + \dots + u_{15}$
    \end{enumerate}

    \UGHOST{
        $u$~est une suite arithmétique donc pour tout~$n$,
        $u_n = u_0 + n\times r = 2n-5$.

        Pour calculer les sommes, il y a deux méthodes. Choisissez celle que
        vous comprenez le mieux.

        Méthode 1:
        \begin{itemize}
            \item 
        $S=u_0+u_1+\dots+u_8=u_0+\left(u_0+r\right)+\left(u_0+2r\right)+\dots+
            \left(u_0+8r\right)$ \\
        $S=9×u_0+r\left(1+2+…+8\right)
        =9×\left(-5\right)+2×\frac{8\left(8+1\right)} 2=-45+72=27$
            \item
        $T=u_1+u_2+\dots+u_{11}=\left(u_0+r\right)+\left(u_0+2r\right)+
            \dots+\left(u_0+11r\right)$ \\
        $T=11×u_0+r\left(1+2+…+11\right)
        =11×\left(-5\right)+2×\frac{11\left(11+1\right)} 2=-55+132=77$
            \item
        $U=u_2+u_3+\dots+u_{15}=\left(u_0+2r\right)+\left(u_0+3r\right)+
            \dots+\left(u_0+15r\right)$\\
        $U=14×u_0+r\left(2+3+…+15\right)
        = 14×u_0+r\left({\color{red}1}+2+3+…+15\right){\color{red}-r}$\\
        $U=14×\left(-5\right)+2×\frac{15\left(15+1\right)} 2-2=-70+240-2=168$
        \end{itemize}

        \everymath{\displaystyle\everymath{}}
        Méthode 2:
        \begin{itemize}
            \item $S$~est la somme des $9$~premiers termes de la suite
                arithmétique~$u$, donc $S = \frac{9\left(u_0+u_8\right)}{2}$. Or
                $u_8=-5+2×8=11$ donc $S = \frac{9(-5 + 11)}{2} = 27$.
            \item $T$~est la somme de $11$~termes consécutifs de la suite
                arithmétique~$u$, donc $S = \frac{11\left(u_1+u_11\right)}{2}$. Or
                $u_1=-5+2×1=-3$ et $u_{11}=-5+2×11=17$ donc
                $T = \frac{11(-3 + 17)}{2} = 77$.
            \item $U$~est la somme de $14$~termes consécutifs de la suite
                arithmétique~$u$, donc $U = \frac{14\left(u_2+u_15\right)}{2}$. Or
                $u_2=-5+2×2=-1$ et $u_{15}=-5+2×15=25$ donc
                $T = \frac{14(-1 + 25)}{2} = 168$.
        \end{itemize}
    }
\end{example}

\section{Suites géométriques}

\subsection{Définition par récurrence}

\begin{definition}
    On dit qu'une suite $u = (u_n)$ est \define{géométrique} s'il existe un
    nombre réel~$q$ tel que pour tout entier naturel~$n$ on a $u_{n+1} = u_n
    \times q$.

    Le réel $q$~est appelé \define{raison} de la suite géométrique~$u$.
\end{definition}

\begin{remark}
  Cette définition utilise une relation de récurrence. Une suite est géométrique
  si l'on multiplie toujours par le même nombre pour passer d'un terme au
  suivant.
\end{remark}

\begin{remark}
  Pour définir complètement une suite géométrique, il faut donner sa raison,
  mais aussi son premier terme.
\end{remark}

\begin{proposition}
    Soit $u = (u_n)$ une suite ne s'annulant pas. $u$~est géométrique de
    raison~$q$ si et seulement si $\forall n, \dfrac{u_{n+1}}{u_n} = q$.
    Autrement dit, si le quotient de deux termes consécutifs est constant (ne
    dépend pas de~$n$).
\end{proposition}

\begin{remark}
Une suite géométrique modélise des évolutions successives à taux constants.
$q$~est le \emph{coefficient multiplicateur} qui donne~$u_{n+1}$ en fonction de~$u_n$.
Si la suite ne s’annule pas, le \emph{taux de variation relative}~$t$
d’un terme~$u_n$ au terme~$u_{n+1}$ est le quotient:
$t=\dfrac{u_{n+1}-u_n}{u_n}$
\end{remark}

\begin{method}[Déterminer si une suite est géométrique ou non]
    \begin{itemize}
        \item On calcule les trois premiers termes et, si la suite ne s’annule
            pas, les deux premiers quotients.
        \item Pour démontrer qu’une suite est géométrique, on prouve que
            $u_{n+1}=k×u_n$. Si les termes sont non nuls, on peut aussi prouver
            que le quotient  $\frac{u_{n+1}}{u_n}$  est constant (donc
            indépendant de~$n$).
        \item Pour démontrer qu’une suite n’est pas géométrique, il suffit
            d’utiliser un contre-exemple : on calcule deux quotients de termes
            consécutifs et on montre qu’ils ne sont pas égaux.
    \end{itemize}
\end{method}

\begin{remark}
    La méthode va changer selon que l'on veut montrer que la suite est
    géométrique ou qu'elle ne l'est pas. Attention: vérifier que $u_8 / u_7 =
    u_7 / u_6 = u_3 / u_2$ ne démontre pas que $u$~est géométrique; tout au
    plus cela permet de dire que la suite~$u$ \emph{semble} géométrique.
\end{remark}

\begin{remark}
    On peut se passer de rédiger les calculs des premiers termes lorsqu’on est
    certain que la suite est géométrique (si on a cherché au brouillon ou à la
    calculatrice) mais c’est bien utile pour conjecturer.
\end{remark}

\begin{example}
    Indiquer si les suites ci-dessous sont géométriques ou non, et si oui
    déterminer leur raison:
    \begin{enumerate}
        \item Soit la suite  $\left(u_n\right)$  définie sur~$\mdN$
            par: $u_n=4×3^{n+1}$

            \UGHOST{
                On calcule les trois premiers termes :
                $u_0=4×3^1=12$, $u_1=4×3^2=36$
                et   $u_2=4×3^3=108$.

                $\frac{u_1}{u_0}=\frac{36}{12}=3$  et
                $\frac{u_2}{u_1}=\frac{108}{36}=3$. On constate que
                $\frac{u_1}{u_0}=\frac{u_2}{u_1}$: il \emph{semble} que la suite
                $(u_n)$  soit géométrique.

                La suite  $\left(u_n\right)$  ne s’annule pas, on calcule le
                quotient $\frac{u_{n+1}}{u_n} = \frac{4×3^{n+1+1}}{4×3^{n+1}} =
                \frac{4×3^{n+1}×3}{4×3^{n+1}} = \frac{4×3^{n+1}}{4×3^{n+1}}×3=3$

                Donc la suite  $\left(u_n\right)$  est géométrique de raison
                $3$  et de premier terme  $u_0=12$.
            }
        \item Soit la suite  $\left(v_n\right)$  définie sur~$\mdN$
            par: $v_n=n²-3$.

            \UGHOST{
                On calcule les trois premiers termes :
                $v_0=-3$
                $v_1=1^2-3=-2$
                $v_2=2^2-3=1$

                Calculons les quotients :
                $\frac{v_1}{v_0}=\frac{-2}{-3}=\frac 2 3$
                $\frac{v_2}{v_1}=\frac 1{-2}$
                donc $\frac{v_1}{v_0}≠\frac{v_2}{v_1}$.
                Ainsi, la suite  $\left(v_n\right)$  n’est pas géométrique.
            }
    \end{enumerate}
\end{example}

\begin{remark}
    \makelink{https://qrgo.page.link/FUSRF}
    Un exemple en vidéo « Montrer qu'une suite est géométrique »:
    \url{https://qrgo.page.link/FUSRF}
\end{remark}

\subsection{Forme explicite d'une suite géométrique}

\begin{theorem}
    Si $u = (u_n)$ est une suite géométrique de premier terme $u_0$ et de
    raison~$q$ alors $\forall n \in \mdN, u_n = u_0 \times n^q$.
\end{theorem}

\begin{proof}
    Cette démonstration est au programme, elle est à bien comprendre.

    Par définition, on sait que pour tout entier naturel $n$,
    on a ${u_{n+1}} = {u_n} \times q$

    $\left.\begin{gathered}
        {u_1} = {u_0} \times q \\
        {u_2} = {u_1} \times q \\
        {u_3} = {u_2} \times q \\
        ...\\
        {u_n} = {u_{n - 1}} \times q
    \end{gathered} \right\}
    \begin{gathered}
        \text{On multiplie membre à membre:}\\
        u_1 \times u_2 \times \dots \times u_{n - 1} \times u_n =
        u_0 \times u_1 \times \dots \times u_{n - 1} \times q^n
    \end{gathered}$

    On divise par ${u_1} \times {u_2} \times \dots \times {u_{n - 1}}$ de chaque
    côté:
    \[
        \cancel{u_1} \times \cancel{u_2} \times \dots \times \cancel{u_{n - 1}}
        \times {u_n} = {u_0} \times \cancel{u_1} \times \dots \times
        \cancel{u_{n - 1}} \times q^n
    \]
    et l'on obtient ${u_n} = {u_0} \times q^n$.
\end{proof}

\begin{remark}
    \makelink{https://www.youtube.com/watch?v=OpLU8Ci1GnE}
    Une vidéo de cette démonstration est disponible:\\
    \url{https://www.youtube.com/watch?v=OpLU8Ci1GnE}
\end{remark}

\begin{corollary}
    Si $u = (u_n)$ est une suite géométrique de
    raison~$q$ alors pour tous entiers $m$~et~$n$, $u_n = u_m \times q^{n-m}$.
\end{corollary}

\begin{example}
    \makelink{https://qrgo.page.link/gUAHe}
    Un exemple en vidéo: \url{https://qrgo.page.link/gUAHe}.
\end{example}

\subsection{Sens de variation}

\begin{proposition}
    Soit $u = (u_n)$ une suite géométrique de raison~$q$ et premier terme~$u_0$.
    Dans les cas suivants, la suite~$u$ est :

    \begin{center}
    \cellprops{
        tr:first-child td:first-child {
            border-top-style: none;
            border-left-style: none;
        }
        td:nth-child(n+2) { min-width: 8em}
        tr td:nth-child(4) { border: none; }
    }
    \normalfont
    \cellpropsclass{borders}
    \begin{tabular}{cccl}
                    & $u_0 < 0$    & $u_0 > 0$ & \\
        $q < 0$     & \multicolumn{2}{c}{non monotone (ni l'un ni l'autre) } & \\
        $0 < q < 1$ & croissante   & décroissante
            & «se rapproche de~$0$» \\
        $q > 1$     & décroissante & croissante
            & «s'éloigne de~$0$» \\
    \end{tabular}
    \end{center}
\end{proposition}



\subsection{Somme des termes d'une suite géométrique}

\begin{proposition}
    Soient~$n \in \mdN^*$ et $q \ne 1$.
    La somme des $n$~premières puissances naturels de $q$ vaut
    $\displaystyle
    \sum_{k=0}^n q^k = 1 + q + q^2 + \dots + q^{n-1} + q^n = \frac{1-q^{n+1}}{1-q}$
\end{proposition}

\begin{proof}
    Notons $S = 1 + q + q^2 + \dots + q^{n-1} + q^n$. On peut multiplier cette
    égalité par~$q$ puis soustraire. On obtient:

    $\cellprops{table.array td {padding: 0.5ex} }
    \begin{array}[t]{r*{14}{c}}
        S      &=& 1 &+& q &+& \multicolumn{3}{c}{\cdots}
                                 &+& q^{n-1} &+& q^n & & \\
        -qS    &=&   &-& q &-& q^2 &-&
            \multicolumn{3}{c}{\cdots}       &-& q^n &-& q^{n+1} \\
        \hline
        S - qS &=& 1 & \multicolumn{10}{c}{} &-& q^{n+1} \\
    \end{array}$

    On factorise par~$S$: $S(1-q) = 1 - q^{n+1}$, puis comme $1-q\ne0$,
    $S = \dfrac{1-q^{n+1}}{1-q}$
\end{proof}

\begin{remark}
    \makelink{https://qrgo.page.link/7k91a} On peut voir une vidéo de la
    démonstration à l'adresse\\
    \url{https://qrgo.page.link/7k91a}
\end{remark}

\begin{example}
    Calculer la somme $S = 1 + 2 + 4 + \dots + 2^9$.

    \UGHOST{
    $S = \dfrac{1 - 2^{10}}{1-2} = \dfrac{1 - 1024}{-1} = 1023$.
    }

    \makelink{https://qrgo.page.link/g24Kr}
    Un autre exemple est présenté en vidéo:\\
    \url{https://qrgo.page.link/g24Kr}
\end{example}

\begin{corollary}
    Si $u = (u_n)$~est une suite géométrique de raison $q\ne1$, on a
    \[ \sum_{k=0}^n u_k = u_0 + u_1 + \dots + u_n = u_0 \times \frac{1-q^{n+1}}{1-q} \]
    et plus généralement
    \[
        \text{somme de termes consécutifs} =
        \frac{\text{premier terme} - \text{terme suivant le dernier}}
        {1 - \text{raison}}
    \]
\end{corollary}

\begin{example}
    On considère la suite géométrique~$u = (u_n)$ de premier terme~$u_0 = -5$
    et de raison~$q = 2$. Donner l'expression de~$u_n$ en fonction de~$n$, puis
    calculer les sommes suivantes:
    \begin{enumerate}[gathered, label=, labelsep=0pt]
        \item $S = u_0 + u_1 + \dots + u_8$
        \item $T = u_1 + u_2 + \dots + u_{11}$
        \item $U = u_2 + u_3 + \dots + u_{15}$
    \end{enumerate}

    \UGHOST{
        $u$~est une suite géométrique donc pour tout~$n$,
        $u_n = u_0 \times q^n = -5 \times 2^n$.
        \textcolor{red}{Attention: $5 \times 2^n \ne 10^n$}

        \everymath{\displaystyle\everymath{}}
        \begin{itemize}
            \item $S$~est une somme de termes consécutifs de la suite
                géométrique~$u$ de raison~$q = 2 \ne 1$, donc
                $S = \frac{u_0 - u_9}{1 - 2}$ (le terme suivant le dernier est
                le terme suivant $u_8$, c'est-à-dire $u_9$).

                Or $u_9=-5 \times 2^9 = -5 \times 512 = -2560$ donc
                $S = \frac{-5 + 2560}{-1} = -2555$.
            \item $U$~est une somme de termes consécutifs de la suite
                géométrique~$u$ de raison~$q = 2 \ne 1$, donc
                $U = \frac{u_1 - u_{12}}{1 - 2}$. Or $u_{12}=-5 \times 2^{12} =
                -5 \times 4096 = -\<20480>$ donc $S = \frac{-10 + \<20480>}{-1} =
                \<-20470>$.
            \item $T$~est une somme de termes consécutifs de la suite
                géométrique~$u$ de raison~$q = 2 \ne 1$, donc
                $T = \frac{u_2 - u_{16}}{1 - 2}$. Or $u_{16}=-5 \times 2^{16} =
                -5 \times 65536 = -\<327680>$ donc $T = \frac{-20 + \<327680>}{-1} =
                -\<327660>$.
        \end{itemize}
    }
\end{example}

\section{Exercices et exigences du chapitre}

\begin{multicols}{2}
Niveau 1
\begin{itemize}
    \item\nos\ 42 et  p.~33
\end{itemize}

Niveau 2
\begin{itemize}
    \item Tout le niveau 1 plus
    \item\no\  20 p.~31
    \item\no\  54 p.~34
    \item\no\  59 p.~35
\end{itemize}
\end{multicols}


Objectifs du chapitre 9

\def\EL{\\\textbf}
\begin{itemize}
    \item Savoir calculer des termes d’une suite arithmétique ou géométrique
        \EL{19 p.~31, 25 p.~32}
    \item Savoir démontrer qu’une suite est arithmétique ou géométrique
        \EL{Exercices résolus 1 et 2 p.~15, 16, 23 p.~31}
    \item Savoir déterminer le sens de variation d’une suite arithmétique ou géométrique
        \EL{Exercice résolu 3 p.~17, 42 p.~33}
    \item Savoir calculer le terme général d’une suite arithmétique ou géométrique
        \EL{3 et 4 p.~23}
    \item Savoir calculer la somme de termes d’une suite arithmétique ou géométrique
        \EL{Exercice résolu 3 p.~15, 31 p.~32, 67 p.~36}
    \item Savoir modéliser un problème par une suite arithmétique ou géométrique
        \EL{35 p.~32, 56 p.~35, 71 p.~37}
\end{itemize}


\end{document}

