% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{datavisualization,
                datavisualization.formats.functions}
\usepackage{tkz-tab}

\begin{document}
%\StudentMode

\chapter{Second degré}

\chapterquote{C'est très difficile de jongler avec le second degré dans un monde
où tout est pris au premier degré, aussi bien en dessin qu'en texte.}{Charb}

\begin{mathhistory}
    Muhammad Al-Khwârizmî est un mathématicien, géographe et astronome perse né
    dans les années 780. Il rédige plusieurs traités théoriques qui reprennent
    les connaissances mathématiques grecques ou indiennes. L’un de ses plus
    célèbres ouvrages est «l’abrégé du calcul par la restitution et la
    comparaison»; il fut traduit en latin et ce titre fut à l’origine du mot
    algèbre.

    Cet ouvrage, considéré comme le premier manuel d’algèbre, traite
    notamment de la résolution des équations du second degré.
\end{mathhistory}

\section{Formes d'un polynôme du second degré}

\begin{activity}

\end{activity}

\subsection{Polynôme du second degré}

\begin{definition}
Soit~$f$~une fonction. On dit que $f$~est une \define{fonction polynomiale de
degré~$2$} si on peut la mettre sous la forme~$f(x) = ax^2 + bx + c$ où
$a$,~$b$, et~$c$ sont des nombres réels avec $a\neq 0$.\end{definition}

\begin{example}
    Les fonctions suivantes sont-elles polynomiales du second degré ?
    \begin{enumerate}[gathered, label={$\Alph*(x)={}$},labelsep=0pt,start=16,
                       centered]
        \item $6x^2-5x+3$
        \item $2 - 3x^2$
        \item $x + 7$
        \item $\sqrt2 x + \sqrt3 x^2 - 1$
        \item $\sqrt{2x} + \sqrt3 x^2 - 1$
        \item $(x+3)(1-x)$
    \end{enumerate}
    \IfStudentT{\bigfiller{4}}
\end{example}

\begin{definition}[Vocabulaire]
    \begin{itemize}
        \item L'expression $ax^2 + bx + c$ est la forme \emph{développée,
            réduite et ordonnée} de~$f(x)$ (ou plus simplement \emph{forme
            développée}). On l'appelle aussi \emph{trinôme du second degré}.
        \item Toute écriture de~$f(x)$ sous forme de produit de deux facteurs du
            premier degré est appelée forme \emph{factorisée} de~$f$.
    \end{itemize}
\end{definition}

\begin{remark}
    Toutes les fonctions polynôme du second degré n’admettent pas une forme
    factorisée.
\end{remark}

\subsection{Racines et forme factorisée}

\begin{definition}
    Soit~$f$ une fonction polynomiale de degré~$2$. Les~\define{racines}
    de~$f$, si elles existent, sont les solutions de l'équation~$f(x) = 0$.
\end{definition}

\begin{remark}
    Une fonction polynôme du second degré admet au plus deux racines.
\end{remark}

\begin{proposition}
    Soit~$f:x\mapsto ax^2 + bx + c$ une fonction polynomiale du second degré.
    Si $f$~admet deux réels $x_1$~et~$x_2$ pour racines, alors $f$~s'écrit sous
    forme factorisée:
    \[ f(x) = a(x-x_1)(x-x_2) \]
\end{proposition}

\begin{proposition}
    Soit~$f:x\mapsto ax^2 + bx + c$ une fonction polynomiale du second degré
    (donc $a\ne0$).  Si $f$~admet deux réels $x_1$~et~$x_2$ pour racines, alors:
    \begin{itemize}
        \item La \emph{somme} des racines est $S = x_1 + x_2 = -\dfrac{b}{a}$
        \item Le \emph{produit} des racines est $P = x_1 x_2 = \dfrac{c}{a}$
    \end{itemize}
\end{proposition}

\begin{remark}
    Il est intéressant de repérer une racine «évidente», c'est-à-dire, lorsque
    qu'elle existe, une racine parmi les nombres $1$, $-1$, $2$, $-2$: si on
    connaît une racine d’une fonction polynôme du second degré, on peut calculer
    l’autre racine rapidement en utilisant la somme ou le produit des racines.
\end{remark}

\begin{example}
    Soit $f$~définie sur~$\mdR$ par $f(x) = 2x^2 - 7x + 5$.
\end{example}

\subsection{Signe d'un polynôme du second degré}

\begin{theorem}
    \label{thm:sign-two-roots}
    Soit~$f$~un polynôme du second degré admettant deux racines $x_1 < x_2$.
    Alors $f$~admet le tableau de signes
    suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3.3]
                    {$x$/.7,$P(x)$/1.2}{$-\infty$,$x_1$,$x_2$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},z,
                      \genfrac{}{}{0pt}{}
                            {\hbox{signe opposé}}
                            {\hbox{(signe de~$-a$)}}
                     ,z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{theorem}

\begin{proof}
    La forme factorisée de~$f(x)$ est $f(x) = a (x - x_1) (x - x_2)$ d'où le
    tableau:

    \hfill
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.7,espcl=2.8]
                    {$x$/.7,
                     $x-x_1$/.7, $x-x_2$/.7,
                     $a$/.7,
                     $P(x)$/.8}
                     {$-\infty$,$x_1$,$x_2$,$+\infty$}
        \tkzTabLine {,-,z,+,t,+}
        \tkzTabLine {,-,t,-,z,+}
        \tkzTabLine {,\text{signe de~$a$},t,\text{signe de~$a$},
                                          t,\text{signe de~$a$}}
        \tkzTabLine {,\text{signe de~$a$},z,\text{signe de~$-a$},
                                          z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{proof}

\section{Forme canonique et variations}

\subsection{Forme canonique}

\begin{definition}
Soit~$f$~une fonction polynomiale de degré~$2$. On appelle \define{forme
canonique} de~$P$ toute écriture de~$f(x)$ de la forme:
\[
\GHOST{f(x) = a(x-\alpha)^2 + \beta \text{ avec $\beta = f(\alpha)$}}
\]
\end{definition}

\begin{example}
    Soit $f:x \mapsto x^2 + 6x + 5$.\\
    Montrer qu'une forme canonique de~$f$ est $f(x) = (x+3)^2 - 4$.
    Indiquer $\alpha$~et~$\beta$.
\end{example}

\begin{example}
    Retrouver les formes canoniques dans l'activité~1.
\end{example}

\begin{activity}
    \begin{enumerate}
        \item Pour factoriser $P(x) = 3x^2 + 6x - 45$ dans~$\mdR$, Sheldon a
            écrit la démonstration ci-dessous. Expliquer chaque étape de son
            calcul.
            $P(x)           = 3(x^2 + 2x - 15)$ \hfiller\\
            $\phantom{P(x)} = 3(x^2 + 2x + 1 - 16)$ \hfiller\\
            $\phantom{P(x)} = 3\left[ (x+1)^2 - 16 \right]$ \hfiller\\
            $\phantom{P(x)} = 3\left[ (x+1)^2 - 4^2 \right]$ \hfiller\\
            $\phantom{P(x)} = 3 (x+1 \; + 4) (x+1 \; - 4)$ \hfiller\\
            $P(x)           = 3(x+5)(x-3)$ \hfiller
        \item Donner une forme canonique de~$P(x)$, puis sa forme factorisée.
        \item Appliquer la même méthode pour donner une forme canonique, ainsi
            qu'une forme factorisée, de~$Q(x) = x^2 - 12x + 35$.
        \item Résoudre $P(x)\ge 0$~et~$Q(x)<0$ pour $x\in\mdR$.
    \end{enumerate}
\end{activity}

\clearpage

\begin{theorem}
    \label{thm:canonical-form}
    Soit $P:x\mapsto ax^2 + bx + c$. Une forme canonique de~$P$ est:
    \[ P(x) = a\left[ \left( x+\frac{b}{2a} \right)^2 -
        \frac{b^2-4ac}{4a^2} \right] \]
\end{theorem}

\iffalse
\begin{proof}
    \UGHOST{
        $\displaystyle P(x) = ax^2 + bx + c \\
        = a\left[ x^2 + \frac{b}{a}x + \frac{c}{a} \right] \\
        = a\left[ x^2 + 2 \times \frac{b}{2a} \times x +
                \left( \frac{b}{2a} \right)^2
            - \left( \frac{b}{2a} \right)^2 + \frac{c}{a} \right] \\
        = a \left[ \left( x + \frac{b}{2a} \right)^2
            - \frac{b^2}{4a^2} + \frac{4ac}{4a^2} \right] \\
        = a \left[ \left( x + \frac{b}{2a} \right)^2
            - \frac{b^2 - 4ac}{4a^2} \right]
        $
    }
\end{proof}
\fi

\begin{remark}
    C'est aussi dire~$\alpha = -\dfrac{b}{2a}$ et $\beta = P(\alpha) =
    -\dfrac{b^2-4ac}{4a}$.
\end{remark}

\subsection{Variations d'un polynôme du second degré}

\begin{proposition}
    Soit~$f:x\mapsto ax^2+bx+c$ un polynôme du second degré. Alors:
    \begin{enumerate}[gathered, label=]
        \item \begin{tikzpicture}
                \begin{scope}[TAB]
                    \tkzTabInit [lgt=1.5,espcl=2.5]
                                {$x$/1.2,\var$f(x)$/2.2}
                                {$\;-\infty$,$\GHOST{\frac{-b}{2a}}$,
                                $+\infty\;$}
                    \IfStudentF{\tkzTabVar {+/,-/$\beta$,+/}}
                \end{scope}
                \node[above] at (current bounding box.north) {si $a>0$};
            \end{tikzpicture}
        \item \begin{tikzpicture}
                \begin{scope}[TAB]
                    \tkzTabInit [lgt=1.5,espcl=2.5]
                                {$x$/1.2,\var$f(x)$/2.2}
                                {$\;-\infty$,$\GHOST{\frac{-b}{2a}}$,
                                $+\infty\;$}
                    \IfStudentF{\tkzTabVar {-/,+/$\beta$,-/}}
                \end{scope}
                \node[above] at (current bounding box.north) {si $a<0$};
            \end{tikzpicture}
    \end{enumerate}

    Ces résultats sont illustrés par \vref{fig:parabole}.
\end{proposition}

\begin{figure}
\def\parab#1#2#3#4{%
    \begin{tikzpicture}
        \def\a{#1} \def\alp{#2} \def\bet{#3}
        \pgfmathsetmacro\yend{\a * pow(#4, 2) + (\bet)}
        \pgfmathsetmacro\ymin{min(0, \bet, \yend) - 0.4}
        \pgfmathsetmacro\ymax{max(0, \bet, \yend) + 0.4}
        \pgfmathsetmacro\vpos{ifthenelse(\a > 0, "below", "above")}
        \edef\Spos{\vpos\space left}
        \pgfmathsetmacro\Bpos{ifthenelse(\alp > 0, "left", "right")}
        \pgfmathsetmacro\Apos{%
            ifthenelse(\bet == 0, "\vpos\space right", "\Spos")%
        }
        \pgfmathsetmacro\Bpos{%
            ifthenelse(\alp == 0 || \bet == 0, "\vpos\space\Bpos", "\Bpos")%
        }
        \pgfmathsetmacro\Bstyle{%
            ifthenelse(\bet == 0, "", "draw")%
        }
        \datavisualization [school book axes,
                            all axes={ticks={none},
                                      unit length=.8cm},
                            visualize as scatter/.list={points,sym},
                            visualize as smooth line=P,
                            sym={style={mark=none}},
                        ]
        data [set=P, format=function] {
            var x : interval[(\alp-#4):(\alp+#4)];
            func y = \a * pow(\value x - \alp, 2) + (\bet);
        }
        data point [set=points, x=\alp, y=\bet,  name=S]
        data point [set=sym,    x=\alp, y=\ymin, name=min]
        data point [set=sym,    x=\alp, y=\ymax, name=max]
        data point [set=sym,    x=\alp, y=\yend, name=med]
        data point [set=sym,    x=\alp, y=0,     name=alp]
        info {
            \path [every node/.style={inner sep=.5ex, black},
                   \Bstyle, help lines]
                (S)  node[\Spos]  {$S$}
                -- (S -| 0,0) node [\Bpos] {$\beta$};
            \draw [dashed] (min) -- (max);
            \path (med) node [right] {$x=\frac{-b}{2a}$};
            \path (alp) node [\Apos] {$\alpha$};
        }
        ;
    \end{tikzpicture}%
}
    \ffigbox{}{
        \begin{subfloatrow}
            \ffigbox{}{
                \caption{$a<0$}\label{fig:parabole-aneg}
                \parab{-0.7}{2.5}{2.5}{3}
            }
            \ffigbox{}{
                \caption{$a>0$}\label{fig:parabole-apos}
                \parab{0.6}{2.3}{1}{2.8}
            }
        \end{subfloatrow}
        \caption{Allure et sommet d'une parabole.}
        \label{fig:parabole}
    }
\end{figure}

\begin{proposition}
    Si $f(x) = a(x-\alpha)^2 + \beta$, alors $S(\alpha;\beta)$ est le sommet de
    la parabole qui représente graphiquement~$f$.
\end{proposition}

\section{Équations du second degré}

\subsection{Discriminant}

\begin{activity}
    \label{activ:canon-to-fact}
    \begin{enumerate}
        \item Factoriser le polynôme $P(x) = (x+4)^2 - 9$.
            \IfStudentT{\bigfiller{1}}
        \item Peut-on factoriser $Q(x) = (x-5)^2 - 25$ ? Et $R(x) = (x-5)^2 -
            26$ ?
            \IfStudentT{\bigfiller{2}}
        \item Peut-on factoriser $S(x) = (x-5)^2 + 25$ ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{activity}

On veut factoriser~$P$. D'après le théorème précédent, on cherche donc à
factoriser $P(x) = a (A^2 - D)$ avec $A = x-\alpha$ et $D =
\dfrac{b^2-4ac}{4a^2}$. D'après \cref{activ:canon-to-fact}, cela dépend du signe
de~$D$; or on remarque que $D$ est toujours du signe de $b^2-4ac$.

\begin{definition}
    Soit $P:x\mapsto ax^2+bx+c$. On appelle \define{discriminant} de~$P$ le
    nombre \UGHOST{$\Delta=b^2-4ac$.}
\end{definition}

\subsection{Racines d'un polynôme du second degré}

\begin{theorem}[Théorème fondamental]
    \label{thm:fundamental-theorem}
    Soit~$P:x\mapsto ax^2 + bx + c$ une fonction polynomiale du second degré de
    discriminant~$\Delta$.
    \begin{itemize}
        \item \UGHOST{Si $\Delta<0$, $P$ n'a aucune racine et \emph{n'a pas de
            forme factorisée}.}
        \item \UGHOST{Si $\Delta=0$, $P(x) = a(x-\alpha)^2$ où $\alpha =
            \dfrac{-b}{2a}$ est l'unique racine de~$P$.}
        \item Si $\Delta>0$, $P(x) = a(x-x_1)(x-x_2)$ où les racines
            de~$P$ sont:
                \begin{enumerate}[gathered, label={$x_\arabic*={}$},labelsep=0pt,
                                   before*=\centering]
                    \item \GHOST{$\dfrac{-b-\sqrt{\Delta}}{2a}$}
                    \item \GHOST{$\dfrac{-b+\sqrt{\Delta}}{2a}$}
                \end{enumerate}
    \end{itemize}
\end{theorem}

\iffalse
\begin{proof}
    \IfStudentTF{
        \bigfiller{19}
    }{
    D'après \vref{thm:canonical-form}, on a:
    $\displaystyle P(x) = a\left[ \left( x+\frac{b}{2a} \right)^2 -
    \frac{\Delta}{4a^2}\right]$

    Si $\Delta < 0$, alors $ \left( x+\frac{b}{2a} \right)^2 -
    \frac{\Delta}{4a^2} \geq -\frac{\Delta}{4a^2} > 0$ et ainsi $P(x) \neq 0$
    pour tout reél~$x$. Comme un produit de facteurs du premier degré a au
    moins une racine, c'est donc que $P$ n'a pas de forme factorisée
    dans~$\mdR$.

    Si $\Delta = 0$, la formule devient $P(x) = a \left( x+\frac{b}{2a}
    \right)^2$ et l'on obtient aisément $\alpha = \frac{-b}{2a}$ comme unique
    racine.

    Si $\Delta > 0$, on peut écrire $\Delta = \left( \sqrt{\Delta}
    \right)^2$ donc $\frac{\Delta}{4a^2} = \left( \frac{\sqrt{\Delta}}{2a}
    \right)^2$. Ainsi:
    \[ P(x) = a \left[ \left( x+\frac{b}{2a} \right)^2 -
        \left( \frac{\sqrt{\Delta}}{2a} \right)^2 \right]
      = a \left( x+\frac{b}{2a} - \frac{\sqrt{\Delta}}{2a} \right)
        \left( x+\frac{b}{2a} + \frac{\sqrt{\Delta}}{2a} \right) \]
    et le résultat annoncé en découle directement.
    }
\end{proof}

\begin{figure}
    \ffigbox{}{
        \begin{subfloatrow}
            \ffigbox{}{
                \caption{}\label{fig:solutions-deltavar}
                \begin{tikzpicture}[baseline]
                    \datavisualization [school book axes,
                                        all axes={ticks={none},grid={step=1},
                                                  include value=5,
                                                  unit length=.8cm},
                                        visualize as smooth line/.list={
                                                    -2, -1, 0, 1},
                                    ]
                    data [format=function] {
                        var set : {-2, -1, 0, 1};
                        var x : interval[.2*\value{set}:5-.2*\value{set}];
                        func y = .6 * pow(\value x - 2.5, 2) + 1.5*\value{set};
                    }
                    ;
                \end{tikzpicture}
            }
            \ffigbox{}{
                \caption{}\label{fig:solutions-deltapos}
                \begin{tikzpicture}[baseline]
                    \def\a{0.8} \def\alp{2.5} \def\bet{-2.5}
                    \pgfmathsetmacro\d{sqrt(-\bet / \a)}
                    \datavisualization [school book axes,
                                        all axes={ticks={none},
                                                  unit length=.8cm},
                                        visualize as smooth line=P,
                                        visualize as scatter=points,
                                    ]
                    data [set=P, format=function] {
                        var x : interval[-0.5:5.5];
                        func y = \a * pow(\value x - \alp, 2) + \bet;
                    }
                    data point[set=points, x=\alp, y=\bet, name=S]
                    data point[set=points, x=(\alp-\d), y=0, name=x1]
                    data point[set=points, x=(\alp+\d), y=0, name=x2]
                    info {
                        \coordinate (one) at (visualization cs: x=0, y=1);
                        \draw[dash dot,shorten <=-2ex,shorten >=-2ex]
                            (current bounding box.north -| S) --
                                node [pos=0.2,right] {$x=\frac{-b}{2a}$}
                                (current bounding box.south -| S);
                        \scoped[help lines,
                                shorten <=3.6pt, shorten >=-1ex] {
                            \draw (x1) -- (x1 |- one);
                            \draw (x2) -- (x2 |- one);
                        }
                        \foreach \x in { x1, x2 }
                            \draw[<->] (\x |- one) -- (S |- one)
                                node[pos=0.5,above]
                                    {$\frac{\sqrt{\Delta}}{2a}$};
                        \path[every node/.style={inner sep=.5ex}]
                            (x1) node[below left]  {$x_1$}
                            (x2) node[below right] {$x_2$}
                            (S)  node[below left]  {$S$};
                    }
                    ;
                \end{tikzpicture}
            }
        \end{subfloatrow}
        \caption{Visualisation graphique du théorème fondamental avec $a>0$.
            \subref{fig:solutions-deltavar}~quand $\Delta<0$ la parabole ne
            coupe pas l'axe; quand~$\Delta=0$ elle le touche en son sommet;
            quand~$\Delta>0$, les points d'intersection s'écartent à mesure
            que~$\Delta$~croît.
            \subref{fig:solutions-deltapos}~interprétation graphique des
            solutions dans le cas~$\Delta\geq0$.}
        \label{fig:solutions}
    }
\end{figure}
\fi

\clearpage

\section{Signe d'un polynôme du second degré}

\begin{theorem}
    \label{thm:sign-delta-neg}
    Soient~$P$~un polynôme du second degré de discriminant~$\Delta<0$.
    Alors $P$~admet le tableau de signes suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3]
                    {$x$/.7,$P(x)$/.8}{$-\infty$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},}
    \end{tikzpicture}
\end{theorem}

\begin{theorem}
    \label{thm:sign-delta-zero}
    Soient~$P$~un polynôme du second degré de discriminant~$\Delta=0$.
    Alors $P$~admet le tableau de signes suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3]
                    {$x$/.7,$P(x)$/.8}{$-\infty$,$\frac{-b}{2a}$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{theorem}

\begin{theorem}
    \label{thm:sign-delta-pos}
    Soit~$f$~un polynôme du second degré de discriminant $\Delta > 0$: $f$~admet
    deux racines $x_1 < x_2$.  Alors $f$~admet le tableau de signes suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3.3]
                    {$x$/.7,$P(x)$/1.2}{$-\infty$,$x_1$,$x_2$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},z,
                      \genfrac{}{}{0pt}{}
                            {\hbox{signe opposé}}
                            {\hbox{(signe de~$-a$)}}
                     ,z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{theorem}

\end{document}

\begin{proof}
    Le théorème fondamental donne $P(x) = a \left(x +
    \frac{b}{2a} \right)^2$ d'où le tableau:

    \hfill
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.7,espcl=3]
                    {$x$/.7,
                     $x+\frac{b}{2a}$/.7, $x+\frac{b}{2a}$/.7,
                     $a$/.7,
                     $P(x)$/.8}
                     {$-\infty$,$\frac{-b}{2a}$,$+\infty$}
        \tkzTabLine {,-,z,+}
        \tkzTabLine {,-,z,+}
        \tkzTabLine {,\text{signe de~$a$},t,\text{signe de~$a$}}
        \tkzTabLine {,\text{signe de~$a$},z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{proof}


