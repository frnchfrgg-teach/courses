% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[aspectratio=169]{beamer}

\usetheme{RivaudVideo}
\usepackage{perso}

\usepackage{tikz}
\usepackage{xparse}
\NewDocumentCommand\circled {m} {%
    \tikz[baseline=(char.base)]{
        \node[shape=circle,draw,inner sep=0.3ex] (char) {\textup{#1}};}%
}
\setbeamertemplate{enumerate items}{\circled{\arabic{enumi}}}
\setbeamertemplate{enumerate subitem}{\textit{\alph{enumii}}\textup{)}}
\setbeamerfont{enumerate item}{family=\rmfamily}

\usepackage{rivmath}
\usepackage{calc}
\usepackage{sidefigure}

\setmainlanguage{french}

\author{J. Rivaud}
\title{Produit scalaire}

\def\Prob(#1){p\!\left(#1\right)}
\def\d{\mathrm{d}}


\usetikzlibrary{
    overlay-beamer-styles, patterns, intersections, backgrounds,
}

%\usetikzlibrary{datavisualization
%                datavisualization.formats.functions}

\usepackage{multicol}

\rivmathlib{geometry}

\colorlet{Green}{green!50!black}
\tikzset{
    alert/.style={alt=#1{red}{}},
}%

\gdef\savedPH{}
\NewDocumentCommand\PH{o}{%
    \IfValueTF{#1}{\gdef\savedPH{#1}#1}{\phantom\savedPH}%
}

\begin{document}

\section{Projeté orthogonal}

\begingroup
\def\uuuu#1#2#3#4{
    \draw[red,thick,alt=<.(#1)>{}{transparent}] (#2) -- (#3) pic[perp marker];
}
\def\tttt#1#2#3{Projeté orthogonal de~$#1$ sur~$(#2)$:\quad $#3$.}
\begin{frame}
    \onslide<+->
    \begin{sidefigure}[c,fc]
        \begin{tikzpicture}[scale=2]
            \path (0,0)  coordinate[label=below:$A$] (A);
            \path (0:2) coordinate[label=below:$B$] (B);
            \path ($ (B)!1!-90:(A) $) coordinate[label=above:$C$] (C)
                ($ (C)!1!-90:(B) $) coordinate[label=above:$D$] (D)
                ($ (A)!1/2!(B) $) coordinate (I)
                ($ (A)!1/2!(C) $) coordinate (O)
                ($ (A)!1/2!(O) $) coordinate (J);
            \draw (A) -- (B) -- (C) -- (D) -- cycle;
            \only<+->{
                \draw (A) -- (O) pic[tick marker] node[above] {$O$} -- (C);
            }
            \only<+->{
                \path (A) -- (I) pic[tick marker] node[below] {$I$} -- (B);
            }
            \only<+->{
                \path (A) -- (J) pic[tick marker] node[above] {$J$} -- (O);
            }
            \uuuu{1}{O}{I}{}
            \uuuu{2}{A}{B}{}
            \uuuu{4}{B}{O}{}
            \uuuu{5}{I}{J}{}
        \end{tikzpicture}
    \sidetext
        On considère un carré $ABCD$~de centre~$O$. On note $I$~le milieu
        de~$[AB]$ et $J$~le milieu de~$[OA]$.
        Faire la figure, puis déterminer:
        \begin{enumerate}
            \item le projeté orthogonal de~$O$ sur~$(AB)$;
            \item le projeté orthogonal de~$A$ sur~$(BC)$;
            \item les projetés orthogonaux sur~$(AC)$ de $A$, puis~$B$, et
                enfin~$I$.
        \end{enumerate}
    \end{sidefigure}
    \begin{itemize}[<+->]
        \item\tttt{O}{AB}{I}
        \item\tttt{A}{BC}{B}
        \item\tttt{A}{AC}{A}
        \\\visible<+->{\tttt{B}{AC}{O}}
        \\\visible<+->{\tttt{C}{AC}{J}}
    \end{itemize}
\end{frame}
\endgroup

\section{Travail d'une force}

\begingroup
\NewDocumentCommand\dopic{mmO{}O{}}{%
    \label{fig:\thesubsection-force-work-#1}%
    \begin{tikzpicture}[
            x=0.5cm,y=0.5cm,scale=0.8,
            baseline=(current bounding box),
            #1/.try,
        ]
        \draw[blue] (0,0) coordinate[label=below:$A$] (A)
                pic[point marker] {};
        \path (A) +(-2.5,-1.3) coordinate (SW);
        \path ($ (A)!-1!(SW) $) coordinate (NE);
        \fill ($ (SW)!1/5!(SW-|NE) $) +(0,-0.2) circle[radius=0.2];
        \fill ($ (SW)!4/5!(SW-|NE) $) +(0,-0.2) circle[radius=0.2];
        \path (A) +#2 coordinate (F);
        #4
        \draw[thick,->] (A)
            -- node[auto,#3,circle,inner sep=0.2ex] {$\vect{F}$} (F);
        \draw[->,blue]  (A) -- +(4,0) coordinate[label=below:$B$] (B);
        \path (current bounding box.south west)
                +(-0.3,0) coordinate (BSW);
        \path (current bounding box.north east)
                +(0.3,0.3) coordinate (BNE);
        \scoped[on background layer, shift=(A)] {
            \draw[help lines,step=1] (BSW) grid (BNE);
            \draw[fill,fill opacity=0.05] (NE) rectangle (SW);
        }
        \draw (BSW) -- (BNE |- BSW);
        \path[pattern = north east lines]
            (BSW) +(0,-0.3) rectangle (BNE |- BSW);
        \node[below=0pt of current bounding box.south]
            {\footnotesize\ref{fig:\thesubsection-force-work-#1}};
    \end{tikzpicture}%
    \ignorespaces
}%
\def\hl#1{\only<.>{\gappto\mytikzstyles{#1/.style=red,}}}
\def\doref#1{\hl{#1}\ref{fig:\thesubsection-force-work-#1}}

\subsection{\circled{1} Force et mouvement}

\begin{frame}
    \gdef\mytikzstyles{}
    \onslide<+->
    \begin{minipage}{0.45\linewidth}
        Objet très lourd, en déplacement de $A$~à~$B$, force
        constante~$\vect{F}$.

        \begin{itemize}[<+->]
            \item Cas de force motrice:
                \onslide<+->
                \doref{med}, \doref{big}, \doref{medup}.

                \onslide<+->
                Cas de force sans effet:
                \onslide<+->
                \doref{null}.

                \onslide<+->
                Cas de force résistive:
                \onslide<+->
                \doref{neg}, \doref{negup}.
            \item Comparaison des cas \doref{med} et \doref{medup}:

                \onslide<+->\hl{med}\hl{medup}
                Elles ont le même effet !
            \item D'autres forces donnant le même\\
                résultat dans le cas \ref{fig:1-force-work-big}:

                \onslide<+->\hl{otherF}
                $\vect{F_1}$~et~$\vect{F_2}$.
        \end{itemize}

        \medskip

        \onslide<+->\hl{otherF}
        \textbf{Bilan:} $\vect{F}$ se décompose en:
        \begin{itemize}
            \item Une composante colinéaire: utile
            \item Une composante verticale: sans effet
        \end{itemize}
    \end{minipage}
    \hfill
    \onslide<1->
    \begin{minipage}{0.5\linewidth}
        \tikzset{otherF/.style=transparent}
        \expandafter\tikzset\expandafter{\mytikzstyles}
        \begin{enumerate}[gathered,label=,labelsep=0pt,itemjoin=\hfill,
                ref=\sffamily\bfseries(\alph{enumi})]
            \item \dopic{neg}{(-4,0)}[swap]
            \item \dopic{med}{(2,0)}
            \item \dopic{negup}{(-4,2)}[][\path (-4,4);]
            \item \dopic{null}{(0,4)}
            \item \dopic{big}{(3,-1)}[swap][
                \path(-4,3);
                \draw[thick,->,otherF/.try] (A)
                    -- node[pos=1,above,inner sep=0pt] {$\vect{F_1}$} (3,0);
                \draw[thick,->,otherF/.try] (A)
                    -- node[pos=1,right,inner sep=0pt] {$\vect{F_2}$} (3,2);
                ]
            \item \dopic{medup}{(2,3)}
        \end{enumerate}
    \end{minipage}
\end{frame}

\subsection{\circled{2} Travail d'une force}

\NewDocumentCommand\OP{mO{above}O{below}}{
    \begin{scope}[overlay,#1H/.try,transparent/.retry]
        \node[#2] at (F) {$C$};
        \coordinate (H) at (F |- A);
        \draw[dashed] (A) -- (H);
        \draw (F) -- (H) node[#3]{$H$};
    \end{scope}
}
\def\initforce#1{%
    \par
    \smallskip
    \onslide<+->\doref{#1}%
    \onslide<+->\hl{#1H}%
    \onslide<+->\hl{#1H}%
}

\begin{frame}
    \gdef\mytikzstyles{}
    \onslide<+->
    \begin{minipage}{0.45\linewidth}
        Objet très lourd, en déplacement de $A$~à~$B$, force
        constante~$\vect{F}$.

        \onslide<+->
        $AB = \SI{4}{\m}$; $1$~carreau = \SI{1}{\newton}.

        Calcul du travail de la force:

        \initforce{neg}
        $W = -AB \times AH = -4 \times 4 = \SI{-16}{\joule}$
        \initforce{med}
        $W = AB \times AH = 4 \times 2 = \SI{8}{\joule}$
        \initforce{negup}
        $W = -AB \times AH = -4 \times 4 = \SI{-16}{\joule}$
        \initforce{null}
        $W = 0$
        \initforce{big}
        $W = AB \times AH = 4 \times 3 = \SI{12}{\joule}$
        \initforce{medup}
        $W = AB \times AH = 4 \times 2 = \SI{8}{\joule}$
    \end{minipage}
    \hfill
    \onslide<1->
    \begin{minipage}{0.5\linewidth}
        \expandafter\tikzset\expandafter{\mytikzstyles}
        \begin{enumerate}[gathered,label=,labelsep=0pt,itemjoin=\hfill,
                ref=\sffamily\bfseries(\alph{enumi})]
            \item \dopic{neg}{(-4,0)}[swap][\OP{neg}]
            \item \dopic{med}{(2,0)}[][\OP{med}]
            \item \dopic{negup}{(-4,2)}[][\path (-4,4); \OP{negup}]
            \item \dopic{null}{(0,4)}[][\OP{null}[left][left]]
            \item \dopic{big}{(3,-1)}[swap][
                    \path (-4,3);
                    \OP{big}[below][above]
                ]
            \item \dopic{medup}{(2,3)}[][\OP{medup}]
        \end{enumerate}
    \end{minipage}
\end{frame}
\endgroup

\section{Utiliser la formule du projeté orthogonal}

\begin{frame}
    \onslide<+->
    \begin{sidefigure}[t,ft]
        \begin{tikzpicture}[scale=1.5]
            \path (0,0)  coordinate[label=below:$A$] (A);
            \path (0:2) coordinate[label=below:$B$] (B);
            \path ($ (B)!1!-90:(A) $) coordinate[label=above:$C$] (C)
                ($ (C)!1!-90:(B) $) coordinate[label=above:$D$] (D)
                ($ (A)!1/2!(B) $) coordinate[label=below:$I$] (I);
            \draw
                (A) -- (I) pic[tick marker] -- (B) -- (C) -- (D) -- cycle;
            \only<.(2)-.(4)>{
                \draw[->,thick] (A) -- (B);
                \draw[->,thick] (A) -- (C);
                \only<.(3)->{
                    \draw[red,thick] (C) -- (B) pic[perp marker=r]
                        node[right,overlay] {$H$};
                }
            }
            \only<.(5)-.(7)>{
                \draw[->,thick] (A) -- (B);
                \draw[->,thick] (A) -- (D);
                \only<.(6)->{
                    \draw[red,thick] (D) -- (A) pic[perp marker]
                        node[left,overlay] {$H$};
                }
            }
            \only<.(8)->{
                \draw[->,thick,alt=<.(9)->{blue}{}] (I) -- (C);
                \draw[->,thick] (B) -- (I);
                \only<.(9)->{
                    \draw[->,thick,blue] (I) -- (A);
                }
                \only<.(10)->{
                    \draw[red,thick] (C) -- (B) pic[perp marker=r]
                        node[right,overlay] {$H$};
                }
                \only<.(11)>{
                    \begin{scope}[overlay]
                    \draw[red,dashed]
                        (I) -- ($ (I)!-1/5!(C) $) coordinate (Hp);
                    \draw[red,thick] (A) -- (Hp) pic[perp marker]
                        node[below] {$H'$};
                    \end{scope}
                }
            }
        \end{tikzpicture}
    \sidetext
        \begin{itemize}
            \item $ABCD$ carré de côté $2$.
            \item $I$ le milieu de $[AB]$.
        \end{itemize}
        Déterminer les produits scalaires $\vect{AB}\cdot \vect{AC}$,
        $\vect{AB}\cdot \vect{AD}$ et $\vect{IC}\cdot \vect{BI}$.

    \onslide<+->
    \begin{itemize}[<+->]
        \item Pour $\vect{AB}\cdot\vect{AC}$:\\
            \onslide<+->
            Projeté orthogonal de~$C$ sur~$(AB)$:\quad $H = B$. \\
            \onslide<+->
            $\vect{AB}\cdot\vect{AC} = AB \times AH = 2 \times 2 = 4$.
        \item Pour $\vect{AB}\cdot\vect{AD}$:\\
            \onslide<+->
            Projeté orthogonal de~$D$ sur~$(AB)$:\quad $H = A$. \\
            \onslide<+->
            $\vect{AB}\cdot\vect{AD} = 0$.
        \item Pour $\vect{IC}\cdot\vect{BI}$:
            \onslide<+->
            $\vect{IC}\cdot\vect{BI} = \color{blue}\vect{IC}\cdot\vect{IA}$ \\
            \onslide<+->
            Projeté orthogonal de~$C$ sur~$(AB)$:\quad $H = B$. \\
            $\vect{IC}\cdot\vect{BI} = \vect{IC}\cdot\vect{IA} =
                -IH\times IA = -1 \times 1 = -1$
    \end{itemize}
    \onslide<1->
    \end{sidefigure}
\end{frame}

\section{Utiliser la formule du cosinus}

\begin{frame}
    \onslide<+->
    Dans le triangle~$ABC$, on a $AB=10$, $AC=2$ et
    $\widehat{BAC} = \dfrac{\pi}{3}$.\\
    Calculer $\vect{AB}\cdot\vect{AC}$.

    \bigskip

    \onslide<+->
    \begin{align*}
        \vect{AB}\cdot\vect{AC} &= AB \times AC \times \cos\widehat{BAC} \\
        &\visible<+->{= 10 \times 2 \times\cos\left(\dfrac{\pi}{3}\right)} \\
        &\visible<+->{= 10 \times 2 \times \dfrac{1}{2}} \\
        &\visible<+->{= 10}
    \end{align*}
\end{frame}

\section{Utiliser la formule des coordonnées}

\begin{frame}
    \onslide<+->
    Soient $\point{A}(-1;3)$, $\point{B}(5;1)$, $\point{C}(3;5)$
    et~$\point{D}(6;14)$ dans un repère~$(O;I;J)$ orthonormé. Montrer que
    $(AB)$~et~$(CD)$ sont perpendiculaires.

    \bigskip

    \onslide<+->
    $\vect{AB}(5-(-1);1-3)$ soit
    $\vect{AB}(\alert<.(2)>{6};\alert<.(3)>{-2})$
    \quad
    \onslide<+->
    $\vect{CD}(6-3;14-5)$ soit
    $\vect{CD}(\alert<.(1)>{3};\alert<.(2)>{9})$

    \onslide<+->
    $\vect{AB}\cdot\vect{CD} = \alert<.>{6} \times \alert<.>{3}
    + \alert<+>{(-2)} \times \alert<.>{9} =
    \onslide<+-> 18 - 18 = 0$.

    \medskip

    \onslide<+->
    $\vect{AB}$ et $\vect{CD}$ sont donc orthogonaux:
    $(AB)$~et~$(CD)$ sont perpendiculaires.
\end{frame}

\section{Utiliser plusieurs formules successivement}

\begin{frame}
    \onslide<+->
    Dans un repère~$(O;I;J)$ orthonormé, on considère
    $\point{B}(5;1)$~et~$\point{L}(2;4)$.
    \begin{enumerate}
        \item Calculer $\vect{OB}\cdot\vect{OL}$
        \item Calculer $OB$ et $OL$.
        \item En déduire une mesure de l'angle $\widehat{BOL}$, qu'on donnera
            en degrés arrondi à \<0.1>~près.
    \end{enumerate}
    \bigskip
    \begin{itemize}[<+->]
        \item $\vect{OB}(5;1)$ et $\vect{OL}(2;4)$, donc
            $\vect{OB}\cdot\vect{OL} = 5 \times 2 + 1 \times 4 = 14$.

            \onslide<+->
            $OB = \sqrt{5^2 + 1^2} = \sqrt{26}$
            \qquad
            $OL = \sqrt{2^2 + 4^2} = \sqrt{20}$

        \item $14 = \vect{OB}\cdot\vect{OL}
            = OB \times OL \times \cos\widehat{BOL}$

            \onslide<+->
            $\cos\widehat{BOL} = \dfrac{14}{\sqrt{26}\sqrt{20}}
            \approx \<0.614>$

            \onslide<+->
            $\widehat{BOL} \approx \arccos\<0.614>
            \approx \SI{0.91}{radians}$%
            \onslide<+->
            ${} \approx \SI{52.1}{\degree}$
    \end{itemize}
\end{frame}


\end{document}



\section{Produit scalaire et cosinus}

\subsection{Exemple}

\begin{frame}
    \onslide<+->
    On considère un carré $ABCD$ de côté $3$.
    Calculer $\vect{BC}\cdot \vect{AC}$.
    \onslide<+->
    \hfill
    \begin{tikzpicture}[baseline=(current bounding box)]
        \path (0,0)               coordinate[label=above:$A$] (A);
        \path (3,0)               coordinate[label=above:$B$] (B);
        \path ($ (B)!1!90:(A) $)  coordinate[label=below:$C$] (C);
        \path ($ (C)!1!90:(B) $)  coordinate[label=below:$D$] (D);
        \draw (A) -- (B) -- (C) -- (D) -- cycle;
        \begin{scope}[thick, alt=<-+>{->}{<-}, alert=<.(1)>]
            \draw (B) -- (C); \draw (A) -- (C);
        \end{scope}
        \draw[->, visible on=<.(2)->, alert=<.(2)>]
            ($ (C)!1cm!(B) $) arc[radius=1cm, start angle=90, end angle=135]
            node[pos=0.4, auto, swap, inner sep=0pt] {$\frac{\pi}{4}$};
    \end{tikzpicture}

    \onslide<.->
    $\vect{BC} \cdot \vect{AC} =
        BC \times AC \times \alert<+>{\cos\vectangle(BC,AC)}$%
    \onslide<.->
    ${} = BC \times AC \times \alert<.>{\cos\vectangle(CB,CA)}$

    \onslide<+->
    $\vectangle(CB,CA) = \dfrac{\pi}{4} \;[2\pi]$ donc
    $\cos\vectangle(CB,CA) = \dfrac{\sqrt{2}}{2}$.

    \onslide<+->
    $BC = 3$\\
    $AC^2 = AB^2 +BC^2$ car $ABC$~rectangle en~$B$, donc
    $AC^2 = 9 + 9$ et $AC = \sqrt{18} = 3\sqrt{2}$.

    \bigskip
    \onslide<+->
    $\vect{BC} \cdot \vect{AC} = 3 \times 3\sqrt{2} \times \dfrac{\sqrt{2}}{2}
        = \dfrac{9 \times 2}{2} = 9$.
\end{frame}


\section{Découverte d'une formule de polarisation}

\subsection{\circled{1} Un triangle}

\begin{frame}
    \onslide<+->
    \begin{enumerate}
        \item Dessiner un triangle~$ABC$ tel que $AB=3$, $AC=4$ et~$BC=5$.
            Calculer $p = AB^2 + AC^2 - BC^2$. À quelle situation géométrique
            cela correspond-il ?
    \end{enumerate}
    \onslide<+->
    \begin{tikzpicture}[baseline=(current bounding box)]
        \path (0,0)  coordinate[label=below:$A$] (A);
        \path (0:3) coordinate[label=below:$B$] (B);
        \def\BAC{90}\def\AC{4}
        \draw[name path=CA, help lines] (A) +(\BAC-10:\AC)
            arc[radius=\AC, start angle=\BAC-10, end angle=\BAC+10];
        \def\ABC{55}\def\BC{5}
        \draw[name path=CB, help lines] (B) +(170-\ABC:\BC)
            arc[radius=\BC, start angle=170-\ABC, end angle=190-\ABC];
        \path[name intersections={of=CA and CB, by=C}];
        \path (C) node[above] {$C$};
        \draw[miter limit=2]
            (A) -- pic[perp marker,visible on=<.(2)->,alert=<.(2)>] {}
            (B) -- (C) -- cycle;
    \end{tikzpicture}
    \quad
    \begin{minipage}{\linewidth-5cm}
        \begin{itemize}[<+->]
            \item $p = 3^2 + 4^2 - 5^2 = 9 + 16 - 25 = 0$
            \item $AB^2 + AC^2 = BC^2$ \\
                réciproque du théorème de \bsc{Pythagore} \\
                $\Longrightarrow$
                $ABC$~est rectangle en~$A$.
        \end{itemize}
    \end{minipage}
\end{frame}


\subsection{\circled{2} et \circled{3} Aigu, obtus}

\begin{frame}
    \onslide<+->
    \begin{multicols}{2}
    \begin{enumerate}[start=2]
        \item Dessiner un triangle~$ABC$ tel que $AB=3$, $AC=4$
            et~$BC=\<3.5>$.\\
            Calculer $p = AB^2 + AC^2 - BC^2$.\\
            Que remarque-t-on ?
        \item Dessiner un triangle~$ABC$ tel que $AB=3$, $AC=4$
            et~$BC=\<6>$.\\
            Calculer~$p$. Que remarque-t-on ?
    \end{enumerate}
    \end{multicols}
    \onslide<+->
    \begin{tikzpicture}[baseline=(current bounding box)]
        \path (0,0)  coordinate[label=below:$A$] (A);
        \path (0:3) coordinate[label=below:$B$] (B);
        \def\BAC{60}\def\AC{4}
        \draw[name path=CA, help lines] (A) +(\BAC-10:\AC)
            arc[radius=\AC, start angle=\BAC-10, end angle=\BAC+10];
        \def\ABC{70}\def\BC{3.5}
        \draw[name path=CB, help lines] (B) +(170-\ABC:\BC)
            arc[radius=\BC, start angle=170-\ABC, end angle=190-\ABC];
        \path[name intersections={of=CA and CB, by=C}];
        \path (C) node[above] {$C$};
        \draw[miter limit=2] (A) -- (B) -- (C) -- cycle;
    \end{tikzpicture}
    \hfill
    \begin{minipage}{\linewidth/2-3cm}
        À gauche:\\
        $p = 3^2 + 4^2 - \<3.5>^2 = \<12.75>$\\[1ex]%
        \onslide<+(1)->{$\phantom{p}>0$ quand $\widehat{BAC}$~aigu}

        \bigskip
        \onslide<.->
        À droite:\\
        $p = 3^2 + 4^2 - 6^2 = -11$\\[1ex]%
        \onslide<+->{$\phantom{p}<0$ quand $\widehat{BAC}$~obtus}
    \end{minipage}
    \hfill
    \begin{tikzpicture}[baseline=(current bounding box)]
        \path (0,0)  coordinate[label=below:$A$] (A);
        \path (0:3) coordinate[label=below:$B$] (B);
        \def\BAC{115}\def\AC{4}
        \draw[name path=CA, help lines] (A) +(\BAC-10:\AC)
            arc[radius=\AC, start angle=\BAC-10, end angle=\BAC+10];
        \def\ABC{30}\def\BC{6}
        \draw[name path=CB, help lines] (B) +(170-\ABC:\BC)
            arc[radius=\BC, start angle=175-\ABC, end angle=185-\ABC];
        \path[name intersections={of=CA and CB, by=C}];
        \path (C) node[above] {$C$};
        \draw[miter limit=2] (A) -- (B) -- (C) -- cycle;
    \end{tikzpicture}
\end{frame}


\subsection{\circled{4} et \circled{5} Points alignés}

\begin{frame}
    \begin{multicols}{2}
        \begin{enumerate}[<+->][start=4]
            \item Placer trois points $A$, $B$ et~$C$ alignés dans cet ordre,
                tels que $AB = 3$ et $AC=4$. Calculer~$p = AB^2 + AC^2 - BC^2$
                et le comparer avec~$AB \times AC$.

                \onslide<+->
                \begin{center}
                    \begin{tikzpicture}[scale=0.8]
                        \draw (0,0) coordinate[label=above:$A$]
                            --  pic[tick marker] {}
                            (10:3) coordinate[label=above:$B$]
                                pic[tick marker] {}
                            -- (10:4) coordinate[label=above:$C$]
                                pic[tick marker] {};
                    \end{tikzpicture}
                \end{center}

                $\PH[p] = 3^2 + 4^2 - 1^2 = 9 + 16 - 1 = 24$\\
                $\PH = 2 \times AB \times AC$
                \columnbreak
            \item Même question avec $B$~et~$C$ de part et d'autre de~$A$.

                \onslide<+->
                \begin{center}
                    \begin{tikzpicture}[scale=0.8]
                        \draw (0,0) coordinate[label=above:$A$]
                            --  pic[tick marker] {}
                            (15:3) coordinate[label=above:$B$]
                                pic[tick marker] {}
                            -- (15:-4) coordinate[label=above:$C$]
                                pic[tick marker] {};
                    \end{tikzpicture}
                \end{center}

                $\PH[p] = 3^2 + 4^2 - 7^2 = 9 + 16 - 49 = -24$\\
                $\PH = -2 \times AB \times AC$
        \end{enumerate}
    \end{multicols}
\end{frame}

\section{Formule de polarisation: démonstration}

\begin{frame}
\onslide<+->

\begin{block}{Proposition}
    $\vect{u}\cdot\vect{v} = \dfrac{1}{2}\left(
        \norm{\vect{u}}^2 + \norm{\vect{v}}^2 - \norm{\vect{v}-\vect{u}}^2
        \right)$
\end{block}

\onslide<+->
\begin{proof}
    Si $\vect{u} = \vect{0}$,
    $\norm{\vect{u}}^2 + \norm{\vect{v}}^2 - \norm{\vect{v}-\vect{u}}^2
    = \norm{\vect{v}}^2 - \norm{\vect{v}}^2 = 0 = 2 \vect{u}\cdot\vect{v}$.

    \onslide<+->{%
    Sinon, il existe
    un repère orthonormé direct $(O;I;J)$
    tel que $\vect{u} = \norm{u}\vect{OI}$.\\
    \onslide<+->{%
    Dans ce repère,
    $\vect{u}(\norm{u};0)$, et
    $\vect{v}(\norm{v}\cos\theta;\norm{v}\sin\theta)$ avec
    $\theta = \vectangle(u,v)$.

    \onslide<+->{%
    Ainsi, $\vect{w} = \vect{v} - \vect{u}$ a pour coordonnées
    $\vect{w}(\norm{v}\cos\theta - \norm{u};\norm{v}\sin\theta)$ et donc\\
    $\norm{w}^2 = x_w^2 + y_w^2
    = \left( \norm{v}\cos\theta - \norm{u} \right)^2
        + \left( \norm{v}\sin\theta \right)^2$\\
    \onslide<+->{$
    = \alert<.(2)>{\left( \norm{v}\cos\theta \right)^2}
        \alert<.(3)>{- 2 \norm{u} \norm{v} \cos\theta}
        + \alert<.(1)>{\norm{u}^2}
        + \alert<.(2)>{\left( \norm{v}\sin\theta \right)^2}$\\
    \onslide<+->{$
    = \alert<.>{\norm{u}^2}
        + \alert<+>{\norm{v}^2 (\alert<.(2)>{\cos^2\theta + \sin^2\theta})}
        \alert<+>{- 2 \vect{u}\cdot\vect{v}}$.

    \onslide<+->{%
    $\norm{w}^2 = \norm{u}^2 + \norm{v}^2 - 2 \vect{u}\cdot\vect{v}$. \\
    On en déduit $\norm{u}^2 + \norm{v}^2 - \norm{w}^2 =
    2\vect{u}\cdot\vect{v}$.
}}}}}}
    \null\hspace*{0pt plus 5filll}
    \begin{tikzpicture}[scale=1.6]
        \begin{scope}[rotate=20]
            \draw[->,thick] (0,0) -- node[below] {$\vect{u}$} (0:0.6);
            \draw[->,thick] (0,0) -- node[auto] {$\vect{v}$} (45:1.5)
                coordinate (V);
            \begin{scope}[visible on=<3->]
            \draw (-1.2, 0) -- (1.2, 0);
            \draw (0, -1.2) -- (0, 1.2);
            \draw (0, 0) circle [radius=1];
            \draw (0,0) coordinate (O) node [anchor=60]  {$O$};
            \path (1,0) coordinate (I)
                node[below right, inner ysep=0pt] {$I$};
            \path (0,1)
                node[above left, inner ysep=0pt] {$J$};
            \end{scope}
            \begin{scope}[visible on=<4->]
            \draw[densely dotted]
                (V) -- (V |- O)  (V) -- (V -| O);
            \draw[->] (0:0.4) arc[radius=0.4, start angle=0, end angle=45];
            \node at (22.5:0.6) {$\theta$};
            \end{scope}
        \end{scope}
        \coordinate (X) at (current bounding box.south -| I);
        \pgfresetboundingbox
        \useasboundingbox (X);
    \end{tikzpicture}%
\end{proof}

\end{frame}

\section{Exemple d'utilisation de la formule de polarisation}

\begin{frame}
    \onslide<+->
    Soit $ABC$ un triangle tel que $AB=6$, $AC=5$ et $BC=8$.
    \begin{enumerate}
        \item Calculer $\vect{AB}\cdot \vect{AC}$.
        \item $\vect{AB}$~et~$\vect{AC}$ sont-ils orthogonaux ?
    \end{enumerate}
    \bigskip
    \hfill
    \onslide<+->
    \begin{tikzpicture}[scale=0.5]
        \path (0,0)  coordinate[label=below:$A$] (A);
        \path[use as bounding box] (0:6) coordinate[label=below:$B$] (B);
        \def\BAC{90}\def\AC{5}
        \draw[name path=CA, help lines] (A) +(\BAC-10:\AC)
            arc[radius=\AC, start angle=\BAC-10, end angle=\BAC+10];
        \def\ABC{40}\def\BC{8}
        \draw[name path=CB, help lines] (B) +(170-\ABC:\BC)
            arc[radius=\BC, start angle=170-\ABC, end angle=190-\ABC];
        \path[name intersections={of=CA and CB, by=C}];
        \path (C) node[above] {$C$};
        \draw[miter limit=2]
            (A) -- (B) -- (C) -- cycle;
    \end{tikzpicture}
    \everymath{\displaystyle}
    \begin{itemize}[<+->]
        \item $\vect{AC}-\vect{AB} = \vect{AC}+\vect{BA} = \vect{BC}$
        \item $\PH[\vect{AB}\cdot\vect{AC}] =
            \frac{1}{2}\left(AB^2 + AC^2 - \norm{\vect{AC}-\vect{AB}}^2\right)
            = \frac{1}{2}\left( AB^2 + AC^2 - \norm{\vect{BC}}^2 \right)$ \\
            $\PH = \frac{1}{2}\left( 6^2 + 5^2 - 8^2 \right)
            = \frac{-3}{2}$
        \item $\vect{AB}\cdot\vect{AC} \ne 0$: pas orthogonaux.
    \end{itemize}
\end{frame}





\section{Bilinéarité du produit scalaire}

\begin{frame}
    \onslide<+->
    \begin{proposition}
        \begin{enumerate}
            \item Le produit scalaire est distributif par rapport
                à l'addition:
                $\vect{u}\cdot(\vect{v}+\vect{w}) =
                \vect{u}\cdot\vect{v} + \vect{u}\cdot\vect{w}$
            \item Pour deux réels $k$ et $k'$:
                $ (k\vect{u})\cdot (k'\vect{v}) =
                (k\times k')\vect{u}\cdot \vect{v}$
        \end{enumerate}
    \end{proposition}
    \onslide<+->
    \begin{proof}
        Fixons un repère orthonormé, dans lequel
                $\vect{u}(x_u;y_u)$, $\vect{v}(x_v;y_v)$
                et~$\vect{w}(x_w;y_w)$.
        \begin{enumerate}
            \item Alors $(\vect{v}+\vect{w})\vect(x_v+x_w;y_v+y_w)$,
                \onslide<+->
                donc
                $ \begin{aligned}[t]
                    \vect{u}\cdot(\vect{v}+\vect{w}) &
                      = x_u(x_v+x_w) + y_u(y_v+y_w) \\
                    \onslide<+->
                    & = x_u x_v + x_u x_w + y_u y_v + y_u y_w \\
                    \onslide<+->
                    & = x_u x_v + y_u y_v + x_u x_w + y_u y_w \\
                    \onslide<+->
                    & = \vect{u}\cdot\vect{v} + \vect{u}\cdot\vect{w}
                \end{aligned}$
            \item<+-> $(k\vect{u})\cdot (k'\vect{v})
                = k x_u \times k' x_v + k y_u \times k'y_v
                \onslide<+->
                = kk' x_u x_v + kk' y_u y_v
                \onslide<+->
                = kk' (x_u x_v + y_u y_v)$ \qedhere
        \end{enumerate}
    \end{proof}
\end{frame}


\section{Norme d'un vecteur}

\begin{frame}
    \onslide<+->
    On considère le triangle~$ABC$ tel que $AB=6$, $AC=5$ et $BC=8$,\\
    et l'on pose $\vect{u} = \vect{AB}$ et $\vect{v} = \vect{AC}$.
    \begin{enumerate}
        \item Déterminer $\norm{\vect{u}}$, $\norm{\vect{v}}$ et
            $\norm{\vect{v} - \vect{u}}$.
        \item A-t-on $\norm{\vect{v} - \vect{u}} = \norm{\vect{v}} -
            \norm{\vect{u}}$ ?
    \end{enumerate}
    \onslide<+->
    \begin{tikzpicture}[scale=0.5,baseline=(current bounding box)]
        \path (0,0)  coordinate[label=below:$A$] (A);
        \path (0:6) coordinate[label=below:$B$] (B);
        \draw[name path=CA, help lines] (A) +(80:5)
            arc[radius=5, start angle=80, end angle=110];
        \draw[name path=CB, help lines] (B) +(130:8)
            arc[radius=8, start angle=130, end angle=150];
        \path[name intersections={of=CA and CB, by=C}];
        \path (C) node[above] {$C$};
        \draw[->,thick] (A) -- node[below]{$\vect{u}$} (B);
        \draw[->,thick] (A) -- node[auto]{$\vect{v}$} (C);
        \draw[->,thick] (B) -- node[auto,swap]{$\vect{v}-\vect{u}$} (C);
    \end{tikzpicture}
    \quad
    \begin{minipage}{\linewidth-8cm}
        \begin{itemize}[<+->]
            \item<.-> $\vect{v} - \vect{u} = \vect{AC} - \vect{AB}
                = \vect{AC} + \vect{BA} = \vect{BC}$
            \item $\norm{\vect{u}} = AB = 6$\\
                $\norm{\vect{v}} = \norm{\vect{AC}} = 5$ \\
                $\norm{\vect{v}-\vect{u}} = \norm{\vect{BC}} = BC = 8$
            \item $\norm{\vect{v}} - \norm{\vect{u}} = 5 - 6 = -1 \ne 8$
        \end{itemize}
    \end{minipage}
\end{frame}


\end{document}
