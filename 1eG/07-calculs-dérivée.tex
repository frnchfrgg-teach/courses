% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}

\def\qrlink#1{Voir \url{#1}.\makelink[up]{#1}}
\persocourssetup{qrcodes}


\begin{document}

\StudentMode
\def\UGHOST#1{{\color{blue}#1}}
\let\GHOST\UGHOST

\chapter{Calcul de dérivées}

\section{Opérations simples}

\subsection{Dérivée usuelles}

\begin{theorem}
    \label{thm:common-derivatives}
    On connait les fonctions dérivées suivantes:

    \begin{center}
    $\begin{array}{L*{3}{cL}}
        \firsthline
        \text{Fonction} & \text{Dérivable sur} & \text{Fonction dérivée} \\
        \hline
        f(x) = k \text{ avec $k \in \mdR$} & \mdR & f'(x) = 0 \NL
        f(x) = x & \mdR & f'(x) = 1 \NL
        f(x) = mx + p \text{ avec $m,p \in \mdR$} & \GHOST{\mdR} &
                f'(x) = \GHOST{m} \NL
        f(x) = x^2 & \GHOST{\mdR} & f'(x) = \GHOST{2x} \NL
        f(x) = x^n \text{ avec $n \ge 0$ } & \textcolor{red}{\mdR} &
        f'(x) = \textcolor{red}{nx^{n-1}} \NL
        f(x) = \dfrac{1}{x} & \GHOST{\begin{gathered}
                    \IN]-\infty;0;[ \text{ ou } \\
                    \IN]0;+\infty;[ \end{gathered}} &
                f'(x) = \GHOST{-\dfrac{1}{x^2}} \NL
        f(x) = \sqrt{x} & \GHOST{\IN]0;+\infty;[} &
                f'(x) = \GHOST{\dfrac{1}{2\sqrt{x}}} \\
        \lasthline
    \end{array}$
    \end{center}
\end{theorem}

\begin{remark}
    Toutes les lignes ont déjà été vues dans le chapitre sur les fonctions de
    référence, sauf la cinquième qui est admise.
\end{remark}

\begin{example}
    \begin{enumerate}
        \item La fonction $f:x\mapsto x^5$ est dérivable sur~$\mdR$ et pour
            tout~$x\in\mdR$, $f'(x) = 5 x^{5-1} = 5x^4$.
            On a $f'(3) = \UGHOST{5 \times 3^4 = 5 \times 81 = 405}$.
        \item La fonction $f:x\mapsto 5x+6$ est dérivable sur~$\UGHOST{\mdR}$ et
            pour tout~$x\in\UGHOST{\mdR}$, $f'(x) = \UGHOST{5}$.
            On a $f'(2) = \UGHOST{5}$.
        \item La fonction $f:x\mapsto -4$ est dérivable sur~$\UGHOST{\mdR}$ et
            pour tout~$x\in\UGHOST{\mdR}$, $f'(x) = \UGHOST{0}$.
            On a $f'(10) = \UGHOST{0}$.
        \item La fonction $f:x\mapsto \dfrac{x}{3} + 1$ est dérivable
            sur~$\UGHOST{\mdR}$ et pour tout~$x\in\UGHOST{\mdR}$,
            $f'(x) = \UGHOST{\dfrac{1}{3}}$.
            On a $f'(10) = \UGHOST{\dfrac{1}{3}}$.
    \end{enumerate}
\end{example}

\begin{exercise}[Dérivée et tangente de la fonction inverse]
    52 p.~79
\end{exercise}

\subsection{Premiers théorèmes opératoires}

\begin{theorem}
    Si $u$ est une fonction dérivable sur~$I$ et $k \in \mdR$ est une constante,
    alors $f = k u$~est dérivable sur~$I$ et $f' = k u'$.
\end{theorem}

\begin{remark}
    $f = k u$ veut dire $f(x) = k \times u(x)$ pour tout~$x \in I$. De même
    $f' = k u'$ signifie $\forall x\in I, f'(x) = k u'(x)$.
\end{remark}

\begingroup\color{gray}
\begin{proof}
    (Pour la culture uniquement)
    Soit $a \in I$. On a $\frac{f(a+h) - f(a)}{h} = \frac{ku(a+h) - ku(a)}{h} =
    k \frac{u(a+h)-u(a)}{h}$. Or $\lim_{h\to0} \frac{u(a+h) - u(a)}{h} = u'(a)$
    donc $\lim_{h\to0} \frac{f(a+h) - f(a)}{h} = k u'(a)$. En définitive
    $f$~est dérivable en~$a$ et $f'(a) = ku'(a)$.
\end{proof}
\endgroup

\begin{example}
    \begin{enumerate}
        \item La fonction $f:x\mapsto \dfrac{x^2}{2}$ est dérivable
            sur~$\UGHOST{\mdR}$ et pour tout~$x\in\UGHOST{\mdR}$,
            $f'(x) = \UGHOST{\frac{1}{2} \times 2x = x}$.
            On a $f'(8) = \UGHOST{8}$.
        \item La fonction $f:x\mapsto 7x^5$ est dérivable
            sur~$\UGHOST{\mdR}$ et pour tout~$x\in\UGHOST{\mdR}$,
            $f'(x) = \UGHOST{7 \times 5x^4 = 35x^4}$.
            On a $f'(2) = \UGHOST{35 \times 16 = 560}$.
        \item La fonction $f:x\mapsto \dfrac{3}{x}$ est dérivable
            sur~$\UGHOST{\mdR^{*}}$ et sa dérivée est la fonction
            $f': x \mapsto \UGHOST{3\times\dfrac{-1}{x^2} = \dfrac{-3}{x^2}}$.
        \item La fonction $f:x\mapsto -7\sqrt{x}$ est dérivable
            sur~$\UGHOST{\IN]0;+\infty;[}$ et sa dérivée est la fonction
            $f': x \mapsto \UGHOST{-7\times\dfrac{1}{2\sqrt{x}} =
                \dfrac{-7}{2\sqrt{x}}}$.
    \end{enumerate}
\end{example}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f = u + v$~est
    dérivable sur~$I$ et $f' = u' + v'$.
\end{theorem}

\begin{remark}
    $f = u + v$ veut dire $f(x) = u(x) + v(x)$ pour tout~$x \in I$.
\end{remark}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = x^3 + \frac{x^2}{2}$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa
    fonction dérivée.

    \UGHOST{%
    Rédaction type: $f$ est dérivable sur~$\mdR$ comme somme de fonctions qui le
    sont, et $f'(x) = 3x^2 + \dfrac{1}{2} \times (2x) =
    3x^2 + x$.%
    }
\end{example}

\begin{remark}
    Tous les polynômes sont donc dérivables sur~$\mdR$.
\end{remark}

\begin{exercise}*[Déterminer l'équation d'une tangente]
    Soit $f$~la fonction définie par $f(x) = 4x^2 - 3x + 5$. Déterminer
    l'équation de la tangente à~$\mcC_f$ au point d'abscisse~$-2$.

    \UGHOST{%
        $f$~est dérivable sur~$\mdR$ car c'est un polynôme, et pour
        tout~$x\in\mdR$, $f'(x) = 4 \times 2x - 3 = 8x - 3$. Ainsi,
        $f'(-2) = 8 \times (-2) - 3 = -19$. Or $f(-2) = 4 \times (-2)^2 - 3
        \times (-2) + 5 = 27$ donc la tangente a pour équation
        $y = f'(-2)\left( x-(-2) \right) + f(-2)$ soit
        $y = -19(x+2) + 27$ ce qui donne $y = -19x - 11$.
    }
\end{exercise}

\subsection{Variations d'un polynôme}

\begin{exercise}*[Étudier les variations d'un polynôme]
    Soient les fonctions $f$~et~$g$ définies sur~$\mdR$ par:
    $f(x) = -2x^2 + 3x - 1$ et $g(x) = x^3 − 3x$
    \begin{enumerate}
        \item Étudier les variations de la fonction $f$.
        \item Étudier les variations de la fonction $g$.
    \end{enumerate}
    \qrlink{https://www.youtube.com/watch?v=23_Ba3N0fu4}
\end{exercise}

\begin{exercise}*[Étudier la position relative d'une courbe avec une de ses
    tangentes]
    On reprend la fonction~$g$ définie sur~$\mdR$ par $g(x) = x^3 − 3x$
    \begin{enumerate}
        \item Déterminer une équation de la tangente~$\mcT$ à la courbe de~$g$
            au point d'abscisse~$0$.
        \item Déterminer la position relative de $\mcC_g$~et~$T$.
    \end{enumerate}

    \begingroup \slshape
        Déterminer la position relative de deux courbes représentant des
        fonctions $g$~et~$h$ revient à savoir si $\mcC_g$ est située au-dessus
        ou en-dessous de $\mcC_h$, ce qui dépend des intervalles.

        Pour cela on étudie le signe de la différence $g(x) − h(x)$.
    \endgroup

    \qrlink{https://www.youtube.com/watch?v=EyxP5HIfyF4}
\end{exercise}

\section{D'autres opérations}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f=uv$~est
    dérivable sur~$I$ et $f' = u'v + uv'$.
\end{theorem}

\begin{proof}
    Voir page 150 du livre.
\end{proof}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = (3x + 2)(\sqrt{x} + 1)$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa fonction
    dérivée.

    \UGHOST{%
        Rédaction type: On pose $u(x) = 3x+2$ et $v(x) = \sqrt{x} + 1$.\\
        Alors $u'(x) = 3$ et $v'(x) = \dfrac{1}{2\sqrt{x}}$.\\
        Or $f = uv$ donc $f$ est dérivable sur $I = \IN]0;+\infty;[$ et\\
        pour tout $x \in I$,
        $f'(x) = 3 \times (\sqrt{x} + 1) + (3x+2) \times \dfrac{1}{2\sqrt{x}}$
    }
\end{example}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, et \emph{si $v(x) \neq
    0$ pour tout~$x\in I$}, alors $f=\dfrac{u}{v}$~est dérivable sur~$I$ et $f'
    = \dfrac{u'v - uv'}{v^2}$.
\end{theorem}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = \dfrac{-2x+5}{x^2+1}$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa fonction
    dérivée.

    \UGHOST{%
        On remarque d'abord que le dénominateur ne s'annule pas car un réel au
        carré est toujours positif ou nul, et l'on ajoute~$1$.
    }

    \UGHOST{%
        Rédaction type: On pose $u(x) = -2x+5$ et $v(x) = x^2 + 1$.\\
        Alors $u'(x) = -2$ et $v'(x) = 2x$.\\
        De plus $v(x) \ne 0$ pour $x \in \mdR$.\\
        Or $f = \dfrac{u}{v}$ donc $f$ est dérivable sur $\mdR$ et\\
        pour tout $x \in \mdR$,
        $\begin{aligned}[t]
        f'(x) &= \dfrac{-2 \times (x^2 + 1) \mathbin{ \color{red} - }
                (-2x+5) \times 2x}{(x^2+1)^2} \\
        &= \dfrac{-2x^2 -2 - (-4x^2 + 10x)}{(x^2+1)^2} \\
        &= \dfrac{-2x^2 -2 + 4x^2 - 10x}{(x^2+1)^2} \\
        f'(x) &= \dfrac{2x^2 - 10x - 2}{(x^2+1)^2} \\
        \end{aligned}$
    }
\end{example}

\begin{remark}
    On ne développe jamais le dénominateur (en bas), car c'est un carré donc
    trouver son signe est facile. En revanche on développe le numérateur si
    nécessaire pour réduire et factoriser la différence.
\end{remark}

\begin{corollary}
    Soit $n$ un entier strictement négatif. Alors $f$~est dérivable sur
    $\IN]-\infty;0;[$ et $\IN]0;+\infty;[$, et sa fonction dérivée est
    $f':x\mapsto n x^{n-1}$
\end{corollary}

\begin{remark}
    C'est la même formule que pour $n>0$. On rappelle que
    $x^{-3} = \dfrac{1}{x^3}$
\end{remark}

\begin{theorem}
    Soient $m$~et~$p$ deux réels, ainsi que $g$~une fonction dérivable sur~$I$.
    En notant $J$ l'ensemble des réels tels que $mx+p\in I$.

    La fonction $f:x\mapsto g(mx+p)$ est définie et dérivable sur~$J$ et pour
    tout~$x \in J$, $f'(x) = m g'(mx+p)$.
\end{theorem}

\begin{example}
    \begin{enumerate}
        \item Exprimer la dérivée de la fonction~$f$ définie
            sur~$\IN]-\frac{3}{2};+\infty;[$ par $f(x) = \sqrt{2x+3}$.
        \item Exprimer la dérivée de la fonction~$h$ définie sur~$\mdR$ par
            $h(x) = (-4x+5)^3$.
    \end{enumerate}
\end{example}

\end{document}
