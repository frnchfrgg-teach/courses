% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}
\usetikzlibrary{decorations.markings}

\rivmathlib{geometry}

\begin{document}
\StudentMode

\chapter{Trigonométrie}

\section{Repérage sur le cercle trigonométrique}

\subsection{Enroulement de la droite des réels}

\begin{definition}
    On munit le plan d'un repère orthonormé~$(O;I;J)$.  Le \define{cercle
    trigonométrique}~$\mcC$ est le cercle de centre~$O$ et de rayon~$1$, sur
    lequel on choisit une orientation:
    \begin{itemize}
        \item le \define{sens direct} --- ou positif, ou encore
            \define{trigonométrique} --- est \emph{contraire} au sens de
            rotation des aiguilles d'une montre;
        \item le \define{sens indirect} --- ou négatif --- est le sens des
            aiguilles d'une montre.
    \end{itemize}
\end{definition}

On peut «enrouler» la droite d'équation~$x=1$ sur le cercle~$\mcC$ de la façon
suivante: si $M(1;t)$~est un point de cette droite on imagine que le
segment~$[IM]$ est un mètre à ruban qu'on peut faire tourner autour du cercle
sans l'étirer: la distance entre $I$~et~$M$ \emph{en suivant} le mètre à ruban
ne change jamais quelque soit la forme qu'on donne à ce dernier. Quand on
enroule une longueur de ruban de plus en plus grande, il forme une spirale de
plus en plus serrée qui se confond avec le cercle. Chaque point sur la droite
verticale est ainsi transformé en un point sur le cercle --- voir
\vref{fig:enroul}.

\begin{figure}
    \centering
    \caption{Enroulement de la demi-droite d'équation~$x=1$ pour $y \ge 0$. En
        haut à gauche, le ruban est le long de la demi-droite. En haut à droite,
        on a commencé à enrouler le ruban sur le cercle. En bas à gauche, le
        ruban est partiellement enroulé et le reste forme une spirale. En bas à
        droite on a totalement enroulé le ruban et l'on voit que $A$~et~$C$ se
        superposent.
    }
    \label{fig:enroul}
    \def\sqsize{2.3}
    % Found with Maxima to have a C^1 piecewise-defined function
    % - from 0 to \mp: 3rd order polynomial with value=0, derivative=1 at 0
    % - from \mp to +\infty: acos(1/t)
    \pgfmathsetmacro\mp{2*pi/3}
    \pgfmathsetmacro\malpha{0.05352575414691953}
    \pgfmathsetmacro\mbeta{-0.3449475860352801}
    \foreach \angle [evaluate=\angle using \angle*pi/12] in {0, 5, 18, 24}{
        \begin{tikzpicture}[x=1cm,y=1cm,
                            grid/.style={help lines, black!20},
                            axes/.style=->,
                            circle/.style=semithick
                            ]
            \path[clip]
                (-\sqsize, -\sqsize) rectangle (\sqsize, \sqsize);
            \draw[grid]
                (-\sqsize, -\sqsize) grid (\sqsize, \sqsize);
            \draw[axes] (-\sqsize, 0) -- (\sqsize, 0);
            \draw[axes] (0, -\sqsize) -- (0, \sqsize);
            \draw[circle] (0,0) circle[radius=1];
            \node at (0,0) [below left]  {$O$};
            \node at (1,0) [below right] {$I$};
            \node at (0,1) [above left]  {$J$};
            \draw[thick] (1, 0) -- (1, -3);
            \pgfmathsetmacro\mq{1-\angle/2/pi}
            \gdef\thepoints{}
            \foreach \t in {0, 0.05, ..., 14.5} {
                \pgfmathtruncatemacro\test{\t < \angle}
                \ifnum\test=1\relax
                    \def\mr{1}
                \else
                    \pgfmathsetmacro\mt{(\t-\angle) * \mq}
                    \pgfmathtruncatemacro\test{\mt > \mp}
                    \ifnum\test=1\relax
                        \pgfmathsetmacro\mc{rad(acos(1/\mt))}
                    \else
                        \pgfmathsetmacro\mc{\malpha * pow(\mt,3) +
                                            \mbeta * pow(\mt,2) + \mt}
                    \fi
                    \pgfmathsetmacro\me{pow(\mq,2)*\mt + (1-pow(\mq,2))*\mc}
                    \pgfmathsetmacro\mr{1/cos(\me r)}
                    \pgfmathtruncatemacro\test{\mr > 4}
                    \ifnum\test=1\relax
                        \breakforeach % at end of iteration
                    \fi
                \fi
                \xappto\thepoints{({\t r}:\mr)}
            }
            \draw[thick,
                  postaction=decorate,
                  decoration={
                            markings,
                            mark=at position 1 * 1cm with {
                                \pic[point marker];
                                \node at (0, 2ex) {$A$};
                            },
                            mark=at position pi * 1cm with {
                                \pic[point marker];
                                \node at (0, -2ex) {$B$};
                            },
                            mark=at position (2*pi + 1) * 1cm with {
                                \pic[point marker];
                                \node at (0, -2ex) {$C$};
                            },
                  }]
                plot coordinates{\thepoints};
        \end{tikzpicture}%
    }
\end{figure}

\begin{remark}
    Lorsqu'on enroule l’axe dans le sens \emph{direct}, ce sont les points
    d'\emph{abscisses positives} qui se superposent au cercle~$\mcC$, dans le
    sens \emph{indirect}, ce sont des points d'\emph{abscisses négatives}.
\end{remark}

\subsection{Point repéré par un réel}

\begin{definition}
    On dit que $M\in\mcC$ est le \define{point associé} à~$t\in\mdR$, ou encore
    que $t$ \define[nombre repérant un point]{repère}~$M$, si le point $N(1;t)$
    se retrouve en~$M$ après enroulement de la droite d'équation $x = 1$.

    À tout réel~$x$ correspond un unique point image~$M$ sur le cercle.
    Réciproquement, si $M$~est repéré par le réel~$x$, alors il l'est aussi par
    une infinité de réels. Ils sont de la forme $x + k\times 2\pi, k \in\mdZ$,
    noté aussi $x + 2k\pi, k \in\mdZ$.
\end{definition}


\begin{example}[Lire des réels qui repèrent un point donné]
    \label{exa:lire-mesure}
    \leavevmode\\
    \begin{minipage}{\textwidth-4.5cm}
        \parskip=2ex
        Dans la figure ci-contre:

        $J$ est repéré par \UGHOST{$\dfrac{\pi}{2}$ ou $\dfrac{-3\pi}{2}$ ou
        $\dfrac{5\pi}{2}$}

        $K$ est repéré par \UGHOST{$\dfrac{5\pi}{6}$ ou $\dfrac{-19\pi}{6}$}

        $L$ est repéré par \UGHOST{$\dfrac{5\pi}{4}$ ou $\dfrac{-3\pi}{4}$}
    \end{minipage}
    \hfill
    \begin{tikzpicture}[scale=1.5, baseline=0]
        \draw[help lines] (-1.2, -1.2) grid[step=0.5] (1.2, 1.2);
        \draw[->] (-1.2, 0) -- (1.2, 0);
        \draw[->] (0, -1.2) -- (0, 1.2);
        \draw (0, 0) circle [radius=1];
        \coordinate[label=below left:$O$] (O) at (0,0);
        \node at (1,0) [below right] {$I$};
        \draw (0,1)    coordinate[label=above right:$J$] (J) pic[point marker];
        \draw (150:1)  coordinate[label=above left:$K$]  (K) pic[point marker];
        \draw (-135:1) coordinate[label=below left:$L$]  (L) pic[point marker];
        \draw[dashed] (J) -- pic[dist marker] {}
                      (K) -- pic[dist marker] {}
                      (O);
        \path (O) -- pic[dist marker] {} (J);
    \end{tikzpicture}
    \parfillskip=0pt\par
\end{example}

\IfStudentT{\begin{multicols}{2}}
\begin{example}[Placer le point associé]
    \leavevmode\penalty 0\relax
    \hbox{Placer} sur un cercle trigonométrique les points associés aux réels
    $\pi$;~$-\frac{\pi}{2}$; $\frac{\pi}{3}$ et~$-\frac{\pi}{6}$.

    \IfStudentT{\noindent
        %\columnbreak
        \begin{tikzpicture}[scale=2]
            \draw[help lines] (-1.2, -1.2) grid[step=0.5] (1.2, 1.2);
            \draw[->] (-1.2, 0) -- (1.2, 0);
            \draw[->] (0, -1.2) -- (0, 1.2);
            \draw (0, 0) circle [radius=1];
            \coordinate[label=below left:$O$] (O) at (0,0);
            \node at (1,0) [below right] {$I$};
        \end{tikzpicture}
    }
\end{example}
\IfStudentT{\end{multicols}}

\subsection{Degrés, longueur de l'arc et radians}

Un radian correspond à la mesure d’un angle au centre qui intercepte un arc de
longueur~$1$ sur le cercle trigonométrique. Le symbole du radian est noté
rad.

On a la correspondance suivante --- si $0 \le x \le 360$ et $0 \le y \le
2\pi$:
\[  \everymath{\displaystyle}
    \begin{array}{LlL*{9}{cL}}
    \firsthline
    \text{Degrés} & 0 & 30 & 45 & 60 & 90 & 120 & 180 & 270 & 360 \NL
    \text{Radians} & \GHOST{0} & \GHOST{\frac{\pi}{6}} & \GHOST{\frac{\pi}{4}} &
                    \GHOST{\frac{\pi}{3}} & \GHOST{\frac{\pi}{2}} &
                    \GHOST{\frac{2\pi}{3}} & \GHOST{\pi} &
                    \GHOST{\frac{3\pi}{2}} & \GHOST{2\pi} \\
    \lasthline
\end{array} \]

\subsection{Mesure principale}

\begin{definition}
    Si $M$~est un point sur le cercle trigonométrique, parmi toutes les mesures
    de l'angle~$\vectangle(OI;OM)$ --- c'est-à-dire tous les réels qui
    repèrent~$M$ --- une seule est dans~$\IN]-\pi;\pi;]$. On l'appelle
    \define{mesure principale} de~$\vectangle(OI;OM)$.
\end{definition}

\begin{remark}
    La mesure principale est la longueur de l'arc $\overparen{IM}$ \emph{le plus
    court possible}, en choisissant le signe~«$+$» si l'arc est parcouru dans le
    sens des ronds-points et le signe~«$-$» sinon.
\end{remark}

\begin{method}[Déterminer une mesure principale]
    Si $\alpha$~est une mesure de l'angle:
    \begin{itemize}
        \item On vérifie si $\alpha \in \IN]-\pi;\pi;]$ (on peut comparer le
            numérateur et le dénominateur dans~$\alpha$);
        \item Si ce n'est pas le cas, on fait apparaitre le multiple de~$2\pi$
            le plus proche.
    \end{itemize}
\end{method}

\begin{example}
    \begin{enumerate}
        \item $\dfrac{-3\pi}{7}$ est une mesure principale car $3<7$ donc
            $\dfrac{-3\pi}{7} \in \IN]-\pi;\pi;]$.
        \item $\dfrac{-31\pi}{7}$ n'est pas une mesure principale. $2\pi =
            \dfrac{14\pi}{7}$; son multiple le plus proche est $-4\pi =
            \dfrac{-28\pi}{7}$. Ainsi $\dfrac{-31\pi}{7} = \dfrac{-28\pi}{7}
            -\dfrac{-3\pi}{7} = \dfrac{-3\pi}{7} + 2k\pi$: la mesure principale
            est~$\dfrac{-3\pi}{7}$.
    \end{enumerate}
\end{example}

\section{Cosinus et sinus}

\subsection{Définition et propriétés}

\begin{definition}
    Soient $x$~un réel et $M$~son point associé sur le cercle
    trigonométrique~$\mcC$ (voir \vref{fig:cos-sin}).

    L’abscisse de M est appelée \define{cosinus} de~$x$, noté $\cos(x)$ ou
    $\cos x$.

    L’ordonnée de M est appelée \define{sinus} de~$x$, noté $\sin(x)$ ou
    $\sin x$.
\end{definition}

\begin{figure}
\ffigbox{}{
    \begin{subfloatrow}[2]
        \ffigbox{}{\caption{}\label{fig:cos-sin}%
            \TeacherMode
            \begin{tikzpicture}[scale=2.4]
                \draw (-1.1, 0) -- (1.1, 0);
                \draw (0, -1.1) -- (0, 1.1);
                \draw (0, 0) circle [radius=1];
                \path (0,0) coordinate[label=below right:$O$] (O);
                \node at (1,0) [below right] {$I$};
                \node at (0,1) [above left]  {$J$};
                \draw[->] (0,0) -- node[sloped,above,pos=1/2] {$\vect{u}$}
                        (35:1) coordinate[label=right:$M$] (M);
                \IfStudentF{
                    \draw[help lines] (M |- O) -- (M) -- (M -| O);
                    \node[below] at (M |- O) {$\cos x$};
                    \node[left]  at (M -| O) {$\sin x$};
                    % phantom
                    \begin{scope}[transparent]
                        \node[above right] at (90:1) {$\frac{\pi}{2}$};
                        \node[below right] at (-90:1) {$\frac{-\pi}{2}$};
                    \end{scope}
                }
            \end{tikzpicture}%
        }%
        \ffigbox{}{\caption{}\label{fig:cos-sin-values}%
            \TeacherMode
            \begin{tikzpicture}[scale=2.4]
                \path (0,0) coordinate[label=below right:$O$] (O);
                \begin{scope}[every label/.style={inner sep=0pt,ghost}]
                    \foreach \n/\d in {1/6,1/4,1/3,2/3,3/4,5/6} {
                        \ifnum\n=1\def\nx{}\else\let\nx\n\fi
                        \foreach \s in {,-} {
                            \path
                                ({\s180*\n/\d}:1) coordinate
                                [label={\s180*\n/\d}:$\frac{\s\nx\pi}{\d}$]
                                (M);
                            \draw[help lines] (M |- O) -- (M) -- (M -| O);
                        }
                    }
                \end{scope}
                \node[ghost, above left] at (180:1) {$\pi$};
                \node[ghost, above right] at (90:1) {$\frac{\pi}{2}$};
                \node[ghost, below right] at (-90:1) {$\frac{-\pi}{2}$};
                \draw (-1.1, 0) -- (1.1, 0);
                \draw (0, -1.1) -- (0, 1.1);
                \draw (0, 0) circle [radius=1];
                \node at (1,0) [below right] {$I$};
                \node at (0,1) [above left]  {$J$};
            \end{tikzpicture}%
        }%
    \end{subfloatrow}
    \caption{%
        \subref{fig:cos-sin}~Définition de $\cos x$~et~$\sin x$ avec le
        point~$M$ repéré par~$x$.
        \subref{fig:cos-sin-values}~Construction de valeurs remarquables pour
        $\cos$~et~$\sin$.
    }
}
\end{figure}

\begin{table}
\ttabbox{}{
    \caption{Valeurs remarquables de $\cos$~et~$\sin$}
    \label{tbl:cos-sin-values}
    \everymath{\displaystyle}
    \let\studentsize\Large
    \begin{tabular}{LlL*{7}{McL}}
    \firsthline
    Degrés & 0 & 30 & 45 & 60 & 90 & 120 & 270 \NL
    Radians & {0} & {\frac{\pi}{6}} & {\frac{\pi}{4}} &
              {\frac{\pi}{3}} & {\frac{\pi}{2}} &
              {\frac{2\pi}{3}} & {\frac{3\pi}{2}} \NL
    $\cos x$ & \,\GHOST{1}\, & \GHOST{\frac{\sqrt{3}}{2}} &
            \GHOST{\frac{\sqrt{2}}{2}} & \GHOST{\frac{1}{2}} &
            \quad\GHOST{0}\quad & \GHOST{-\frac{1}{2}} & \GHOST{0} \NL
    $\sin x$ & \GHOST{0} & \GHOST{\frac{1}{2}} &
            \GHOST{\frac{\sqrt{2}}{2}} & \GHOST{\frac{\sqrt{3}}{2}} & \GHOST{1}&
            \GHOST{\frac{\sqrt{3}}{2}} & \GHOST{-1} \\
    \lasthline
\end{tabular}
}
\end{table}


\begin{proposition}
    Pour tout~$x\in\mdR$,
    \begin{enumerate}
        \item $\UGHOST{-1} \le \cos x \le \UGHOST{1}$ et
            $\UGHOST{-1} \le \sin x \le \UGHOST{1}$;
        \item $\cos(x+2\pi) = \UGHOST{\cos x}$ et
            $\sin(x+2\pi) = \UGHOST{\sin x}$;
        \item $\cos^2 x + \sin^2 x = \UGHOST{1}$.
    \end{enumerate}
\end{proposition}

\begin{remark}
    Attention: la notation traditionnelle $\cos^2 x$ signifie $(\cos x)^2$, à ne
    pas confondre avec $\cos(x^2)$ !
\end{remark}

\iffalse

\subsection{Les fonctions \texorpdfstring{$\cos$~et~$\sin$}{cos et sin}}

À partir de ces valeurs, on peut tracer les fonctions
$\cos : x \mapsto \cos x$ et $\sin : x \mapsto \sin x$.
Voir \vref{fig:cos+sin-repr}.

\begin{figure}
    \caption{Représentation graphique des fonctions $\cos$~et~$\sin$.}
    \label{fig:cos+sin-repr}
    \leavevmode\hbox to 0pt{\hss
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={ticks={tick typesetter/.code=}},
                            x axis={ticks and grid={
                                step=(pi/4),minor steps between steps=2}},
                            y axis={label=$\cos x$, ticks and grid={
                                major at={-1,(-1/2*sqrt(3)),(-1/2*sqrt(2)),
                                    (-1/2),1,(1/2*sqrt(3)),(1/2*sqrt(2)),
                                    (1/2)}
                            }},
                            visualize as smooth line=cos,
                            %cos = {style={ghost}},
                        ]
        data [set=cos, format=function] {
            var x : interval[-4:8] samples 100;
            func y = cos(\value x r);
        }
        ;
    \end{tikzpicture}\hss}%
    \\[1cm]
    \leavevmode\hbox to 0pt{\hss
        \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={ticks={tick typesetter/.code=}},
                            x axis={ticks and grid={
                                step=(pi/4),minor steps between steps=2}},
                            y axis={label=$\sin x$, ticks and grid={
                                major at={-1,(-1/2*sqrt(3)),(-1/2*sqrt(2)),
                                    (-1/2),1,(1/2*sqrt(3)),(1/2*sqrt(2)),
                                    (1/2)}
                            }},
                            visualize as smooth line=sin,
                            %sin = {style={ghost}},
                        ]
        data [set=sin, format=function] {
            var x : interval[-4:8] samples 100;
            func y = sin(\value x r);
        }
        ;
    \end{tikzpicture}\hss}%
\end{figure}

\begin{remark}
    \begin{enumerate}
        \item On peut voir la périodicité des courbes.
        \item La courbe de~$\cos$ semble symétrique par rapport \UGHOST{à
            l'axe~$(Ox)$}: on dit que $\cos$~est \UGHOST{\emph{paire}}.
        \item La courbe de~$\sin$ semble symétrique par rapport \UGHOST{au
            point~$O$}: on dit que $\sin$~est \UGHOST{\emph{impaire}}.
    \end{enumerate}
\end{remark}

\fi


\section{Angles associés}

\begin{figure}
\ffigbox{}{
    \begin{tikzpicture}[scale=3]
        \draw (-1.2, 0) -- (1.2, 0);
        \draw (0, -1.2) -- (0, 1.2);
        \draw (0, 0) circle [radius=1];
        \node at (0,0) [below right]  {$O$};
        \node at (1,0) [below right] {$I$};
        \node at (0,1) [above left]  {$J$};
    \end{tikzpicture}%
    \caption{%
        Cosinus et sinus des angles associés.
    }
    \label{fig:assoc}
}
\end{figure}

\begin{proposition}[Angles opposés et supplémentaires]
    \leavevmode\\
    \UGHOST{$\cos\left( \pi + x \right)= -\cos x$ \pause et
        $\sin\left( \pi + x \right) = -\sin x$} \\
    \UGHOST{$\cos\left( -x \right)= \cos x$ \pause et
        $\sin\left( -x \right) = -\sin x$} \\
    \UGHOST{$\cos\left( \pi - x \right)= -\cos x$ \pause et
        $\sin\left( \pi - x \right) = \sin x$}
\end{proposition}

\begin{proposition}[Angles complémentaires]
    \leavevmode\\
    \UGHOST{$\cos\left( \frac{\pi}{2}-x \right)= \sin x$ \pause et
        $\sin\left( \frac{\pi}{2} -x \right) = \cos x$} \\
    \UGHOST{$\cos\left( \frac{\pi}{2}+x \right)= -\sin x$ \pause et
        $\sin\left( \frac{\pi}{2} +x \right) = \cos x$}
\end{proposition}

\begin{example}
  On sait que $\cos\dfrac{\pi{}}{3}=\dfrac{1}{2}$. 
  En déduire $\cos\left(-\dfrac{\pi{}}{3}\right)$ et
  $\cos\dfrac{4\pi{}}{3}$.
\end{example}

\section{(In)équations trigonométriques}

\subsection{Équations \texorpdfstring{$\cos x=a$}{cos x = a} ou
\texorpdfstring{$\sin x=a$}{sin x = a}}

\begin{method}[Résoudre $\cos x=a$ avec $x\in{}\mdR$]
    Cela revient à chercher quels réels repèrent les points du cercle dont
    l'abscisse est égale à $a$.
    \begin{enumerate}
        \item Pour résoudre l'équation $\cos x=a$ pour $x\in{}\mdR$, on résout
            d'abord cette équation dans $\IN]-\pi;\pi;]$. Dans le cas général,
            $a\in\IN]-1;1;[$.

            Il existe un unique nombre $b$ dans~$\IN]0;\pi;[$ tel que
            $a = \cos b$. L'équation est donc équivalente à $\cos x = \cos b$
            dont les solutions sont $b$ et $-b$. Une valeur approchée de $b$
            peut être obtenue à l'aide de la calculatrice.
        \item L'ensemble des solutions dans $\mdR$ est obtenu en soustrayant ou
            en ajoutant un nombre entier de fois $2\pi$ :
            $b+k\times2\pi$ et $-b+k\times 2\pi$, $k\in\mdZ$.
\end{enumerate}
\end{method}

\begin{example}
    Résoudre dans $\IN]-\pi;\pi;]$ puis dans $\mdR$ l'équation
    $\cos x=\dfrac{1}{2}$.
\end{example}

\begin{method}[Résoudre $\sin x=a$ avec $x\in{}\mdR$]
    Cela revient à chercher quels réels repèrent les points du cercle dont
    l'abscisse est égale à $a$.
    \begin{enumerate}
        \item Pour résoudre l'équation $\cos x=a$ pour $x\in{}\mdR$, on résout
            d'abord cette équation dans $\IN]-\pi;\pi;]$. Dans le cas général,
            $a\in\IN]-1;1;[$.

            Il existe un unique nombre $b$
            dans~$\IN]-\dfrac{\pi}{2};\dfrac{\pi}{2};[$ tel que $a = \sin b$.
            L'équation est donc équivalente à $\cos x = \cos b$
            dont les solutions sont $b$ et $\pi-b$.
        \item L'ensemble des solutions dans $\mdR$ est obtenu en soustrayant ou
            en ajoutant un nombre entier de fois $2\pi$ :
            $b+k\times2\pi$ et $\pi-b+k\times 2\pi$, $k\in\mdZ$.
\end{enumerate}
\end{method}

\begin{example}
    Résoudre l'équation $\sin x=\dfrac{\sqrt{2}}{2}$ dans~$\mdR$.
\end{example}

\end{document}

\subsection{Inéquation du type \texorpdfstring{$\cos x\ge a$}{cos x ≥ a}
    ou \texorpdfstring{$\sin x \ge a$}{sin x ≥ a}}

\begin{example}
  Résoudre l'inéquation  $\cos x>\dfrac{\sqrt{3}}{2}$ dans 
  $\left]-\pi \ ;\ \pi \right]$. 
\end{example}

\begin{remark}
    \begin{enumerate}
        \item Des cas particuliers peuvent se présenter, il faut faire attention
            à traduire $\cos x\ge a$ (resp. $\sin x\ge a$) par~: on cherche les
            nombres réels qui repèrent un point sur le cercle dont l'abscisse
            (resp. ordonnée) est supérieure ou égale à~$a$.
        \item L'intervalle de résolution $I$ n'est pas toujours
            $\IN]-\pi;\pi;]$. L'intervalle $\IN[0;2\pi;[$ est un autre
            intervalle possible pour décrire le cercle trigonométrique.
    \end{enumerate}
\end{remark}




