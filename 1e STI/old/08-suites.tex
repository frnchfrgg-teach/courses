% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\def\genericform#1{<\textsl{#1}>}

\begin{document}

\StudentMode

\chapter{Suites numériques}

\chapterquote{Ne commence rien dont tu puisses te repentir dans la
suite.}{Pythagore}

\section{Activités}

\begin{activity}
    Durant l'été, la rédaction d'un hebdomadaire décide d'organiser un
    jeu-concours sur la base d'énigmes « Quel est le nombre qui suit ? ». Les
    lecteurs ayant répondu correctement un maximum de semaines d'affilée seront
    départagés pour gagner un voyage.
    \begin{itemize}
        \item La première semaine, l'énigme est: $-5$; $-3$; $-1$; $1$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item La deuxième semaine, l'énigme est: $2$; $-4$; $8$; $-16$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En troisième semaine: $0$; $1$; $4$; $9$; $16$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En quatrième semaine: $24$; $6$; $\frac{3}{2}$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En cinquième semaine: $1$; $2$; $5$; $14$; $41$; $122$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En sixième semaine: $768$; $384$; $192$; $96$; $48$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En septième semaine: $1$; $2$; $3$; $5$; $8$; $13$; $21$; $34$;
            \ldots
            \IfStudentT{\bigfiller{1}}
    \end{itemize}

    \begin{enumerate}
        \item Trouver chaque nombre manquant en expliquant votre démarche.
    \end{enumerate}

    Une telle liste de nombres est appelée \emph{suite}, et les nombres qui la
    composent sont appelés \emph{termes} de la suite.

    \begin{enumerate}[resume]
        \item Quel serait le dixième terme de chacune des suites précédentes ?
    \end{enumerate}

    À chaque terme de la suite on attribue un rang, appelé \emph{indice}; le
    terme d'indice~$5$ de la suite~$u$ est noté $u_5$. L'indice augmente d'une
    unité à chaque terme, mais le premier terme n'a pas forcément l'indice~$1$.

    \begin{enumerate}[resume]
        \item La suite de la première semaine est notée~$t$, et son premier
            terme est~$t_1$. Quel indice a le $5$\ieme\ terme ?
            \IfStudentT{\bigfiller{1}}
        \item La suite de la deuxième semaine est notée~$u$, de premier
            terme~$u_0=2$. Quel est le $6$\ieme\ terme ? Quelle est la valeur
            de~$u_6$ ?
            \IfStudentT{\bigfiller{1}}
        \item En notant~$v$ la troisième suite, pouvez-vous trouver une formule
            en fonction de~$n$ qui permette de calculer~$v_n$ ? On supposera que
            le premier terme de~$v$ est $v_0$. Et si ça avait été~$v_1$ ?
            \IfStudentT{\bigfiller{2}}
        \item Déterminer une formule permettant de calculer~$t_2$ à partir
            de~$t_1$, puis $t_3$ à~partir de~$t_2$. Écrire une formule
            mathématique qui décrit comment passer d'un terme de~$t$ au suivant.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}

    Une telle formule est appelée \emph{formule de récurrence}.

    \begin{enumerate}[resume]
        \item Déterminer une formule de récurrence pour~$u$, puis pour~$w$ la
            septième suite.
            \IfStudentT{\bigfiller{2}}
        \item Pouvez-vous trouver une formule de récurrence pour~$v$ ?
            \IfStudentT{\bigfiller{1}}
        \item Cherchez une formule \emph{en fonction de~$n$} pour~$t_n$, puis
            pour~$u_n$.
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\begin{activity}
    Une équipe de chercheurs étudie l'évolution au cours du temps d'une
    population de fourmis à l'intérieur d'une fourmilière. Elle estime que,
    chaque mois, la population s'accroit naturellement de \SI{5}{\percent} et
    qu'en moyenne \<100>~fourmis ne reviennent pas à la fourmilière. Le 1\ier\
    janvier 2010, la population est estimée à~\<4000>. La population de fourmis
    peut-elle tripler ? Si oui, en combien de mois ?
    %\IfStudentT{\bigfiller{8}}
\end{activity}


\section{Modes de génération d'une suite}

\subsection{Définition}

\begin{definition}
    Une \define{suite}~$u$ est une \emph{fonction} définie sur les nombres
    entiers --- généralement sur~$\mdN$, mais aussi sur $\mdN\setminus\left\{
    0 \right\}$ quand le premier terme est~$u_1$.

    Les entiers de l'ensemble de définition sont appelés \define{indices};
    l'image d'un indice~$n$ est notée $u_n$ et est appelée \define{terme}
    d'indice~$n$.
\end{definition}

Pour le moment, les suites ne sont qu'un changement de vocabulaire: voir
\cref{tab:vocab-match}.

\begin{table}
    \centering
    \begin{tabular}{cc}
        \hline
        Fonctions    & Suites \\
        \hline
        $x$          & $n$ \\
        «antécédent» & indice \\
        $f(x)$       & $u_n$ \\
        image de~$x$ & terme d'indice~$n$ \\
        \hline
    \end{tabular}
    \caption{Correspondance entre vocabulaire et notations des fonctions et des
    suites}
    \label{tab:vocab-match}
\end{table}

\subsection{Suites définies par leur terme général}

On peut définir une suite~$u$ par une formule explicite qui dépend uniquement
de~$n$: $\forall x \in \mdN, u_n = f(n)$ avec $f$~une fonction.

\begin{definition}
    Une telle formule de la forme $u_n = \text{\genericform{formule avec~$n$}}$
    s'appelle \define{terme général} de~$u$.
\end{definition}

\begin{remark}
    Lorsqu'une suite est définie par son terme général, on peut calculer
    directement n'importe quel terme dont on connait l'indice.
\end{remark}

\begin{example}
    \label{ex:def-generic}
    Soit $u$~définie par~$\forall n \in \mdN, u_n = n^2 + \frac{1}{n + 1}$.
    \begin{enumerate}
        \item Quel est le premier terme de~$u$ ?
            \IfStudentT{\hfiller}
        \item Calculer $u_0$, $u_1$, $u_2$ et~$u_{1000}$.
            \IfStudentT{\bigfiller{2}}
        \item Calculer le $1000$-ème terme de~$u$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{exercise}[Premiers termes par terme général]
    26~et~27 p.~127
\end{exercise}

\subsection{Suites définies par (une relation de) récurrence}

Contrairement aux fonctions, pour lesquelles seule la méthode précédente existe,
on peut définir une suite~$u$ par la donnée:
\begin{itemize}
    \item \UGHOST{d'un ou plusieurs termes initiaux (les premiers termes);}
    \item \UGHOST{d'une formule définissant un terme quelconque en fonction des
        précédents.}
\end{itemize}

\begin{definition}
    Une telle formule de la forme $u_n = \text{\genericform{formule avec~$n$,
    $u_{n-1}$\ldots}}$ ou $u_{n+1} = \text{\genericform{formule avec~$n$,
    $u_n$\ldots}}$ est appelée \define{relation de récurrence} de~$u$.
\end{definition}

\begin{remark}
    Lorsqu'une suite est définie par récurrence, on ne peut pas calculer
    directement n'importe quel terme dont on connait l'indice: pour calculer
    $u_{100}$ \UGHOST{on a besoin de~$u_{99}$, donc de $u_{98}$, donc $u_{97}$,
    etc.}
\end{remark}

\begin{remark}
    Ce n'est pas la présence de $u_n = \dots$ qui indique un terme général: en
    effet, $u_n = u_{n-1} + 2n$ est une relation de récurrence. Une formule est
    une relation de récurrence dès que plusieurs termes de la suite sont
    présents dans la formule.

    \UGHOST{\textsl{Si le membre de droite ne contient pas de
    terme de la forme $u_{n\pm k}$, c'est une définition par terme général,
    sinon c'est une relation de récurrence.}}
\end{remark}

\begin{example}
    Soit $v$ définie par $v_1 = 1$ et $\forall n \ge 1, v_{n+1} = v_n \times 2$.
    \begin{enumerate}
        \item Calculer $v_1$, $v_2$, et~$v_3$.
            \IfStudentT{\bigfiller{2}}
        \item Calculer le quatrième terme de~$v$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{example}
    \label{ex:def-recursion}
    Soit $w$ définie par $w_0 = 0$ et $\forall n \in \mdN,
        w_{n+1} = w_n + n + 1$.
    \begin{enumerate}
        \item Calculer $w_1$, $w_2$, et~$w_3$.
            \IfStudentT{\bigfiller{2}}
        \item Calculer le quatrième terme de~$w$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{exercise}[Premiers termes par récurrence]
    28~et~29 p.~127
\end{exercise}

\subsection{Suites définies par un algorithme}

On peut définir une suite~$u$ par un \emph{algorithme} qui demande en entrée la
valeur de~$n$ et renvoie en sortie la valeur de~$u_n$.

\begin{remark}
    Cette méthode de génération de~$u$ recouvre les méthodes précédentes: un
    algorithme peut en effet facilement calculer~$u_n$ avec son terme général,
    et si $u$~est définie par récurrence, il suffit de faire une boucle qui
    calcule $u_0$, puis~$u_1$, puis~$u_2$, etc. jusqu'à $u_n$.
\end{remark}

\begin{example}
    Écrire un algorithme calculant les termes de la suite de
    \cref{ex:def-generic}. Au début de l'algorithme, la variable $n$ contient
    l'indice du terme à calculer, et à la fin de l'algorithme la variable $u$
    doit contenir la valeur du terme.
    \IfStudentT{\bigfiller{5}}
\end{example}

\begin{example}
    Écrire un algorithme calculant le terme d'indice~$n$ de la suite de
    \cref{ex:def-recursion}, la variable~$n$ contenant l'indice au début de
    l'algorithme et la variable~$w$ devant contenir la valeur correspondante à
    la fin.
    \IfStudentT{\bigfiller{8}}
\end{example}


\subsection{Représentation graphique}

\begin{recall}
    Si $f$~est une fonction, $\mcC_f$ est l'ensemble des points~$M(x;y)$ tels
    que $x\in\mcD_f$ et $y=f(x)$.
\end{recall}

\begin{definition}
    Si $u$~est une suite définie pour $n\ge n_0$, on représente~$u$ par le
    \define{nuage de points}~$\mcN_u$ constitué des points~$M(n;u_n)$ pour tous
    les entiers~$n\ge n_0$.
\end{definition}

\begin{example}
    Soit $u$~définie sur $\mdN\setminus\SET{0}$ par $\forall n \ge 1, u_n =
    \frac{16}{2^n}$.

    \StudentOnly
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            x axis={ticks and grid={step=1},
                                    unit length=0.8cm},
                            y axis={ticks and grid={step=1,
                                    minor steps between steps=1},
                                    unit length=0.8cm},
                            visualize as scatter=u,
                            u={style={ghost}},
                        ]
        data [set=u, format=function] {
            var x : {1, ..., 9};
            func y = 16 / pow(2, \value x);
        }
        ;
    \end{tikzpicture}%
    \EndStudentOnly
\end{example}

\end{document}

\begin{exercise}[Dessus ou pas, et chercher le(s) point(s) d'ordonnée~$2015$]
    36 p.~127
\end{exercise}

\section{Suites arithmétiques}

\subsection{Définition par récurrence}

\begin{definition}
    On dit qu'une suite $u = (u_n)$ est \define{arithmétique} s'il existe un
    nombre réel~$r$ tel que pour tout entier naturel~$n$ on a $u_{n+1} = u_n +
    r$.

    Le réel $r$~est appelé \define{raison} de la suite arithmétique~$u$.
\end{definition}

\begin{remark}
  Cette définition utilise une relation de récurrence. Une suite est
  arithmétique si l'on ajoute toujours le même nombre pour passer d'un terme au
  suivant.
\end{remark}

\begin{remark}
  Pour définir complètement une suite arithmétique, il faut donner sa raison,
  mais aussi son premier terme.
\end{remark}

\begin{proposition}
    $u = (u_n)$ est arithmétique de raison~$r$ si et seulement si $\forall n,
    u_{n+1} - u_n = r$. Autrement dit, si la différence entre deux termes
    consécutifs est \GHOST{constante (ne dépend pas de~$n$)}.
\end{proposition}

\begin{remark}
    La méthode va changer selon que l'on veut montrer que la suite est
    arithmétique ou qu'elle ne l'est pas.
\end{remark}

\begin{example}
    Indiquer si les suites ci-dessous sont arithmétiques ou non, et si oui
    déterminer leur raison:
    \begin{enumerate}
        \item $t_n = n^2$\\
            Méthode: calculer $t_1 - t_0$ puis $t_2 - t_1$, et conclure.
            \bigfiller{2}
        \item $u_n = 2 + 3n$\\
            Méthode: déterminer une formule pour $t_{n+1}$ puis pour
            $t_{n+1} - t_n$. Conclure.
            \bigfiller{3}
        \item $\left\{
                \begin{aligned}
                    &v_0 = 0\\
                    &v_{n + 1} = v_n - 5
                \end{aligned} \right.$\\
            Méthode: déterminer $v_{n+1} - v_n$ et conclure.
            \bigfiller{1}
        \item $\left\{
                \begin{aligned}
                    &w_0 = 0\\
                    &w_{n + 1} = w_n + n + 1
                \end{aligned} \right.$\\
            Méthode: déterminer quelques termes, puis quelques différences, et
            conclure.
            \bigfiller{3}
    \end{enumerate}
\end{example}

\subsection{Terme général d'une suite arithmétique}

\begin{remark}
    On parle aussi de \emph{forme explicite}.
\end{remark}

\begin{theorem}
    Si $u = (u_n)$ est une suite arithmétique de premier terme $u_0$ et de
    raison~$r$ alors $\forall n \in \mdN, u_n = u_0 + n \times r$.
\end{theorem}

\begin{proof}
    Par définition, on sait que pour tout entier naturel $n$,
    on a ${u_{n+1}} = {u_n} + r$. 

    Donc $\left\{ \begin{array}{c}
        {u_1} = {u_0} + r\\
        {u_2} = {u_1} + r\\
        {u_3} = {u_2} + r\\
        ...\\
        {u_n} = {u_{n - 1}} + r
      \end{array} \right.$.

    On ajoute alors membre \`{a} membre et on obtient :
    \[ 
    {u_1} + {u_2} + ... + {u_{n - 1}} + {u_n} = {u_0} + {u_1} + ... +
    {u_{n - 1}} + nr
    \]

    On retranche ${u_1} + {u_2} +
    ... + {u_{n - 1}}$ de chaque côté et on obtient ${u_n} = {u_0} + nr$.
\end{proof}

\begin{corollary}
    Si $u = (u_n)$ est une suite arithmétique de
    raison~$r$ alors pour tous entiers $p$~et~$q$, $u_q = u_p + (q-p) \times r$.
\end{corollary}

\begin{proof}
    D'après le théorème précédent, $u_q = u_0 + q r$. De même, $u_p = u_0 +
    pr$. Mais alors $u_0 = u_p - pr$ et ainsi $u_q = u_p - pr + qr$. En
    factorisant par~$r$ on obtient le résultat annoncé.
\end{proof}

\begin{example}
    Dans les cas suivants, déterminer une forme explicite de la suite, puis
    calculer son terme d'indice~$100$.
    \begin{enumerate}
        \item $\left\{\begin{aligned}
                &u_0 = 11\\
                &u_{n + 1} = u_n - 3
            \end{aligned} \right.$
            \hfiller
            \bigfiller{3}
        \item $\left\{ \begin{aligned}
                &v_1 = -30\\
                &v_{n + 1} = v_n + 5
            \end{aligned} \right.$
            \hfiller
            \bigfiller{3}
    \end{enumerate}
\end{example}

\subsection{Somme des termes d'une suite arithmétique}

\begin{proposition}
    La somme des entiers naturels de $1$~à~$n$ inclus vaut $\dfrac{n(n+1)}{2}$
\end{proposition}

\begin{proof}
  On peut exprimer la somme des $n$ premiers entiers naturels de deux
  mani\`{e}res :

  $\begin{aligned}[t]
    S = 1 &+ 2 &+ \cdots &+ n\\
    S = n &+ (n - 1) &+ \cdots &+ 1 \\\hline
    2S = (n + 1) &+ (n + 1) &+ \cdots &+ (n + 1)
  \end{aligned}$ On  additionne les deux égalités.

  Puis on obtient $2S = n\left(n + 1\right)$ d'o\`{u} le résultat
  $S = \dfrac{n\left( n + 1 \right)}{2}$.
\end{proof}

\begin{remark}[Notation]
  La somme de $k = 0$ \`{a} $n$ des ${u_k}$ se note
  \[ S = \sum_{k = 0}^n u_k = {u_0} + {u_1} + \dots + {u_n} \]

  Ainsi, la propriété précédente peut s'écrire
  $\sum_{k = 0}^n k = \frac{{n(n + 1)}}{2} $.
\end{remark}

\end{document}


