% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}


\rivmathlib{geometry}

\begin{document}

\chapter{Fonctions de référence}

\chapterquote{Une citation sans références est à peu près aussi utile qu'une
horloge sans aiguilles.}{Paul Desalmand}

\section{Les basiques}

\subsection{Fonctions affines}

\begin{definition}
    On appelle \define{fonction affine} toute fonction pouvant s'écrire sous la
    forme $f(x) = mx+p$ avec $m$~et~$p$ deux réels.
\end{definition}

\begin{example}
    Les fonctions suivantes sont-elles affines ?
    \begin{enumerate}[gathered,label={$\falph*(x)={}$},labelsep=0pt]
        \item $2x+3$;
        \item $-3-3x$;
        \item $x^2 + 1$;
        \item $-\sqrt{2}x-1$;
        \item $-\sqrt{2x}-1$;
        \item $\dfrac{1-x}{2}$.
    \end{enumerate}
\end{example}

\begin{example}[Recherche de formule]
    Soit $f$~une fonction affine telle que $f(8) = 11$~et $f(10) = 10$.
    Déterminer l'expression de~$f$.
\end{example}

\begin{proposition}
    Soit $f:x\mapsto mx+p$. Alors:
    \begin{sidebyside}{2}
    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1,$f(x)$/2}
                        {{$\;-\infty$},$\frac{-p}{m}$,$+\infty\;$}
            \tkzTabLine{,-,z,+,}
            \tkzTabVar {-/,R,+/}
        \end{scope}
        \node[above] at (current bounding box.north) {si $m>0$};
    \end{tikzpicture}

    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1,$f(x)$/2}
                        {{$\;-\infty$},$\frac{-p}{m}$,$+\infty\;$}
            \tkzTabLine{,+,z,-,}
            \tkzTabVar {+/,R,-/}
        \end{scope}
        \node[above] at (current bounding box.north) {si $m<0$};
    \end{tikzpicture}
    \end{sidebyside}
\end{proposition}

\subsection{Fonction inverse}

\begin{figure}
    \caption{Représentation graphique de la fonction inverse.}
    \label{fig:inv-repr}
    \begin{tikzpicture}
        \def\xmax{8}\def\ymax{6.1}
        \datavisualization [school book axes,
                            all axes={grid={step=1},
                                      ticks={major at/.list={0,1}},
                                      unit length=0.45cm},
                            visualize as smooth line=inv,
                        ]
        data [set=inv, format=function] {
            var x : interval[-\xmax:-1];
            func y = 1 / (\value x);
        }
        data [set=inv, format=function] {
            var x : interval[-1:(-1/\ymax)];
            func y = 1 / (\value x);
        }
        data point[set=inv, outlier=true]
        data [set=inv, format=function] {
            var x : interval[(1/\ymax):1];
            func y = 1 / (\value x);
        }
        data [set=inv, format=function] {
            var x : interval[1:\xmax];
            func y = 1 / (\value x);
        }
        ;
    \end{tikzpicture}%
\end{figure}

\begin{proposition}
    Soit $f:x\mapsto \dfrac{1}{x}$. On a le tableau suivant (voir
    aussi \vref{fig:inv-repr}):
    \begin{center}
        \begin{tikzpicture}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1,$f(x)$/2}
                        {$\;-\infty$,$0$,$+\infty\;$}
            \tkzTabLine{,-,d,+,}
            \tkzTabVar {+/,-D+//,-/}
        \end{tikzpicture}
    \end{center}
\end{proposition}

\begin{remark}
    Howard dit « Si~$a < b$ alors~$\dfrac{1}{a} > \dfrac{1}{b}$ ».
    Sheldon répond « C'est faux ! ».
    Qui a raison ? Quelle partie du raisonnement tombe à l'eau ?

    Conclusion, danger: on ne peut pas dire que la fonction inverse est
    décroissante sur $\IN]-\infty;0;[ \cup \IN]0;+\infty;[$, mais seulement sur
    $\IN]-\infty;0;[$~\emph{et}~$\IN]0;+\infty;[$ \emph{séparément}.
\end{remark}

\begin{example}
    \begin{enumerate}
        \item Comparer des inverses
        \item Encadrement $x$ $\implies$ encadrement $\frac{1}{x}$.
    \end{enumerate}
\end{example}


\section{Fonction valeur absolue}

\subsection{Définition}


\begin{definition}
    On appelle \define{fonction valeur absolue} la fonction définie sur~$\mdR$
    par $\abs{x} = \sqrt{x^2}$.
\end{definition}

\begin{figure}
    \caption{Représentation graphique de la valeur absolue.}
    \label{fig:abs-repr}
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={grid={step=1},
                                      ticks={major at/.list={0,1}},
                                      unit length=0.45cm},
                            visualize as line=abs,
                        ]
        data [set=abs, format=function] {
            var x : interval[-8:8] samples 3;
            func y = abs(\value x);
        }
        ;
    \end{tikzpicture}%
\end{figure}


\begin{remark}
    Dans un repère orthonormé, on prend $A(x;0)$. Comment calcule-t-on la
    distance~$OA$ ? Ainsi, $\abs{x}$~est la distance entre $x$~et~$0$ quand ils
    sont représentés sur un axe gradué. Autrement dit c'est la valeur de~$x$ en
    ne tenant pas compte de son signe.
\end{remark}

\begin{proposition}
    \label{prop:sqrt-expr}
    Soit~$x\in\mdR$. Alors $\abs{x} = \begin{cases}
                                         x & \text{si $x\ge 0$} \\
                                        -x & \text{si $x\le 0$}
                                \end{cases}$
\end{proposition}

\begin{example}
    Soit $f:x\mapsto\abs{x}$. Calculer $f(0)$,~$f(10)$, $f(5)$, $f(2)$,
    $f(-2)$, $f(-10)$ et~$f(-42)$.
\end{example}

\subsection{Propriétés}

\begin{proposition}
    Pour~tout~$x\in\mdR$, $\abs{x} \ge 0$. Mieux, $x \ne 0 \iff \abs{x} \ne
    0$.
\end{proposition}

\begin{proof}
    C'est évident en utilisant l'expression donnée à \vref{prop:sqrt-expr}.
\end{proof}

\begin{proposition}
    On a le tableau suivant:
    \begin{tikzpicture}[TAB]
        \tkzTabInit [lgt=1,espcl=2]
                    {$x$/1,$\abs{x}$/2}
                    {$\;-\infty$,$0$,$+\infty\;$}
        \tkzTabVar {+/,-/$0$,+/}
    \end{tikzpicture}
\end{proposition}

\begin{exercise}[Variations avec $\abs{\cdot}$]
    85~p.~38 questions 3~et~4; 86~p.~38 questions 3~et~4.
\end{exercise}


\section{Fonctions associées}

\begin{TP}[Avec \bsc{GeoGebra}]
    Soit~$f$ la fonction définie sur~$\mdR$ par $f(x) = \frac{1}{4}x^2 + x -
    8$.

    \begin{enumerate}
        \item \begin{enumerate}
            \item Construire la fonction~$f$ dans \bsc{GeoGebra}.
            \item Dresser le tableau de variations de~$f$.
        \end{enumerate}
        \item On définit la fonction~$g$ par $g(x) = f(x) + 3$.
            \begin{enumerate}
                \item Construire la courbe de~$g$ dans \bsc{GeoGebra}.
                \item Quelle transformation géométrique permet de passer de
                    $\mcC_f$~à~$\mcC_g$ ? Vérifier en construisant cette
                    transformation dans \bsc{GeoGebra}.
                \item Dresser le tableau de variations de~$g$. Que
                    remarque-t-on?
                \item Refaire la procédure avec $x\mapsto f(x) + a$ où $a$~est
                    un curseur de \bsc{GeoGebra}.
            \end{enumerate}
        \item On définit la fonction~$h$ par $h(x) = f(x+3)$.
            \begin{enumerate}
                \item Construire la courbe de~$h$ dans \bsc{GeoGebra}.
                \item Quelle transformation géométrique permet de passer de
                    $\mcC_f$~à~$\mcC_h$ ? Vérifier en construisant cette
                    transformation dans \bsc{GeoGebra}.
                \item Calculer une formule de~$h$ et vérifier sa cohérence en
                    traçant cette courbe dans \bsc{GeoGebra} pour tester si elle
                    se superpose avec la courbe de~$h$.
                \item Dresser le tableau de variations de~$h$. Que
                    remarque-t-on?
                \item Refaire la procédure avec $x\mapsto f(x - b)$ où $b$~est
                    un curseur de \bsc{GeoGebra}.
            \end{enumerate}
        \item On définit la fonction~$i$ par $i(x) = 2 \times f(x)$.
            \begin{enumerate}
                \item Construire la courbe de~$i$ dans \bsc{GeoGebra}.
                \item Dresser le tableau de variations de~$i$. Que
                    remarque-t-on?
                \item Refaire la procédure avec $x\mapsto c \times f(x)$ où
                    $c$~est un curseur de \bsc{GeoGebra}.
            \end{enumerate}
        \item On définit la fonction~$j$ par $j(x) = \dfrac{1}{f(x)}$.
            \begin{enumerate}
                \item Quel est l'ensemble de définition de~$j$ ?
                \item Construire la courbe de~$j$ dans \bsc{GeoGebra}.
                \item Dresser le tableau de variations de~$j$. Que
                    remarque-t-on?
            \end{enumerate}
        \item On définit la fonction~$k$ par $k(x) = \sqrt{f(x)}$.\\
            Répondre à la question~5 en remplaçant $j$~par~$k$.
        \item On définit la fonction~$l$ par $l(x) = \abs{f(x)}$.
            \begin{enumerate}
                \item Construire la courbe de~$l$ dans \bsc{GeoGebra}.
                \item Quelle transformation géométrique permet de passer de
                    $\mcC_f$~à~$\mcC_g$ sur~$\IN[-5;8;]$ ? Partout ailleurs ?
            \end{enumerate}
    \end{enumerate}
\end{TP}

\begin{proposition}
    Soit $f$~une fonction définie sur un intervalle~$I$.
    \begin{enumerate}
        \item Pour tout~$k\in\mdR$, $f+k:x\mapsto f(x)+k$ a le même sens de
            variation que~$f$.
        \item Pour tout~$\alpha > 0$, $\alpha f : x\mapsto \alpha f(x)$ a le
            même sens de variation que~$f$.\\
            Pour tout~$\alpha < 0$, $\alpha f$ varie en sens contraire de~$f$.
        \item Si $f \ge 0$ sur~$I$, c'est-à-dire $f(x) \ge 0$ pour tout~$x\in
            I$, alors $\sqrt{f}: x \mapsto \sqrt{f(x)}$ a le même sens de
            variation que~$f$.
        \item Si $f$~est de signe constant et ne s'annule pas sur~$I$, alors
            $\dfrac{1}{f}: x \mapsto \dfrac{1}{f(x)}$ varie en sens contraire
            de~$f$.
    \end{enumerate}
\end{proposition}

\end{document}
