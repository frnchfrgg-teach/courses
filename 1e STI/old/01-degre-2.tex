% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{datavisualization,
                datavisualization.formats.functions}
\usepackage{tkz-tab}

\begin{document}
\StudentMode

\chapter{Second degré}

\chapterquote{C'est très difficile de jongler avec le second degré dans un monde
où tout est pris au premier degré, aussi bien en dessin qu'en texte.}{Charb}

\section{Formes d'un polynôme du second degré}

\subsection{Polynôme du second degré}

\begin{definition}
Soit~$P$~une fonction. On dit que $P$~est une \define{fonction polynomiale de
degré~$2$} si on peut la mettre sous la forme~$P(x) = ax^2 + bx + c$ où
$a$,~$b$, et~$c$ sont des nombres réels ($a\neq 0$). Cette forme est appelée
forme \emph{développée, réduite et ordonnée} de~$P$.
\end{definition}

\begin{example}
    Les fonctions suivantes sont-elles polynomiales du second degré ?
    \begin{enumerate}[gathered, label={$\Alph*(x)={}$},labelsep=0pt,start=16,
                       centered]
        \item $6x^2-5x+3$
        \item $2 - 3x^2$
        \item $x + 7$
        \item $\sqrt2 x + \sqrt3 x^2 - 1$
        \item $\sqrt{2x} + \sqrt3 x^2 - 1$
        \item $(x+3)(1-x)$
    \end{enumerate}
    \IfStudentT{\bigfiller{4}}
\end{example}

Toute écriture de~$P(x)$ sous forme de produit de deux facteurs du premier degré
est appelée forme \emph{factorisée} de~$P$.

\begin{example}
    Soit $P(x) = x^2 + 6x + 5$.
    \begin{enumerate}
        \item Montrer que $P(x) = (x+3)^2 - 4$.
            \IfStudentT{\bigfiller{1}}
        \item Montrer que $P(x) = (x+1)(x+5)$.
            \IfStudentT{\bigfiller{1}}
        \item Laquelle des trois formes est la plus adaptée pour résoudre
            $P(x) = 0$ ?
            \IfStudentT{\bigfiller{1}}
        \item Et pour résoudre $P(x) = -4$ ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\subsection{Forme canonique}

\begin{definition}
Soit~$P$~une fonction polynomiale de degré~$2$. On appelle \define{forme
canonique} de~$P$ toute écriture de~$P(x)$ dans laquelle la variable~$x$
n'apparait qu'une seule fois. On utilise souvent la forme:
\[
\GHOST{P(x) = a(x-\alpha)^2 + \beta \text{ avec $\beta = P(\alpha)$}}
\]
\end{definition}

\begin{example}
    Soit $P:x \mapsto x^2 + 6x + 5$.\\
    Une forme canonique de $P$ est \UGHOST{$P(x) = (x+3)^2 - 4$.}
\end{example}

\begin{theorem}
    \label{thm:canonical-form}
    Soit $P:x\mapsto ax^2 + bx + c$. Une forme canonique de~$P$ est:
    \[ P(x) = a\left[ \left( x+\frac{b}{2a} \right)^2 -
        \frac{b^2-4ac}{4a^2} \right] \]
\end{theorem}

\begin{proof}
    Développer le membre de droite.
\end{proof}

\begin{remark}
    C'est aussi dire~$\alpha = -\dfrac{b}{2a}$ et $\beta = P(\alpha) =
    -\dfrac{b^2-4ac}{4a}$.
\end{remark}

\subsection{Variations d'un polynôme du second degré}

\begin{proposition}
    Soit~$P:x\mapsto ax^2+bx+c$ un polynôme du second degré. Alors:
    \begin{enumerate}[gathered, label=]
        \item \begin{tikzpicture}
                \begin{scope}[TAB]
                    \tkzTabInit [lgt=1.5,espcl=2.5]
                                {$x$/1.2,$P(x)$/2.2}
                                {$\;-\infty$,$\GHOST{\frac{-b}{2a}}$,
                                $+\infty\;$}
                    \IfStudentF{\tkzTabVar {+/,-/$\beta$,+/}}
                \end{scope}
                \node[above] at (current bounding box.north) {si $a>0$};
            \end{tikzpicture}
        \item \begin{tikzpicture}
                \begin{scope}[TAB]
                    \tkzTabInit [lgt=1.5,espcl=2.5]
                                {$x$/1.2,$P(x)$/2.2}
                                {$\;-\infty$,$\GHOST{\frac{-b}{2a}}$,
                                $+\infty\;$}
                    \IfStudentF{\tkzTabVar {-/,+/$\beta$,-/}}
                \end{scope}
                \node[above] at (current bounding box.north) {si $a<0$};
            \end{tikzpicture}
    \end{enumerate}

    Ces résultats sont illustrés par \vref{fig:parabole}.
\end{proposition}

\begin{figure}
\def\parab#1#2#3#4{%
    \begin{tikzpicture}
        \def\a{#1} \def\alp{#2} \def\bet{#3}
        \pgfmathsetmacro\yend{\a * pow(#4, 2) + (\bet)}
        \pgfmathsetmacro\ymin{min(0, \bet, \yend) - 0.4}
        \pgfmathsetmacro\ymax{max(0, \bet, \yend) + 0.4}
        \pgfmathsetmacro\vpos{ifthenelse(\a > 0, "below", "above")}
        \edef\Spos{\vpos\space left}
        \pgfmathsetmacro\Bpos{ifthenelse(\alp > 0, "left", "right")}
        \pgfmathsetmacro\Apos{%
            ifthenelse(\bet == 0, "\vpos\space right", "\Spos")%
        }
        \pgfmathsetmacro\Bpos{%
            ifthenelse(\alp == 0 || \bet == 0, "\vpos\space\Bpos", "\Bpos")%
        }
        \pgfmathsetmacro\Bstyle{%
            ifthenelse(\bet == 0, "", "draw")%
        }
        \datavisualization [school book axes,
                            all axes={ticks={none},
                                      unit length=.8cm},
                            visualize as scatter/.list={points,sym},
                            visualize as smooth line=P,
                            sym={style={mark=none}},
                        ]
        data [set=P, format=function] {
            var x : interval[(\alp-#4):(\alp+#4)];
            func y = \a * pow(\value x - \alp, 2) + (\bet);
        }
        data point [set=points, x=\alp, y=\bet,  name=S]
        data point [set=sym,    x=\alp, y=\ymin, name=min]
        data point [set=sym,    x=\alp, y=\ymax, name=max]
        data point [set=sym,    x=\alp, y=\yend, name=med]
        data point [set=sym,    x=\alp, y=0,     name=alp]
        info {
            \path [every node/.style={inner sep=.5ex, black},
                   \Bstyle, help lines]
                (S)  node[\Spos]  {$S$}
                -- (S -| 0,0) node [\Bpos] {$\beta$};
            \draw [dashed] (min) -- (max);
            \path (med) node [right] {$x=\frac{-b}{2a}$};
            \path (alp) node [\Apos] {$\alpha$};
        }
        ;
    \end{tikzpicture}%
}
    \ffigbox{}{
        \begin{subfloatrow}
            \ffigbox{}{
                \caption{$a<0$}\label{fig:parabole-aneg}
                \parab{-0.8}{2.5}{2.5}{3}
            }
            \ffigbox{}{
                \caption{$a>0$}\label{fig:parabole-apos}
                \parab{0.8}{2.3}{1}{2.8}
            }
        \end{subfloatrow}
        \caption{Allure et sommet d'une parabole.}
        \label{fig:parabole}
    }
\end{figure}

\section{Équations du second degré}

\begin{activity}
    \begin{enumerate}
        \item Factoriser les expressions suivantes:
            \begin{enumerate}[gathered, label={$\Alph*(x)={}$},labelsep=0pt]
                \item $x^2+2x$
                \item $x^2 + 2x + 1$
                \item $x^2-4$
                \item $16x^2-9$
                \item $4x^2+12x+9$
                \item $x^2 + 1$
            \end{enumerate}
            \IfStudentT{\bigfiller{5}}
        \item Résoudre $A(x) = 0$, $B(x) = 0$, etc.
            \IfStudentT{\bigfiller{5}}
    \end{enumerate}
\end{activity}

\subsection{Discriminant}

\begin{activity}
    \label{activ:canon-to-fact}
    \begin{enumerate}
        \item Factoriser le polynôme $P(x) = (x+4)^2 - 9$.
            \IfStudentT{\bigfiller{1}}
        \item Peut-on factoriser $Q(x) = (x-5)^2 - 25$ ? Et $R(x) = (x-5)^2 -
            26$ ?
            \IfStudentT{\bigfiller{3}}
        \item Peut-on factoriser $S(x) = (x-5)^2 + 25$ ?
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\begin{definition}
    Soit $P:x\mapsto ax^2+bx+c$. On appelle \define{discriminant} de~$P$ le
    nombre \UGHOST{$\Delta=b^2-4ac$.}
\end{definition}

\begin{activity}
    \label{activ:solutions}
    \begin{enumerate}
        \item Soit $P(x) = (x - 3)^2 $.
            \begin{enumerate}
                \item Résoudre~$P(x)=0$, puis développer~$P$.
                \item Calculer $-\frac{b}{2a}$; que remarque-t-on ?
                \item Calculer $\Delta$ (ou pas).
            \end{enumerate}
        \item Soit $Q(x) = x^2+4$.
            \begin{enumerate}
                \item Factoriser~$Q$ si possible et résoudre~$Q(x)=0$.
                \item Calculer $\Delta$.
            \end{enumerate}
        \item Soit $R(x) = x^2-4$.
            \begin{enumerate}
                \item Factoriser~$R$ si possible et résoudre~$R(x)=0$.
                \item Calculer $-\frac{b}{2a}$; que remarque-t-on ? Pouvait-on
                    s'y attendre ?
                \item Calculer $\Delta$, puis $\sqrt{\Delta}$. Remarques ?
            \end{enumerate}
        \item Soit $T(x) = 8x^2 + 2x$.
            \begin{enumerate}
                \item Factoriser~$T$ si possible et résoudre~$T(x)=0$.
                \item Calculer $-\frac{b}{2a}$,~$\Delta$, puis~$\sqrt{\Delta}$.
                    Remarques ? Quel ajustement faire ?
            \end{enumerate}
    \end{enumerate}
\end{activity}


\subsection{Racines d'un polynôme du second degré}

\begin{definition}
    Soit~$P$ une fonction polynomiale de degré~$2$. On appelle~\define{racines}
    de~$P$ les solutions de l'équation~$P(x) = 0$.
\end{definition}

\begin{theorem}[Théorème fondamental]
    \label{thm:fundamental-theorem}
    Soit~$P:x\mapsto ax^2 + bx + c$ une fonction polynomiale du second degré de
    discriminant~$\Delta$.
    \begin{itemize}
        \item \UGHOST{Si $\Delta<0$, $P$ n'a aucune racine et \emph{n'a pas de
            forme factorisée}.}
        \item \UGHOST{Si $\Delta=0$, $P(x) = a(x-\alpha)^2$ où $\alpha =
            \dfrac{-b}{2a}$ est l'unique racine de~$P$.}
        \item Si $\Delta>0$, $P(x) = a(x-x_1)(x-x_2)$ où les racines
            de~$P$ sont:
                \begin{enumerate}[gathered, label={$x_\arabic*={}$},labelsep=0pt,
                                   before*=\centering]
                    \item \GHOST{$\dfrac{-b-\sqrt{\Delta}}{2a}$}
                    \item \GHOST{$\dfrac{-b+\sqrt{\Delta}}{2a}$}
                \end{enumerate}
    \end{itemize}
    \Cref{fig:solutions}~est une interprétation géométrique de ces résultats.
\end{theorem}

\begin{figure}
    \ffigbox{}{
        \begin{subfloatrow}
            \ffigbox{}{
                \caption{}\label{fig:solutions-deltavar}
                \begin{tikzpicture}[baseline]
                    \datavisualization [school book axes,
                                        all axes={ticks={none},grid={step=1},
                                                  include value=5,
                                                  unit length=.8cm},
                                        visualize as smooth line/.list={
                                                    -2, -1, 0, 1},
                                    ]
                    data [format=function] {
                        var set : {-2, -1, 0, 1};
                        var x : interval[.2*\value{set}:5-.2*\value{set}];
                        func y = .6 * pow(\value x - 2.5, 2) + 1.5*\value{set};
                    }
                    ;
                \end{tikzpicture}
            }
            \ffigbox{}{
                \caption{}\label{fig:solutions-deltapos}
                \begin{tikzpicture}[baseline]
                    \def\a{0.8} \def\alp{2.5} \def\bet{-2.5}
                    \pgfmathsetmacro\d{sqrt(-\bet / \a)}
                    \datavisualization [school book axes,
                                        all axes={ticks={none},
                                                  unit length=.8cm},
                                        visualize as smooth line=P,
                                        visualize as scatter=points,
                                    ]
                    data [set=P, format=function] {
                        var x : interval[-0.5:5.5];
                        func y = \a * pow(\value x - \alp, 2) + \bet;
                    }
                    data point[set=points, x=\alp, y=\bet, name=S]
                    data point[set=points, x=(\alp-\d), y=0, name=x1]
                    data point[set=points, x=(\alp+\d), y=0, name=x2]
                    info {
                        \coordinate (one) at (visualization cs: x=0, y=1);
                        \draw[dash dot,shorten <=-2ex,shorten >=-2ex]
                            (current bounding box.north -| S) --
                                node [pos=0.2,right] {$x=\frac{-b}{2a}$}
                                (current bounding box.south -| S);
                        \scoped[help lines,
                                shorten <=3.6pt, shorten >=-1ex] {
                            \draw (x1) -- (x1 |- one);
                            \draw (x2) -- (x2 |- one);
                        }
                        \foreach \x in { x1, x2 }
                            \draw[<->] (\x |- one) -- (S |- one)
                                node[pos=0.5,above]
                                    {$\frac{\sqrt{\Delta}}{2a}$};
                        \path[every node/.style={inner sep=.5ex}]
                            (x1) node[below left]  {$x_1$}
                            (x2) node[below right] {$x_2$}
                            (S)  node[below left]  {$S$};
                    }
                    ;
                \end{tikzpicture}
            }
        \end{subfloatrow}
        \caption{Visualisation graphique du théorème fondamental avec $a>0$.
            \subref{fig:solutions-deltavar}~quand $\Delta<0$ la parabole ne
            coupe pas l'axe; quand~$\Delta=0$ elle le touche en son sommet;
            quand~$\Delta>0$, les points d'intersection s'écartent à mesure
            que~$\Delta$~croît.
            \subref{fig:solutions-deltapos}~interprétation graphique des
            solutions dans le cas~$\Delta\geq0$.}
        \label{fig:solutions}
    }
\end{figure}

\clearpage

\section{Signe d'un polynôme du second degré}

\begin{theorem}
    \label{thm:sign-delta-neg}
    Soient~$P$~un polynôme du second degré de discriminant~$\Delta<0$.
    Alors $P$~admet le tableau de signes suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3]
                    {$x$/.7,$P(x)$/.8}{$-\infty$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},}
    \end{tikzpicture}
\end{theorem}

\begin{theorem}
    \label{thm:sign-delta-zero}
    Soient~$P$~un polynôme du second degré de discriminant~$\Delta=0$.
    Alors $P$~admet le tableau de signes suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3]
                    {$x$/.7,$P(x)$/.8}{$-\infty$,$\frac{-b}{2a}$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{theorem}

\begin{theorem}
    \label{thm:sign-delta-pos}
    Soient~$P$~un polynôme du second degré de discriminant~$\Delta>0$. On~note
    $x_1 < x_2$ les deux racines de~$P$. Alors $P$~admet le tableau de signes
    suivant:

    \penalty\predisplaypenalty
    \noindent\hfil
    \upshape
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.3,espcl=3.3]
                    {$x$/.7,$P(x)$/1.2}{$-\infty$,$x_1$,$x_2$,$+\infty$}
        \tkzTabLine {,\text{signe de~$a$},z,
                      \genfrac{}{}{0pt}{}
                            {\hbox{signe opposé}}
                            {\hbox{(signe de~$-a$)}}
                     ,z,\text{signe de~$a$}}
    \end{tikzpicture}
\end{theorem}

\begin{proof}
    Le théorème fondamental donne $P(x) = a (x - x_1) (x - x_2)$ d'où le
    tableau:

    \hfill
    \begin{tikzpicture}[t style/.style=,scale=(8em/3cm)]
        \tkzTabInit [lgt=1.7,espcl=2.8]
                    {$x$/.7,
                     $x-x_1$/.7, $x-x_2$/.7,
                     $a$/.7,
                     $P(x)$/.8}
                     {$-\infty$,$x_1$,$x_2$,$+\infty$}
        \IfStudentTF{
            \tkzTabLine{,,t,,t,,}
            \tkzTabLine{,,t,,t,,}
        }{
            \tkzTabLine {,-,z,+,t,+,}
            \tkzTabLine {,-,t,-,z,+,}
        }
        \tkzTabLine {,\text{signe de~$a$},t,\text{signe de~$a$},
                                          t,\text{signe de~$a$}}
        \IfStudentTF{
            \tkzTabLine{,,t,,t,,}
        }{
        \tkzTabLine {,\text{signe de~$a$},z,\text{signe de~$-a$},
                                          z,\text{signe de~$a$}}
        }
    \end{tikzpicture}
\end{proof}

\end{document}
