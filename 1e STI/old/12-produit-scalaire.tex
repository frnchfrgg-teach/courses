% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc, patterns}

\rivmathlib{geometry}

\usepackage{sesacompat}
\usepackage{multicol}

\persocourssetup{qrcodes=prod-scal}

\begin{document}
\StudentMode

\chapter{Produit scalaire}

\section{Produit scalaire et orthogonalité}

\begin{activity}
    \makelink{forces}
    On considère un objet en forme de pavé, de masse très importante, en
    déplacement rectiligne de gauche à droite sur un sol horizontal grâce à des
    roulettes. Entre les positions $A$~et~$B$ distantes de \SI{1}{\m}, on
    applique une force constante de \SI{20}{\newton} sur le solide. Cette force,
    représentée par un vecteur~$\vect{F}$ partant du centre de masse~$O$, peut
    accélérer ou ralentir le mouvement, mais pas changer sa trajectoire car
    l'objet est trop lourd pour être soulevé.

    \thisfloatsetup{heightadjust=none}
    \begin{figure}
    \def\basepic{%
            \path[pattern = north east lines]
                (0,0) rectangle (4,-0.3);
            \draw (0,0) -- (4,0);
            \fill (1,0.1) circle[radius=0.1];
            \fill (2.5,0.1) circle[radius=0.1];
            \draw[fill=black,fill opacity=0.05]
                (0.5,0.2) coordinate (A)
                rectangle (3,1.5) coordinate (B);
            \path ($ (A)!1/2!(B) $) coordinate[label=below:$O$] (O)
                    pic[point marker] {};
    }%
    \ffigbox{}{
        \begin{subfloatrow}[2]
            \ffigbox{}{\caption{}\label{fig:force-work-neg}%
                \begin{tikzpicture}
                    \basepic
                    \draw[thick,->]
                        (O) -- node[auto,swap]
                        {$\vect{F}$} +(180:2);
                \end{tikzpicture}%
            }%
            \ffigbox{}{\caption{}\label{fig:force-work-null}%
                \begin{tikzpicture}
                    \basepic
                    \draw[thick,->]
                        (O) -- node[auto]
                        {$\vect{F}$} +(90:2);
                \end{tikzpicture}%
            }%
        \end{subfloatrow}
        \begin{subfloatrow}[2]
            \ffigbox{}{\caption{}\label{fig:force-work-small}%
                \begin{tikzpicture}
                    \basepic
                    \draw (O) -- +(0:0.6);
                    \draw (O) +(0:0.4)
                        arc[radius=0.4, start angle=0, end angle=60];
                    \path (O) +(30:0.6) node {$\frac{\pi}{3}$};
                    \draw[thick,->]
                        (O) -- node[auto]
                        {$\vect{F}$} +(60:2);
                \end{tikzpicture}%
            }%
            \ffigbox{}{\caption{}\label{fig:force-work-big}%
                \begin{tikzpicture}
                    \basepic
                    \draw[thick,->]
                        (O) -- node[auto]
                        {$\vect{F}$} +(0:2);
                \end{tikzpicture}%
            }%
        \end{subfloatrow}
        \caption{%
            Forces motrices ou résistives, avec un travail plus ou moins grand.
        }
        \label{fig:force-work}%
    }
    \end{figure}

    \begin{enumerate}
        \item Parmi tous les cas représentés dans \cref{fig:force-work},
            le(s)quel(s) correspondent à une force \emph{motrice} --- qui
            favorise le déplacement ? Dans quel(s) cas la force n'a-t-elle aucun
            effet sur le mouvement, et quand est-elle \emph{résistive}
            --- elle s'oppose au mouvement ?
            \IfStudentT{\bigfiller{5}}
        \item Classer les différents cas dans l'ordre « d'efficacité » si l'on
            veut entretenir le mouvement.
            \IfStudentT{\bigfiller{1}}
        \item \makelink{work}
            Pour traduire cette efficacité les physiciens ont introduit la
            notion de \emph{travail} d'une force, qui représente la quantité
            d'énergie que la force va ajouter ou retirer au système.

            Le travail, en \emph{joules}, est égal à
            \[ W = F \times AB \times \cos\vectangle(F,AB) \]
            où $F$~est le vecteur force et $\vect{AB}$~le vecteur correspondant
            au déplacement.

            Dans chaque cas, calculer le travail de la force. Est-ce cohérent
            avec le classement intuitif ?
            \IfStudentT{\bigfiller{8}}
    \end{enumerate}
\end{activity}

\subsection{Norme d'un vecteur}

\begin{definition}
    \makelink{norme}
    Soient $A$~et~$B$ deux points du plan, et $\vect{u} = \vect{AB}$. La
    \define{norme de~$\vect{u}$} est la longueur du vecteur~$\vect{u}$:
    $ \norm{\vect{u}} = \norm{\vect{AB}} = AB $
\end{definition}

\begin{example}
    On considère le triangle~$ABC$ tel que $AB=6$, $AC=5$ et $BC=8$, et l'on
    pose $\vect{u} = \vect{AB}$ et $\vect{v} = \vect{AC}$.
    \begin{enumerate}
        \item Déterminer $\norm{\vect{u}}$, $\norm{\vect{v}}$ et $\norm{\vect{v}
            - \vect{u}}$.\\
            \UGHOST{$\norm{\vect{u}} = \norm{\vect{AB}} = 6$;}
            \UGHOST{$\norm{\vect{v}} = \norm{\vect{AC}} = 5$}

            \UGHOST{$\norm{\vect{v}-\vect{u}} =
                \norm{\vect{AC} - \vect{AB}}$}%
            \UGHOST{${} = \norm{\vect{AC} + \vect{BA}}$} \\
            \UGHOST{${} = \norm{\vect{BA} + \vect{AC}} = \norm{\vect{BC}} = 8$}
        \item A-t-on $\norm{\vect{v} - \vect{u}} = \norm{\vect{v}} -
            \norm{\vect{u}}$ ?

            \UGHOST{$\norm{\vect{v}} - \norm{\vect{u}} = 5 - 6 = -1 \ne 8$}
    \end{enumerate}
\end{example}


\begin{proposition}
    Soit $\vect{u}(x;y)$ dans un repère orthonormé. Alors
    $\norm{\vect{u}} = \sqrt{x^2 + y^2}$.
\end{proposition}


\subsection{Première formule du produit scalaire}

\begin{definition}
    Le \define{produit scalaire} de $\vect{u}$~et~$\vect{v}$, noté
    $\vect{u}\cdot\vect{v}$ --- qui se lit «$\vect{u}$ scalaire $\vect{v}$» ---
    est défini par :
    \begin{itemize}
        \item $\vect{u}\cdot\vect{v} = \UGHOST{
            \norm{\vect{u}} \times \norm{\vect{v}} \times \cos\vectangle(u,v)
            }$\\
            \UGHOST{si $\vect{u} \ne \vect{0}$ et $\vect{v} \ne \vect{0}$;}
        \item $\vect{u}\cdot\vect{v} = \UGHOST{0}$
            si $\vect{u} = \UGHOST{\vect{0}}$ ou
            $\vect{v} = \UGHOST{\vect{0}}$.
    \end{itemize}
\end{definition}

\begin{remark}
    Le produit scalaire de deux vecteurs n'est pas un vecteur mais \emph{un
    nombre réel}.
\end{remark}

\begin{remark}
    \begin{itemize}
        \item Si $\vect{u}$~et~$\vect{v}$ sont colinéaires de même sens,
            $\vect{u}\cdot\vect{v} = \UGHOST{
                \norm{\vect{u}} \times \norm{\vect{v}}
            }$.
        \item Si $\vect{u}$~et~$\vect{v}$ sont colinéaires de sens contraire,
            $\vect{u}\cdot\vect{v} = \UGHOST{
                - \norm{\vect{u}} \times \norm{\vect{v}}
            }$.
    \end{itemize}
\end{remark}

\begin{remark}
    Si $A$, $B$ et~$C$ sont trois points distincts, alors:
    \[ \vect{AB} \cdot \vect{AC}
        = AB\times AC \times \cos\left(\widehat{BAC}\right) \]
\end{remark}

\begin{example}
    \makelink{ex-uvcos}
    On considère un carré $ABCD$ de côté $3$.
    Calculer $\vect{BC}\cdot \vect{AC}$.
    \IfStudentT{\bigfiller{4}}
\end{example}

\begin{definition}
    $\vect{u}\cdot\vect{u}$ est également noté~$\vect{u}^2$, appelé
    \define{carré scalaire} de~$\vect{u}$.
\end{definition}

\begin{proposition}
    On a les propriétés suivantes:
    \begin{enumerate}
        \item le produit scalaire est \emph{commutatif}, c'est-à-dire que
            $\vect{u}\cdot\vect{v}=\vect{v}\cdot\vect{u}$;
        \item $\vect{u}^2=\norm{\vect{u}}^2$.
    \end{enumerate}
\end{proposition}


\subsection{Orthogonalité}

\begin{definition}
    On dit que deux vecteurs $\vect{u}$ et $\vect{v}$ sont \define[vecteurs
    orthogonaux]{orthogonaux} --- ce que l'on note $\vect{u}\perp\vect{v}$ ---
    si et seulement si \UGHOST{$\vect{u}\cdot\vect{v}=0$}.
\end{definition}

\begin{remark}
  Le vecteur nul est orthogonal à tout vecteur du plan.
\end{remark}

\begin{proposition}
    Deux vecteurs non nuls $\vect{u}$~et~$\vect{v}$ sont orthogonaux si et
    seulement si $\left(\vect{u};\vect{v}\right)=\pm\dfrac{\pi}{2}\;[2\pi]$.
\end{proposition}

\begin{remark}
    Concrètement, cela veut dire que deux vecteurs non nuls sont orthogonaux
    quand ils «forment un angle droit».
\end{remark}



\begin{corollary}
    Deux droites du plan sont perpendiculaires si et seulement si un vecteur
    directeur de l'une est orthogonal à un vecteur directeur de l'autre.
\end{corollary}


\section{Autres expressions du produit scalaire}

\subsection{Polarisation}

\begin{proposition}
    $\vect{u}\cdot\vect{v} = \UGHOST{\dfrac{1}{2}\left(
        \norm{\vect{u}}^2 + \norm{\vect{v}}^2 - \norm{\vect{v}-\vect{u}}^2
        \right)}$
\end{proposition}


\begin{example}
    \makelink{ex-polar}
    Soit $ABC$ un triangle tel que $AB=6$, $AC=5$ et $BC=8$.
    \begin{enumerate}
        \item Calculer $\vect{AB}\cdot \vect{AC}$.
            \IfStudentT{\bigfiller{3}}
        \item $\vect{AB}$~et~$\vect{AC}$ sont-ils orthogonaux ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\TeacherMode
\begin{proposition}
    $\vect{u}\cdot\vect{v} = \UGHOST{\dfrac{1}{2}\left(
    \norm{\vect{u}+\vect{v}}^2 - \norm{\vect{u}}^2 - \norm{\vect{v}}^2
        \right)}$
\end{proposition}
\StudentMode

\subsection{Produit scalaire et coordonnées}

\begin{proposition}
    Soient $\vect{u}(x;y)$ et $\vect{v}(x';y')$ dans un repère orthonormé. Alors
    $\vect{u}\cdot\vect{v} = xx' + yy'$.
\end{proposition}


\begin{example}
    \makelink{ex-orth}
    Soient $\point{A}(-1;3)$, $\point{B}(5;1)$, $\point{C}(3;5)$
    et~$\point{D}(6;14)$ dans un repère~$(O;I;J)$ orthonormé. Montrer que
    $(AB)$~et~$(CD)$ sont perpendiculaires.
    \IfStudentT{\bigfiller{3}}
\end{example}

\begin{example}
    \makelink{ex-angle}
    Dans un repère~$(O;I;J)$ orthonormé, on considère
    $\point{B}(5;1)$~et~$\point{L}(2;4)$.
    \begin{enumerate}
        \item Calculer $\vect{OB}\cdot\vect{OL}$, $OB$ et $OL$.
            \IfStudentT{\bigfiller{4}}
        \item En déduire une mesure de l'angle $\widehat{BOL}$, qu'on donnera
            en degrés arrondi à \<0.1>~près.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{example}

\subsection{Projeté orthogonal}

\let\vv\vect

\begin{definition}
    Dans le plan, on considère une droite~$(AB)$ et un point~$C$.
    Le \define{projeté orthogonal} de~$C$ sur la droite~$(AB)$ est
    l'intersection~$H$ de~$(AB)$ et de la perpendiculaire à~$(AB)$ passant
    par~$C$.
\end{definition}

\begin{proposition}
    Soient $A$, $B$ et $C$ trois points distincts du plan et $H$ le projeté
    orthogonal de $C$ sur $(AB)$.
    Alors $\vect{AB}\cdot \vect{AC}=\vect{AB}\cdot \vect{AH}$.

Plus précisément, si $H\neq A$, il y a deux configurations possibles :
\setlength\multicolsep{0pt}
\begin{multicols}{2}
    \begin{itemize}
        \item $\vv{AB}$ et $\vv{AH}$ ont même sens :
            \begin{center}
                \begin{tikzpicture}[general,scale=0.8]
                    \draw (0,0)--(5,0)     ;
                    \draw[dashed] (3,2)--(3,0) ;
                    \draw[->,color=A1, epais] (1,0)--(4,0);
                    \draw[->,color=A1, epais] (1,0)--(3,2);
                    \draw[->,color=A1, epais] (1,0)--(3,0);
                    \draw (1,0) node[below]{$A$} ;
                    \draw (4,0) node[below]{$B$} ;
                    \draw (3,2) node[above]{$C$} ;
                    \draw (3,0) node[below]{$H$} ;
                    \draw (3,0.3)--(3.3,0.3)--(3.3,0) ;
                \end{tikzpicture}	
            \end{center}
            $\vv{AB}\cdot \vv{AC}=\vv{AB}\cdot \vv{AH}=AB\times AH$
        \item $\vv{AB}$ et $\vv{AH}$ sont de sens opposés :
            \begin{center}
                \begin{tikzpicture}[general,scale=0.8]
                    \draw (0,0)--(5,0) ;
                    \draw[dashed] (0.5,2)--(0.5,0) ;
                    \draw[->,color=A1, epais] (2,0)--(4.5,0);
                    \draw[->,color=A1, epais] (2,0)--(0.5,2);
                    \draw[->,color=A1, epais] (2,0)--(0.5,0);
                    \draw (2,0) node[below]{$A$} ;
                    \draw (4.5,0) node[below]{$B$} ;
                    \draw (0.5,2) node[above]{$C$} ;
                    \draw (0.5,0) node[below]{$H$} ;
                    \draw (0.5,0.3)--(0.2,0.3)--(0.2,0) ;
                \end{tikzpicture}	
            \end{center}
            $\vv{AB}\cdot \vv{AC}=\vv{AB}\cdot \vv{AH}=-AB\times AH$
    \end{itemize}
\end{multicols}
\end{proposition}

\begin{example}
    \makelink{ex-proj}
    On considère le carré $ABCD$ de côté $2$ et $I$ le milieu de $[AB]$.
    \begin{enumerate}
        \item Faire une figure.
        \item Calculer $\vv{AB}\cdot \vv{AC}$ puis $\vv{IC}\cdot \vv{BI}$.
            \IfStudentT{\bigfiller{6}}
    \end{enumerate}
\end{example}


\section{Propriétés algébriques du produit scalaire}

\begin{proposition}
    On a les propriétés suivantes:
    \begin{enumerate}
        \item Le produit scalaire est \define{distributif} par rapport
            à l'addition:
            \[\vect{u}\cdot(\vect{v}+\vect{w}) =
            \vect{u}\cdot\vect{v} + \vect{u}\cdot\vect{w}\]
        \item Pour deux réels $k$ et $k'$:
            \[ (k\vect{u})\cdot (k'\vect{v}) =
            (k\times k')\vect{u}\cdot \vect{v} \]

            En particulier $(-\vect{u})\cdot\vect{v} = \vect{u}\cdot(-\vect{v})
            = -\vect{u}\cdot\vect{v}$
    \end{enumerate}
\end{proposition}


\begin{proposition}[Identités remarquables]
    On a:
    \begin{enumerate}
        \item $\norm{\vect{u}+\vect{v}}^2 = \left(\vect{u}+\vect{v}\right)^2 =
            \vect{u}^2+2\vect{u}\cdot \vect{v}+\vect{v}^2 =
            \norm{\vect{u}}^2+2\vect{u}\cdot \vect{v}+\norm{\vect{v}}^2$
        \item $\norm{\vect{u}-\vect{v}}^2 = \left(\vect{u}-\vect{v}\right)^2 =
            \vect{u}^2-2\vect{u}\cdot \vect{v}+\vect{v}^2 =
            \norm{\vect{u}}^2-2\vect{u}\cdot \vect{v}+\norm{\vect{v}}^2$
        \item $(\vect{u}+\vect{v})\cdot(\vect{u}-\vect{v}) =
            \vect{u}^2-\vect{v}^2 =
            \norm{\vect{u}}^2-\norm{\vect{v}}^2$
    \end{enumerate}
\end{proposition}



\end{document}
