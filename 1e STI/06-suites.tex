% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\def\genericform#1{<\textsl{#1}>}

\begin{document}

\StudentMode

\chapter{Suites numériques}

%\chapterquote{Ne commence rien dont tu puisses te repentir dans la
%suite.}{Pythagore}

\begin{mathhistory}
    Leonardo Fibonacci dit «Léonard de Pise», né vers 1175
    à Pise a laissé son nom à une célèbre suite dans un
    recueil de petits problèmes.

    Il s’est intéressé à la prolifération des lapins : possédant
    au départ un couple de lapins, combien de couples de
    lapins obtient on en douze mois si chaque couple
    engendre tous les mois un nouveau couple à compter
    du second mois de son existence ?
    A vous de chercher !

    Il a beaucoup travaillé sur les nombres et en particulier
    le nombre d’or que l’on retrouve dans l’art,
    l’architecture mais aussi dans la nature (disposition des
    feuilles, des fleurs, spirale des coquillages...)
\end{mathhistory}

\iffalse

\section{Activités}

\begin{activity}
    Durant l'été, la rédaction d'un hebdomadaire décide d'organiser un
    jeu-concours sur la base d'énigmes « Quel est le nombre qui suit ? ». Les
    lecteurs ayant répondu correctement un maximum de semaines d'affilée seront
    départagés pour gagner un voyage.
    \begin{enumerate}
        \item Trouver chaque nombre manquant en expliquant votre démarche.
    \end{enumerate}

    \begin{itemize}
        \item La première semaine, l'énigme est: $-5$; $-3$; $-1$; $1$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item La deuxième semaine, l'énigme est: $2$; $-4$; $8$; $-16$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En troisième semaine: $0$; $1$; $4$; $9$; $16$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En quatrième semaine: $24$; $6$; $\frac{3}{2}$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En cinquième semaine: $1$; $2$; $5$; $14$; $41$; $122$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En sixième semaine: $768$; $384$; $192$; $96$; $48$; \ldots
            \IfStudentT{\bigfiller{1}}
        \item En septième semaine: $1$; $2$; $3$; $5$; $8$; $13$; $21$; $34$;
            \ldots
            \IfStudentT{\bigfiller{1}}
    \end{itemize}

    Une telle liste de nombres est appelée \emph{suite}, et les nombres qui la
    composent sont appelés \emph{termes} de la suite.

    \begin{enumerate}[resume]
        \item Quel serait le dixième terme de chacune des suites précédentes ?
    \end{enumerate}

    À chaque terme de la suite on attribue un rang, appelé \emph{indice}; le
    terme d'indice~$5$ de la suite~$u$ est noté $u_5$. L'indice augmente d'une
    unité à chaque terme, mais le premier terme n'a pas forcément l'indice~$1$.

    \begin{enumerate}[resume]
        \item La suite de la première semaine est notée~$t$, et son premier
            terme est~$t_1$. Quel indice a le $5$\ieme\ terme ?
            \IfStudentT{\bigfiller{1}}
        \item La suite de la deuxième semaine est notée~$u$, de premier
            terme~$u_0=2$. Quel est le $6$\ieme\ terme (notation et valeur) ?
            Quelle est la valeur de~$u_6$ ?
            \IfStudentT{\bigfiller{1}}
        \item En notant~$v$ la troisième suite, pouvez-vous trouver une formule
            en fonction de~$n$ qui permette de calculer~$v_n$ ? On supposera que
            le premier terme de~$v$ est $v_0$. Et si ça avait été~$v_1$ ?
            \IfStudentT{\bigfiller{2}}
        \item Déterminer une formule permettant de calculer~$t_2$ à partir
            de~$t_1$, puis $t_3$ à~partir de~$t_2$. Écrire une formule
            mathématique qui décrit comment passer d'un terme de~$t$ au suivant.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}

    Une telle formule est appelée \emph{formule de récurrence}.

    \begin{enumerate}[resume]
        \item Déterminer une formule de récurrence pour~$u$, puis pour~$w$ la
            septième suite.
            \IfStudentT{\bigfiller{2}}
        \item Pouvez-vous trouver une formule de récurrence pour~$v$ ?
            \IfStudentT{\bigfiller{1}}
        \item Cherchez une formule \emph{en fonction de~$n$} pour~$t_n$, puis
            pour~$u_n$.
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\begin{activity}
    Une équipe de chercheurs étudie l'évolution au cours du temps d'une
    population de fourmis à l'intérieur d'une fourmilière. Elle estime que,
    chaque mois, la population s'accroit naturellement de \SI{5}{\percent} et
    qu'en moyenne \<100>~fourmis ne reviennent pas à la fourmilière. Le 1\ier\
    janvier 2010, la population est estimée à~\<4000>. La population de fourmis
    peut-elle tripler ? Si oui, en combien de mois ?
    \IfStudentT{\bigfiller{6}}
\end{activity}

\fi


\section{Modes de génération d'une suite}

\subsection{Définition}

\begin{definition}
    Une \define{suite}~$u$ est une \emph{fonction} définie sur les nombres
    entiers --- généralement sur~$\mdN$, mais parfois uniquement à partir d'un
    entier naturel~$k$.

    Une suite \define{numérique} est à valeurs dans~$\mdR$.

    Les entiers de l'ensemble de définition sont appelés \define{indices};
    l'image d'un indice~$n$ est notée $u_n$ et est appelée \define{terme}
    d'indice~$n$.
\end{definition}

Pour le moment, les suites ne sont qu'un changement de vocabulaire: voir
\cref{tab:vocab-match}.

\begin{table}[h]
    \centering
    \begin{tabular}{cc}
        \hline
        Fonctions    & Suites \\
        \hline
        $x$          & $n$ \\
        «antécédent» & indice \\
        $f(x)$       & $u_n$ \\
        image de~$x$ & terme d'indice~$n$ \\
        \hline
    \end{tabular}
    \caption{Correspondance de vocabulaire et notations entre fonctions et
    suites}
    \label{tab:vocab-match}
\end{table}

\begin{remark}
    Grâce à une suite on obtient donc une liste ordonnée de nombres réels:
    $u_0$, $u_1$, $u_2$, etc. Le premier terme d’une suite est assez
    souvent~$u_0$, mais certaines suites démarrent avec $u_1$ voire avec un
    terme d'indice supérieur.
\end{remark}

\subsection{Suites définies par une formule explicite}


\begin{definition}
    On peut définir une suite~$u$ par une \define{formule explicite} qui dépend uniquement
    de~$n$: $\forall x \in \mdN, u_n = f(n)$ avec $f$~une fonction.
    Une telle formule de la forme $u_n = \text{\genericform{formule avec~$n$}}$
    s'appelle \define{terme général} de~$u$.
\end{definition}

\begin{remark}
    Lorsqu'une suite est définie par son terme général, on peut calculer
    directement n'importe quel terme dont on connait l'indice.
\end{remark}

\begin{example}
    \label{ex:def-generic}
    Soit $u$~définie par~$\forall n \in \mdN, u_n = n^2 + \frac{1}{n + 1}$.
    \begin{enumerate}
        \item Quel est le premier terme de~$u$ ?
            \IfStudentT{\hfiller}
        \item Calculer $u_0$, $u_1$, $u_2$ et~$u_{1000}$.
            \IfStudentT{\bigfiller{2}}
        \item Calculer le $1000$-ème terme de~$u$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{example}
    Soit $v$~définie par~$v_n = \sqrt{n-3}$.
    \begin{enumerate}
        \item Quel est le premier terme de~$u$ ?
            \IfStudentT{\hfiller}
        \item Calculer à l'aide de la calculatrice les trois premiers termes
            de~$v$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\subsection{Suites définies par (une relation de) récurrence}

\begingroup\let\UGHOST\textbf
Contrairement aux fonctions, pour lesquelles seule la méthode précédente existe,
on peut définir une suite~$u$ par la donnée:
\begin{itemize}
    \item \UGHOST{d'un ou plusieurs termes initiaux (les premiers termes);}
    \item \UGHOST{d'une formule définissant un terme quelconque en fonction des
        précédents.}
\end{itemize}

\begin{definition}
    Une telle formule de la forme $u_n = \text{\genericform{formule avec~$n$,
    $u_{n-1}$\ldots}}$ ou $u_{n+1} = \text{\genericform{formule avec~$n$,
    $u_n$\ldots}}$ est appelée \define{relation de récurrence} de~$u$.
\end{definition}

\begin{remark}
    Lorsqu'une suite est définie par récurrence, on ne peut pas calculer
    directement n'importe quel terme dont on connait l'indice: pour calculer
    $u_{100}$ \UGHOST{on a besoin de~$u_{99}$, donc de $u_{98}$, donc $u_{97}$,
    etc.}
\end{remark}

\begin{remark}
    Ce n'est pas la présence de $u_n = \dots$ qui indique un terme général: en
    effet, $u_n = u_{n-1} + 2n$ est une relation de récurrence. Une formule est
    une relation de récurrence dès que plusieurs termes de la suite sont
    présents dans la formule.

    \UGHOST{\textsl{Si le membre de droite ne contient pas de
    terme de la forme $u_{n\pm k}$, c'est une définition par terme général,
    sinon c'est une relation de récurrence.}}
\end{remark}

\begin{example}
    Soit $v$ définie par $v_1 = 1$ et $\forall n \ge 1, v_{n+1} = v_n \times 2$.
    \begin{enumerate}
        \item Calculer $v_1$, $v_2$, et~$v_3$.
            \IfStudentT{\bigfiller{2}}
        \item Calculer le quatrième terme de~$v$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{example}
    \label{ex:def-recursion}
    Soit $w$ définie par $w_0 = 0$ et $\forall n \in \mdN,
        w_{n+1} = w_n + n + 1$.
    \begin{enumerate}
        \item Calculer $w_1$, $w_2$, et~$w_3$.
            \IfStudentT{\bigfiller{2}}
        \item Calculer le quatrième terme de~$w$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{exercise}[Premiers termes par récurrence]
    28~et~29 p.~127
\end{exercise}
\endgroup

\subsection{Suites définies par un algorithme}

On peut définir une suite~$u$ par un \emph{algorithme} qui demande en entrée la
valeur de~$n$ et renvoie en sortie la valeur de~$u_n$.

\begin{remark}
    Cette méthode de génération de~$u$ recouvre les méthodes précédentes: un
    algorithme peut en effet facilement calculer~$u_n$ avec son terme général,
    et si $u$~est définie par récurrence, il suffit de faire une boucle qui
    calcule $u_0$, puis~$u_1$, puis~$u_2$, etc. jusqu'à $u_n$.
\end{remark}

\begin{example}
    Écrire un algorithme calculant les termes de la suite de
    \cref{ex:def-generic}. On définira une fonction prenant l'indice~$n$ du
    terme à calculer, et renvoyant la valeur de ce terme.
    %\IfStudentT{\bigfiller{5}}
\end{example}

\begin{example}
    Écrire un algorithme calculant le terme d'indice~$n$ de la suite de
    \cref{ex:def-recursion}. On définira une fonction prenant l'indice~$n$ du
    terme à calculer, et renvoyant la valeur de ce terme.
\end{example}

\IfStudentT{\bigfiller{3}}

\subsection{Représentation graphique}

\begin{recall}
    Si $f$~est une fonction, $\mcC_f$ est l'ensemble des points~$M(x;y)$ tels
    que $x\in\mcD_f$ et $y=f(x)$.
\end{recall}

\begin{definition}
    Si $u$~est une suite définie pour $n\ge n_0$, on représente~$u$ par le
    \define{nuage de points}~$\mcN_u$ constitué des points~$M(n;u_n)$ pour tous
    les entiers~$n\ge n_0$.
\end{definition}

\begin{example}
    Soit $u$~définie sur $\mdN\setminus\SET{0}$ par $\forall n \ge 1, u_n =
    \frac{16}{2^n}$. Construire le nuage de points de la suite~$u$.

    \StudentOnly
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            x axis={ticks and grid={step=1},
                                    unit length=0.8cm},
                            y axis={ticks and grid={step=1,
                                    minor steps between steps=1},
                                    unit length=0.8cm},
                            visualize as scatter=u,
                            u={style={ghost}},
                        ]
        data [set=u, format=function] {
            var x : {1, ..., 9};
            func y = 16 / pow(2, \value x);
        }
        ;
    \end{tikzpicture}%
    \EndStudentOnly
\end{example}

\begin{exercise}[Dessus ou pas, et chercher le(s) point(s) d'ordonnée~$2015$]
    36 p.~127
\end{exercise}

\clearpage

\section{Sens de variation}

\subsection{Première approche}

\begin{recall}
    Si $f$~est une fonction, $f$~est strictement croissante (ou
    décroissante) si et seulement si \UGHOST{$\forall a < b, f(a) < f(b)$}
    (respectivement \UGHOST{$\forall a < b, f(a) > f(b)$}).
\end{recall}

\begin{definition}
    Une suite~$u$ est strictement croissante (ou strictement décroissante,
    croissante, décroissante) si et seulement si
    \UGHOST{$\forall i<j, u_i < u_j$}
    (respectivement \UGHOST{$u_i > u_j$, $u_i \le u_j$, $u_i \ge u_j$}).
\end{definition}

\begin{example}
    Soit $u$~définie par $\forall n \ge 1, u_n = 5n + 2$.
    \IfStudentT{\bigfiller{3}}
\end{example}

\begin{exercise}
    24 p.~151
\end{exercise}

\begin{proposition}
    Soit $u$~une suite définie sur~$\mdN$ par $\forall n \in \mdN, u_n = f(n)$
    où $f$~est une fonction définie sur~$\IN[0;+\infty;[$. Si $f$~est
    strictement croissante (ou strictement décroissante, \ldots), alors
    \UGHOST{$u$ l'est aussi.}
\end{proposition}

\begin{proof}
    \UGHOST{Soient $0\le i<j$. Puisque $f$~est strictement décroissante
    sur~$\IN[0;+\infty;[$, $f(i) > f(j)$. C'est dire $u_i > u_j$ et ce pour tous
    $i<j$: $u$~est strictement décroissante.}
\end{proof}

\begin{example}
    \begin{enumerate}
        \item $\forall n \in \mdN, u_n = n^2$.
            \IfStudentT{\bigfiller{1}}
        \item $\forall n \in \mdN, v_n = \sqrt{n}$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{remark}
    Attention: la réciproque est fausse ! La proposition «Si $u$~est croissante,
    $f$~aussi» n'est pas toujours vérifiée.

    Par exemple, prenons $u_n = n
    \sin(2\pi \times n)$. Alors $f:x\mapsto x \sin(2\pi \times x)$ n'est pas
    croissante --- voir sa représentation graphique --- mais $u$ l'est ! En
    fait, on peut même montrer que $u_n = n$ pour tout~$n$\ldots
\end{remark}

\begin{exercise}
    28 p.~151
\end{exercise}

\subsection{Par récurrence}

\begin{activity}
    $u_{n+1} = u_n + n^2$
\end{activity}

\begin{theorem}
    \label{thm:increasing-seq}
    $u$~est strictement croissante si et seulement si $\forall n\in\mdN, u_n <
    u_{n+1}$. De même pour les autres.
\end{theorem}

\begin{example}
    Suite précédente
\end{example}

\begin{proof}
    Pas cette année\ldots Un petit teaser quand même: blblbl
\end{proof}

\begin{exercise}
    26 p.~151

    Remarque: en ignorant l'énoncé, il était plus rapide de comparer
    $u_i$~et~$u_j$ directement ici\ldots C'est plutôt rare.
\end{exercise}

\begin{exercise}
    23 p.~151
\end{exercise}


\end{document}


