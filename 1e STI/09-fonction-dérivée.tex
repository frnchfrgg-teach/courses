% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}

\begin{document}

\chapter{Fonctions dérivées}

\begin{activity}[Avec \bsc{Geogebra}]
    On considère la fonction $f:\mdR\to\mdR,x\mapsto x^2$.
    \begin{enumerate}
        \item \begin{enumerate}
                \item Tracer la fonction~$f$ dans \bsc{Geogebra}.
                \item Placer un point~$A$ sur~$\mcC_f$ --- dans un premier temps
                    le positionner à l'abscisse~$2$.
            \end{enumerate}
        \item \begin{enumerate}
                \item Construire la tangente~$\mcT$ à~$\mcC_f$ en~$A$ avec
                    l'outil tangente de \bsc{Geogebra}.
                \item À l'aide du graphique, déterminer une équation réduite
                    de~$\mcT$.
                    \IfStudentT{\bigfiller{1}}
            \end{enumerate}
        \item Ajouter un objet «pente» à la tangente~$\mcT$ --- on
            l'appellera~$m$. Retrouve-t-on bien le nombre dérivé de~$f$ en~$2$ ?
            \IfStudentT{\bigfiller{1}}
        \item \begin{enumerate}
                \item Créer un point~$B$ de même abscisse que~$A$ et
                    d'ordonnée~$m$.
                \item Déplacer~$A$. Que se passe-t-il ? Confirmer en activant la
                    trace de~$B$.
                    \IfStudentT{\bigfiller{2}}
                \item De quelle fonction le point~$B$ semble-t-il décrire la
                    courbe ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item Refaire les questions avec $g:\mdR\to\mdR,x\mapsto x^3$ en lieu et
            place de~$f$.
            \IfStudentT{\bigfiller{4}}
    \end{enumerate}
\end{activity}


\StudentMode

\section{Dériver sur un intervalle}

\begin{definition}
    Soit~$f$ une fonction définie sur un intervalle~$I$. On dit que $f$~est
    \define{dérivable sur~$I$} si $f$~est dérivable \UGHOST{en tout~$a\in I$.}
    Dans ce cas, la fonction définie sur~$I$ qui à chaque~$a\in I$ associe
    \UGHOST{le nombre dérivé de~$f$ en~$a$ est appelée \define{fonction dérivée
    de~$f$} et est notée~$f'$. En d'autres termes $f' : a \mapsto f'(a)$.}
\end{definition}

\begin{remark}
    $f'$~est une fonction définie de manière compliquée, presque algorithmique:
    pour calculer l'image de~$1$ par~$f'$ on calcule le taux
    d'accroissement puis on cherche la limite quand~$h\to0$. Et on
    répète la procédure pour déterminer chaque image !
\end{remark}

\section{Dérivées de fonctions «simples»}

\begin{theorem}
    \label{thm:common-derivatives}
    \leavevmode
    \begin{center}
    $\begin{array}{L*{3}{cL}}
        \firsthline
        \text{Fonction} & \text{Dérivable sur} & \text{Fonction dérivée} \\
        \hline
        f(x) = k \text{ avec $k \in \mdR$} & \mdR & f'(x) = 0 \NL
        f(x) = x & \mdR & f'(x) = 1 \NL
        f(x) = mx + p \text{ avec $m,p \in \mdR$} & \GHOST{\mdR} &
                f'(x) = \GHOST{m} \NL
        f(x) = x^2 & \GHOST{\mdR} & f'(x) = \GHOST{2x} \NL
        f(x) = x^n \text{ avec $n \ge 0$ } & \GHOST{\mdR} &
                f'(x) = \GHOST{nx^{n-1}} \NL
        f(x) = \dfrac{1}{x} & \GHOST{\begin{gathered}
                    \IN]-\infty;0;[ \text{ ou } \\
                    \IN]0;+\infty;[ \end{gathered}} &
                f'(x) = \GHOST{-\dfrac{1}{x^2}} \NL
        f(x) = \cos{x} & \GHOST{\mdR} & f'(x) = \GHOST{-\sin{x}} \NL
        f(x) = \sin{x} & \GHOST{\mdR} & f'(x) = \GHOST{\cos{x}} \\
        \lasthline
    \end{array}$
    \end{center}
\end{theorem}

\begin{example}
    \begin{enumerate}
        \item La fonction $f:x\mapsto x^5$ est dérivable sur~$\mdR$ et pour
            tout~$x\in\mdR$, $f'(x) = 5 x^{5-1} = 5x^4$.
            On a $f'(3) = \UGHOST{5 \times 3^4 = 5 \times 81 = 405}$.
        \item La fonction $f:x\mapsto 5x+6$ est dérivable sur~$\UGHOST{\mdR}$ et
            pour tout~$x\in\UGHOST{\mdR}$, $f'(x) = \UGHOST{5}$.
            On a $f'(2) = \UGHOST{5}$.
        \item La fonction $f:x\mapsto -4$ est dérivable sur~$\UGHOST{\mdR}$ et
            pour tout~$x\in\UGHOST{\mdR}$, $f'(x) = \UGHOST{0}$.
            On a $f'(10) = \UGHOST{0}$.
        \item La fonction $f:x\mapsto \dfrac{x}{3} + 1$ est dérivable
            sur~$\UGHOST{\mdR}$ et pour tout~$x\in\UGHOST{\mdR}$,
            $f'(x) = \UGHOST{\dfrac{1}{3}}$.
            On a $f'(10) = \UGHOST{\dfrac{1}{3}}$.
        \item Soit $f:x\mapsto x^2$. Calculer $f'(-2)$.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{example}

\section{Premières opérations sur les dérivées}

\begin{theorem}
    Si $u$ est une fonction dérivable sur~$I$ et $k \in \mdR$ est une constante,
    alors $f = k u$~est dérivable sur~$I$ et $f' = k u'$.
\end{theorem}

\begin{remark}
    $f = k u$ veut dire $f(x) = k \times u(x)$ pour tout~$x \in I$. De même
    $f' = k u'$ signifie $\forall x\in I, f'(x) = k u'(x)$.
\end{remark}

\begin{example}
    La fonction $f:x\mapsto \dfrac{x^2}{2}$ est dérivable
            sur~$\UGHOST{\mdR}$ et pour tout~$x\in\UGHOST{\mdR}$,
            $f'(x) = \UGHOST{\frac{1}{2} \times 2x = x}$.
            On a $f'(8) = \UGHOST{8}$.
\end{example}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f = u + v$~est
    dérivable sur~$I$ et $f' = u' + v'$.
\end{theorem}

\begin{remark}
    $f = u + v$ veut dire $f(x) = u(x) + v(x)$ pour tout~$x \in I$.
\end{remark}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = x^3 + \frac{\cos x}{2}$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa
    fonction dérivée.

    \UGHOST{%
    Rédaction type: $f$ est dérivable sur~$\mdR$ comme somme de fonctions qui le
    sont, et $f'(x) = 2x^3 + \dfrac{1}{2} \times (-\sin{x}) =
    2x^3 - \dfrac{\sin{x}}{2}$.%
    }
\end{example}

\begin{exercise}*[Déterminer l'équation d'une tangente]
    Soit $f$~la fonction définie par $f(x) = 4x^2 - 3x + 5$. Déterminer
    l'équation de la tangente à~$\mcC_f$ au point d'abscisse~$-2$.
    \IfStudentT{\bigfiller{4}}
\end{exercise}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f=uv$~est
    dérivable sur~$I$ et $f' = u'v + uv'$.
\end{theorem}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = (3x + 2)(\sqrt{x} + 1)$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa fonction
    dérivée.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{exercise}
    65 p.~80
\end{exercise}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, et \emph{si $v(x) \neq
    0$ pour tout~$x\in I$}, alors $f=\dfrac{u}{v}$~est dérivable sur~$I$ et $f'
    = \dfrac{u'v - uv'}{v^2}$.
\end{theorem}

\IfStudentT{\bigfiller{7}}

\end{document}
