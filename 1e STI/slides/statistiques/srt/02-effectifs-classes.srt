Dans cette vidéo on dispose des données brutes ou un tableau des effectifs des modalités d'une série, et on va regrouper ces modalités par classes.

Nous allons regarder la classe d'intervalle [...;...[: il
faut trouver les modalités qui sont dans cet intervalle, et sommer leur effectifs. Il y a .... ce qui donne un effectif de ...+...=...

Pour la deuxième classe, on somme les effectifs des modalités ......,
ce qui fait une somme de 14. On procède de même pour les autres classes.

Les fréquences se calculent toujours de la même façon en divisant l'effectif
par l'effectif total, qui est toujours ....

--------------------
