Dans cette vidéo on va construire le polygone des effectifs cumulés croissants
à partir des effectifs des classes d'une série statistique.

On ne calcule pas l'effectif cumulé croissant d'un intervalle (car "inférieur
ou égal" à un intervalle ne veut rien dire), mais l'on va s'intéresser à
l'effectif cumulé croissant des bornes des intervalles des classes.

Puisque la modalité minimum possible de la série est donnée par la borne gauche
du premier intervalle, on peut considérer que l'effectif cumulé croissant de
cette borne ..... est zéro. De même, les seules personnes qui chaussent du ....
ou moins dans cette classe sont celles dont les pointures sont entre ....
et ....; il y en a ....

Pour l'effectif cumulé de ...., on somme les effectifs des deux premières classes pour obtenir ...., et ainsi de suite jusqu'à la valeur .... qui assure d'avoir tout le monde.

On va pouvoir se servir de ces effectifs cumulés pour construire une courbe. Pour toutes les pointures inférieures à ...., l'effectif cumulé croissant est nul, ce qu'on peut représenter par une demi-droite horizontale dont voici un tout petit bout.

On sait aussi que pour une pointure de ...., l'effectif cumulé est ....
Pour relier ces deux points, on considère que les effectifs augmentent de
manière régulière à l'intérieur de la classe place c'est-à-dire que les
personnes sont réparties de manière uniforme: on trace ainsi un segment entre le point de coordonnées ..... et celui de coordonnées ..... À mesure que la pointure augmente, l'effectif augmente régulièrement.

De la même façon, on construit un deuxième segment reliant le point tracé à un un nouveau point de coordonnées ...... et ainsi de suite jusqu'à la pointure
...... dont l'effectif cumulé est l'effectif total ....

Toutes les pointures supérieures ont le même effectif cumulé car il n'y a plus personne qui s'ajoute. Cela dessine une demi-droite horizontale.

---------------------
