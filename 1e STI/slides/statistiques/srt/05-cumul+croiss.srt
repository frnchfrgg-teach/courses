Dans cette vidéo on va calculer les effectifs et fréquences cumulés d'une série
statistique.

L'idée est de calculer l'effectif de la classe qui va de moins l'infini jusqu'à
la valeur en question, c'est à dire le nombre d'élèves qui chaussent cette
pointure ou moins.

Évidemment il n'y a que les personnes qui chaussent du ... qui comptent parmi
celles qui chaussent du ... ou moins, donc on obtient le premier effectif
cumulé croissant juste à partir de l'effectif. Ici, l'effectif cumulé croissant
de la pointure ... est ....

Pour l'effectif cumulé croissant de la pointure ...: au groupe existant
d'élèves chaussant du ... ou moins on ajoute ... nouveaux élèves qui chaussent
du ..., et l'on obtient bien tous les élèves qui chaussent du ... ou moins. On
somme donc les deux valeurs en rouge pour obtenir le résultat: ....

On fait de même pour les suivants: l'effectif cumulé croissant de ... est ...,
celui de ... est ..., et ainsi de suite jusqu'à l'effectif cumulé croissant de
.... qui est .... soit la totalité des élèves de la classe.

On peut faire de même pour calculer les fréquences cumulées croissantes, en
sommant les valeurs en rouge à chaque étape. On obtient bien 100% pour la
fréquence cumulée croissante de .... car on a alors tous les élèves.

Une autre manière d'obtenir les fréquences cumulées croissantes est de partir
des effectifs cumulés croissants et de les diviser par l'effectif total en
utilisant la définition classique de la fréquence. Cela donnera le même
résultat, aux arrondis près (ici je me suis arrangé pour que les arrondis
"tombent juste", mais ce n'est pas obligatoire).

---------------------
