Dans cette vidéo on va commenter les propos d'un maire en s'appuyant sur les
propriétés de la médiane et de la moyenne.

Lorsque le maire dit que la moitié de ses concitoyens gagne 4500€ ou plus il
fait une erreur car c'est le salaire moyen dans sa ville qui est de 4500 euros
et pas le salaire médian.

La médiane va partager la population en deux groupes de même effectif: l'un
ayant des modalités inférieur à la médiane l'autre ayant les modalités
supérieurs à la médiane.

Par contre la moyenne répartit équitablement le total des salaires, ce qui
reviendrait à mettre tous les salaires dans une grande caisse commune et
repartager en parts égales ce qui donne pas du tout le même résultat évidemment
puisqu'il y a des gens qui gagnent beaucoup plus que d'autres.

Ça aurait était vrai s'il avait parlé de la médiane, qui bien sûr est probablement nettement plus faible.

---------------

