Dans cette vidéo on va déterminer la médiane d'une série à partir d'un tableau
des effectifs, sur l'exemple des pointures des élèves.

Pour calculer la médiane avec la méthode du collège, on écrit la série brute
avec les répétitions dans l'ordre croissant, et l'on fait deux paquets de
taille égale (en laissant un nombre central tout seul si l'effectif total est
impair).

La médiane est alors le nombre tout seul (dans le cas impair) ou le milieu des
deux nombres centraux. Ici c'est .....

Comment faire sans écrire toute la série (si par exemple l'effectif total est
1000) ? On calcule d'abord les effectifs cumulés, et la moitié de l'effectif
total, soit ..... Attention, .... n'est pas la médiane (c'est .....) !

Si l'on considère tous les nombres de la série qui sont inférieurs ou égaux à ..., on en a colorié ....: c'est l'effectif cumulé croissant. Il y a .... valeurs inférieures ou égales à ...., .... valeurs ≤ ..., et ainsi de suite.

Lorsqu'on a sélectionné les modalités inférieures ou égales, l'effectif cumulé est ..., ce qui dépasse N/2 = ... et suffit donc à passer sur la deuxième ligne.

La médiane sera donc atteinte lorsqu'on est en train d'écrire des ...., ce qui correspond à la première modalité dont l'effectif cumulé croissant atteint ou dépasse N/2. La médiane est ainsi ....

On peut aussi utiliser les fréquences cumulées croissantes, dans ce cas la médiane est la première modalité dont la fréquence cumulée croissante atteint ou dépasse 50% (ou 0,5).

----------------
