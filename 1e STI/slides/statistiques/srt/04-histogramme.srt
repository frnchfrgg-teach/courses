Dans cette vidéo nous allons construire un histogramme à partir d'un tableau
d'effectifs par classes.

Un histogramme est une représentation graphique où chaque classe correspond à
un rectangle dont l'aire est proportionnelle à l'effectif.

Pour la première classe, d'intervalle [...;...[, on construit un rectangle posé
sur l'axe des abscisses, dont la base va de l'abscisse ... à l'abscisse ...
(comme l'intervalle).

On doit s'arranger pour que l'aire de ce rectangle corresponde à l'effectif de
la classe, c'est-à-dire .... On pourrait bien sûr calculer la hauteur en
divisant l'aire voulue par la largeur du rectangle, mais c'est plus simple de
compter les carreaux en choisissant bien l'échelle. Ici j'ai pris ... petits
carreaux pour 1 élève.

Cette première classe comporte ... élèves, ce qui fait ... carreaux à répartir
sur ... carreaux de large. Cela donne donc ce rectangle. Pour la deuxième
classe qui contient ... élèves, on répartit ... carreaux entre les abscisses
... et ... puisque ce sont les bornes de l'intervalle., et l'on obtient un
deuxième rectangle.

On procède de même pour les autres classes. On peut remarquer que même si l'effectif d'une classe est plus grand, cela n'implique pas forcément que le rectangle est plus haut: si la classe est plus large, l'aire est plus facile à répartir. Finalement la hauteur représente une sorte de densité: ... élèves seront plus serrés sur ... carreaux de large que ... élèves sur ... carreaux.

------------------------
