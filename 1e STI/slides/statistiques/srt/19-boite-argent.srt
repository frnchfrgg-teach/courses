Dans cette vidéo on va construire le diagramme en boite (ou boite à moustaches)
de la série des montants d'argent de poche d'un groupe d'amis.

On dispose d'un tableau des fréquences, à partir duquel on a déterminé un grand
nombre d'indicateurs statistiques dans les vidéos précédentes: l'argent de
poche minimum est 0€, le premier quartile 1,2€, la médiane est 2€, le troisième
quartile est 5€ et le maximum est 200€.

Pour construire le diagramme en boite, il faut tout d'abord choisir une échelle
adaptée aux données. Ici on doit pouvoir placer le minimum et le maximum soit
une étendue de 200. Une solution est de prendre 1mm pour 2€ ce qui fera une
largeur totale de 10cm, mais on peut prendre 1mm pour 1€ si l'on dispose de
20cm de large (avec une page A4 par exemple).

On trace tout d'abord un petit trait vertical au niveau de l'abscisse 0 qui
représente le minimum de la série, puis à l'abscisse 200 pour le maximum.
Ensuite, on place le premier quartile 1,2 avec un trait vertical un peu plus
grand; il est évident que placer 1,2€ avec l'échelle choisie sera peu précis
car on doit mesurer moins d'un millimètre ! On place la médiane 2€ et le
troisième quartile 5€ de la même façon; ils sont plus simples à placer car ils
tombent sur des millimètres ou exactement au milieu d'une graduation.

On ferme la boite en reliant horizontalement les traits des quartiles, puis on
complète le diagramme avec les moustaches qui relient les bords de la boite aux
extrema, c'est-à-dire le premier quartile au minimum et le troisième quartile
au maximum.

On a terminé la construction. La moustache gauche représente toujours environ
1/4 des amis (mais clairement pas 1/4 de l'étendue des sommes d'argent), et de
même pour les trois autres parties du diagramme: la moustache de droite est
nettement plus longue que celle de gauche alors qu'elles correspondent toutes
les deux à peu près au même nombre d'amis ! C'est un signe que la série est
très déséquilibrée à cause de la valeur maximum; à tel point que c'est
compliqué de trouver une échelle adaptée qui permette de représenter les
valeurs avec précision.

