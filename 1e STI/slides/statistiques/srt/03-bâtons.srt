Dans cette vidéo on va construire le diagramme en bâtons d'une série
statistique à partir de son tableau des effectifs.

Un diagramme en bâtons est une représentation graphique d'une série statistique où chaque modalité est représentée par un segment.

La modalité ... est représentée par le segment d'abscisse ... démarrant de l'ordonnée zéro et se terminant à l'ordonnée ... qui est l'effectif de la modalité.

Pour la modalité ... on trace le segment d'abscisse ... entre les points d'ordonnée zéro et d'ordonnée ..., et l'on procède ainsi pour toutes les modalités.

--------------------
