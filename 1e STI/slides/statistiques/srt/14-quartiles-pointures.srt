Dans cette vidéo on va déterminer les quartiles d'une série à partir du tableau des fréquences.

On calcule les fréquences cumulées croissantes, puis on cherche la plus petite modalité dont la fréquence cumulée croissante atteint ou dépasse 25%.
Ici, c'est Q1 = ...., de fréquence cumulée ....

Lorsqu'on atteint ou dépasse 50%, c'est la médiane qui vaut ...., et lorsqu'on atteint ou dépasse 75% on obtient le troisième quartile Q3 = ....

On peut faire le même procédé avec les effectifs, en prenant soin de regarder quand on dépasse N/4 pour le Q1 et 3N/4 pour Q3, c'est-à-dire 3/4 de l'effectif.

------------
