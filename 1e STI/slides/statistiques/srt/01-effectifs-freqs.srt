Dans cette vidéo, nous allons remplir un tableau des effectifs à partir des
données brutes d'une série statistique, puis déterminer les fréquences
correspondantes.

On va chercher la modalité la plus petite dans notre bonne série de données, et
compter le nombre de fois qu'elle apparaît. Ici c'est .... qui apparaît ....
fois. Ensuite la modalité suivante est .... qui apparaît .... fois, et ainsi de
suite de sorte à compter l'ensemble de nos données.

Les fréquences sont les proportions d'apparition d'une modalité par rapport à
l'effectif total. Ici on a .... valeurs dans la série. La fréquence de la
modalité .... est donc .... / .... ce qui fait environ ....%.

Dans ce tableau, les arrondis ne sont pas toujours identiques. C'est juste pour assurer que la somme des fréquences fasse 1. Il n'est pas exigible que vous respectiez cette contrainte dans vos tableaux.

----------------------


