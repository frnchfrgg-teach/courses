Dans cette vidéo on commence à étudier la série des nombres de victoires à
Fortnite dans un groupe pour préparer la découverte de la notion de variance.

Un groupe de copains se lance un défi: gagner un maximum de fois au jeu
Fortnite. Ils consignent les résultats.

La difficulté ici est que les deux lignes ont un intitulé commençant par
« Nombre de », ce qui brouille les pistes. Cependant, un joueur peut avoir plusieurs victoires tandis que chaque victoire n'est obtenue que par un unique joueur. Le nombre de joueurs ne peut donc pas être le caractère, et c'est donc que la population est le groupe de joueurs et le caractère le nombre de victoires obtenues par chaque joueur.

Pour calculer de nombre moyen de victoires, on calcule le nombre total de victoires obtenues par le groupe en multipliant chaque nombre de victoires par l'effectif correspondant, puis en additionnant. On effectue donc ....

Enfin, on divise par l'effectif total 3 + 1 + .... Les 64 victoires sont donc partagées parmi les 16 joueurs du groupe ce qui donne 4 victoires en moyenne.
