Dans cette vidéo, on va déterminer l'étendue et l'écart inter-quartile d'une série de montant d'argent de poche.

On dispose du tableau des fréquences.

On peut directement lire sur la première ligne la modalité minimum, ici 0€, et la modalité maximum: 200€. L'étendue est la différence, soit 200 - 0 = 200€.

Les premiers et troisième quartiles ont été déterminés dans une précédente vidéo, par exemple en regardant la plus petite pointure dont la fréquence cumulée croissante dépasse 0,25 pour le premier et 0,75 pour le troisième.

On obtient l'écart interquartile en calculant leur différence Q3 - Q1 égale
5 - 1.2 = 3.8€.

L'étendue 200€ n'est pas à peu près le double de l'écart interquartile. Ce n'est pas très étonnant car l'intervalle interquartile représente la moitié de la population, pas la moitié de l'étendue. On peut l'expliquer plus précisément en remarquant la valeur 200€, exceptionnellement grande, qui impacte beaucoup l'étendue mais n'a aucun effet sur l'écart interquartile qui ignore les extrêmes.

