Dans cette vidéo, on va calculer l'écart-type de la série des montants d'argent
de poche d'un groupe d'amis.

On dispose du tableau des fréquences, et de la variance qu'on a calculé dans une vidéo précédente. Il suffit de prendre la racine carrée de cette variance: on calcule racine de 952,3, ce qui fait environ 30,86€.

Cette valeur est en €, ce qui est bien plus parlant que des euros au carré pour l'interprétation.
