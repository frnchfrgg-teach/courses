Dans cette vidéo on va déterminer les quartiles sur un exemple à partir des fréquences cumulées croissantes.

Reprenons le tableau avec les fréquences cumulées croissantes qu'on a construit
pour la médiane, et cette fois ci nous cherchons la plus petite modalité pour
laquelle la fréquence cumulée atteint ou dépasse 25%. Ils sont dépassés à
1,2€ c'est donc le premier quartile.

Les 75% de fréquence cumulée sont exactement atteints pour la modalité
5€, donc la condition est remplie et le troisième quartile est 5€.

--------------
