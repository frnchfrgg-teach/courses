Dans cette vidéo, on va déterminer l'étendue et l'écart inter-quartile de la
série des pointures d'un groupe d'élèves.

On dispose du tableau des effectifs, ou du tableau des fréquences.

On peut directement lire sur la première ligne la modalité maximum, ici la plus
grande pointure, et la modalité minimum. L'étendue sera la différence, ici 46 moins 38 égale 8.

Les premiers et troisième quartiles ont été déterminés dans une précédente vidéo, par exemple en regardant la plus petite pointure dont la fréquence cumulée croissante dépasse 0,25 pour le premier et 0,75 pour le troisième.

On obtient l'écart interquartile en calculant leur différence Q3 - Q1 égale
45 - 42 = 3.
