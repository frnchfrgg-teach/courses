Dans cette vidéo on introduit la notion de variance en calculant les écarts au carré.

Un groupe de copains se lance un défi: gagner un maximum de fois au jeu
Fortnite, et consignent les résultats. Ils définissent leur score en calculant
la différence entre le nombre de victoires de chaque joueur, et la moyenne du
groupe.

Dans une vidéo précédente on a calculé les scores, ainsi que la moyenne de ces scores dans l'espoir d'obtenir la différence moyenne de niveau du groupe. Malheureusement, les scores négatifs compensent les scores positifs, et cette moyenne est nulle.

Une idée est donc de mettre les scores au carré pour faire disparaitre les signes moins. On obtient (-4)^2 = +16, (-2)^2 = 4, et ainsi de suite.

On calcule ensuite la moyenne de ces scores au carré, en utilisant toujours les mêmes effectifs: 16 × 3 + 4 × 1 + 1 × 4 et ainsi de suite, divisé par 16. Le résultat est 6,625.

On a résolu le problème de la moyenne nulle, car tous les carrés sont positifs et donc leur moyenne est elle aussi positive.
