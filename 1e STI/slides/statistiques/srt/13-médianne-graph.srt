Dans cette vidéo on va déterminer la médiane d'une série donnée avec des classes, à partir du polygone des effectifs cumulés.

Pour calculer la pointure médiane des élèves, on repart du graphique des
effectifs cumulés croissants et on va chercher la pointure dont l'effectif
cumulé croissant est la moitié de l'effectif total, soit .....

Mais ça sur une courbe c'est lire un antécédent. Nous allons donc chercher
l'antécédent de .... sur ce graphique, ce qui donne environ .... qui sera notre
médiane.

Lorsqu'on détermine la médiane à partir des modalités distinctes, on obtient ..., ce qui est légèrement différent. Cette différence vient de ce que les données par classe sont une approximation, une simplification du problème. On voit que si les classes sont bien choisies la différence est faible.

Bien sûr dans notre cas simplifier le problème n'a pas vraiment d'intérêt
puisqu'il était déjà simple au départ mais lorsqu'on a beaucoup plus de données
et beaucoup plus de modalités différentes alors les regrouper par classes prend
tout son sens, et si les classes sont bien choisies on aura l'assurance que la
médiane obtenue sera pertinente pour analyser les données.

------------
