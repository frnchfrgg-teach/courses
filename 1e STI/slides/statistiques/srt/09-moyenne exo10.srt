Dans cette vidéo, on va calculer une moyenne sur un exemple, à partir des
fréquences puis à partir des effectifs.

Lorsqu'on n'a que les fréquences, on peut utiliser la formule du cours: x1 f1 +
x2 f2 + x3 f3 + ... + xk fk.

Ici, la moyenne est 0€ fois 7,5% (ou encore 0,075) plus 0,5€ fois 10% (ou 0,1)
plus 1,2 × 0,15, et ainsi de suite. On ne divise pas par l'effectif total (on
peut imaginer diviser par la fréquence totale qui est toujours 1).

On obtient 9€ et 3 cents.

Si l'on part du principe qu'il y a 40 personnes, alors il y a 7,5% de 40 personnes soit 40×0.075 = 3 collègues qui ont 0€ en monnaie. Il y a 10% des collègues, soit un dixième de 40 ce qui donne 4 collègues qui ont 50 cents dans leur poche.

On procède ainsi pour toutes les modalités.

En faisant le produit de chaque modalité par l'effectif correspondant, et en
sommant le tout on obtient alors la somme totale détenue par le groupe de collègues, qu'on divise par 40 pour obtenir la moyenne: encore une fois c'est 9€ et 3 cents.

------------------
