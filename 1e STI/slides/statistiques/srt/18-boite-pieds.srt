Dans cette vidéo on va construire le diagramme en boite (ou boite à moustaches)
des pointures d'un groupe d'élèves.

On dispose d'un tableau des effectifs, à partir duquel on a déterminé un grand
nombre d'indicateurs statistiques dans les vidéos précédentes: la pointure
minimum est 38, le premier quartile 42, la médiane est 43, le troisième
quartile est 45 et le maximum est 46.

Pour construire le diagramme en boite, il faut tout d'abord choisir une échelle
adaptée aux données. Ici on doit pouvoir placer le minimum et le maximum soit
une étendue de 8. Un bon choix pourrait être de prendre 1cm pour une pointure
même si d'autres échelles sont envisageables.

On trace tout d'abord un petit trait vertical au niveau de l'abscisse 38 qui représente le minimum de la série, puis à l'abscisse 46 pour le maximum. Ensuite, on place le premier quartile 42 avec un trait vertical un peu plus grand; la médiane 43 et le troisième quartile 45 de même.

On ferme la boite en reliant horizontalement les traits des quartiles, puis on complète le diagramme avec les moustaches qui relient les bords de la boite aux extrema, c'est-à-dire le premier quartile au minimum et le troisième quartile au maximum.

On a terminé la construction. On peut remarquer que la moustache gauche représente environ 1/4 des élèves (mais pas 1/4 de l'étendue des pointures), la partie gauche de la boite correspond aussi à environ 1/4 des élèves, ainsi que la partie droite et la moustache de droite.
