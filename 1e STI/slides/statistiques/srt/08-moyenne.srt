Dans cette vidéo on calcule la moyenne d'une série à partir de son tableau des
effectifs.

Dans un premier temps on dispose des effectifs des toutes les modalités
distinctes. De la même façon que la moyenne des notes est la somme des notes
divisée par le nombre de notes, la pointure moyenne sera la somme de toutes les
pointures divisée par le nombre d'élèves.

Cette somme des pointures est ... + ... + ... + ... etc. La pointure ...
apparait plusieurs fois dans cette somme car plusieurs élèves ont cette
pointure.

Cela revient à calculer ... × ... (pour la première modalité) + ... × ... (pour
toutes les pointures ...) + ... × ... + ... × ... et ainsi de suite jusqu'à ...
× ...

On divise par l'effectif total ..., et l'on obtient environ .....

Dans un second temps on dispose des effectifs des classes de pointures.  On va
procéder de la même manière, à ceci près qu'on considère que la "valeur" d'une
classe est le nombre exactement au milieu de l'intervalle, c'est-à-dire la
somme des extrémités divisée par deux.

On a donc (... + ...) / 2 × ... pour la première classe de pointures, soit
... × ..., (... + ...) / 2 × ... c'est-à-dire ... × ... pour la deuxième, et ainsi de suite. On divise toujours par l'effectif total ..., et l'on obtient
....

C'est différent de ce qu'on avait obtenu avec les modalités distinctes. C'est
normal puisque lorsqu'on est passé par des données par classe on a simplifié le
problème. On a réduit le nombre d'informations, on a perdu des détails mais la
différence est assez faible puisque les classes n'étaient pas trop mal choisies
au départ.

------------------
