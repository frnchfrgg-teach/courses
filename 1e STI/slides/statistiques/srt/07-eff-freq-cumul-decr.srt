Dans cette vidéo on va calculer les effectifs et fréquences
cumulées décroissantes à partir du tableau des effectifs.

On remarque que les effectifs cumulés
décroissants de la pointure ....
c'est le nombre d'élèves qui chausse du
... ou plus: il n'y a que ceux qui chaussent du .... car aucun élève n'a une pointure supérieure.

Pour la pointure ...., on ajoute à ce groupe chaussant du .... des nouveaux élèves, ceux qui chaussent du ....; on obtient donc l'effectif cumulé décroissant en additionnant les nombres en rouge.

On procède de même pour les autres effectifs, en allant de droite à gauche, et l'on obtient encore l'effectif total ..... qui est l'effectif cumulé décroissant de la pointure ....

Pour les fréquences c'est la même technique. Bien sûr on peut aussi déterminer les fréquences à partir des effectifs en divisant par l'effectif total .....

---------------
