Dans cette vidéo on va déterminer la médiane à partir des fréquences sur un
exemple.

L'énoncé est donné en fréquences, donc on va calculer les fréquences cumulées,
et chercher la première fois où l'on a dépasse 50%.

La médiane est donc deux euros.

La moyenne est de 9 euros et trois cents, ce qui est bien plus élevé que 2
euros. C'est normal: la moyenne revient à mettre tout l'argent des amis dans un
même pot commun et partager de manière équitable, et les 200€ d'un seul
collègue font augmenter considérablement cette moyenne: ceux qui ont plus que
9,03€ sont bien moins nombreux que ceux qui ont moins, donc cette moyenne est
trop élevée pour être la médiane qui partage les collègues en deux groupes de
même effectif.

-------------
