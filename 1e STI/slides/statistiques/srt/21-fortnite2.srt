Dans cette vidéo on introduit l'écart de chaque joueur à la moyenne des
victoires d'un groupe jouant à Fortnite, pour découvrir la notion de variance.

Un groupe de copains se lance un défi: gagner un maximum de fois au jeu
Fortnite, et consignent les résultats. Pour pouvoir se comparer au groupe sur
le long terme, ils décident de définir un score en calculant la différence
entre le nombre de victoires de chaque joueur, et la moyenne du groupe.

Dans une vidéo précédente on a calculé le nombre moyen de victoires dans le
groupe, qui est 4. Les joueurs n'ayant jamais gagné auront donc un score de 0 -
4 = -4. Ceux qui ont gagné 2 fois ont un score de 2 - 4 = -2, et ainsi de
suite.

On calcule la moyenne des scores en utilisant les mêmes effectifs qu'avant: on
obtient ..... / 16 = 0

Cela s'explique car les écarts positifs compensent les écarts négatifs.

En fait, la moyenne des écarts est nulle pour toutes les séries. La
démonstration n'est pas à savoir, mais la voici pour la culture:

La moyenne de la série x est l'addition des valeurs x1, x2, x3, etc. de la
série en n'oubliant pas de multiplier par les effectifs n1, n2, n3, divisée par
l'effectif total N.

On définit la série y des écarts à la moyenne (ou des scores), en retranchant la moyenne à toutes les valeurs. La moyenne de cette nouvelle série y se calcule de la même façon que celle de x.

On remplace y1 par sa définition x1 - moyenne de x, y2 par x2 - moyenne de x, et ainsi de suite jusqu'à yk = xk - moyenne de x. On n'oublie pas le dénominateur N.

Ensuite, on développe pour obtenir x1 fois n1 - moyenne de x fois n1, et ainsi de suite.

On regroupe tous les termes avec les valeurs x1, x2, etc, et ensuite tous les termes avec la moyenne de x qu'on met en facteur sans oublier le signe moins. Et on complète avec le dénominateur N.

On reconnait alors la définition de la moyenne de x. D'autre part la somme
n1 + n2 + n3 ... est égale à l'effectif total N. Au final on obtient moyenne de x - moyenne de x × 1 ce qui donne 0.
