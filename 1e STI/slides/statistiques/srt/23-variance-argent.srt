Dans cette vidéo, on va calculer à la main la variance de la série des montants d'argent de poche d'un groupe d'amis.

On dispose du tableau des fréquences, et de la moyenne qu'on a calculé dans une vidéo précédente. On calcule d'abord les écarts à la moyenne: les collègues ayant 0€ ont donc un écart de -9€03 avec la moyenne.

Ceux ayant 2€ ont 7€03 de moins que la moyenne, ceux qui ont 10€ ont 97 centimes de plus, tandis que lorsqu'on a 200€, l'écart à la moyenne est de 190€97.

Ensuite, on met ces écarts au carré, et l'on obtient environ 81,54 pour les collègues ayant 0€, et ainsi de suite jusqu'à 35,64 et enfin 36469,54.

Calculons enfin la moyenne de ces carrés: on rappelle qu'ici pas besoin de diviser par l'effectif total car la moyenne est calculée à partir de la fréquence: la variance est donc 81,54 × 0,075 (puisque 7,5% = 0,075), plus
72,76 × 0,1, etc, plus 35,64 × 0,05 plus 36469,54 × 0,025.

On obtient environ 952,3. Cette valeur est en € au carré.
