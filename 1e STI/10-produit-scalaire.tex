% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc, patterns, matrix, backgrounds, angles}

\rivmathlib{geometry}

\usepackage{sesacompat}
\usepackage{multicol}

\persocourssetup{qrcodes=prod-scal}
%\AtEndDocument{\foreachlink{\makelink[here]{#1} }}

\begin{document}
\StudentMode

\chapter{Produit scalaire}

\section{Les trois formules}

\subsection{Projeté orthogonal}

\begin{sidefigure}[fc]
    \begin{tikzpicture}[scale=0.7,rotate=10]
        \path (1,0) coordinate[label=below:$A$] (A) ;
        \path (4,0) coordinate[label=below:$B$] (B) ;
        \path (3,1.7) coordinate[label=above:$C$] (C) ;
        \path (A -| C) coordinate[label=below:$H$] (H) ;
        \draw ($ (A)!-1/3!(B) $)
                -- (A) pic[tick marker] {}
                -- (B) pic[tick marker] {}
                -- ($ (A)!4/3!(B) $);
        \draw[dashed] (C) -- (H) pic[perp marker] {} ;
        \draw (C) pic[point marker] {};
        %\draw[->, thick] (A) -- (B);
        %\draw[->, thick] (A) -- (C);
        %\draw[->] (A) -- (H);
    \end{tikzpicture}
\sidetext
    \begin{definition}
        Dans le plan, on considère une droite~$(AB)$ et un point~$C$. Le
        \define{projeté orthogonal} de~$C$ sur la droite~$(AB)$ est
        l'intersection~$H$ de~$(AB)$ et de la perpendiculaire à~$(AB)$ passant
        par~$C$.
    \end{definition}
\end{sidefigure}


\begin{example}
    \makelink{proj}
    On considère un carré $ABCD$~de centre~$O$. On note $I$~le milieu de~$[AB]$
    et $J$~le milieu de~$[OA]$.
    Faire la figure, puis déterminer:
    \begin{enumerate}
        \item le projeté orthogonal de~$O$ sur~$(AB)$;
        \item le projeté orthogonal de~$A$ sur~$(BC)$;
        \item les projetés orthogonaux sur~$(AC)$ de $A$, puis~$B$, et enfin~$I$.
    \end{enumerate}
    \IfStudentT{\bigfiller{5}}
\end{example}

\subsection{Formule du projeté orthogonal}

\begin{activity}
    \makelink{forces}
    Un chariot se déplace de $A$~à~$B$ sur des rails. Pendant ce déplacement, on
    lui applique une force constante représentée par un vecteur~$\vect{F}$
    partant du centre de masse de l'objet. On suppose que l'objet est trop lourd
    pour être soulevé ou sorti de ses rails.

    \NewDocumentCommand\dopic{mmO{}O{}}{%
        \ffigbox{}{\caption{}\label{fig:force-work-#1}%
        \begin{tikzpicture}[
                x=0.5cm,y=0.5cm,scale=1.1,
                #1/.try,
            ]
            \draw[blue] (0,0) coordinate[label=below:$A$] (A)
                    pic[point marker] {};
            \path (A) +(-2.5,-1.3) coordinate (SW);
            \path ($ (A)!-1!(SW) $) coordinate (NE);
            \fill ($ (SW)!1/5!(SW-|NE) $) +(0,-0.2) circle[radius=0.2];
            \fill ($ (SW)!4/5!(SW-|NE) $) +(0,-0.2) circle[radius=0.2];
            \path (A) +#2 coordinate (F);
            #4
            \draw[thick,->] (A)
                -- node[auto,#3,circle,inner sep=0.2ex] {$\vect{F}$} (F);
            \draw[->,blue]  (A) -- +(4,0) coordinate[label=below:$B$] (B);
            \path (current bounding box.south west)
                    +(-0.3,0) coordinate (BSW);
            \path (current bounding box.north east)
                    +(0.3,0.3) coordinate (BNE);
            \scoped[on background layer, shift=(A)] {
                \draw[help lines,step=1] (BSW) grid (BNE);
                \draw[fill,fill opacity=0.05] (NE) rectangle (SW);
            }
            \draw (BSW) -- (BNE |- BSW);
            \path[pattern = north east lines]
                (BSW) +(0,-0.3) rectangle (BNE |- BSW);
        \end{tikzpicture}%
        }\ignorespaces
    }%
    \thisfloatsetup{heightadjust=none}
    \begin{figure}
    \ffigbox{}{
        \begin{subfloatrow}[2]
            \dopic{neg}{(-4,0)}[swap]
            \dopic{med}{(2,0)}
        \end{subfloatrow}
        \begin{subfloatrow}[2]
            \dopic{negup}{(-4,2)}[][\path (-4,4);]
            \dopic{null}{(0,4)}
        \end{subfloatrow}
        \begin{subfloatrow}[2]
            \dopic{big}{(3,-1)}[swap][\path(-4,3);]
            \dopic{medup}{(2,3)}
        \end{subfloatrow}
        \caption{%
            Forces motrices ou résistives, avec un travail plus ou moins grand.
        }
        \label{fig:force-work}%
    }
    \end{figure}

    \begin{enumerate}
        \item \begin{itemize}
                \item Parmi tous les cas représentés dans la figure
                    \ref{fig:force-work}, le(s)quel(s) correspondent à une force
                    \emph{motrice} (qui favorise le déplacement) ?  Dans quel(s)
                    cas la force n'a-t-elle aucun effet sur le mouvement ?  Dans
                    quel(s) cas est-elle \emph{résistive} (elle s'oppose au
                    mouvement) ?

                    \IfStudentT{
                        motrice: \hfiller~
                        aucun effet: \hfiller~
                        résistive: \hfiller
                    }
                \item Entre les cas
                    \subref{fig:force-work-med}~et~\subref{fig:force-work-medup},
                    quelle force favorise le plus le déplacement ?
                    \IfStudentT{\bigfiller{1}}

                \item Sur la figure
                    \ref{fig:force-work}\subref{fig:force-work-big}
                    représenter une autre force ayant le même effet que la
                    force~$\vect{F}$ déjà représentée.
            \end{itemize}
    \end{enumerate}
    \textbf{Bilan :}
    \textit{
        L'intensité effective de la force (représentée par la longueur du
        vecteur~$\vect{F}$) n'a pas d'importance, c'est la composante
        horizontale (colinéaire au mouvement) de la force qui favorise ou freine
        le déplacement: tout l'effort fourni verticalement n'a aucun effet.
    }
    \begin{enumerate}[resume]
        \item \makelink{travail}
            Pour traduire cette efficacité les physiciens ont introduit la
            notion de \emph{travail} d'une force, qui représente la quantité
            d'énergie que la force va ajouter ou retirer au système.

            Soient~$C$ tel que $\vect{F} = \vect{AC}$ et $H$~le projeté
            orthogonal de~$C$ sur~$(AB)$. Alors le travail, en \emph{joules},
            est égal à $W = AB \times AH$ si $\vect{AB}$~et~$\vect{AH}$ sont de
            même sens, $W = AB \times AH$ si $\vect{AB}$~et~$\vect{AH}$ sont de
            sens contraire, et $W = 0$ si $\vect{AB}$~et~$\vect{AH}$ forment un
            angle droit.

            On prend une échelle d'un carreau pour $\SI{1}{\newton}$.
            La~distance entre $A$~et~$B$ est $\SI{4}{\m}$.
            Dans chaque cas, calculer le travail de la force.
            \IfStudentT{
                \foreach \x in {a,b,c,d,e,f} {%
                    \par \textsf{\textbf{(\x)}} \hfiller \par
                }
            }
    \end{enumerate}
\end{activity}

On peut généraliser la notion de travail d’une force pendant un déplacement à
deux vecteurs quelconques $\vect{AB}$~et~$\vect{AC}$:

\begin{definition}
    Soient $A$, $B$ et $C$ trois points du plan tels que $A \ne B$.
    On note~$H$ le projeté orthogonal de~$C$ sur~$(AB)$. Le \define{produit
    scalaire} de $\vect{AB}$~et~$\vect{AC}$ est le nombre réel noté
    $\vect{AB}\cdot\vect{AC}$ défini par l'une des trois configurations
    possibles:

    \begin{center}
        \leavevmode\kern-2cm
        \begin{tikzpicture}
            \matrix[matrix of nodes,
                anchor=base,
                row 2/.style={every picture/.style={
                                scale=0.7,
                                anchor=center,
                                baseline=(current bounding box),
                            }}] {
            $\vect{AH} = \vect{0}$ &
            $\vect{AB}$ et $\vect{AH}$ de même sens &
            $\vect{AB}$ et $\vect{AH}$ de sens opposé \\
        \begin{tikzpicture}[scale=0.7,rotate=55]
            \path (1,0) coordinate[label={below:$A = H$}] (A) ;
            \path (4,0) coordinate[label=above left:$B$] (B) ;
            \path (1,2) coordinate[label=above:$C$] (C) ;
            \path (A -| C) coordinate (H) ;
            \draw ($ (A)!-0.1!(B) $) -- ($ (A)!1.1!(B) $);
            \draw[dashed] (C) -- (H) pic[perp marker] {} ;
            \draw[->, thick] (A) -- (B);
            \draw[->, thick] (A) -- (C);
        \end{tikzpicture}
        &
        \begin{tikzpicture}[rotate=10]
            \path (1,0) coordinate[label=below:$A$] (A) ;
            \path (4,0) coordinate[label=below:$B$] (B) ;
            \path (3,1.7) coordinate[label=above:$C$] (C) ;
            \path (A -| C) coordinate[label=below:$H$] (H) ;
            \draw ($ (A)!-1/3!(B) $) -- ($ (A)!4/3!(B) $);
            \draw[dashed] (C) -- (H) pic[perp marker] {} ;
            \draw[->, thick] (A) -- (B);
            \draw[->, thick] (A) -- (C);
            \draw[->] (A) -- (H);
        \end{tikzpicture}
        &
        \begin{tikzpicture}[rotate=-10]
            \path (1,0) coordinate[label=above:$A$] (A) ;
            \path (4,0) coordinate[label=above:$B$] (B) ;
            \path (-0.5,-2) coordinate[label=below:$C$] (C) ;
            \path (A -| C) coordinate[label=above:$H$] (H) ;
            \draw ($ (H)!-1/5!(B) $) -- ($ (H)!6/5!(B) $);
            \draw[dashed] (C) -- (H) pic[perp marker] {} ;
            \draw[->, thick] (A) -- (B);
            \draw[->, thick] (A) -- (C);
            \draw[->] (A) -- (H);
        \end{tikzpicture}
        \\
            $\vect{AB}\cdot \vect{AC}= 0$ &
            $\vect{AB}\cdot \vect{AC} = AB\times AH$ &
            $\vect{AB}\cdot \vect{AC} = -AB\times AH$ \\
        };
        \end{tikzpicture}
        \kern-2cm
    \end{center}
\end{definition}


\begin{remark}
    Le produit scalaire de deux vecteurs n'est pas un vecteur mais \emph{un
    nombre réel}.
\end{remark}

\begin{remark}
    Si $A = B$ on définit
    $\vect{AB}\cdot\vect{AC} = \vect{0}\cdot\vect{AC} = 0$.
\end{remark}

\begin{sidefigure}[t,ft]
    \begin{tikzpicture}
        \path (0,0) coordinate[label=below:$A$] (A) ;
        \path (2,0) coordinate[label=below:$B$] (B) ;
        \path ($ (B)!1!-90:(A) $) coordinate[label=above:$C$] (C);
        \path ($ (C)!1!-90:(B) $) coordinate[label=above:$D$] (D);
        \path ($ (A)!1/2!(B) $) coordinate[label=below:$I$] (I);
        \draw (A) -- (I) pic[tick marker] {} -- (B) -- (C) -- (D) -- cycle;
    \end{tikzpicture}
\sidetext
    \begin{example}
        \makelink{ex-proj}
        On considère le carré $ABCD$ de côté $2$ et $I$ le milieu de $[AB]$.
        Déterminer les produits scalaires $\vect{AB}\cdot \vect{AC}$,
        $\vect{AB}\cdot \vect{AD}$ et $\vect{IC}\cdot \vect{BI}$.
        \IfStudentT{\bigfiller{3}}
    \end{example}
\end{sidefigure}
\IfStudentT{\bigfiller{1}}

\subsection{Formule du cosinus}

\begin{theorem}
    \label{thm:ABACcos}
    Soient $A$, $B$, $C$ trois points avec $A \ne B$ et $A \ne C$. Alors:
    \[ \vect{AB}\cdot\vect{AC} = AB \times AC \times \cos \widehat{BAC} \]
\end{theorem}

\begin{proposition}
    Le projeté du vecteur~$\vect{u}$ sur l'axe des~$x$ est
    $r \cos\theta \vect{\imath}$ où $r$~est la longueur du vecteur~$\vect{u}$ et
    $\theta$~est l'angle entre le vecteur~$\vect{u}$ et le
    vecteur~$\vect{\imath}$ qui définit l'axe des abscisses.
\end{proposition}

\begin{example}
    \makelink{ex-cos}
    Dans le triangle~$ABC$, on a $AB=10$, $AC=2$ et $\widehat{BAC} =
    \dfrac{\pi}{3}$.\\
    Calculer $\vect{AB}\cdot\vect{AC}$.
    \IfStudentT{\bigfiller{2}}
\end{example}

\subsection{Formule du produit scalaire dans une base orthonormée}

\begin{proposition}
    Soient $\vect{u}(x;y)$ et $\vect{v}(x';y')$ dans une base orthonormée.
    Alors:
    \[\vect{u}\cdot\vect{v} = xx' + yy'\]
\end{proposition}


\section{Orthogonalité}

\subsection{Définition}

\begin{definition}
    \begin{itemize}
        \item On dit que deux vecteurs non nuls $\vect{u}$~et~$\vect{v}$ sont
            \define[vecteurs orthogonaux]{orthogonaux}, et l'on note
            $\vect{u}\perp\vect{v}$, si et seulement si une mesure de l'angle
            entre $\vect{u}$~et~$\vect{v}$ est $\pm\dfrac{\pi}{2}$.
        \item On décide que le vecteur nul~$\vect{0}$ est orthogonal à tous les
            vecteurs.
    \end{itemize}
\end{definition}

\begin{remark}
    Si $A \ne B$ et $A \ne C$, alors $\vect{AB} \cdot \vect{AC} = 0$ si~et
    seulement si~$\hat{BAC}$ est un angle droit.
\end{remark}

\subsection{Prouver que deux vecteurs sont orthogonaux}

\begin{proposition}
    Deux vecteurs $\vect{u}$~et~$\vect{v}$ sont orthogonaux si et
    seulement si $\vect{u}\cdot\vect{v}=0$.
\end{proposition}

\begin{exercise}*
    \makelink{ex-orth}
    Soient $\point{A}(-1;3)$, $\point{B}(5;1)$, $\point{C}(3;5)$
    et~$\point{D}(6;14)$ dans un repère orthonormé~$(O;I;J)$. Montrer que
    $(AB)$~et~$(CD)$ sont perpendiculaires.
    \IfStudentT{\bigfiller{3}}
\end{exercise}

\subsection{Droites perpendiculaires}

\begin{recall}
    Soit $\mcD$~une droite. Les vecteurs directeurs de~$\mcD$ sont les
    vecteurs~$\vect{AB}$ avec $A \ne B$ deux points de la droite~$\mcD$. Si l'on
    a une équation cartésienne $D:ax + by + c = 0$ alors un vecteur directeur
    de~$\mcD$ est $\vect{u}(-b;a)$. Si $\mcD$~n'est pas verticale, un autre
    vecteur directeur possible est $\vect{v}(1;m)$ où $m$~est le coefficient
    directeur de~$\mcD$.
\end{recall}

\begin{proposition}
    Soient $\mcD$~et~$\mcD'$ deux droites du plan, de vecteurs directeurs
    $\vect{u}$~et~$\vect{u'}$. Les droites $\mcD$~et~$\mcD'$ sont
    perpendiculaires si et seulement si les vecteurs $\vect{u}$~et~$\vect{u'}$
    sont orthogonaux.
\end{proposition}

\section{Propriétés du produit scalaire}

\subsection{Règles de calcul}

\begin{proposition}
    Le produit scalaire est \define{commutatif}:
            $\vect{u}\cdot\vect{v}=\vect{v}\cdot\vect{u}$.
    On dit aussi que le produit scalaire est \define{symétrique}.
\end{proposition}

\begin{proposition}
    On a les propriétés suivantes:
    \begin{enumerate}
        \item Le produit scalaire est \define{distributif} par rapport
            à l'addition:
            \[\vect{u}\cdot(\vect{v}+\vect{w}) =
            \vect{u}\cdot\vect{v} + \vect{u}\cdot\vect{w}\]
        \item Pour deux réels $k$ et $k'$:
            \[ (k\vect{u})\cdot (k'\vect{v}) =
            (k\times k')\vect{u}\cdot \vect{v} \]
    \end{enumerate}
    On dit que le produit scalaire est \define{bilinéaire}.
\end{proposition}

\begin{example}
    Simplifier $\vect{u} \cdot (2\vect{u} + 5 \vect{v})
    - 5 \vect{v}\cdot\vect{u}$.\\
    $\vect{u} \cdot (2\vect{u} + 5 \vect{v})
    - 5 \vect{v}\cdot\vect{u}
    = \vect{u} \cdot (2\vect{u})
    + \vect{u} \cdot (5 \vect{v})
    - 5 \vect{v}\cdot\vect{u}
    = 2 \vect{u}\cdot\vect{u}
    + 5 \vect{u}\cdot\vect{v}
    - 5 \vect{v}\cdot\vect{u}
    = 2 \norm{\vect{u}}^2$
\end{example}


\begin{exercise}*
    \makelink{ex-angle}
    Dans un repère~$(O;I;J)$ orthonormé, on considère
    $\point{B}(5;1)$ et~$\point{L}(2;4)$.
    \begin{enumerate}
        \item Calculer $\vect{OB}\cdot\vect{OL}$.
            \IfStudentT{\bigfiller{1}}
        \item Calculer $OB$ et $OL$.
            \IfStudentT{\bigfiller{2}}
        \item En déduire une mesure de l'angle $\widehat{BOL}$, qu'on donnera
            en degrés arrondi à \<0.1>~près.
            %\IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{exercise}





\end{document}
