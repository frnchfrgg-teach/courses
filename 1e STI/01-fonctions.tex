% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usetikzlibrary{datavisualization,
                datavisualization.formats.functions,
                fit, shapes.geometric}

\newdimen\mywidth

\begin{document}

\StudentMode

\chapter{Généralités sur les fonctions}

\begin{mathhistory}
    En introduisant la géométrie des coordonnées, \bsc{Descartes} le premier
    établit le lien entre deux quantités variables et une équation traduisant
    une relation traduisant une relation de dépendance entre $x$~et~$y$. Une
    fonction est alors associée à une courbe.

    \bsc{Leibniz} introduit le terme fonction en 1673 et la notation~$f(x)$.  À
    la suite d'une correspondance nourrie avec \bsc{Jean Bernoulli}, il donne en
    1718 une définition associant une fonction à son expression. \bsc{Euler}, en
    1748, affine cette notion d'expression, obtenue à partir d'une combinaison
    d'opérations et de modes de calcul connus, et parle d'expression analytique.
\end{mathhistory}

\section{Définitions et notations}

\iffalse
\begin{activity}[Optimiser une casserole]
    Tout d'abord, expérimentons: mesurez la hauteur et le rayon des casseroles
    de votre cuisine. Qu’observez-vous?

    Maintenant, interrogeons-nous: un fabricant de casseroles doit produire des
    casseroles en inox de volume fixé. Quel objectif cherche-t-il à atteindre en
    tant que chef d’entreprise?

    Et enfin, le plus délicat, prouvons que notre observation se justifie
    mathématiquement en démontrant qu’à volume~$V$ fixé, la surface~$S$ de métal
    constituant une casserole est minimale lorsque sa hauteur~$h$ est égale à
    son rayon~$r$:
    \begin{enumerate}
        \item Exprimez le volume~$V$ de la casserole en fonction de $r$~et~$h$.
        \item Déduisez une expression de~$h$ en fonction de $V$~et~$r$.
        \item Justifiez que $S = \pi r^2 + 2\pi r h = \pi r^2 + \frac{2V}{r}$.
        \item On prend ici $V=\SI{1728}{\cm\cubed}$
            \begin{enumerate}
                \item Déterminer, à l'aide d'essais numériques ou à l'aide de la
                    calculatrice graphique, la valeur de~$r$ permettant
                    d'obtenir la surface minimale de métal.
                \item Quelle est alors la valeur de~$h$ ?
            \end{enumerate}
        \item Refaire la question précédente avec $V = \SI{1}{\liter}$.
    \end{enumerate}
\end{activity}
\fi

\subsection{Ensemble de définition, images, antécédents}

\begin{definition}
    Soit $\mcD$~un intervalle de~$\mdR$ (ou une union d'intervalles).
    Une \define{fonction} définie sur~$\mcD$ est un objet mathématique qui à
    chaque nombre réel~$x$ de~$\mcD$ associe un \emph{unique} nombre~$y$, appelé
    \define{image} de~$x$ et noté~$f(x)$ .

    On écrit $f:\mcD\to\mdR, x\mapsto y$.
\end{definition}

\begin{remark}
    \begin{itemize}
        \item $\mcD$~est appelé «ensemble de définition de~$f$»; c'est
            l'ensemble des nombres qui ont une image;
        \item \emph{tous} les nombres de l'ensemble de définition ont une image;
        \item chacun de ces nombres a \emph{une seule} image;
        \item deux nombres peuvent avoir la même image.
    \end{itemize}
\end{remark}

\begin{definition}
    Soit $f$~une fonction définie sur~$\mcD$. On dit que $x\in\mcD$ est un
    \define{antécédent} de~$y$ par~$f$ si $f(x)=y$. Les antécédents
    de~$y$ par~$f$ sont tous les $x\in\mcD$ dont l'image est~$y$.
\end{definition}

\begin{example}
    Soit~$V$ le volume d’un cône de hauteur~$h$ et d’aire de base~$B$. $B$~est
    une fonction de~$V$, dont l’expression algébrique
    est~$\dfrac{3V}{h}$, que l’on peut noter~$f(V)$.

    Si $h$~est fixé et vaut~\SI{2}{\cm}, on peut calculer différentes images et
    les regrouper dans un tableau :
    \[\begin{array}{c*{4}{c}}
        \firsthline
        V    \text{ en \si{\cm\cubed}}   & 1      & 2 & 3      & 4 \\
        f(V) \text{ en \si{\cm\squared}} & \<1,5> & 3 & \<4,5> & 6 \\
        \lasthline
    \end{array}
    \qquad
    f(1) = \dfrac{3 \times 1}{2} = \<1,5> \]

    On peut également calculer un antécédent de~$\<22,5>$, en résolvant
    l'équation $f(V) = \<22,5>$:
    \[ \dfrac{3V}{2} = \<22,5> \iff 3V = \<22,5> \times 2 \iff V =
    \dfrac{45}{3} \iff V = 15 \]

    Un antécédent de~$\<22,5>$ par~$f$ est~$15$.
\end{example}

\subsection{Courbe représentative}

\begin{activity}
\end{activity}

\begin{definition}
    La \define{courbe représentative} ou \define{représentation graphique}
    de~$f$ est l'ensemble (souvent noté~$\mcC_f$) des points de coordonnées
    $\left( x; f(x) \right)$ pour tous les réels~$x$ de l'ensemble de
    définition~$\mcD$.

    Autrement dit, $M \in \mcC_f \iff x_M \in \mcD \text{ et } f(x_M) = y_M$.
    On dit aussi que $\mcC_f$~est la \define{courbe d'équation $y=f(x)$}.
    Voir \cref{fig:courbe}.
\end{definition}

\begin{figure}
    \caption{Représentation graphique d'une fonction.}
    \label{fig:courbe}
    \centering
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={grid={step=1},ticks={major at=1},
                                      unit length=0.8cm},
                            x axis={include value/.list={-4,6}},
                            visualize as smooth line=f,
                        ]
        data [set=f, format=function] {
            var x : interval[-3.5:3.5];
            func y = pow(\value x,3)/4 - 2*\value x + 1;
        }
        ;
    \end{tikzpicture}%
\end{figure}


\begin{example}
    On considère la fonction $g: x\mapsto 2x^2-6$. Le point
    $C(3;12)$~appartient-il à la courbe~$\mcC_g$ ?
\end{example}

On peut voir ces rappels dans la vidéo:
\qrcode[hyperlink,height=1.5cm]{https://www.youtube.com/watch?v=E4SY8_L-DTA}

\end{document}
