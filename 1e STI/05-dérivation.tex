% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}

\NewDocumentCommand\Tx{O{f}m}{\tau_{#1}(#2,h)}

\begin{document}

\StudentMode

\chapter{Dérivation\\Approche graphique}

\bigfiller{12}

%\TeacherMode
\def\Pacome{Nassime}
\begin{figure}
    \centering
    \caption{Évolution de la distance entre \Pacome\ et son point de départ.}
    \label{fig:rando-pacome}
    \begin{tikzpicture}
        \datavisualization [scientific axes={width=8cm, clean},
                            x axis={grid,ticks={step=1,
                                        minor steps between steps=1},
                                    label=Temps en minutes},
                            y axis={grid, ticks={step=100,
                                        minor steps between steps=4},
                                    label=Distance en mètres},
                            visualize as smooth line=f,
                            f={label in data={text=$\mcC_f$,when=x is 8}},
        ]
        data[set=f, format=function] {
            var x : interval[0:10];
            func y = 20 * \value x * (10 - \value x);
        }
        info {
            \IfStudentF{
                %\path[clip] (data bounding box.north west)
                %    rectangle (data bounding box.south east);
                \path[clip] (visualization cs:x=0, y=0)
                    rectangle (visualization cs:x=10, y=500);
                \coordinate (A) at (visualization cs:x=1, y=180);
                \coordinate (D) at (visualization cs:x=2, y=320);
                \coordinate (C) at (visualization cs:x=3, y=420);
                \coordinate (B) at (visualization cs:x=4, y=480);
                \coordinate (X) at (visualization cs:x=3, y=480);
                \foreach \p/\s in {B/dashed,C/densely dotted,D/dash dot} {
                    \draw[\s] ($ (A)!-1!(\p) $) -- ($ (A)!3!(\p) $);
                    \draw (\p) pic[point marker] {} node[anchor=-20] {$\p$};
                }
                \draw ($ (A)!-1!(X) $) -- ($ (A)!3!(X) $);
                \draw (A) pic[point marker] {} node[anchor=-20] {$A$};
            }
        }
        ;
    \end{tikzpicture}
\end{figure}
\begin{activity}
    \Pacome\ part faire une randonnée sur une portion de la route~66, rectiligne
    et sans aucune intersection. Il paramètre son récepteur GPS (ou
    GLONASS) pour qu'à intervalles réguliers ce dernier enregistre dans un
    fichier la position, et le temps écoulé depuis le départ.

    De retour chez lui, \Pacome\ décide de tracer sa distance à vol d'oiseau au
    point de départ, en fonction du temps écoulé. Il obtient le résultat de
    \vref{fig:rando-pacome}, où $f(t)$~est la distance en mètres et $t$~le temps
    en minutes.

    \begin{enumerate}
        \item \begin{enumerate}
                \item Après~\SI{1}{min} de randonnée, à quelle distance \Pacome\
                    était-il de son point de départ ? Et après~\SI{4}{min} ?
                    \IfStudentT{\bigfiller{1}}
                \item Quelle a été la vitesse moyenne de \Pacome, en mètres par
                    minute, entre la 1\iere\ minute et la 4\ieme ? Convertir en
                    \si{\km\per\hour}.
                    \IfStudentT{\bigfiller{2}}
                \item Soient $A(1;180)$~et~$B(4;480)$. Calculer le coefficient
                    directeur de la droite~$(AB)$. Que remarque-t-on ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item Déterminer et interpréter géométriquement la vitesse moyenne de
            \Pacome\ entre $t_0=1$~et~$t_1=3$, puis entre $t_0=1$~et~$t_1=2$.
            \IfStudentT{\bigfiller{4}}
        \item On note~$h = t_1 - t_0$ la durée de mesure de la vitesse moyenne.
            Que tente-t-on de mesurer si l'on prend $h$~de plus en plus petit ?
            Citer des exemples de la vie courante où l'on utilise cette méthode.
            \IfStudentT{\bigfiller{2}}
        \item \begin{enumerate}
                \item Comment déterminer le plus précisément possible la vitesse
                    \emph{instantanée} de \Pacome\ à l'instant~$t=1$ ?
                    À quelle construction géométrique cette vitesse
                    correspond-elle ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item Déterminer le coefficient directeur de la corde~$(EF)$ où
            $E(7;420)$~et~$F(8;320)$. Que remarque-t-on ? Est-ce gênant ?
            \IfStudentT{\bigfiller{3}}
        \item Estimer la vitesse de variation instantanée de la distance à
            l'instant~$t=7$.
            \IfStudentT{\bigfiller{5}}
    \end{enumerate}
\end{activity}

\StudentMode

\section{Taux d'accroissement}

\begin{definition}
    Soient $f$~une fonction définie sur un intervalle~$I$ et $a$~et~$b$ deux
    nombres appartenant à~$I$. On appelle \define{taux d'accroissement} de~$f$
    entre $a$~et~$b$ le nombre réel \UGHOST{$\frac{f(b)-f(a)}{b-a}$.}
\end{definition}

\begin{example}
    Soit $f:\mdR\to\mdR, x \mapsto x^2+1$.
    \begin{enumerate}
        \item Calculer le taux d'accroissement de~$f$ entre $1$~et~$2$.
        \item Même question entre $4$~et~$3$. Remarque ?
        \item Même question entre $-1$~et~$0$. Remarque ?
    \end{enumerate}
    \IfStudentT{\bigfiller{6}}
\end{example}

\begin{remark}
    Le taux d'accroissement de~$f$ entre $a$~et~$b$ est \UGHOST{le coefficient
    directeur} de la corde~$(AB)$ où $A$~est~$B$ sont les points de~$\mcC_f$
    d'abscisses $a$~et~$b$.
\end{remark}

\begin{definition}
    Soient $f$~une fonction définie sur~$I$, $a\in I$ et $h\in\mdR^*$~tel que
    $a+h\in I$. On note~$\Tx{a}$ le taux d'accroissement de~$f$ entre
    $a$~et~$a+h$:
    \[ \GHOST{\Tx{a} = \frac{f(a+h) - f(a)}{h}} \]
\end{definition}

\IfStudentT{\bigfiller{3}}

\section{Nombre dérivé de~$f$ en~$a$}

\subsection{Nombre dérivé}

\begin{definition}
    Soient $f$~une fonction définie sur~$I$ et $a\in I$. On dit que $f$~est
    \define{dérivable en~$a$} si il existe un nombre~$\ell$ dont le taux
    d'accroissement $\Tx{a}$ s'approche aussi près qu'on veut pourvu que $h$
    soit assez proche de~$0$.

    Dans ce cas, $\ell$ est appelé \define{nombre dérivé de~$f$ en~$a$} et est
    noté~$f'(a)$. On écrit:
    \[ \lim_{h\to 0} \frac{f(a+h) - f(a)}{h} = f'(a)
        \qquad \text{ou} \qquad
        \frac{f(a+h) - f(a)}{h} \tendsto[h\to 0] f'(a)
    \]
\end{definition}

\begin{example}[Exercice type]
    Reprenons $f:\mdR\to\mdR, x \mapsto 2x^2+1$. La fonction $f$ est-elle
    dérivable en~$2$ ? Si oui déterminer~$f'(2)$.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{exercise}
    Soit~$g$ définie sur~$\mdR_+$ par $g(t)=\sqrt{t}$
    \begin{enumerate}
        \item En utilisant la quantité conjuguée, montrer que $\sqrt{4+h}-2 =
            \dfrac{h}{\sqrt{4+h}+2}$.
        \item En déduire que $g$~est dérivable en~$4$, et déterminer~$g'(4)$.
    \end{enumerate}
\end{exercise}

\begin{example}
    Soit $f$~définie sur~$\mdR_+$ par $f(x) = \sqrt{x}$. $f$~est définie en~$0$,
    mais n'est pas dérivable en~$0$. En effet:
    \[
        \Tx{0} = \frac{\sqrt{0+h} - \sqrt{0}}{h} = \frac{\sqrt{h}}{h}
               = \frac{1}{\sqrt{h}}
    \]
    Si $h \le \<0.0001>$ alors $\sqrt{h} \le \<0.01>$ donc $\frac{1}{\sqrt{h}}
    \ge 100$; si $h \le 10^{-8}$, $\frac{1}{\sqrt{h}} \ge 10^4$. Plus
    généralement si $h \le 10^k$ alors\footnote{Pour simplifier on suppose que
    $k$~est pair.} $\frac{1}{\sqrt{h}} \ge 10^{k/2}$: Plus $h$ est petit, plus
    $\Tx{0}$ devient grand et finit par dépasser puis s'éloigner de n'importe
    quel nombre.
\end{example}

\subsection{Nombre dérivé et tangente}

Soit $f$ une fonction définie sur~$I$ et dérivable en~$a \in I$. On fixe
$A\left( a \mathrel; f(a) \right) \in \mcC_f$. Si $h \ne 0$ et $a + h \in I$,
alors on peut définir $M\left( a+h \mathrel; f(a+h) \right)$ qui est un point
de~$\mcC_f$ distinct de~$A$. Le taux d'accroissement~$\Tx{a}$ est le
coefficient de la droite~$(AM)$ --- on dit aussi la corde\footnote{Pensez à
l'expression « prendre un virage à la corde ».}~$(AM)$. Voir
\vref{fig:corde+tangente-loin}.

Lorsque $h$~se rapproche de~$0$, le point~$M$ se rapproche du point~$A$ et la
corde~$(AM)$ ressemble de plus en plus à une droite tangente à~$\mcC_f$ en~$A$
--- voir \vref{fig:corde+tangente-pres}.
Le coefficient directeur de cette tangente est la valeur limite de~$\Tx{a}$
quand $h$~tend vers~$0$ --- c'est-à-dire~$f'(a)$.

\begin{figure}
    \def\myfigure#1{%
        \begin{tikzpicture}[
                declare function={
                    a = 1.5;
                    h = #1;
                    f(\x) = 1 / (\x + 1.5) + pow(\x, 2) / 8;
                    fp(\x) = -1 / pow(\x + 1.5, 2) + \x / 4;
                    t(\x) = fp(a) * (\x - a) + f(a);
                    d = (f(a+h) - f(a)) / h;
                    c(\x) = d * (\x - a) + f(a);
                }
            ]
            \datavisualization [school book axes,
                                all axes={unit length=0.65cm},
                                x axis={ticks and grid={
                                    major at={(a) as $a$, (a+h) as $a+h$}},
                                    label=$x$},
                                y axis={ticks and grid={none}, label=$y$},
                                visualize as smooth line=f,
                                f={style=thin, label in data={
                                        text=$\mcC_f$,when=x is -0.8}},
                                visualize as line=t,
                                visualize as line=c,
                                c={style=densely dashed},
            ]
            data[set=f, format=function] {
                var x : interval[-1:6] samples 70;
                func y = f(\value x);
            }
            data point[set=t, x=-0.5, y=(t(-0.5))]
            data point[set=t, x=6,    y=(t(6))]
            data point[set=c, x=0.5, y=(c(0.5))]
            data point[set=c, x=(a), y=(f(a)), name=A]
            data point[set=c, x=(a+h), y=(f(a+h)), name=B]
            data point[set=c, x=6, y=(c(6))]
            info {
                \foreach \x in {A,B} {
                    \path (\x) node[above left] {$\x$} pic[point marker]{};
                }
            }
            ;
        \end{tikzpicture}%
    }
    \ffigbox{}{
        \begin{subfloatrow}
            \ffigbox{}{\caption{}\label{fig:corde+tangente-loin}\myfigure{3.5}}
            \ffigbox{}{\caption{}\label{fig:corde+tangente-pres}\myfigure{0.7}}
        \end{subfloatrow}
        \caption{Interprétation graphique du taux d'accroissement et du nombre
            dérivé. $\Tx{a}$~est le coefficient directeur de la corde~$(AB)$.
            \subref{fig:corde+tangente-loin} $h$~est relativement grand et
            $(AB)$~n'est pas proche de la tangente.
            \subref{fig:corde+tangente-pres} $h$~est plus petit et
            $(AB)$~ressemble nettement plus à la tangente.}
        \label{fig:corde+tangente}
    }
\end{figure}


Puisque c'est difficile de définir correctement ce qu'est une tangente à une
courbe quelconque, on va se servir de ce résultat comme d'une définition:

\medskip

\begin{definition}
    Soit $f$ une fonction définie sur~$I$ et dérivable en~$a \in I$. Le
    coefficient directeur de la \define{tangente à~$\mcC_f$ au point
    d'abscisse~$a$} est le nombre dérivé~$f'(a)$.
\end{definition}

\medskip


\begin{proposition}
    Soit $f$ une fonction définie sur~$I$ et dérivable en~$a \in I$. Une
    équation de la tangente à~$\mcC_f$ au point d'abscisse~$a$ est
    \[ \UGHOST{ y = f'(a) (x - a) + f(a) } \]
\end{proposition}

\clearpage

\begin{proof}
    \UGHOST{%
    Notons $A\left( a \mathrel; f(a) \right)$. La tangente~$\mcT$ à~$\mcC_f$
    en~$A$ a pour coefficient directeur~$f'(a)$: elle admet une équation de la
    forme $y = mx + p$ avec $m = f'(a)$. Or $A\in\mcT$ donc $y_A = m x_A + p$,
    c'est-à-dire $f(a) = m a + p$. Mais alors $p = f(a) - m a = f(a) - f'(a) a$
    donc une équation de~$\mcT$ est $y = f'(a)x + f(a) - f'(a) a$. Il reste à
    regrouper le premier et le dernier terme, et factoriser par~$f'(a)$.
    }
\end{proof}

\begin{example}[Exercice type]
    \label{ex:tangent-eq}
    Reprenons $f:\mdR\to\mdR, x \mapsto x^2+1$. Déterminer une équation de la
    tangente à~$\mcC_f$ au point d'abscisse~$2$.
    \IfStudentT{\bigfiller{12}}
\end{example}


\section{Fonction dérivée}

\begin{activity}[Avec \bsc{Geogebra}]
    On considère la fonction $f:\mdR\to\mdR,x\mapsto x^2+1$.
    \begin{enumerate}
        \item \begin{enumerate}
                \item Tracer la fonction~$f$ dans \bsc{Geogebra}.
                \item Placer un point~$A$ sur~$\mcC_f$ --- dans un premier temps
                    le positionner à l'abscisse~$2$.
            \end{enumerate}
        \item \begin{enumerate}
                \item Construire la tangente~$\mcT$ à~$\mcC_f$ en~$A$.
                \item À l'aide du graphique, déterminer une équation réduite
                    de~$\mcT$.
                    \IfStudentT{\bigfiller{1}}
                \item Comparer avec le résultat trouvé à \vref{ex:tangent-eq}.
                    \IfStudentT{\bigfiller{1}}
            \end{enumerate}
        \item Ajouter un objet «pente» à la tangente~$\mcT$ --- on
            l'appellera~$m$. Retrouve-t-on bien le nombre dérivé de~$f$ en~$2$ ?
            \IfStudentT{\bigfiller{1}}
        \item \begin{enumerate}
                \item Créer un point~$B$ de même abscisse que~$A$ et
                    d'ordonnée~$m$.
                \item Déplacer~$A$. Que se passe-t-il ? Confirmer en activant la
                    trace de~$B$.
                    \IfStudentT{\bigfiller{2}}
                \item De quelle fonction le point~$B$ semble-t-il décrire la
                    courbe ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item Refaire les questions avec $g:\mdR\to\mdR,x\mapsto x^3$ en lieu et
            place de~$f$ --- hormis les calculs de nombre dérivé et d'équation
            de tangente.
            \IfStudentT{\bigfiller{4}}
    \end{enumerate}
\end{activity}

\vspace*{2cm}

\subsection{Dériver sur un intervalle}

\begin{definition}
    Soit~$f$ une fonction définie sur un intervalle~$I$. On dit que $f$~est
    \define{dérivable sur~$I$} si $f$~est dérivable \UGHOST{en tout~$a\in I$.}
    Dans ce cas, la fonction définie sur~$I$ qui à chaque~$a\in I$ associe
    \UGHOST{le nombre dérivé de~$f$ en~$a$ est appelée \define{fonction dérivée
    de~$f$} et est notée~$f'$. En d'autres termes $f' : a \mapsto f'(a)$.}
\end{definition}

\begin{remark}
    $f'$~est une fonction définie de manière compliquée, presque algorithmique:
    pour calculer l'image de~$1$ par~$f'$ on calcule le taux
    d'accroissement~$\Tx{1}$ puis on cherche la limite quand~$h\to0$. Et on
    répète la procédure pour déterminer chaque image !
\end{remark}

\begin{example}
    Soient $f$~définie sur~$\mdR$ par $f(x) = 42$ et $a\in\mdR$.
    \begin{enumerate}
        \item Calculer $\Tx{a}$. Que remarque-t-on ?
            \IfStudentT{\bigfiller{1}}
        \item $f$~est-elle dérivable en~$a$ ? Si oui, déterminer~$f'(a)$.
            \IfStudentT{\bigfiller{2}}
        \item Quelle formule pourrait-on proposer pour~$f'$ --- sans passer par
            le taux d'accroissement de~$f$ ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{example}
    Soit $g$~une fonction affine définie sur $\mdR$: $g(x) = mx + p$. Montrer
    que $g$~est dérivable sur~$\mdR$ et que $g'(a) = m$ pour tout~$a \in \mdR$.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{theorem}[Dérivée de la fonction carré]
    \label{thm:diff-x2}
    Soit $f:\mdR\to\mdR, x \mapsto x^2$. La fonction~$f$ est dérivable
    sur~$\mdR$ et pour tout~$a\in\mdR$, $f'(a) = 2a$.
\end{theorem}

\begin{proof}
    \UGHOST{%
    Soit $a\in\mdR$. $f(a) = a^2$ et $f(a+h) = (a+h)^2 = a^2 + 2ah + h^2$. Ainsi
    $f(a+h) - f(a) = 2ah + h^2 = h(2a + h)$. Mais alors $\Tx{a} =
    \frac{f(a+h) - f(a)}{h} = 2a + h$. En définitive, $\lim_{h\to 0} \Tx{a} =
    2a$: la fonction~$f$ est dérivable en~$a$ et~$f'(a) = 2a$. C'est vrai pour
    tout~$a\in\mdR$ et on a notre résultat.%
    }
\end{proof}

\subsection{Dérivées usuelles}

\begin{theorem}
    \label{thm:common-derivatives}
    \leavevmode
    \begin{center}
    $\begin{array}{L*{3}{cL}}
        \firsthline
        \text{Fonction} & \text{Dérivable sur} & \text{Fonction dérivée} \\
        \hline
        f(x) = k \text{ avec $k \in \mdR$} & \mdR & f'(x) = 0 \NL
        f(x) = x & \mdR & f'(x) = 1 \NL
        f(x) = mx + p \text{ avec $m,p \in \mdR$} & \GHOST{\mdR} &
                f'(x) = \GHOST{m} \NL
        f(x) = x^2 & \GHOST{\mdR} & f'(x) = \GHOST{2x} \NL
        f(x) = x^n \text{ avec $n \ge 0$ } & \GHOST{\mdR} &
                f'(x) = \GHOST{nx^{n-1}} \NL
        f(x) = \dfrac{1}{x} & \GHOST{\begin{gathered}
                    \IN]-\infty;0;[ \text{ ou } \\
                    \IN]0;+\infty;[ \end{gathered}} &
                f'(x) = \GHOST{-\dfrac{1}{x^2}} \NL
        f(x) = \sqrt{x} & \GHOST{\IN]0;+\infty;[} &
                f'(x) = \GHOST{\dfrac{1}{2\sqrt{x}}} \\
        \lasthline
    \end{array}$
    \end{center}
\end{theorem}

\begin{proof}
    On a déjà démontré les quatre premières lignes; les autres seront admises ou
    vues en exercice.
\end{proof}

\begin{example}
    La fonction $f:x\mapsto x^5$ est dérivable sur~$\mdR$ et pour
    tout~$x\in\mdR$, $f'(x) = 5 x^{5-1} = 5x^4$.
\end{example}

\begin{exercise}[Dérivée et tangente de la fonction inverse]
    52 p.~79
\end{exercise}

\subsection{Théorèmes opératoires}

\begin{theorem}
    Si $u$ est une fonction dérivable sur~$I$ et $k \in \mdR$ est une constante,
    alors $f = k u$~est dérivable sur~$I$ et $f' = k u'$.
\end{theorem}

\begin{remark}
    $f = k u$ veut dire $f(x) = k \times u(x)$ pour tout~$x \in I$. De même
    $f' = k u'$ signifie $\forall x\in I, f'(x) = k u'(x)$.
\end{remark}

\begin{proof}
    Soit $a \in I$. On a $\frac{f(a+h) - f(a)}{h} = \frac{ku(a+h) - ku(a)}{h} =
    k \frac{u(a+h)-u(a)}{h}$. Or $\lim_{h\to0} \frac{u(a+h) - u(a)}{h} = u'(a)$
    donc $\lim_{h\to0} \frac{f(a+h) - f(a)}{h} = k u'(a)$. En définitive
    $f$~est dérivable en~$a$ et $f'(a) = ku'(a)$.
\end{proof}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f = u + v$~est
    dérivable sur~$I$ et $f' = u' + v'$.
\end{theorem}

\begin{remark}
    $f = u + v$ veut dire $f(x) = u(x) + v(x)$ pour tout~$x \in I$.
\end{remark}

\begin{proof}
    \GHOST{%
    Soit $a \in I$.
    \begin{align*}
        \frac{f(a+h) - f(a)}{h} &= \frac{\left( u(a+h) + v(a+h) \right) -
                                         \left( u(a) + v(a) \right)}{h} \\
                                &= \frac{u(a+h) - u(a) + v(a+h) - v(a)}{h} \\
                                &= \frac{u(a+h) - u(a)}{h} +
                                   \frac{v(a+h) - v(a)}{h}
    \end{align*}
    $\frac{u(a+h) - u(a)}{h}$~tend vers~$u'(a)$ tandis que $\frac{v(a+h) -
    v(a)}{h}$~tend vers~$v'(a)$. La somme tend ainsi vers $u'(a) + v'(a)$: la
    fonction~$f$ est dérivable en~$a$ et $f'(a) = u'(a) + v'(a)$.
    }
\end{proof}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = x^3 + \frac{4}{x} +
    2\sqrt{x}$. Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa
    fonction dérivée.

    \UGHOST{%
    Rédaction type: $f$ est dérivable car $x\mapsto x^3$, $x\mapsto\frac{1}{x}$
    et $x\mapsto\sqrt{x}$ le sont et $f'(x) = 2x^3 + 4 \times \frac{-1}{x^2} + 2
    \times \frac{1}{2\sqrt{x}} = 2x^3 - \frac{4}{x^2} + \frac{1}{\sqrt{x}}$.%
    }
\end{example}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f=uv$~est
    dérivable sur~$I$ et $f' = u'v + uv'$.
\end{theorem}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = (3x + 2)(\sqrt{x} + 1)$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa fonction
    dérivée.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{exercise}
    65 p.~80
\end{exercise}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, et \emph{si $v(x) \neq
    0$ pour tout~$x\in I$}, alors $f=\dfrac{u}{v}$~est dérivable sur~$I$ et $f'
    = \dfrac{u'v - uv'}{v^2}$.
\end{theorem}

\IfStudentT{\bigfiller{7}}

\end{document}
