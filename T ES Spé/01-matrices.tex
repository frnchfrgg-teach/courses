% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{datavisualization,
                datavisualization.formats.functions}
\usepackage{tkz-tab}

\usepackage{xparse}

\def\matzero{\mbfzero}
\NewDocumentCommand\matrixterms{mO{i}mO{j}m}{%
    \left( #1_{#2,#4} \right)_{\substack{%
        1 \le #2 \le #3 \\
        1 \le #4 \le #5
    }}%
}

\begin{document}

\StudentMode

\chapter{Matrices}

\chapterquote{La Matrice est universelle. Elle est omniprésente. Elle est avec
nous ici, en ce moment même.}{Morpheus}

\begin{activity}
    Quatre imprimeurs A, B, C et D proposent des tarifs différents à leur
    clients, qu'on résume dans les tableaux ci-dessous. Tous les prix sont
    exprimés par feuille imprimée:

    \begin{sidebyside}{2}
        Impression standard papier satiné:
        \begin{tabular}{l*{3}{c}}
            \firsthline
                & A5 & A4 & A3 \\
            \hline
            A & \SI{0.10}{\euro} & \SI{0.25}{\euro} & \SI{0.55}{\euro} \\
            B & \SI{0.15}{\euro} & \SI{0.20}{\euro} & \SI{0.40}{\euro} \\
            C & \SI{0.20}{\euro} & \SI{0.20}{\euro} & \SI{0.25}{\euro} \\
            D & \SI{0.25}{\euro} & \SI{0.20}{\euro} & \SI{0.20}{\euro} \\
            \lasthline
        \end{tabular}

        Supplément plastification:
        \begin{tabular}{l*{3}{c}}
            \firsthline
                & A5 & A4 & A3 \\
            \hline
            A & \SI{0.15}{\euro} & \SI{0.30}{\euro} & \SI{0.95}{\euro} \\
            B & \SI{0.15}{\euro} & \SI{0.25}{\euro} & \SI{0.70}{\euro} \\
            C & \SI{0.20}{\euro} & \SI{0.20}{\euro} & \SI{0.45}{\euro} \\
            D & \SI{0.20}{\euro} & \SI{0.20}{\euro} & \SI{0.20}{\euro} \\
            \lasthline
        \end{tabular}
    \end{sidebyside}

    \begin{enumerate}
        \item Quand est-il intéressant de prendre l'imprimeur~A ? Et le~C ?
            \IfStudentT{\bigfiller{2}}
        \item Ce mois-ci, les quatre imprimeurs font une réduction
            de~\SI{20}{\percent} sur la plastification.
            Déterminer la nouvelle
            grille tarifaire pour la plastification.
            \IfStudentT{\bigfiller{5}}
    \end{enumerate}

    Mathématiquement, on représente ces données par une \emph{matrice}.
\end{activity}

\section{Définitions}

\begin{definition}
    Soient $n>0$ et $p>0$ deux entiers naturels non nuls.
    On appelle \define{matrice} de taille $n \times p$ un tableau de $np$ nombres
    réels organisés en $n$~lignes et $p$~colonnes. On la note
    \[
        M = \matrixterms{a}{n}{p} = \begin{bmatrix}
                a_{1,1} & a_{1,2} & \cdots & a_{1,p} \\
                a_{2,1} & \ddots  &        & a_{2,p} \\
                \vdots  &         & \ddots & \vdots  \\
                a_{n,1} & a_{n,2} & \cdots & a_{n,p}
            \end{bmatrix}
        \quad\text{ou}\quad
        M = \begin{pmatrix}
                a_{1,1} & a_{1,2} & \cdots & a_{1,p} \\
                a_{2,1} & \ddots  &        & a_{2,p} \\
                \vdots  &         & \ddots & \vdots  \\
                a_{n,1} & a_{n,2} & \cdots & a_{n,p}
            \end{pmatrix}
    \]
\end{definition}

\begin{example}
    \label{ex:prix}
    Ici on peut noter $S$~la matrice des prix d'impression standard et $P$~celle
    des tarifs de plastification.

    Alors $S = \begin{bmatrix}
        \<0.1>  & \<0.25> & \<0.55> \\
        \<0.15> & \<0.2>  & \<0.4>  \\
        \<0.2>  & \<0.2>  & \<0.25> \\
        \<0.25> & \<0.2>  & \<0.2>
    \end{bmatrix}$
    et
    $P = \begin{bmatrix}
        \<0.15> & \<0.3>  & \<0.95> \\
        \<0.15> & \<0.25> & \<0.7>  \\
        \<0.2>  & \<0.2>  & \<0.45> \\
        \<0.2>  & \<0.2>  & \<0.2>
    \end{bmatrix}$

    \begin{enumerate}
        \item Quelles sont les tailles de ces matrices ?
            \IfStudentT{\hfiller}
        \item On note $S = \matrixterms{s}{n}{p}$. Donner les valeurs $s_{2,2}$,
            $s_{2,3}$ et~$s_{4,1}$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{definition}
    Soient $n>0$ et $p>0$ deux entiers naturels non nuls, et $M$ une matrice de
    taille $n\times p$.
    \begin{itemize}
        \item Lorsque $n = p$, on dit que $M$ est une \define{matrice carré}
            d'ordre~$n$.
        \item Lorsque $n = 1$, on dit que $M$ est une \define{matrice ligne}.
        \item Lorsque $p = 1$, on dit que $M$ est une \define{matrice colonne}.
    \end{itemize}
\end{definition}

\begin{definition}
    Deux matrices $A$~et~$B$ sont \define{égales} si (et seulement si) elles ont
    la même taille et si leurs coefficients situés à la même place sont deux à
    deux égaux.

    En d'autres termes, $A = B$ si $A = \matrixterms{a}{n}{p}$ et $B =
    \matrixterms{b}{n}{p}$ avec $a_{1,1} = b_{1,1}$, $a_{2,1} = b_{2,1}$, \dots,
    $a_{i,j} = b_{i,j}$ pour tous $i,j$.
\end{definition}

\begin{example}
    On considère les matrices $A = \begin{bmatrix}
        2x  & 3 \\
        -38 & y+1 
    \end{bmatrix}$ et $B = \begin{bmatrix}
        26  & 3 \\
        1-3x & 5 
    \end{bmatrix}$.

    Déterminer $x$~et~$y$ pour que $A$~et~$B$ soient égales.
    \IfStudentT{\bigfiller{4}}
\end{example}

\section{Opérations sur les matrices}

\subsection{Multiplication scalaire}

\begin{definition}
    Si $M$~est une matrice et~$k\in\mdR$, on définit la matrice~$kM$ de même
    taille dont tous les coefficients sont multipliés par~$k$. Cette opération
    est appelée \define{multiplication scalaire}, ou encore
    \define{multiplication par un réel}.
\end{definition}

\begin{example}
    En reprenant les matrices $S$~et~$P$ de \cref{ex:prix}, donner une formule
    pour définir $P'$~la matrice des prix de plastification avec réduction
    de~\SI{20}{\percent}. Indiquer le résultat.
    \IfStudentT{\bigfiller{4}}
\end{example}

\subsection{Addition}

\begin{activity}
    En reprenant les données de l'imprimeur précédent, construire la grille
    tarifaire d'une impression plastifiée sans réduction (impression et
    plastification comprises). Et avec la réduction ?
\end{activity}

\begin{definition}
    Si $A$~et~$B$ sont deux matrices \emph{de même taille}, alors la
    \define{matrice somme} $A + B$ est la matrice obtenue en ajoutant deux à
    deux les coefficients qui occupent la même position.

    Autrement dit:
    si $A = \begin{bmatrix}
                a_{1,1} & \cdots & a_{1,p} \\
                \vdots  &        & \vdots  \\
                a_{n,1} & \cdots & a_{n,p}
            \end{bmatrix}$
    et $B = \begin{bmatrix}
                b_{1,1} & \cdots & b_{1,p} \\
                \vdots  &        & \vdots  \\
                b_{n,1} & \cdots & b_{n,p}
            \end{bmatrix}$ \\
    alors $A+B = \begin{bmatrix}
                a_{1,1}+b_{1,1} & \cdots & a_{1,p}+b_{1,p} \\
                \vdots  &        & \vdots  \\
                \vdots  &        & \vdots  \\
                a_{n,1}+b_{n,1} & \cdots & a_{n,p}+b_{n,p}
            \end{bmatrix}$
\end{definition}

\begin{example}
    On note $T$~la matrice des prix cumulés (impression et plastification).
    \begin{enumerate}
        \item Écrire~$T$ en fonction de $S$~et~$P$, puis la calculer.
            \IfStudentT{\bigfiller{4}}
        \item Même question pour~$T'$ avec la réduction.
            %\IfStudentT{\bigfiller{4}}
    \end{enumerate}
\end{example}

\begin{proposition}
    Soient $A$, $B$, et~$C$ trois matrices de même taille. Alors:
    \begin{itemize}
        \item La matrice~$A + B$ a la même taille que~$A$ et que~$B$.
        \item La matrice nulle~$\matzero$ (de même taille que $A$) dont tous les
            coefficients sont nuls est \emph{neutre} pour l'addition:
            $A + \matzero = \matzero + A = A$.
        \item L'addition de matrices est \emph{commutative}: $A + B = B + A$.
        \item L'addition de matrices est \emph{associative}: $(A+B) + C = A +
            (B+C)$.
    \end{itemize}
\end{proposition}

\begin{remark}
    L'opposé de la matrice~$M$ est la matrice qui donne~$\matzero$ quand on
    l'ajoute à~$M$. On la note~$-M$, et on l'obtient en prenant l'opposé de tous
    les coefficients de~$M$. Autrement dit, $-M = (-1)M$. Soustraire une
    matrice c'est ajouter son opposé.
\end{remark}

\subsection{Multiplication des matrices}

\begin{activity}
    \label{activ:matrix-mult}
    Un client a besoin de faire imprimer $250$ feuilles~A5, $250$ feuilles~A4 et
    $85$ feuilles~A3. Calculer le prix qu'il devra payer pour chacun des
    imprimeurs, en expliquant la démarche.
    \IfStudentT{\bigfiller{9}}
\end{activity}


\begin{definition}
    Soient $A = \begin{bmatrix}
                a_{1,1} & \cdots & a_{1,n}
            \end{bmatrix}$
    une matrice ligne et $B = \begin{bmatrix}
                b_{1,1} \\ \vdots \\ b_{n,1}
            \end{bmatrix}$
    une matrice colonne.
    Le produit de $A$~par~$B$ est la matrice de taille~$1\times1$ (avec un seul
    coefficient):
    \[
        A \times B = \begin{bmatrix}
                a_{1,1} & \cdots & a_{1,n}
            \end{bmatrix} \times \begin{bmatrix}
                b_{1,1} \\ \vdots \\ b_{n,1}
            \end{bmatrix}
        = \begin{bmatrix}
            a_{1,1} \times b_{1,1} +
            a_{1,2} \times b_{2,1} +
            a_{1,3} \times b_{3,1} +
            \dots
        \end{bmatrix}
    \]
\end{definition}

\clearpage

\begin{definition}
    Si $A$~est de taille~$n\times p$ et $B$~est de taille~$p\times q$, on
    définit la matrice $AB$, de taille~$n\times q$, comme suit:

    Si $A = \begin{bmatrix}
                a_{1,1} & \cdots & a_{1,p} \\
                \vdots  &        & \vdots  \\
                a_{n,1} & \cdots & a_{n,p}
            \end{bmatrix}$
    et $B = \begin{bmatrix}
                b_{1,1} & \cdots & b_{1,q} \\
                \vdots  &        & \vdots  \\
                b_{p,1} & \cdots & b_{p,q}
            \end{bmatrix}$
    alors $AB = \begin{bmatrix}
                c_{1,1} & \cdots & c_{1,q} \\
                \vdots  &        & \vdots  \\
                c_{n,1} & \cdots & c_{n,q}
            \end{bmatrix}$
    où $c_{i,j}$ est le coefficient obtenu en multipliant la
    $i$\nobreakdash ème ligne de~$A$ avec la $j$\nobreakdash ème colonne de~$B$:
    $\displaystyle c_{i,j} = \sum_{k=1}^p a_{i,k} b_{k,j} = a_{i,1} b_{1,j} +
    a_{i,2} b_{2,j} + \dots + a_{i,p} b_{p,j}$
\end{definition}

\newsavebox\matA
\newsavebox\matB
\newsavebox\matC
\begin{remark}
    En pratique, on n'apprend pas pas cœur les formules précédentes, mais on
    utilise le moyen mnémotechnique qu'est la disposition de calcul suivante:
    \sbox\matA{$\begin{bmatrix}
            a_{1,1} & \cdots  & \cdots & a_{1,p} \\
            \vdots  &         &        & \vdots  \\
            a_{i,1} & a_{i,2} & \cdots & a_{i,p} \\
            \vdots  &         &        & \vdots  \\
            a_{n,1} & \cdots  & \cdots & a_{n,p}
    \end{bmatrix}$}
    \sbox\matB{$\begin{bmatrix}
            b_{1,1} & \cdots & b_{1,j} & \cdots & b_{1,q} \\
            \vdots  &        & b_{2,j} &        & \vdots  \\
            \vdots  &        & \vdots  &        & \vdots  \\
            b_{p,1} & \cdots & b_{p,j} & \cdots & b_{p,q}
    \end{bmatrix}$}
    \sbox\matC{$\begin{bmatrix}
            c_{1,1} & \cdots & \cdots  & \cdots & c_{1,q} \\
            \vdots  &        & \vdots  &        & \vdots  \\
            \vdots  & \cdots & c_{i,j} & \cdots & \vdots  \\
            \vdots  &        & \vdots  &        & \vdots  \\
            c_{n,1} & \cdots & \cdots  & \cdots & c_{n,q}
    \end{bmatrix}$}
    \[
    \cellprops{ array td { padding: 0pt } }
    \begin{array}{ccl}
        &
        \usebox\matB
        &
        \left. \vrule width 0pt height \ht\matB \right\}
        \text{\small $p$ lignes}
        \\
        \underbrace{\usebox\matA}_{\text{\small $p$ colonnes}}
        &
        \underbrace{\usebox\matC}_{\text{\small $q$ colonnes}}
        &
        \left. \vrule width 0pt height \ht\matC \right\}
        \text{\small $n$ lignes}
    \end{array}
    \]
    Avec cette disposition, on voit bien que le nombre de lignes du résultat est
    celui de~$A$, et le nombre de colonnes du résultat vient de~$B$. Le produit
    n'existe que si le nombre de colonnes de~$A$ et le nombre de lignes de~$B$
    sont égaux: pour calculer~$c_{i,j}$ on multiplie chaque nombre de la ligne
    de~$A$ à la hauteur de~$c_{i,j}$ avec un nombre de la colonne de~$B$
    exactement au dessus de~$c_{i,j}$, puis on somme ces $p$~résultats entre
    eux.
\end{remark}

\begin{example}
    Poser et calculer le produit $
    \begin{bmatrix}
        1 & 2 & 3 \\
        2 & 3 & 4
    \end{bmatrix}
    \begin{bmatrix}
        1 & 2 \\
        3 & 4 \\
        5 & 6
    \end{bmatrix}$
    \IfStudentT{\bigfiller{5}}
\end{example}

\begin{example}
    En définissant une matrice~$Q$ indiquant les quantités des différentes
    feuilles dont le client a besoin, déterminer une formule qui donne les prix
    proposés pour toute la commande selon les imprimeurs. Poser le calcul, et
    comparer le résultat à \vref{activ:matrix-mult}.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{remark}
    Contrairement à la somme des matrices, le produit ne s'effectue pas en
    multipliant deux à deux les coefficients qui sont à la même position. Cette
    multiplication ne serait utile ni aux mathématiciens, ni pour traduire les
    situations pratiques.
\end{remark}

\begin{remark}
    Si $A$~et~$B$ sont deux matrices, le produit~$AB$ n'est généralement pas
    égal au produit~$BA$:
    \begin{enumerate}
        \item Soient $A = \begin{bmatrix}
                1 & 2 \\
                3 & 4 \\
                5 & 6
            \end{bmatrix}$
            et $B = \begin{bmatrix}
                7 & 8 \\
                9 & 10
            \end{bmatrix}$;
            peut-on calculer~$AB$? Et~$BA$?
            \IfStudentT{\bigfiller{2}}
        \item Soient $A = \begin{bmatrix}
                1 & 2 \\
                3 & 4
            \end{bmatrix}$
            et $B = \begin{bmatrix}
                1 & 1 \\
                2 & 2
            \end{bmatrix}$;
            calculer $AB$~puis~$BA$.
            \IfStudentT{\bigfiller{4}}
    \end{enumerate}
\end{remark}

\begin{remark}
    La propriété \emph{un produit est nul si et seulement si un des facteurs est
    nul} n'est pas vraie pour les matrices. On peut avoir $AB=\matzero$ même si
    $A\ne\matzero$ et $B\ne\matzero$:

    Soient $A = \begin{bmatrix}
        4 & -1 \\
        8 & -2
    \end{bmatrix}$
    et $B = \begin{bmatrix}
        5 & 8 \\
        20 & 32
    \end{bmatrix}$; calculer~$AB$.
    \IfStudentT{\bigfiller{4}}
\end{remark}

\subsection{Utilisation de \bsc{Maxima}}

Dans \bsc{Maxima}, on définit la matrice $A = \begin{bmatrix}
                1 & 2 & 3 \\
                4 & 5 & 6 \\
                7 & 8 & 9
            \end{bmatrix}$ par la commande:\\
\verb|A : matrix([1, 2, 3], [4, 5, 6], [7, 8, 9])|

On écrit le produit~$AB$ en utilisant le point: \verb|A.B| pour éviter la
confusion avec la variable nommée \verb|AB|.

Le produit $A^5 = A A A A A$ s'écrit \verb|A ^^ 5| --- à ne pas confondre avec
$5^6$ qui s'écrit \verb|5 ^ 6|. Malheureusement, \verb|A^5| existe dans
\bsc{Maxima}, mais ne calcule la puissance chaque coefficient pris séparément,
ce qui n'a aucun intérêt pour nous. Attention à ne pas oublier les deux
circonflexe, sinon le résultat sera faux.


\section{Matrice inverse}

\subsection{Matrice identité (ou unité)}

\begin{activity}
    Calculer les produits suivants:
    \begin{enumerate}[gathered]
        \item $\begin{bmatrix}
                4 & -1 \\
                8 & -2
            \end{bmatrix}
            \begin{bmatrix}
                1 & 0 \\
                0 & 1
            \end{bmatrix}$
        \item $\begin{bmatrix}
                1 & 0 \\
                0 & 1
            \end{bmatrix}
            \begin{bmatrix}
                4 & -1 \\
                8 & -2
            \end{bmatrix}$
        \item $\begin{bmatrix}
                1 & 0 & 0 \\
                0 & 1 & 0 \\
                0 & 0 & 1
            \end{bmatrix}
            \begin{bmatrix}
                1  & 2 \\
                -1 & -2 \\
                3  & 1
            \end{bmatrix}$
    \end{enumerate}
\end{activity}

\begin{definition}
    On appelle \define{matrice identité} d'ordre~$n$ (ou encore matrice unité
    d'ordre~$n$) la matrice carrée~$I_n = \matrixterms{a}{n}{n}$ définie par
    $a_{i,j} = 1$ si $i=j$ et $a_{i,j} = 0$ sinon. C'est la matrice de taille $n
    \times n$ avec des zéros partout sauf pour les nombres de la diagonale qui
    sont des un.
\end{definition}

\begin{proposition}
    \begin{enumerate}
        \item Si $A$~a $n$ colonnes, alors $A I_n = A$.
        \item Si $B$~a $n$ lignes, alors $I_n B = B$.
    \end{enumerate}
\end{proposition}

\subsection{Matrice inverse}

\begin{activity}
    \begin{enumerate}
        \item On pose $a = 5$. Peut-on trouver~$a'$ tel que $a + a' = 0$ ?
            Comment s'appelle le nombre~$a'$ ?
        \item On pose $b = 8$. Peut-on trouver~$b'$ tel que $b \times b' = 1$ ?
            Comment s'appelle le nombre~$b'$ ?
        \item Les nombres réels ont-ils tous un opposé ? Un inverse ?
        \item On pose $A = \begin{bmatrix}
                1 & 0 \\
                2 & 1
            \end{bmatrix}$.
            Peut-on trouver~$A'$ tel que $A A' = I_n$ ?
        \item Et avec $B = \begin{bmatrix}
                0 & 2 \\
                2 & 0
            \end{bmatrix}$ ?
            Et $C = \begin{bmatrix}
                1 & 2 \\
                2 & 4
            \end{bmatrix}$ ?
    \end{enumerate}
\end{activity}

\begin{definition}
    Soit $A$~une matrice carrée de taille $n \times n$. On appelle
    \define{matrice inverse} de~$A$ l'unique matrice~$A'$, si elle existe, telle
    que $AA' = A'A = I_n$. La matrice inverse de~$A$ est notée~$A^{-1}$.
\end{definition}

\begin{definition}
    Toutes les matrices n'ont pas de matrice inverse. Quand $A^{-1}$~existe on
    dit que $A$~est \emph{inversible}.
\end{definition}

\subsection{Avec \bsc{Maxima}}

Pour calculer la matrice inverse de~$A$ avec \bsc{Maxima}, on tape
\verb|A ^^ -1| ou encore \verb|invert(A)|, au choix.
%Attention encore aux deux circonflexes.
Si la matrice~$A$ n'est pas inversible, \bsc{Maxima} affiche une
erreur:%\\
« expt: undefined: 0 to a negative exponent ».

\begin{example}
    Indiquer si les matrices suivantes sont inversibles, et si oui donner leur
    inverse:
    \begin{enumerate}[gathered, label={$\Alph*={}$}, labelsep=0pt]
        \item $\begin{bmatrix}
                1 & 2 \\
                3 & 4
            \end{bmatrix}$
        \item $\begin{bmatrix}
                1 & 2 & 3 \\
                4 & 5 & 6 \\
                7 & 8 & 9
            \end{bmatrix}$
        \item $\begin{bmatrix}
                1 & -1 & 1  \\
                2 & 2  & -2 \\
                1 & 0  & 1
            \end{bmatrix}$
    \end{enumerate}
\end{example}

\section{Résolution de systèmes}

\begin{activity}
    \begin{enumerate}
        \item Résoudre à la main le système
            $(S) = \left\{\begin{gathered}
                2x + 3y = 8 \\
                -3x + y = -1
            \end{gathered}\right.$
        \item On pose $A = \begin{bmatrix}
                2 & 3 \\
                -3 & 1
            \end{bmatrix}$
            et
            $B = \begin{bmatrix}
                8 \\
                -1
            \end{bmatrix}$.
            \begin{enumerate}
                \item $A$~est-elle inversible ? Déterminer~$A^{-1}$.
                \item Calculer $A^{-1}B$. Que remarque-t-on ?
            \end{enumerate}
    \end{enumerate}
\end{activity}

\begin{definition}
    Un \define{système linéaire} de $2$~équations à $2$~inconnues est un système
    de la forme
    $(S) = \left\{\begin{gathered}
        ax + by = u \\
        cx + dy = v
    \end{gathered}\right.$
    On associe à ce système la matrice carrée
    $A = \begin{bmatrix}
        a & b \\
        c & d
    \end{bmatrix}$
    et la matrice colonne
    $B = \begin{bmatrix}
        u \\
        v
    \end{bmatrix}$.
    On pose aussi
    $X = \begin{bmatrix}
        x \\
        y
    \end{bmatrix}$.
\end{definition}

\begin{definition}
    Un \define{système linéaire} de $3$~équations à $3$~inconnues est un système
    de la forme
    $(S) = \left\{\begin{gathered}
        ax + by + cz = u \\
        dx + ey + fz = v \\
        gx + hy + kz = w
    \end{gathered}\right.$
    On associe à ce système la matrice carrée
    $A = \begin{bmatrix}
        a & b & c \\
        d & e & f \\
        g & h & k
    \end{bmatrix}$
    et la matrice colonne
    $B = \begin{bmatrix}
        u \\
        v \\
        w
    \end{bmatrix}$.
    On pose aussi
    $X = \begin{bmatrix}
        x \\
        y \\
        z
    \end{bmatrix}$.
\end{definition}

On peut étendre ces définitions à n'importe quel système linéaire de
$n$~équations à $n$~inconnues. Il est facile alors de vérifier que le
système~$(S)$ est équivalent à l'équation matricielle $AX = B$.

Si $A$~est inversible, alors $AX = B$ équivaut à $A^{-1}AX = A^{-1}B$
c'est-à-dire $X = A^{-1} B$. Il suffit de calculer l'inverse d'une matrice, et
un produit de matrices, pour résoudre n'importe quel système linéaire.

\begin{example}
    Résoudre le système
    $\left\{\begin{gathered}
        x - y + z = 20 \\
        x + y - z = 10 \\
        x + z = -2
    \end{gathered}\right.$
\end{example}

\end{document}
