% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}

\setmainlanguage{french}

\rivmathlib{geometry}

\usepackage{sesacompat}
\let\exercice\exercise \let\endexercice\endexercise

\begin{document}

\chapter{Nombres complexes}

\section{Définition et représentation}

\subsection{Forme algébrique}



\begin{theorem}
  Il existe un ensemble noté $\mdC$ appelé
  ensemble des nombres complexes qui possède les
    propriétés suivantes :
    \begin{itemize}
        \item $\C$ contient l'ensemble des nombres réels ;
        \item il contient un nombre $i$ tel que {\bf {$i^2=-1$}} ;
        \item il est muni d'une {\bf addition et d'une multiplication} qui ont
            les {\bf mêmes propriétés que dans $\mdR$}, l'ensemble des nombres
            réels.
    \end{itemize}
\end{theorem}


\begin{definition}
    Tout nombre complexe peut s'\'ecrire sous la forme :
    $z=a+\ci b$ avec $a,b\in \R$.

    Cette \'ecriture est appel\'ee \MotDefinition [Forme algébrique de $\boldmath{z}$]{forme algébrique de $z$}[forme algébrique de z]{} :
    \begin{itemize}
    \item $a$ est appel\'ee \MotDefinition{partie réelle}{} de $z$,
      not\'ee $\text{Re}(z)$.
    \item $b$ est appel\'ee \MotDefinition{partie imaginaire}{} de
      $z$, not\'ee $\text{Im}(z)$.
    \end{itemize}
\end{definition}

\begin{example}
    Soient $z_1 = 3 + 5i$ et $z_2 = 6 - 11i$.
    \begin{enumerate}
        \item $z_3 = z_1 - z_2 = (3 + 5i) - (6 - 11i) = 3 - 6 + 5i + 11i
            = -3 + 16i$: la forme algébrique de $z_3$~est $-3 + 16i$.
        \item $z_4 = z_1 \times z_2 = (3 + 5i) \times (6 - 11i)
            = 3 \times 6 - 3 \times 11i + 5i \times 6 - 5i \times 11i
            = 18 - 33i + 30i - 55i^2 = 18 - i + 55 = 73 - i$: la forme
            algébrique de $z_3$~est $73 - i$.
    \end{enumerate}
\end{example}

\begin{definition}
    Si $z = a + ib$, le \define{conjugué} de~$z$ est $\bar{z} = \bar{a + ib} = a
    - ib$. Ou encore: $\bar{x + iy} = x - iy$.
\end{definition}

\begin{method}[Forme algébrique d'un quotient]
    Si l'on a le quotient $Z = \dfrac{a + ib}{x + iy}$, on utilise le conjugué
    $x - iy$ du dénominateur $x + iy$ pour écrire:
    \[ Z = \frac{(a + ib)(x - iy)}{(x + iy)(x - iy)}
            = \frac{(a + ib)(x - iy)}{x^2 + y^2} \]

    Le nouveau dénominateur donnera toujours un nombre réel, et on peut
    développer puis réduire le numérateur pour obtenir la forme algébrique.
\end{method}

\begin{example}
    $\displaystyle
        Z = \frac{3 + 5i}{6 + 11i}
        = \frac{(3+5i)(6-11i)}{6^2+11^2} = \frac{z_1 z_2}{157}
        = \frac{73 - i}{157} = \frac{73}{157} - \frac{1}{157}i
    $
\end{example}

\subsection{Représentation graphique}

\begin{definition}
    Soit $z={ a}+\ci b$ un nombre complexe avec $a,b\in\R$.
    Le nombre $z$ peut être repr\'esent\'e dans un rep\`ere
    $(O,\vect{u},\vect{v})$ par le point $\point M(a;b)$.

    On dit que $z={ a}+\ci{ b}$ est l'\MotDefinition{affixe}{} du point~$M$.
    On note souvent $M(z)$ ou $M({ a}+\ci{ b})$ ou $z_M = a + i b$.
\end{definition}

On peut ainsi définir l'affixe d'un vecteur par $z_{\vect{AB}} = z_B - z_A$.

\begin{remark}
    \begin{enumerate}
        \item Les complexes {\bf \bf $z=a\in \R$}  sont
        les nombres r\'eels et sont  repr\'esent\'es sur {\bf l'axe des abscisses}.
        \item Les complexes {\bf $z=\ci b$}, $b\in \R$  sont
        les {\bf imaginaires purs} et sont repr\'esent\'es sur {\bf l'axe des ordonn\'ees}.
        \item Le plan est alors appel\'e \MotDefinition{plan complexe}{}.
    \end{enumerate}
\end{remark}

\begin{proposition}
    Soient $\vect{u}$~et~$\vect{v}$ et d'affixes respectives
    $z_{\vect{u}}$~et~$z_{\vect{v}}\ne0$. Alors:
    \begin{itemize}
        \item $\vect{u}$~et~$\vect{v}$ sont colinéaires si et seulement si
            $\dfrac{z_{\vect{u}}}{z_{\vect{v}}}$ est un nombre réel.
        \item $\vect{u}$~et~$\vect{v}$ sont orthogonaux si et seulement si
            $\dfrac{z_{\vect{u}}}{z_{\vect{v}}}$ est un nombre imaginaire.
    \end{itemize}
\end{proposition}

\section{Forme exponentielle}

\subsection{Module, argument et forme trigonométrique}

\def\ang#1#2{\vectangle({#1};{#2})}

\begin{definition}
        Soit $z$ un complexe et $M$ le point d'affixe $z$.
        \begin{itemize}
            \item On appelle \MotDefinition{module}{} de $z$ la distance $OM$.
                Le module de $z$ est noté $\abs{z}$.
            \item Si $z\neq 0$, on appelle \MotDefinition{argument}{}
              de $z$ et l'on note~$\arg (z)$ n'importe quelle mesure en radians
              de l'angle~$\ang{u}{OM}$.
            \item Le complexe nul  n'a pas d'argument et a pour module 0.
        \end{itemize}
\end{definition}


\begin{theorem}
    Soit $z=a+\ci b$ un complexe.
    \begin{itemize}
        \item  $|z|=\sqrt{z\times \bar{z}}=\sqrt{a^2+b^2}.$
        \item Si $z\neq 0$ alors $\theta=\arg(z)$ peut être déterminé par:
        \raisebox{0pt}[0pt]{% smash height above
        $\left\lbrace
        \begin{aligned}
            \cos \theta &=\frac{a}{|z|}  \\
            \sin \theta &=\frac{b}{|z|}
        \end{aligned}\right.
        $}
    \end{itemize}
\end{theorem}

\begin{definition}
    La forme trigonométrique d'un nombre complexe est
    $z = \rho \cos \theta + i \rho \sin \theta$, avec $\rho = \abs{z}$ et
    $\theta = \arg(z)$. On note parfois $z = [\rho; \theta]$.
\end{definition}

\begin{method}[Déterminer la forme trigonométrique]
    Dans la pratique, on trouve la forme trigonométrique de $z = a + ib$ de la
    façon suivante:
    \begin{enumerate}
            \item Calculer $\abs{z} = \sqrt{a^2+b^2}$.
            \item Calculer et simplifier $Z = \dfrac{z}{\abs{z}}$.
            \item Reconnaitre des cosinus et sinus remarquables pour déterminer
                l'argument~$\theta$. On peut placer le nombre $Z$ sur le cercle
                trigonométrique pour déterminer graphiquement~$\theta$.
    \end{enumerate}
\end{method}

\begin{theorem}
    \label{thm:trig-form-compute}
    Pour tous $z$~et~$z'$ nombres complexes:\\
    \parbox\linewidth{%
    \setlength\multicolsep{1ex}
    \begin{multicols}{2}
    \everymath{\displaystyle}
    \begin{itemize}
        \item $\abs{z \times z'} = \abs{z} \times \abs{z'}$
        \item $\abs{\frac{z}{z'}} = \frac{\abs{z}}{\abs{z'}}$
        \item $\arg(z \times z') = \arg(z) + \arg(z')$
        \item $\arg\left(\frac{z}{z'}\right) = \arg(z) - \arg(z')$
    \end{itemize}
    \end{multicols}
    }

    En résumé: si $z = [\rho; \theta]$ et $z' = [\rho'; \theta']$, alors
    \[
        z z' = [\rho\times\rho'; \theta+\theta']
        \qquad\text{et}\qquad
        \frac{z}{z'} = \left[\frac{\rho}{\rho'}; \theta-\theta'\right]
    \]
\end{theorem}

\subsection{Forme exponentielle}


\begin{definition}
    Par convention, on décide de noter
    $\e^{i\theta} = \cos\theta + i\sin\theta$.
    Ainsi, on peut écrire
    $[\rho; \theta] = \rho \cos \theta + i \rho \sin \theta
    = \rho \e^{i\theta}$.
    La \define{forme exponentielle} de~$z$ est $z = \rho \e^{i\theta}$.
\end{definition}

\begin{remark}
    À partir de maintenant on n'utilise plus la notation~$[\rho; \theta]$ pour
    la forme trigonométrique, mais seulement $\rho \cos \theta + i \rho \sin
    \theta$ ou bien directement la forme exponentielle~$\rho \e^{i\theta}$.
\end{remark}

\begin{remark}
    Ce choix pour la forme exponentielle vient de ce qu'en utilisant les règles
    déjà connues sur les produits et les puissances, on retrouve directement les
    mêmes résultats que dans \cref{thm:trig-form-compute} sans avoir à apprendre
    de nouvelle formule:
    \[ \rho \e^{i\theta} \times \rho' \e^{i\theta'}
        = \rho\rho' \; \e^{i\theta}\e^{i\theta'}
        = \rho\rho' \; \e^{i\theta + i\theta'}
        = \rho\rho' \; \e^{i(\theta + \theta')}
    \]
    et
    \[ \frac{\rho \e^{i\theta}}{\rho' \e^{i\theta'}}
        = \frac{\rho}{\rho'} \; \frac{\e^{i\theta}}{\e^{i\theta'}}
        = \frac{\rho}{\rho'} \; \e^{i\theta-i\theta'}
        = \frac{\rho}{\rho'} \; \e^{i(\theta-\theta')}
    \]

\end{remark}

\end{document}
