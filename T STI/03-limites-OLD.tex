% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\begin{document}

\chapter{Limites de fonctions}

\chapterquote{Only two things are infinite, the universe and human stupidity.
But I'm not sure about the former.}{Albert \bsc{Einstein}}

\section{Activités}

\begin{activity}
    \label{activ:heater}
    Le chauffe-eau (1 p.~42).
\end{activity}

\begin{activity}[Oscillation amortie]
    \label{activ:sinc}
    On pose~$g(x) = \dfrac{\sin(x)}{x}$.
    \begin{enumerate}
        \item Dans GeoGebra, tracer~$g$.
        \item Construire un curseur~$t$ allant de $0$~à~$100$, puis les points
            $A(t,g(t))$~et~$B(t,0)$. Créer le segment~$AB$.
        \item Que peut-on dire de la longueur~$AB$ lorsque~$t$ se rapproche
            de~$100$ ? Peut-on dire que $g(t)$~se rapproche de plus en plus
            de~$3$ à mesure que $t$~augmente ?
    \end{enumerate}
\end{activity}

\section{Limite finie à l'infini}

\subsection{Limite finie en~$\pm\infty$}

\begin{definition}
    Soit~$f$ définie sur $I=\left[ a ; +\infty \right]$ ou $I=\left] a ; +\infty
    \right]$. On dit que $f$ admet une \define{limite~$\ell\in\mdR$ en
    $+\infty$}, et on écrit $\displaystyle \lim_{x \to +\infty} f(x) = \ell$ si
    (et seulement si) $f(x)$~reste aussi près qu'on veut de~$\ell$ pourvu qu'on
    prenne $x$~assez grand.
\end{definition}

\begin{remark}
    $\displaystyle \lim_{x \to +\infty} f(x) = \ell$ se lit «la limite de~$f(x)$
    quand $x$~tend vers~$+\infty$ est~$\ell$». On écrit aussi $\displaystyle
    f(x) \tendsto[x \to +\infty] \ell$, qui se lit «$f(x)$~tend
    vers~$\ell$ quand $x$~tend vers~$+\infty$».
\end{remark}

\begin{example}
    Dans l'activité~\ref{activ:heater}, on a vu:
    \[ \lim_{x\to+\infty} \left( 10 + \frac{\<8719>}{\<4185>x} \right) = 10 \]
\end{example}

\begin{example}
    Dans l'activité~\ref{activ:sinc}, on a vu:
    \[ \lim_{x\to+\infty} \frac{\sin x}{x} = 0 \]
\end{example}

\begin{definition}
    On définit de manière analogue le cas où $f$~a une
    \define{limite~$\ell\in\mdR$ en $-\infty$}.
\end{definition}

\begin{exercise}
    Lecture graphique de limites en~$\pm\infty$ (38 p.~74).
\end{exercise}

\subsection{Limites de référence}

\begin{proposition}
    $\displaystyle \lim_{x \to +\infty} \frac{1}{x} = 0$ et
    $\displaystyle \lim_{x \to -\infty} \frac{1}{x} = 0$.
\end{proposition}

\begin{example}[Recherche de seuil]
    \begin{enumerate}
        \item Puisque $\displaystyle \lim_{x \to +\infty} \frac{1}{x} = 0$,
            alors $\dfrac{1}{x}$~sera aussi près qu'on veut de~$0$ dès que
            $x$~sera assez grand. À partir de quelle valeur de~$x$ a-t-on
            $\dfrac{1}{x} \leq \<0.001>$ ? À partir de quand a-t-on
            $\dfrac{1}{x} \leq 10^{-25}$ ?
        \item De même, $\displaystyle \lim_{x \to -\infty} \frac{1}{x} = 0$. On
            se demande donc pour quelles valeurs de~$x<0$ on~a $-10^{-20}
            \leq \dfrac{1}{x} \leq 10^{-20}$ (l'inégalité de droite est
            toujours vraie car $\frac{1}{x} < 0$ et $10^{-20} > 0$).
    \end{enumerate}
\end{example}

\begin{remark}
    Ne Pas Confondre:\\
    $10^{-20} = \<0.00000000000000000001>$\\
    $-10^{20} = -\<100000000000000000000>$
\end{remark}

\begin{remark}
    Howard dit « Si~$A < B$ alors~$\dfrac{1}{A} > \dfrac{1}{B}$ ».
    Sheldon répond « C'est faux ! ».
    Qui a raison ?
\end{remark}

\begin{proposition}
    $\displaystyle \lim_{x \to +\infty} \frac{1}{x^2} = 0$ et
    $\displaystyle \lim_{x \to -\infty} \frac{1}{x^2} = 0$.
\end{proposition}

\begin{proposition}
    $\displaystyle \lim_{x \to +\infty} \frac{1}{\sqrt{x}} = 0$.
\end{proposition}

\begin{proposition}
    Soit $f$~définie par~$f(x) = A$ avec $A\in\mdR$. Alors
    $\displaystyle \lim_{x \to +\infty} f(x) = A$.
\end{proposition}

\subsection{Opérations sur les limites}

\begin{activity}
    \begin{enumerate}
        \item On pose $f(x) = \dfrac{1}{x}$. Indiquer la limite de~$f$
            en~$+\infty$ si elle existe.
        \item On pose $g(x) = \dfrac{1}{x} + 3$.
            \begin{enumerate}
                \item Proposer une conjecture pour la limite de~$g$
                    en~$+\infty$.
                \item À partir de quelle valeur de~$x$ a-t-on $\<2.999> \leq
                    g(x) \leq \<3.001>$ ? Et $3 - 10^{-30} \leq g(x) \leq 3 +
                    10^{-30}$ ?
                \item Déterminer $\displaystyle \lim_{x\to+\infty} (g(x)-3)$.
            \end{enumerate}
        \item On pose $h(x) = \dfrac{1}{x} + \dfrac{1}{x^2}$. Proposer une
            conjecture pour $\displaystyle \lim_{x\to+\infty} h(x)$.
        \item On pose $j(x) = \dfrac{2}{x^2}$. Que dire de $\displaystyle
            \lim_{x\to+\infty} j(x)$ ?
        \item On pose $k(x) = 2g(x)$. Calculer $k(x)$, puis déterminer sa limite
            en~$+\infty$.
        \item Déterminer $\displaystyle \lim_{x\to+\infty} \left(g(x) +
            k(x)\right)$.
        \item Déterminer $\displaystyle \lim_{x\to+\infty}
            \left(g(x)k(x)\right)$.
        \item Déterminer $\displaystyle \lim_{x\to+\infty}
            \frac{g(x)}{k(x)}$.
    \end{enumerate}
\end{activity}

Dans cette partie, $\alpha$ indiquera $+\infty$~ou~$-\infty$ tandis
que $f_1$~et~$f_2$ seront deux fonctions définies sur un intervalle~$I$ dont
$\alpha$~est une borne (par exemple $I = \left] -\infty \mathrel; 5 \right[$
si $\alpha$~est~$-\infty$).

\begin{theorem}
    \label{thm:limitofsum}
    Si $\displaystyle \lim_{x \to \alpha} f_1(x) = \ell_1$
    et $\displaystyle \lim_{x \to \alpha} f_2(x) = \ell_2$, alors
    \[ \lim_{x \to \alpha} \left( f_1(x) + f_2(x) \right) = \ell_1 + \ell_2 \]
\end{theorem}

\begin{remark}
    En particulier, $\displaystyle \lim_{x \to \alpha} \left( f(x) + A
    \right) = \lim_{x \to \alpha} f(x) + A$ si $A\in\mdR$~et $f$~a une limite
    finie.
\end{remark}

\begin{theorem}
    \label{thm:limitofproduct}
    Si $\displaystyle \lim_{x \to \alpha} f_1(x) = \ell_1$
    et $\displaystyle \lim_{x \to \alpha} f_2(x) = \ell_2$, alors
    \[ \lim_{x \to \alpha} \left( f_1(x) \cdot f_2(x) \right) =
        \ell_1 \cdot \ell_2 \]
\end{theorem}

\begin{remark}
    En particulier, $\displaystyle \lim_{x \to \alpha} \left( A f(x) \right) =
    A \lim_{x \to \alpha} f(x)$ si $A\in\mdR$~et $f$~a une limite finie.
\end{remark}

\begin{theorem}
    \label{thm:limitoffrac}
    Si $\displaystyle \lim_{x \to \alpha} f_1(x) = \ell_1$
    et $\displaystyle \lim_{x \to \alpha} f_2(x) = \ell_2 \neq 0$, alors
    \[ \lim_{x \to \alpha} \frac{f_1(x)}{f_2(x)} = \frac{\ell_1}{\ell_2} \]
\end{theorem}

\begin{example}
    Déterminer la limite en $+\infty$ de~$f(x) = 3 - \dfrac{1}{\sqrt{x}}$.
\end{example}

\begin{example}
    Déterminer la limite en $-\infty$ de~$g(x) = \dfrac{5 + 6x}{x}$.
\end{example}

\begin{example}
    \label{example:product}
    Déterminer la limite en $+\infty$ de~$h(x) =
        \dfrac{(5+6x)(3\sqrt{x} - 1)}{x\sqrt{x}}$.
\end{example}

\subsection{Asymptote horizontale}

\begin{definition}
    Si $\displaystyle \lim_{x \to \alpha} f(x) = \ell$, alors quand $x$~tend
    vers~$\alpha$ la courbe de~$f$ est de plus en plus proche de la droite
    horizontale d'équation $y=\ell$. On dit que la droite d'équation~$y=\ell$
    est une \define{asymptote horizontale} de la courbe de~$f$.
\end{definition}

\begin{example}
    La droite d'équation~$y=0$ est une asymptote horizontale à la courbe
    représentative de la fonction inverse.
\end{example}

\begin{example}
    Reprenons la fonction de l'exemple~\ref{example:product}.
    La droite d'équation~$y=18$ est une asymptote horizontale à la courbe
    représentative de la fonction~$h$.
\end{example}

\begin{remark}
    Les fonctions $\sin$~et~$\cos$ ne ressemblent jamais à une droite: on admet
    qu'elles n'ont pas d'asymptote horizontale, donc pas de limite
    en~$\pm\infty$.
\end{remark}

\section{Limite infinie en $\pm\infty$}

\subsection{Définition et limites de référence}

\begin{activity}
    \begin{enumerate}
        \item Il semble que $x^2$ devienne extrêmement grand quand $x$ tend
            vers~$+\infty$. À partir de quel~$x$ a-t-on $x^2 \geq \<10000>$ ?
            Et~$x^2 \geq 10^{56}$ ?
        \item Conjecturer $\displaystyle \lim_{x\to+\infty} x^3$, puis
            $\displaystyle \lim_{x\to-\infty} x^3$ (on pourra représenter la
            courbe).
    \end{enumerate}
\end{activity}

\begin{definition}
    On dit que $f$~admet une \define{limite $+\infty$ en $+\infty$} et on note
    $\displaystyle \lim_{x\to+\infty} f(x) = +\infty$ si $f(x)$~dépasse
    n'importe quel «plafond»~$A$ pourvu que~$x$ devienne assez grand.
\end{definition}

\begin{definition}
    On dit que $f$~admet une \define{limite $-\infty$ en $+\infty$} et on note
    $\displaystyle \lim_{x\to+\infty} f(x) = -\infty$ si $f(x)$~devient
    inférieure à n'importe quel seuil~$A$ pourvu que~$x$ devienne assez grand.
\end{definition}

On a les définitions équivalentes pour les limites en~$-\infty$.

\begin{proposition}
    \everymath{\displaystyle}%
    $\lim_{x\to\pm\infty} x^2 = +\infty$;
    $\lim_{x\to+\infty} x^3 = +\infty$; $\lim_{x\to-\infty} x^3 = -\infty$.\\
    Plus généralement, $\begin{aligned}[t]
        \lim_{x\to+\infty} x^n &= +\infty \text{ tandis que} \\
        \lim_{x\to-\infty} x^n &= \begin{cases}
                                    +\infty &\text{si $n$ est pair} \\
                                    -\infty &\text{si $n$ est impair}
                                \end{cases}
    \end{aligned}$
\end{proposition}

\subsection{Théorèmes opératoires}

\begin{activity}
    \everymath{\displaystyle}%
    \begin{enumerate}
        \item Soient $f:x\mapsto x^3$~et~$g:x\mapsto x^2$ définies sur~$\mdR$.
            Conjecturer $\lim_{x\to+\infty} \left( f(x) + g(x) \right)$.
        \item En $-\infty$, on a $ \lim_{x\to+\infty} f(x) = -\infty$ et
            $\lim_{x\to+\infty} g(x) = +\infty$. Que dire de la limite de la
            somme ? Tracer la somme à la calculatrice pour conclure.
        \item Si l'on prend $h:x\mapsto x^4$, a-t-on $\lim_{x\to+\infty} \left(
            f(x) + h(x) \right) = -\infty$ ? Remarque ?
        \item En calculant~$f(x)g(x)$, montrer que $\lim_{x\to+\infty} \left(
            f(x)g(x) \right) = +\infty$. Déterminer aussi la limite
            en~$-\infty$.
        \item Vérifier que $\lim_{x\to+\infty} \frac{1}{g(x)} = 0$, puis
            déterminer $\lim_{x\to+\infty} \frac{f(x)}{g(x)}$.
        \item Déterminer $\lim_{x\to+\infty} \frac{f(x)}{h(x)}$. Que
            remarque-t-on ?
    \end{enumerate}
\end{activity}

\begin{theorem}[Limite d'une somme]
    N/T
\end{theorem}

\begin{theorem}[Limite d'un produit]
    N/T
\end{theorem}

\begin{theorem}[Limite d'un quotient]
    N/T
\end{theorem}



\end{document}
