% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\begin{document}

\chapter{Compléments de dérivation}

\section{Rappels sur la dérivation}

\subsection{Nombre dérivé et tangente}

\begin{definition}
    Soient~$f$ une fonction définie sur un intervalle~$I$ et $a\in I$.
    On appelle \emph{nombre dérivé} de~$f$ en~$a$ le nombre
    \[ f'(a) = \lim_{h\tendsto 0} \frac{f(a+h) - f(a)}{h} \]
    quand la limite existe.
\end{definition}

\begin{remark}
    Si $f$~est dérivable en~$a$, alors le coefficient directeur de la
    \define{tangente} à~$\mcC_f$ au point d'abscisse~$a$ est~$f'(a)$.

    En particulier, la tangente est \emph{horizontale} si et seulement
    si~$f'(a) = 0$
\end{remark}

\begin{proposition}
    Si $f$~est dérivable en~$a$, alors la tangente à~$\mcC_f$ au point
    d'abscisse~$a$ a pour équation
    \[ y = f'(a)(x-a) + f(a) \]
\end{proposition}

\subsection{Dérivées de fonctions usuelles}

\begin{center}$\begin{array}{L*{3}{cL}}
    \firsthline
    \textbf{Fonction} & \textbf{Dérivable sur} & \textbf{Fonction dérivée} \\
    \hline
    f(x) = k \text{ avec $k \in \mdR$} & \mdR & f'(x) = 0 \NL
    f(x) = mx + p \text{ avec $m,p \in \mdR$} & \GHOST{\mdR} &
            f'(x) = \GHOST{m} \NL
    f(x) = x^2 & \GHOST{\mdR} & f'(x) = \GHOST{2x} \NL
    f(x) = x^n & \GHOST{\mdR} & f'(x) = \GHOST{nx^{n-1}} \NL
    f(x) = \dfrac{1}{x} & \GHOST{\left]-\infty;0\right[
            \text{ ou } \left]0;+\infty\right[} &
            f'(x) = \GHOST{\dfrac{-1}{x^2}} \NL
    f(x) = \sqrt{x} & \GHOST{\left]0;+\infty\right[} &
            f'(x) = \GHOST{\frac{1}{2\sqrt{x}}} \NL
    f(x) = \sin(x) & \GHOST{\mdR} & f'(x) = \GHOST{\cos(x)} \NL
    f(x) = \cos(x) & \GHOST{\mdR} & f'(x) = \GHOST{\sin(x)} \\
    \lasthline
\end{array}$\end{center}

\subsection{Théorèmes opératoires}

\begin{center}$
    \everymath{\displaystyle}
    \begin{array}{LcLcLcLcL}
    \firsthline
    \textbf{Fonction} & \textbf{Dérivée} &
    \textbf{Exemple} & \textbf{Solution} \\
    \hline
    f = u + v & f' = u' + v'
            & f(x) = \cos x + x^2 & f'(x) = \GHOST{-\sin{x} + 2x} \NL
    f = k u & f' = k u'
            & f(x) = 3 x^5 & f'(x) = \GHOST{3 \times 5x^4 = 15x^4} \NL
    f = u \times v & f' = u'v + uv'
            & f(x) = x \sin x & f'(x) = \GHOST{1\times\sin x + x\cos x} \NL
    f = \frac{u}{v} & f' = \frac{u'v - uv'}{v^2}
            & f(x) = \frac{\sin x}{x}
            & f'(x) = \GHOST{\frac{x\cos x - \sin x}{x^2}} \NL
    \lasthline
\end{array}$\end{center}

\subsection{Sens de variation}

\begin{theorem}
    Soit~$f$ une fonction dérivable sur~$I$.
    \begin{enumerate}
        \item Si $f'(x)\geq 0$ pour~tout~$x \in I$ alors $f$~est croissante
            sur~$I$ (si $f'(x) > 0$ alors $f$~est strictement croissante);
        \item Si $f'(x)\leq 0$ pour~tout~$x \in I$ alors $f$~est décroissante
            sur~$I$ (si $f'(x) < 0$ alors $f$~est strictement décroissante);
        \item Si $f'(x) = 0$ pour~tout~$x \in I$ alors $f$~est constante.
    \end{enumerate}
    Réciproquement, si $f$~est croissante (resp. décroissante, resp. constante)
    alors $f'(x) \geq 0$ (resp. $f'(x) \leq 0$, $f'(x) = 0$).
\end{theorem}

\begin{example}
    Dresser le tableau de variations de $f$ définie par
    \[ f(x) = x^3 + \frac{3}{2}x^2-6x+1 \]
\end{example}

\subsection{Extremum local}

\begin{proposition}
    Soit~$f$ une fonction dérivable sur un intervalle \emph{ouvert}~$I$.
    Si~$f'$~s'annule en~$a\in I$ \emph{en changeant de signe}, alors $f(a)$~est
    un extremum local.
\end{proposition}

\begin{exercise}
    Soit $f$~définie sur~$\mdR$ par $f(x) = (100 - 2x)(x+10)$.
    \begin{enumerate}
        \item Calculer si possible $f'(x)$ (on pourra développer).
        \item Montrer que $f$~admet un maximum. En quelle abscisse est-il
            atteint ? Comment aurait-t-on pu le retrouver?
    \end{enumerate}
\end{exercise}

\end{document}
