% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\def\Prob(#1){p\!\left(#1\right)}
\def\d{\mathrm{d}}

\usetikzlibrary{datavisualization, patterns,
                datavisualization.formats.functions}

\persocourssetup{qrcodes={loi-densite}}


\begin{document}

\StudentMode

\chapter{Lois de probabilité à densité}

\begin{activity}
    \begin{enumerate}
        \item \makelink{intro-int}%
            Une formule de tableur permet d'obtenir un nombre entier au hasard
            entre $0$~inclus et $10$~exclus. Chacun des résultats possibles
            arrive avec la même probabilité. On note~$X$ la variable aléatoire
            donnant la valeur renvoyée par la formule.
            \begin{enumerate}
                \item Quelles sont les valeurs possibles de~$X$ ?
                    \IfStudentT{\bigfiller{1}}
                \item Déterminer $\Prob(X = 4)$, puis $\Prob(3 \le X < 7)$.
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
        \item \makelink{intro-10}%
            On modifie la formule pour obtenir un nombre, toujours au hasard
            entre $0$~inclus et $10$~exclus, mais avec \emph{un} chiffre après
            la virgule.
            \begin{enumerate}
                \item Quelles sont les valeurs possibles de~$X$ ?
                    \IfStudentT{\bigfiller{1}}
                \item Déterminer $\Prob(X = 4)$, puis $\Prob(3 \le X < 7)$.
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
        \item \makelink{intro-e5}%
            On modifie encore la formule pour que le nombre choisi au hasard
            de manière équiprobable ait $5$~chiffres après la virgule.
            \begin{enumerate}
                \item Quelles sont les valeurs possibles de~$X$ ?
                    \IfStudentT{\bigfiller{1}}
                \item Déterminer $\Prob(X = 4)$, puis $\Prob(3 \le X < 7)$.
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
        \item \begin{enumerate}
                \item \makelink{intro-end}%
                    Que remarque-t-on ?
                    \IfStudentT{\bigfiller{2}}
                \item Que se passe-t-il si la précision augmente beaucoup (\<50>
                    voire \<1000> chiffres après la virgule) ? Et si on tire au
                    hasard un nombre réel quelconque (avec une écriture décimale
                    potentiellement infinie) ?
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
    \end{enumerate}
\end{activity}

\section{Une loi à densité: la loi uniforme}

\subsection{Loi uniforme sur~$\IN[0;1;]$}

\begin{definition}
    Soit~$X$ une variable aléatoire prenant ses valeurs dans~$\IN[0;1;]$. On dit
    que $X$~suit la loi uniforme sur~$\IN[0;1;]$ si pour tous $0 \le u \le v \le
    1$, $\Prob(X\in\IN[u;v;]) = \Prob(u \le X \le v) = v - u$
\end{definition}

\begin{remark}
    $v - u$ est la longueur de l'intervalle~$\IN[u;v;]$.
\end{remark}

\begin{remark}
    On a $\Prob(X\in\IN[0;1;]) = 1 - 0 = 1$: c'est cohérent puisque
    $X$~a toujours ses valeurs dans cet intervalle.
\end{remark}

\clearpage
\begin{example}
    \makelink{uni-01}%
    Soit $X$~suivant la loi uniforme sur~$\IN[0;1;]$.
    \begin{enumerate}
        \item Déterminer $\Prob(\<0.1>\le X\le\<0.5>)$ et $\Prob(X\le\<0.1>)$.
            \IfStudentT{\bigfiller{2}}
        \item Déterminer $\Prob(X\le\frac{1}{2})$ et
            $\Prob(X>\frac{1}{2})$.
            \IfStudentT{\bigfiller{2}}
        \item Commenter.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\subsection{Loi uniforme sur~$\IN[a;b;]$}

\begin{definition}
    Soit~$X$ une variable aléatoire prenant ses valeurs dans~$\IN[a;b;]$. On dit
    que $X$~suit la loi uniforme sur~$\IN[a;b;]$ si pour tous $a \le u \le v \le
    b$, $\Prob(X\in\IN[u;v;]) = \Prob(u \le X \le v)$ est proportionnelle à la
    longueur $v-u$ de l'intervalle.
\end{definition}

\begin{remark}
    \makelink{uni-ab}%
    Ainsi, il existe un réel~$k$ tel que $\Prob(X\in\IN[u;v;]) = k(v-u)$. Quelle
    valeur de~$k$ doit-on choisir ?
    \IfStudentT{\bigfiller{2}}
\end{remark}

\begin{proposition}
    La variable aléatoire~$X$ suit la loi uniforme sur~$\IN[a;b;]$ si et
    seulement si pour tous $a \le u \le v \le b$,
    \[ \Prob(X\in\IN[u;v;]) = \GHOST{\frac{v-u}{b-a}} \]
\end{proposition}

\begin{example}
    \makelink{uni-exo}%
    Soit $X$~suivant la loi uniforme sur~$\IN[-4;\<12.5>;]$.
    \begin{enumerate}
        \item Déterminer $\Prob(0\le X\le 11)$.
            \IfStudentT{\bigfiller{2}}
        \item Déterminer $\Prob(X < 0)$ et $\Prob(X \ge 0)$.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{example}

\begin{exercise}
    1 p 213
\end{exercise}

\begin{exercise}
    2 p 213
\end{exercise}

\subsection{Interprétation graphique: la densité}

Dans \makelink{uni-graph} un repère orthonormé, on place les points
$A(a;0)$~et~$B(b;0)$. Le rectangle $ABB'A'$ de hauteur $AA'=\frac{1}{b-a}$ est
d'aire
\[ \GHOST{\mcA = L \times h = AB \times AA' = (b-a) \times \frac{1}{b-a} = 1} \]
ce qui représente la probabilité~$\Prob(X\in\IN[a;b;])$. Ce rectangle est
délimité par l'axe des abscisses en bas, les droites d'équation $x=a$~et~$x=b$
sur les côtés et la fonction~$f(x) = \frac{1}{b-a}$ en haut --- voir la
figure~\ref{fig:uniform-density}.

Calculons l'aire du rectangle délimité par l'axe des abscisses, la fonction~$f$,
et sur les côtés les droites $x=u$~et~$x=v$ où $a \le u \le v \le b$. La hauteur
est \UGHOST{toujours $\frac{1}{b-a}$} et la longueur est~\UGHOST{$v - u$}: ainsi
l'aire est \UGHOST{$\frac{v-u}{b-a} = \Prob(X\in\IN[u;v;])$.}

\begin{figure}
    \caption{Interprétation graphique de~$\Prob(X\in\IN[u;v;])$: le
        domaine~$\mcD$ est un rectangle d'aire~$\frac{v-u}{b-a}$ mais c'est
        aussi le domaine sous la courbe~$\mcC_f$ entre $u$~et~$v$.}
    \label{fig:uniform-density}
    \begin{center}
    \begin{tikzpicture}
        \def\a{0.2}\def\b{1.45}\def\u{0.5}\def\v{1}
        \pgfmathsetmacro\h{1/(\b-\a)}
        \datavisualization [school book axes,
                            all axes={unit length=3cm},
                            x axis={include value=1.6,
                                ticks={major at={
                                \a as $a$, \b as $b$,
                                \u as $u$, \v as $v$
                            }}},
                            y axis={ticks and grid={major at={
                                \h as $\frac{1}{b-a}$,
                            }}},
                            visualize as line=f,
                            visualize as line=R,
                            visualize as line=D,
                            f={label in data={text=$\mcC_f$, when=x is \v}},
                            R={polygon, style={every path/.style={
                                draw, dashed, thin,
                                fill=black, fill opacity=0.05,
                            }}},
                            D={polygon, style={every path/.style={
                                draw, thin, pattern=north east lines
                            }}},
                        ]
        data [set=f] {
            x, y
            \a, \h
            \v, \h
            \b, \h
        }
        data [set=R] {
            x, y, name
            \a, 0, A
            \a, \h, A'
            \b, \h, B'
            \b, 0, B
        }
        data [set=D] {
            x, y, name
            \u, 0, SW
            \u, \h
            \v, \h, NE
            \v, 0
        }
        info {
            \path (SW) -- node[fill=black!5,inner sep=2pt] {$\mcD$} (NE);
            \node[above left] at (A) {$A$};
            \node[above] at (A') {$A'$};
            \node[above] at (B') {$B'$};
            \node[above right] at (B) {$B$};
        }
        ;
    \end{tikzpicture}%
    \end{center}
\end{figure}

D'autre part, l'aire de~$\mcD$ est \emph{l'aire sous la courbe} de~$f$ entre
$u$~et~$v$: en définitive on a

\[ \GHOST{ \Prob(X\in\IN[u;v;]) = \int_u^v f(x) \d x } \]

Plus $a$~et~$b$ sont proches, plus les valeurs de~$X$ sont regroupées dans un
intervalle étroit: la densité augmente. Puisque dans le même temps $f(x) =
\frac{1}{b-a}$ devient plus grand, fonction~$f$ semble être un bon indicateur de
la densité des valeurs de~$X$.

\begin{definition}
    $f: \IN[a;b;] \to \mdR, x \mapsto \frac{1}{b-a}$ est appelée
    \define{fonction de densité} de la loi uniforme sur~$\IN[a;b;]$.
\end{definition}

\subsection{Espérance}

\begin{activity}
    \label{activ:esperance}
    \begin{enumerate}
        \item \makelink{moy-pacome}
            Pacôme a eu $7$~notes au premier trimestre, toutes de
            coefficient~$1$: deux~$\<10>$, trois~$\<11,5>$, un~$\<12,5>$
            et~un~$\<14>$. Quelle est sa moyenne ?
            \IfStudentT{\bigfiller{2}}
        \item Au deuxième trimestre, \SI{10}{\percent}~de ses notes sont
            des~$8$, \SI{40}{\percent} sont des~$\<9,5>$, \SI{20}{\percent} sont
            des~$\<11,5>$ et \SI{30}{\percent} sont des~$\<13>$. Quelle est sa
            moyenne ?
            \IfStudentT{\bigfiller{2}}
        \item \makelink{esp-activ}
            Écrire la formule donnant l'espérance d'une variable aléatoire
            prenant les valeurs $x_1$, \dots, $x_n$. Comparer à la formule de la
            somme des probabilités.
            \IfStudentT{\bigfiller{2}}
        \item Que devient le théorème «la somme des probabilités est~$1$» dans le
            cadre des lois à densité ?
            \IfStudentT{\bigfiller{2}}
        \item Proposer une formule pour l'espérance d'une variable aléatoire
            telle que $\Prob(X\in\IN[u;v;]) = \int_u^v f(x) \d x$.
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\begin{definition}
     Soit~$X$ une variable aléatoire de fonction de densité~$f$ sur un
     intervalle~$\IN[a;b;]$. On appelle \define{espérance} de~$X$ le nombre réel
     \[ E(X) = \int_a^b xf(x) \d x \]
\end{definition}

\begin{theorem}
    \label{thm:esperance-uniforme}
    \makelink{uniesp}
    Si $X$ suit la loi uniforme sur~$\IN[a;b;]$, alors $E(X) = \dfrac{a+b}{2}$.
\end{theorem}

\begin{proof}
    \UGHOST{%
    $E(X) = \int_a^b \frac{1}{b-a} x \d x = G(b) - G(a)$
    avec $G(x)=\frac{1}{b-a} \times \frac{x^2}{2} = \frac{x^2}{2(b-a)}$.
    Alors:\\
    $E(X) = \frac{b^2 - a^2}{2(b-a)}
    = \frac{(b+a)(b-a)}{2(b-a)} = \frac{b+a}{2}$.
    }
\end{proof}

\begin{exercise}
    4 p 213
\end{exercise}

\subsection{Variance, écart-type}

\begin{activity}
    \begin{enumerate}
        \item \makelink{var-pacome}
            Calculer \emph{à la main} la variance des notes de~Pacôme au
            deuxième trimestre --- voir l'activité~\ref{activ:esperance}.
            \IfStudentT{\bigfiller{4}}
        \item \makelink{vsigma}
            Donner la \emph{formule} de la variance d'une variable
            aléatoire discrète.
            \IfStudentT{\bigfiller{2}}
        \item Comment calcule-t-on l'écart-type ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{activity}

\begin{definition}
    Soit~$X$ une variable aléatoire de fonction de densité~$f$ sur un
    intervalle~$\IN[a;b;]$. On appelle \define{variance} de~$X$ le nombre réel
    \[ V(X) = \int_a^b \left( x - E(X) \right)^2 f(x) \d x \]
\end{definition}

\begin{definition}
    On appelle \define{écart-type} de~$X$ le nombre réel
    \UGHOST{$\sigma(X) = \sqrt{V(X)}$.}
\end{definition}

\TeacherMode
\begin{theorem}
    \label{thm:V+sigma-uniforme}
    Si $X$ suit la loi uniforme sur~$\IN[a;b;]$, alors
    \[ V(X) = \GHOST{\dfrac{(b-a)^2}{12}}
        \quad\text{et}\quad
    \sigma(X) = \GHOST{\dfrac{b-a}{\sqrt{12}}} \]
\end{theorem}
\StudentMode

\begin{remark}
    La démonstration consiste encore à calculer une intégrale.
\end{remark}

\begin{example}
    \makelink{uni-evs}
    Soit $X$~suivant la loi uniforme sur~$\IN[-4;\<12.5>;]$. Déterminer $E(X)$,
    $V(X)$ et~$\sigma(X)$.
    \IfStudentT{\bigfiller{3}}
\end{example}

\begin{exercise}
    3 p 213
\end{exercise}

\section{Loi exponentielle}

\TeacherOnly
\begin{TP}[Vivre sans vieillir]
    \makeatletter
    \def\code{#1}{\texttt{\nofrench@punctuation#1}}
    \makeatother
    \def\|#1|{\texttt{#1}}
    \def\*#1*{\textsf{\textsl{#1}}}

    Un fabriquant de processeurs informatiques a remarqué que son produit phare
    ne s'use pas: tant qu'il fonctionne, un processeur utilisé depuis des années
    se comporte à tous points de vue comme un neuf.

    Par contre, ce modèle de microprocesseur a chaque mois une chance sur vingt
    de tomber en panne --- neuf ou pas ! Lorsqu'un tel incident se produit,
    c'est définitif: le processeur est à changer.

    Un fabriquant de processeurs informatiques a remarqué que son produit phare
    ne s'use pas: tant qu'il fonctionne, un processeur utilisé depuis des années
    se comporte à tous points de vue comme un neuf.

    Par contre, ce modèle de microprocesseur a chaque mois une chance sur vingt
    de tomber en panne --- neuf ou pas ! Lorsqu'un tel incident se produit,
    c'est définitif: le processeur est à changer.

    \begin{questions}
        \item \begin{enumerate}
                \item Représenter le graphe de la chaine de Markov correspondant
                    à la situation.
                \item Indiquer la matrice~$M$ de transition de la chaine de
                    Markov.
            \end{enumerate}
        \item Dans cette question on simule la durée de vie d'un processeur.
            Ouvrez un document tableur, et nommez « Simulation » sa première
            feuille.
            \begin{enumerate}
                \item La fonction \code{ALEA.ENTRE.BORNES(\*min*;\*max*)} du
                    tableur renvoie un nombre \emph{entier} entre \*min* et
                    \*max* inclus de manière équiprobable. Que fait la formule
                    \code{=SI(ALEA.ENTRE.BORNES(1;20)>1;1;0)} ? La rentrer dans la
                    case~\|B1|.
                \item Entrer la formule
                    \code{=SI(ALEA.ENTRE.BORNES(1;20)>1;1;0)*B1}
                    dans la case~\|C1|. Que fait-elle ? Pourquoi a-t-on
                    multiplié par~\|B1| ?
                \item Prendre le coin de la case~\|C1| puis le faire glisser
                    pour recopier la formule jusqu'en~\|GA1|.  Interpréter les
                    nombres de cette première ligne.
                \item Quelle formule peut-on taper en~\|A1| pour compter le
                    nombre de mois de fonctionnement du microprocesseur ?
            \end{enumerate}
        \item On simule maintenant un grand nombre de processeurs de la marque.
            \begin{enumerate}
                \item Sélectionner les cases de \|A1|~à~\|GA1|, puis tirer vers
                    le bas pour recopier la ligne jusqu'à la ligne~\<1500>.
                \item Y a-t-il des microprocesseurs qui ne durent qu'un mois ?
                    Qui tombent en panne tout de suite ?
                \item Créer et renommer « Étude » la deuxième feuille du
                    tableur. Dans la case~\|B1| de \emph{cette} feuille, entrer
                    une formule calculant la durée de vie minimale de votre
                    échantillon de processeurs (on pourra penser à sélectionner
                    les durées de vie à la souris plutôt que taper la référence
                    à la main). Faites de même pour la moyenne en~\|B2| et le
                    maximum en~\|B3|.
            \end{enumerate}
        \item \begin{enumerate}
                \item Dans la case~\|B5| de la feuille « Étude », entrer le
                    nombre~$0$; dans~\|B6| entrer~$5$, puis tirez vers le bas
                    pour obtenir des nombres de $5$~en~$5$ jusqu'à dépasser la
                    durée de vie maximale de votre échantillon.
                \item Dans la case~\|A5|, entrez la formule
                    \code{=FREQUENCE(\*durées de vie*;B5:B35)} où \*durées de vie*
                    est la plage des durées de vie dans la première feuille.
                    Attention: cette formule doit être matricielle (appelez
                    moi).
                \item À quoi correspondent les valeurs en \|A6|,~\|A7|, etc. ?
                \item Entrer \code{=A6/1500} dans la case~\|C6|, puis tirer vers le
                    bas jusqu'à~\|C35|. À quoi correspondent ces valeurs ?
            \end{enumerate}
        \item \begin{enumerate}
                \item En utilisant l'outil diagramme en colonnes sur les données
                    de \|B6|~à~\|C35|, construire l'histogramme de la série des
                    durées de vie.
                \item Rajouter au graphique une courbe de tendance
                    exponentielle. La courbe obtenue approche-t-elle
                    correctement le haut de l'histogramme ?
                \item Afficher la formule de la courbe de tendance. Que
                    remarquez-vous ?
                \item Pouvez-vous trouver un lien entre les paramètres de la
                    courbe de tendance et la durée de vie moyenne ?
            \end{enumerate}
    \end{questions}
\end{TP}
\EndTeacherOnly

\subsection{Définition}

\begin{activity}
    \begin{enumerate}
        \item Sur l'histogramme du~TP, comment \emph{lit}-on la fréquence de la
            classe~$\IN[5;10;[$ ? La fréquence des processeurs durant moins
            de \<15>~mois ? Entre \<5>~et~\<40> mois ?
            \IfStudentT{\bigfiller{3}}
        \item On choisit au hasard un microprocesseur, et on appelle~$X$ sa
            durée de vie. Estimer $\Prob(X\in\IN[5;10;[)$ puis
            $\Prob(X\in\IN[0;15;[)$.
            \IfStudentT{\bigfiller{1}}
        \item Proposer une méthode pour estimer $\Prob(15 \le X \le 60)$.
            \IfStudentT{\bigfiller{1}}
        \item Et pour $\Prob(8 \le X \le 43)$ ?
            \IfStudentT{\bigfiller{2}}
        \item Quel rôle joue la fonction~$f$ ?
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{activity}

\begin{definition}
    Soient $\lambda>0$~un nombre réel strictement positif et $X$~une variable
    aléatoire à valeurs dans~$\IN[0;+\infty;[$. La variable aléatoire~$X$ suit
    la \define{loi exponentielle} de paramètre~$\lambda$ si pour $0 \le u \le v$
    on a:
    \[ \Prob(X\in\IN[u;v;]) = \Prob(u \le X \le v) =
        \int_u^v \lambda\e^{-\lambda x} \d x \]
\end{definition}

\begin{definition}
    La fonction~$f$ définie sur~$\IN[0;+\infty;[$ par $f: x \mapsto \lambda
    \e^{-\lambda x}$ est la \define{fonction de densité de la loi
    exponentielle} de paramètre~$\lambda$ --- voir la
    figure~\ref{fig:exponential-density}.
\end{definition}

\begin{figure}
    \caption{Interprétation graphique de~$\Prob(X\in\IN[u;v;])$ pour la loi
        exponentielle: c'est l'aire sous la courbe de~$f$ entre $u$~et~$v$ avec
        $f(x) = \lambda \e^{\lambda x}$.}
    \label{fig:exponential-density}
    \begin{center}
    \begin{tikzpicture}
        \def\b{2.5}\def\u{0.5}\def\v{1}\def\l{1}
        \tikzset{declare function={
            f(\x) = \l * exp(-\l*\x);
        }}
        \datavisualization [school book axes,
                            all axes={unit length=3cm},
                            x axis={ticks={major at={
                                \u as $u$, \v as $v$
                            }}},
                            y axis={ticks={major at={
                                \l as $\lambda$,
                            }}},
                            visualize as smooth line=f,
                            visualize as line=R,
                            visualize as line=D,
                            f={label in data={text=$\mcC_f$, when=x is \v}},
                            R={polygon, style={every path/.style={
                                fill=black, fill opacity=0.05,
                            }}},
                            D={polygon, style={every path/.style={
                                draw, thin, pattern=north east lines
                            }}},
                        ]
        data [set=f, format=function] {
            var x : interval[0:\b];
            func y = f(\value x);
        }
        data point [set=R, x=0, y=0]
        data [set=R, format=function] {
            var x : interval[0:\b];
            func y = f(\value x);
        }
        data point [set=R, x=\b, y=0]
        data point [set=D, x=\u, y=0]
        data [set=D, format=function] {
            var x : interval[\u:\v];
            func y = f(\value x);
        }
        data point [set=D, x=\v, y=0]
        ;
    \end{tikzpicture}%
    \end{center}
\end{figure}


\subsection{Espérance}

\begin{activity}
    \begin{enumerate}
        \item Dans le~TP, quelle espérance avez-vous obtenu ?
        \item Quel lien peut-on supposer entre $\lambda$~et~$E(X)$ ?
    \end{enumerate}
\end{activity}

\begin{proposition}
    Si $X$~suit la loi exponentielle de paramètre~$\lambda$, alors $E(X) =
    \frac{1}{\lambda}$.
\end{proposition}

\begin{remark}
    L'espérance est toujours définie par~$E(X) = \int_a^b
    xf(x) \d x$. La démonstration de $E(X) = \frac{1}{\lambda}$ à partir de la
    définition précédente n'est pas au programme --- mais c'est un calcul
    d'intégrale.
\end{remark}

\subsection{Calcul de probabilités}

\begin{proposition}
    Si $X$~suit la loi exponentielle de paramètre~$\lambda$, alors
    pour~$t\ge0$ on a $\Prob(X \le t) = 1 - \e^{-\lambda t}$.
\end{proposition}


\section{Loi normale}

\subsection{Définition de la loi normale}

\begin{definition}
    Soient $\mu$~un nombre réel et $\sigma\ge0$. La \define{loi
    normale~$\mcN(\mu,\sigma)$} d'espérance~$\mu$ et d'écart-type~$\sigma$ est
    la loi de probabilités dont la fonction de densité est la \emph{gaussienne}
    définie sur~$\mdR$ par:
    \[ f(t) = \frac{1}{\sigma \sqrt{2\pi}} \e^{-\frac{(t-\mu)^2}{2\sigma^2}} \]
\end{definition}

\begin{remark}
    Si $X$ suit la loi normale~$\mcN(\mu,\sigma)$, alors
    \[ \Prob(\alpha \le X \le \beta) = \int_\alpha^\beta f(t) \d t =
        \int_\alpha^\beta \frac{1}{\sigma \sqrt{2\pi}}
        \e^{-\frac{(t-\mu)^2}{2\sigma^2}} \d t \]
    Voir la figure~\ref{fig:normal-density}.
\end{remark}

\begin{figure}
    \caption{Interprétation graphique de~$\Prob(X\in\IN[u;v;])$ pour la loi
        normale: c'est l'aire sous la courbe de~$f$ entre $u$~et~$v$.}
    \label{fig:normal-density}
    \begin{center}
    \begin{tikzpicture}
        \def\b{1.8}\def\u{0.5}\def\v{1.2}\def\m{1}\def\s{0.5}
        \pgfmathsetmacro\Xscale{1/(\s * sqrt(2*pi))}
        \pgfmathsetmacro\Yscale{-1/(\s * \s * 2)}
        \tikzset{declare function={
            f(\t) = \Xscale * exp(\Yscale * \t * \t);
        }}
        \datavisualization [school book axes,
                            all axes={unit length=3cm},
                            x axis={ticks={major at={
                                \u as $u$, \v as $v$
                            }}},
                            y axis={ticks={major at={
                            }}},
                            visualize as smooth line=f,
                            visualize as line=R,
                            visualize as line=D,
                            f={label in data={text=$\mcC_f$, when=x is \v}},
                            R={polygon, style={every path/.style={
                                fill=black, fill opacity=0.05,
                            }}},
                            D={polygon, style={every path/.style={
                                draw, thin, pattern=north east lines
                            }}},
                        ]
        data [set=f, format=function] {
            var t : interval[-\b:\b];
            func x = \value t + \m;
            func y = f(\value t);
        }
        data point [set=R, x=(\m-\b), y=0]
        data [set=R, format=function] {
            var t : interval[-\b:\b];
            func x = \value t + \m;
            func y = f(\value t);
        }
        data point [set=R, x=(\m+\b), y=0]
        data point [set=D, x=\u, y=0]
        data [set=D, format=function] {
            var x : interval[\u:\v];
            func y = f(\value x - \m);
        }
        data point [set=D, x=\v, y=0]
        ;
    \end{tikzpicture}%
    \end{center}
\end{figure}

On ne calcule pas cette intégrale à la main !

\begin{method}
    Probabilités d'une variable de loi normale avec la calculatrice.
    \IfStudentT{\bigfiller{6}}
\end{method}

\subsection{Propriétés}

\begin{proposition}
    Si $X$ suit la loi normale~$\mcN(\mu,\sigma)$, alors $E(X) = \mu$ et
    $\sigma(X) = \sigma$.
\end{proposition}

\begin{proposition}[Intervalles standards]
    Si $X$ suit la loi normale~$\mcN(\mu,\sigma)$, alors
    \begin{enumerate}
        \item $\Prob(\mu-\sigma \le X \le \mu+\sigma) \approx \<0,68>$ à
            $10^{-3}$ près;
        \item $\Prob(\mu-2\sigma \le X \le \mu+2\sigma) \approx \<0,954>$ à
            $10^{-3}$ près;
        \item $\Prob(\mu-3\sigma \le X \le \mu+3\sigma) \approx \<0,997>$ à
            $10^{-3}$ près.
    \end{enumerate}
    Voir la figure~\ref{fig:std-int} pour une visualisation graphique de ce
    théorème.
\end{proposition}

\begin{figure}
    \caption{Probabilités des intervalles un, et deux et trois sigma pour la loi
        normale.}
    \label{fig:std-int}
    \begin{center}
    \begin{tikzpicture}
        \def\b{1.8}\def\m{-0.7}\def\s{0.45}
        \pgfmathsetmacro\Xscale{1/(\s * sqrt(2*pi))}
        \pgfmathsetmacro\Yscale{-1/(\s * \s * 2)}
        \tikzset{declare function={
            f(\t) = \Xscale * exp(\Yscale * \t * \t);
        }}
        \datavisualization [school book axes,
                            all axes={unit length=3cm},
                            x axis={ticks and grid={major at={
                                \m as $\mu$,
                                (\m-\s) as $\mu-\sigma$,
                                (\m+\s) as $\mu+\sigma$,
                                (\m-2*\s) as $\mu-2\sigma$,
                                (\m+2*\s) as $\mu+2\sigma$,
                                (\m-3*\s) as $\mu-3\sigma$,
                                (\m+3*\s) as $\mu+3\sigma$,
                                }},
                            },
                            y axis={ticks={major at=}},
                            visualize as smooth line=f,
                            visualize as line/.list={S1,S2,S3},
                            S3={polygon, style={every path/.style={
                                fill=black, fill opacity=0.05,
                            }}},
                            S2={polygon, style={every path/.style={
                                fill=black, fill opacity=0.05,
                            }}},
                            S1={polygon, style={every path/.style={
                                pattern=north east lines
                            }}},
                            every grid/.style={major={low=min}},
                        ]
        data [set=f, format=function] {
            var t : interval[-\b:\b];
            func x = \value t + \m;
            func y = f(\value t);
        }
        data point [set=S1, x=(\m-\s), y=0]
        data [set=S1, format=function] {
            var t : interval[-\s:\s];
            func x = \value t + \m;
            func y = f(\value t);
        }
        data point [set=S1, x=(\m+\s), y=0]
        data point [set=S2, x=(\m-2*\s), y=0]
        data [set=S2, format=function] {
            var t : interval[(-2*\s):(2*\s)];
            func x = \value t + \m;
            func y = f(\value t);
        }
        data point [set=S2, x=(\m+2*\s), y=0]
        data point [set=S3, x=(\m-3*\s), y=0]
        data [set=S3, format=function] {
            var t : interval[(-3*\s):(3*\s)];
            func x = \value t + \m;
            func y = f(\value t);
        }
        data point [set=S3, x=(\m+3*\s), y=0]
        ;
    \end{tikzpicture}%
    \end{center}
\end{figure}


\subsection{De la loi binomiale à la loi normale}

\begin{activity}
    Dans le~TP, dès qu'un microprocesseur a une panne il est définitivement hors
    service. On suppose maintenant que les pannes sont intermittentes,
    c'est-à-dire qu'un mois donné le processeur a une probabilité
    de~$\frac{1}{20}$ d'être en panne, qu'il ait été en panne ou pas les mois
    précédents.
    \begin{enumerate}
        \item \begin{enumerate}
                \item Indiquer quelle expérience aléatoire est répétée plusieurs
                    fois.
                    \IfStudentT{\bigfiller{2}}
                \item Les expériences sont-elles indépendantes ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item On examine la situation sur trois mois.
            \begin{enumerate}
                \item Représenter un arbre pondéré correspondant à la situation.
                    \IfStudentT{\bigfiller{5}}
                \item Quelle est la probabilité qu'un microprocesseur fonctionne
                    trois mois sur trois ?
                    \IfStudentT{\bigfiller{1}}
                \item Et deux mois sur trois ?
                    \IfStudentT{\bigfiller{1}}
            \end{enumerate}
        \item \begin{enumerate}
                \item Quelle est la probabilité qu'un microprocesseur choisi au
                    hasard fonctionne exactement \<34>~mois\footnote{pas
                    forcément consécutifs} sur les trois~ans ?
                    \IfStudentT{\bigfiller{1}}
                \item Au moins \<33>~mois ?
                    \IfStudentT{\bigfiller{1}}
                \item Déterminer l'espérance et l'écart-type du nombre~$X$ de
                    mois de fonctionnement d'un microprocesseur.
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item \begin{enumerate}
                \item Représenter dans \bsc{Geogebra} le diagramme $P(X = k)$
                    pour $0 \le k \le 36$ --- on pourra utiliser la commande
                    \texttt{Binomiale[$n$, $p$]}.
                \item Représenter la \emph{gaussienne} de paramètres
                    $E(X)$~et~$\sigma(X)$ --- c'est la commande
                    \texttt{Normale[$E$, $\sigma$, x]}.
                \item La gaussienne correspond-t-elle bien ?
                    \IfStudentT{\bigfiller{1}}
            \end{enumerate}
        \item Recommencer avec $n=36$ toujours, mais $p=\frac{1}{2}$. Que
            remarque-t-on ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{activity}


On peut ainsi «approcher» une loi binomiale par la loi normale \emph{de même
espérance et même écart-type}, pourvu que $n$~soit «assez grand» et $p$ «ni trop
proche de~$0$, ni trop proche de~$1$». Les conditions précises seront données
dans les énoncés d'exercices.

Rappel: une variable aléatoire~$X$ de loi binomiale~$\mcB(n,p)$ compte le nombre
de succès lors de $n$~expériences \emph{indépendantes} à probabilité~$p$ de
succès chacune. On a

\[ E(X) = np \qquad\text{et}\qquad \sigma(X) = \sqrt{np(1-p)} \]

Lorsque $n$ est trop grand pour calculer les valeurs exactes de $P(a\le X\le b)$
avec la loi binomiale, on peut obtenir des valeurs approchées en utilisant la
loi normale~$\mcN(\mu,\sigma)$ où $\mu = np$ et $\sigma = \sqrt{np(1-p)}$.

\end{document}

