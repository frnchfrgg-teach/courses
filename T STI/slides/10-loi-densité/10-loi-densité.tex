% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[aspectratio=169]{beamer}

\usetheme{RivaudVideo}
\usepackage{perso}

\usepackage{tikz}
\NewDocumentCommand\circled {m} {%
    \tikz[baseline=(char.base)]{
        \node[shape=circle,draw,inner sep=0.3ex] (char) {\textup{#1}};}%
}
\setbeamertemplate{enumerate items}{\circled{\arabic{enumi}}}
\setbeamertemplate{enumerate subitem}{\textit{\alph{enumii}}\textup{)}}
\setbeamerfont{enumerate item}{family=\rmfamily}

\usepackage{rivmath}

\setmainlanguage{french}

\author{J. \bsc{Rivaud}}
\title{Lois de probabilité à densité}

\def\Prob(#1){p\!\left(#1\right)}
\def\d{\mathrm{d}}


\usetikzlibrary{overlay-beamer-styles}

\usetikzlibrary{datavisualization, patterns,
                datavisualization.formats.functions}

\usepackage{multicol}

\begin{document}

\section{De plus en plus d'issues}

\subsection{\circled{1} Entiers}

\begin{frame}
    \onslide<+->
    \begin{enumerate}
        \item Une formule de tableur permet d'obtenir un nombre entier au hasard
            entre $0$~inclus et $10$~exclus. Chacun des résultats possibles
            arrive avec la même probabilité.\\
            On note~$X$ la variable aléatoire
            donnant la valeur renvoyée par la formule.
            \begin{enumerate}
                \item Quels sont valeurs possibles de~$X$ ?
                \item Déterminer $\Prob(X = 4)$, puis $\Prob(3 \le X < 7)$.
            \end{enumerate}
    \end{enumerate}
    \bigskip
    \onslide<+->
    Valeurs de $X$: $0$; $1$; $2$;
    \alert<4>{$3$; $4$; $5$; $6$};
    $7$; $8$; $9$.

    \onslide<+->
    $\Prob(X=4) = \dfrac{\text{issues favorables}}{\text{total}}
    = \dfrac{1}{10}$
    (valeurs \emph{équiprobables})

    \onslide<+->
    $\Prob(3 \le X < 7) = \dfrac{4}{10}$
\end{frame}

\subsection{\circled{2} À $10^{-1}$ près}

\begin{frame}
    \onslide<+->
    \begin{enumerate}[start=2]
        \item Une formule de tableur permet d'obtenir un nombre au hasard
            entre $0$~inclus et $10$~exclus, avec une écriture décimale
            ayant \emph{un chiffre après la virgule}, de manière équiprobable.\\
            On note~$X$ la variable aléatoire
            donnant la valeur renvoyée par la formule.
            \begin{enumerate}
                \item Quels sont valeurs possibles de~$X$ ?
                \item Déterminer $\Prob(X = 4)$, puis $\Prob(3 \le X < 7)$.
            \end{enumerate}
    \end{enumerate}
    \bigskip
    \onslide<+->
    Valeurs de $X$: $0$; $\<0.1>$; $\<0.2>$; \dots ; $\<2.9>$;
    \alert<4>{$3$; $\<3.1>$; \dots; $\<6.9>$}; $7$; \dots; $\<9.9>$ \\
    Valeurs de $X$: $\frac{k}{10}$ avec $k$~entier, $0 \le k \le  99$

    \onslide<+->
    $\Prob(X=4) = \dfrac{1}{100}$ ($100$~valeurs équiprobables)

    \onslide<+->
    De $\<3.0>$~à~$\<6.9>$: $40$~nombres à une décimale \\
    $\Prob(3 \le X < 7) = \dfrac{40}{100} = \dfrac{4}{10}$
\end{frame}

\subsection{\circled{3} À $10^{-5}$ près}

\begin{frame}
    \onslide<+->
    \begin{enumerate}[start=3]
        \item Une formule de tableur permet d'obtenir un nombre au hasard
            entre $0$~inclus et $10$~exclus, avec une écriture décimale ayant
            \emph{$5$ chiffres après la virgule}, de manière équiprobable.\\
            On note~$X$ la variable aléatoire
            donnant la valeur renvoyée par la formule.
            \begin{enumerate}
                \item Quels sont valeurs possibles de~$X$ ?
                \item Déterminer $\Prob(X = 4)$, puis $\Prob(3 \le X < 7)$.
            \end{enumerate}
    \end{enumerate}
    \bigskip
    \onslide<+->
    Valeurs de $X$: $\frac{k}{10^5}$ avec $k$~entier, $0 \le k \le 10^6-1$
    \qquad
    \onslide<+->
    $\Prob(X=4) = \dfrac{1}{10^6} = 10^{-6}$

    \onslide<+->
    \parbox{0.5\textwidth}{%
    \begin{itemize}
        \item Pour $3 \le X < 4$: $10^{5}$~valeurs possibles
        \item Pour $4 \le X < 5$: $10^{5}$~valeurs possibles
        \item Pour $5 \le X < 6$: \dots
        \item Pour $6 \le X < 7$: \dots
    \end{itemize}
    }
    \qquad
    \onslide<+->
    $\Prob(3 \le X < 7) = \dfrac{4 \times 10^5}{10^6} = \dfrac{4}{10}$
\end{frame}

\subsection{\circled{4} Conclusion}

\begin{frame}
    \onslide<+->
    Une formule de tableur permet d'obtenir un nombre au hasard entre $0$~inclus
    et $10$~exclus. $X$~est la variable aléatoire correspondant à la valeur
    obtenue.

    {\small\setlength\tabcolsep{1ex}\centering
    \begin{tabular}{lccc}
        \firsthline
            Type de nombre renvoyé & Entier & Multiple de $\<0.1>$
                                    & Multiple de $10^{-5}$ \\
        \hline
            $\Prob(X=4)$ & $\frac{1}{10}$ & $\frac{1}{100}$ & $10^{-6}$ \\
            $\Prob(3 \le X < 7)$ & $\frac{4}{10}$ & $\frac{4}{10}$
                                    & $\frac{4}{10}$ \\
        \lasthline
    \end{tabular}
    \par}

    \onslide<+->
    \begin{enumerate}[start=4]
        \item \begin{enumerate}
                \item Que remarque-t-on ?
                \item Que se passe-t-il si la précision augmente beaucoup (\<50>
                    voire \<1000> chiffres après la virgule) ? Et si on tire au
                    hasard un nombre réel quelconque (avec une écriture décimale
                    potentiellement infinie) ?
            \end{enumerate}
    \end{enumerate}
    \bigskip
    \begin{itemize}
        \item<+-> Quand le nombre~$N$ de décimales augmente, $\Prob(X=4)$
            diminue.
        \item<.-> $\Prob(3 \le X < 7)$ reste constant.
        \item<+-> $N \to +\infty \Longrightarrow \Prob(X=4) \to 0$.
    \end{itemize}
\end{frame}


\section{Loi uniforme sur~$\IN[0;1;]$}

\begin{frame}
    \onslide<+->
    Soit $X$~suivant la loi uniforme sur~$\IN[0;1;]$.
    \begin{enumerate}
        \item Déterminer $\Prob(\<0.1>\le X\le\<0.5>)$ et $\Prob(X\le\<0.1>)$.
        \item Déterminer $\Prob(X\le\frac{1}{2})$ et
            $\Prob(X>\frac{1}{2})$.
        \item Commenter.
    \end{enumerate}
    \bigskip
    \begin{itemize}[<+->]
        \item $\Prob(\<0.1>\le X\le\<0.5>) = \Prob(X\in\IN[\<0.1>;\<0.5>;])
                = \<0.5> - \<0.1> = \<0.4>$
        \item $\Prob(X \le \<0.1>) = \Prob(X\in\IN[0;\<0.1>;])
                = \<0.1> - 0 = \<0.1>$
        \item $\Prob(X \le \frac{1}{2}) = \frac{1}{2} - 0 = \frac{1}{2}$
        \item $\Prob(X > \frac{1}{2}) = 1 - \frac{1}{2} = \frac{1}{2}$
    \end{itemize}

    \onslide<+->
    Répartition \emph{symétrique}
\end{frame}


\section{Loi uniforme sur~$\IN[a;b;]$}

\begin{frame}
    \onslide<+->
    Loi uniforme sur~$\IN[a;b;]$: il existe $k\in\mdR$ tel que\\
    $\Prob(X\in\IN[u;v;]) = \Prob(u \le X \le v) = k(v-u)$
    pour $a \le u \le v \le b$.

    \medskip
    Quelle valeur prendre pour~$k$ ?

    \vspace{1cm}

    \onslide<+->
    On veut $\Prob(X\in\IN[a;b;]) = 1$:\quad
    \onslide<+->
    $k(b-a) = 1$

    \bigskip

    \onslide<+->
    $k = \dfrac{1}{b-a}$
    \onslide<+->
    \quad donc \quad
    $\Prob(X\in\IN[u;v;]) = \dfrac{v-u}{b-a}$
    pour $a \le u \le v \le b$.

\end{frame}


\section{Loi uniforme sur~$\IN[-4;\<12.5>;]$}

\begin{frame}
    \onslide<+->
    \parbox{0.6\textwidth}{
    Soit $X$~suivant la loi uniforme sur~$\IN[-4;\<12.5>;]$.
    \begin{enumerate}
        \item Déterminer $\Prob(0\le X\le 11)$.
        \item Déterminer $\Prob(X < 0)$ et $\Prob(X \ge 0)$.
    \end{enumerate}
    }
    \onslide<.(1)->{$\Prob(X\in\IN[u;v;]) = \dfrac{v-u}{b-a}$}
    \bigskip
    \begin{itemize}[<+->]
        \item $\Prob(0\le X\le 11) = \Prob(X\in\IN[0;11;])
                = \dfrac{11-0}{\<12.5>-(-4)} = \dfrac{11}{\<16.5>}
                = \dfrac{22}{33} = \dfrac{2}{3}$
        \item $\Prob(X < 0) = \Prob(X\in\IN[-4;0;[)
            = \dfrac{0-(-4)}{\<16.5>} = \dfrac{8}{33}$
        \item $\Prob(X \ge 0) = 1 - \Prob(X < 0) = \dfrac{25}{33}$
    \end{itemize}
\end{frame}


\section{Interprétation graphique de la loi uniforme}

\setbox0=\hbox\bgroup % prevent any frame creation
\tikz \datavisualization data group {unirect} = {
    data [set=f] {
        x, y
        \a, \h
        \v, \h
        \b, \h
    }
    data [set=R] {
        x, y, name
        \a, 0, A
        \a, \h, A'
        \b, \h, B'
        \b, 0, B
    }
    data [set=D] {
        x, y, name
        \u, 0, SW
        \u, \h
        \v, \h, NE
        \v, 0
    }
};
\egroup

\begin{frame}
    \begin{multicols}{2}
    \begin{center}
    $X$~suit la loi uniforme sur~$\IN[a;b;]$.
    \bigskip
    \begin{tikzpicture}
        \def\a{0.2}\def\b{1.45}\def\u{0.5}\def\v{1}
        \pgfmathsetmacro\h{1/(\b-\a)}
        \datavisualization [school book axes,
                            all axes={unit length=3.5cm},
                            x axis={include value=1.6,
                                ticks={major at={\a as $a$, \b as $b$},
                                    /tikz/alt=<3->{major also at/.list={
                                        \u as $u$, \v as $v$
                                    }},
                            }},
                            y axis={ticks and grid={major at={
                                \h as $\frac{1}{b-a}$,
                            }}},
                            visualize as line=f,
                            visualize as line=R,
                            visualize as line=D,
                            f={/tikz/alt=<5->{
                                label in data={text=$\mcC_f$, when=x is \v},
                            }{}},
                            R={polygon, style={every path/.style={
                                draw, dashed, thin,
                                fill=black, fill opacity=0.05,
                            }}},
                            D={polygon, style={every path/.style={
                                draw, thin, pattern=north east lines,
                                visible on=<3->,
                            }}},
                        ]
        data group {unirect}
        info {
            \only<3->{
            \path (SW) -- node[fill=black!5,inner sep=2pt] {$\mcD$} (NE);
            }
            \node[above left] at (A) {$A$};
            \node[above] at (A') {$A'$};
            \node[above] at (B') {$B'$};
            \node[above right] at (B) {$B$};
        }
        ;
    \end{tikzpicture}%
    \end{center}
    \columnbreak

    \onslide<2->
    $\mcA_{ABB'A'} = L \times h = AB \times AA'$ \\
    $\phantom{\mcA_{ABB'A'}} = (b-a) \times \frac{1}{b-a} = 1$

    \bigskip

    \onslide<3->
    $\mcA_{\mcD} = L \times h = (v-u) \times \frac{1}{b-a} = \dfrac{v-u}{b-a}$

    \bigskip

    \onslide<4->
    $\displaystyle
    \alert<5>{\Prob(X\in\IN[u;v;])}
    = \frac{v-u}{b-a} = \mcA_{\mcD}
    \visible<5>{ = \alert<5>{\int_u^v f(x) \d x} }$

    \visible<5>{$f(x) = \frac{1}{b-a}$ pour tout~$x\in\IN[a;b;]$}

    \end{multicols}
\end{frame}


\section{Espérance: du discret au continu}

\subsection{\circled{1} et \circled{2} Calculs de moyenne}

\begin{frame}
    \onslide<+->
    \begin{enumerate}
        \item Pacôme a eu $7$~notes au premier trimestre, toutes de
            coefficient~$1$:\\
            deux~$\<10>$, trois~$\<11,5>$, un~$\<12,5>$
            et~un~$\<14>$. Quelle est sa moyenne ?
        \item Au deuxième trimestre, \SI{10}{\percent}~de ses notes sont
            des~$8$, \SI{40}{\percent} sont des~$\<9,5>$, \SI{20}{\percent} sont
            des~$\<11,5>$ et \SI{30}{\percent} sont des~$\<13>$. Quelle est sa
            moyenne ?
    \end{enumerate}
    \bigskip
    \begin{itemize}[<+->][itemsep=4ex]
        \item Premier trimestre: $\bar{x} = \dfrac
            {2 \times 10 + 3 \times \<11.5> + 1 \times \<12.5> + 1 \times 14}
            {2 + 3 + 1 + 1}
            = \dfrac{81}{7} \approx \<11.57>$
        \item Deuxième trimestre: $\bar{y} =
            \<0.1> \times 8 + \<0.4> \times \<9.5>
            + \<0.2> \times \<11.5> + \<0.3> \times 13
            = \<10.8>$
    \end{itemize}
\end{frame}

\subsection{\circled{3} à \circled{5} Découverte de la formule}

\begin{frame}
    \onslide<1->
    \begin{enumerate}[start=3]
        \item Écrire la formule donnant l'espérance d'une variable aléatoire
            prenant les valeurs \\
            $x_1$, \dots, $x_n$. Comparer à la formule de la
            somme des probabilités.
        \item Que devient «la somme des probabilités est~$1$» pour
            les lois à densité ?
        \item Proposer une formule pour l'espérance d'une variable aléatoire
            de densité~$f$.
    \end{enumerate}
    \onslide<2->
    \medskip
    \everymath{\displaystyle}
    \def\x_####1{\alert<5,7>{x_{####1}}}
    \begin{tabular}{|l|c|c|}
        \firsthline
        Loi de proba &
            \small$\begin{array}{*5{c}}
                \firsthline
                x_i            & x_1 & x_2 & \ldots & x_n \\
                \Prob(X = x_i) & p_1 & p_2 & \ldots & p_n \\
                \lasthline
            \end{array}$ &
            $\Prob(X\in\IN[u;v;]) = \int_u^v f(x) \d x$ \\
        \hline
        \onslide<3->{$\Prob(\Omega) = 1$} &
            \onslide<3->{$p_1 + p_2 + p_3 + \dots + p_n =
                        \sum_{i=1}^n \Prob(X=x_i) = 1$} &
            \onslide<6->{$\int_a^b f(x) \d x = 1$} \\
        \hline
        \onslide<4->{$E(X)$} &
            \onslide<4->{$\x_1 p_1 + \x_2 p_2 + \dots + \x_n p_n =
                        \sum_{i=1}^n \x_i \Prob(X=x_i)$} &
            \onslide<7->{$\int_a^b \alert<5,7>{x} f(x) \d x$} \\
        \lasthline
    \end{tabular}
\end{frame}


\section{Espérance de la loi uniforme}

\begin{frame}
    \everymath{\displaystyle}
    \onslide<+->
    \begin{theorem}
        Si $X$ suit la loi uniforme sur~$\IN[a;b;]$, alors $E(X) = \frac{a+b}{2}$.
    \end{theorem}
    \bigskip
    \onslide<+->
    \begin{proof}
    $E(X) = \int_a^b f(x) x \d x = \int_a^b \frac{1}{b-a} x \d x
        \onslide<+-> = G(b) - G(a)$ \\
    où $G$ est une primitive
    de~$g:x\mapsto \color<+>{blue} \frac{1}{b-a} \alert<.>{x}$.

    \onslide<.->
    $G(x)= {\color<.>{blue}\frac{1}{b-a}} \times \alert<.>{\frac{x^2}{2}}
        \onslide<+-> = \frac{x^2}{2(b-a)}$

    \bigskip
    \onslide<+->
    $E(X) = \frac{b^2}{2(b-a)} - \frac{a^2}{2(b-a)}
    = \frac{\alert<+>{b^2 - a^2}}{2(b-a)}
    \onslide<.-> = \frac{\alert<.>{(b+a)\alert<+>{(b-a)}}}{2\alert<.>{(b-a)}}
    \onslide<.-> = \frac{b+a}{2}$
    \onslide<+->
    \end{proof}
\end{frame}


\section{Variance et écart-type: du discret au continu}

\subsection{\circled{1} Variance d'une série}

\begin{frame}
    \onslide<+->
    Au deuxième trimestre, \SI{10}{\percent}~des notes de Pacôme sont des~$8$,
    \SI{40}{\percent} sont des~$\<9,5>$, \SI{20}{\percent} sont des~$\<11,5>$ et
    \SI{30}{\percent} sont des~$\<13>$.
    \begin{enumerate}
        \item Calculer \emph{à la main} la variance des notes de~Pacôme au
            deuxième trimestre.
    \end{enumerate}
    \bigskip
    \begin{itemize}[<+->][itemsep=4ex]
        \item $\bar{y} = \<0.1> \times 8 + \<0.4> \times \<9.5>
            + \<0.2> \times \<11.5> + \<0.3> \times 13
            = \<10.8>$
        \item $v(y) =
            \<0.1> \times \onslide<+->{(}8
                \onslide<.->{- \bar{y})}^{\onslide<.(1)->{2}} +
            \<0.4> \times \onslide<.->{(}\<9.5>
                \onslide<.->{- \bar{y})}^{\onslide<.(1)->{2}} +
            + \<0.2> \times \onslide<.->{(}\<11.5>
                \onslide<.->{- \bar{y})}^{\onslide<.(1)->{2}} +
            \<0.3> \times \onslide<.->{(}13
                \onslide<.->{- \bar{y})}^{\onslide<.(1)->{2}}
            \onslide<+(1)->
            = \<3.01>$
    \end{itemize}
\end{frame}

\subsection{\circled{2} et \circled{3} Variable aléatoire discrète}

\begin{frame}
    \onslide<+->
    \begin{enumerate}[start=2]
        \item Donner la \emph{formule} de la variance d'une variable
            aléatoire discrète.
        \item Comment calcule-t-on l'écart-type ?
    \end{enumerate}
    \bigskip
    \begin{itemize}[<+->][itemsep=3ex]
        \item Variance d'une série:
            $v(x) = \alert<.(2)>{p_1} {\color<.(2)>{blue}(x_1 - \bar{x})^2} +
                p_2 (x_2 - \bar{x})^2 + \dots +
                p_n (x_n - \bar{x})^2$
        \item Variance d'une variable aléatoire discrète~$X$ prenant les valeurs
            $x_1$, \dots, $x_n$:\\
            $V(X)           = {\color<+>{blue}\left( x_1 - E(X) \right)^2}
                                \alert<.>{\Prob(X=x_1)}$ \\
            $\phantom{V(X)} + \left( x_2 - E(X) \right)^2 \Prob(X=x_2)$ \\
            $\phantom{V(X)} + \dots$ \\
            $\phantom{V(X)} + \left( x_n - E(X) \right)^2 \Prob(X=x_n)$ 
        \item Écart-type: $\sigma(X) = \sqrt{V(X)}$
    \end{itemize}
\end{frame}


\section{Variance et écart-type d'une loi à densité}

\begin{frame}
    \onslide<+->
    \everymath{\displaystyle}
    \def\XX####1{\alert<.(1)>{\left( x_{####1} - E(X) \right)^2}}
    \centering
    \begin{tabular}{|l|c|c|}
        \firsthline
        Loi &
            \small$\begin{array}{*5{c}}
                \firsthline
                x_i            & x_1 & x_2 & \ldots & x_n \\
                \Prob(X = x_i) & p_1 & p_2 & \ldots & p_n \\
                \lasthline
            \end{array}$ &
            $\Prob(X\in\IN[u;v;]) = \int_u^v f(x) \d x$ \\
        \hline
        \onslide<+->{$E(X)$} &
            \onslide<.->{$x_1 p_1 + x_2 p_2 + \dots + x_n p_n =
                        \sum_{i=1}^n x_i \Prob(X=x_i)$} &
            \onslide<.->{$\int_a^b x f(x) \d x$} \\
        \hline
        \onslide<+->{$V(X)$} &
            \onslide<.->{$\begin{gathered}
                \XX{1} p_1 + \dots + \XX{n} p_n \\
                = \sum_{i=1}^n \XX{i} \Prob(X=x_i)
                \end{gathered}$} &
            \onslide<+->{$\int_a^b \alert<.>{\left(x-E(X)\right)^2} f(x) \d x$} \\
        \hline
        \onslide<+->{$\sigma(X)$} &
            \onslide<.->{$\sqrt{V(X)}$} &
            \onslide<.->{$\sqrt{V(X)}$} \\
        \lasthline
    \end{tabular}
\end{frame}

\section{Paramètres d'une loi uniforme: exemple}

\begin{frame}
    \onslide<+->
    Soit $X$~suivant la loi uniforme sur~$\IN[-4;\<12.5>;]$. Déterminer $E(X)$,
    $V(X)$ et~$\sigma(X)$.
    \bigskip
    \everymath{\displaystyle}
    \begin{itemize}[<+->][itemsep=3ex]
        \item $E(X) = \frac{a+b}{2} = \frac{-4 + \<12.5>}{2} = \<4.25>$
        \item $V(X) = \frac{(b-a)^2}{12} = \frac{(\<12.5>-(-4))^2}{12} =
            \<136.125>$
        \item $\sigma(X) = \dfrac{b-a}{\sqrt{12}} = \sqrt{V(X)} \approx
            \<11.67>$
    \end{itemize}
\end{frame}



\end{document}




