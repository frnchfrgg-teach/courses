% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\usetikzlibrary{matrix}

\rivmathlib{geometry}

\begin{document}

\StudentMode

\chapter{Équations}

\section{Définition}

\begin{definition}
    Une \define{équation} est une \emph{égalité} qui n'est pas une affirmation
    comme un théorème, mais une question. Une \define{inconnue} est une lettre
    dont on n'a pas fixé la valeur.
\end{definition}

À chaque fois qu'on choisit des valeurs pour les inconnues, on peut déterminer
si l'équation est \emph{vérifiée} --- l'égalité est vraie --- ou non.

\begin{definition}
    Une \define{solution} de l'équation~$(E)$ est la donnée d'une valeur pour
    chaque inconnue de~$(E)$ de sorte à ce que l'équation~$(E)$ soit vérifiée.
    \define{Résoudre} une équation, c'est trouver et donner \emph{toutes} ses
    solutions.
\end{definition}

\begin{example}
    \begin{enumerate}
        \item Vérifier que~$1$ est une solution de~$x^2 + 2x = 3$.
            \IfStudentT{\bigfiller{1}}
        %\item Savez-vous résoudre cette équation ?
        %    \IfStudentT{\hfiller}
    \end{enumerate}
\end{example}

\begin{exercise}[Vérification]
    86 p. 96
\end{exercise}

\section{Résolution graphique}

\begin{itemize}
    \item Les solutions de $f(x)=k$ sont les abscisses des points d'intersection
        entre la courbe représentative de~$f$ et la droite horizontale
        d'équation $y=k$.
    \item Les solutions de $f(x)=g(x)$ sont les abscisses des points
        d'intersection entre la courbe représentative de~$f$ et celle de~$g$.
\end{itemize}

\section{Résolution algébrique}

\subsection{Équations équivalentes}

\begin{definition}
    Deux équations $(E_1)$~et~$(E_2)$ sont \define{équivalentes} quand
    $(E_1)$~est vérifiée si et seulement si $(E_2)$~l'est. En d'autres termes
    deux équations sont équivalentes quand elles ont exactement les mêmes
    solutions. Dans ce cas, on écrit $(E_1) \iff (E_2)$.
\end{definition}

\begin{remark}
    Par convention en seconde, deux équations écrites l'une sous l'autre sont
    considérées équivalentes. Le symbole $\iff$ n'est donc pas indispensable
    pour l'instant.
\end{remark}

\begin{proposition}
    On obtient une équation équivalente à l'équation~$(E)$ en:
    \begin{itemize}
        \item Ajoutant ou retranchant la même formule de chaque \emph{membre}
            de~$(E)$;
        \item Multipliant ou divisant chaque membre de~$(E)$ par un même
            \emph{nombre non nul}.
    \end{itemize}
\end{proposition}

\begin{remark}
    C'est ainsi qu'on résout toutes les équations du \emph{premier degré}, en
    isolant par étapes l'inconnue.
\end{remark}

\TeacherOnly
\begin{exercise}[Résolution d'équations]
    76 p.~95
\end{exercise}

\begin{exercise}[Problème du premier degré]
    78 p.~96
\end{exercise}
\EndTeacherOnly

\subsection{Équations produits nuls}

Lorsque l'équation contient des inconnues au carré ou plus compliqué, les règles
précédentes ne suffisent plus à la résoudre. On utilise alors la proposition
suivante:

\begin{proposition}
    Dans~$\mdR$, un produit de facteurs est nul si et seulement si \UGHOST{au
    moins l'un des facteurs est nul.}
\end{proposition}

On change alors de stratégie:

\begin{method}[Résolution de degré supérieur]
    \begin{enumerate}
        \item \UGHOST{En retranchant le deuxième membre,} on bascule tout dans
            le membre de gauche pour obtenir une équation \UGHOST{de la forme
            $\textit{<formule>} = 0$.}
        \item On \UGHOST{factorise} le membre de gauche pour obtenir une
            équation de la forme $A_1 A_2 A_3 \dots A_k = 0$ --- c'est une
            équation \UGHOST{produit nul.}
        \item On remplace par le groupe d'équations «$A_1 = 0$~ou $A_2 = 0$~ou
            $A_3 = 0$\ldots»
        \item On résout chaque équation indépendamment.
    \end{enumerate}
\end{method}

\TeacherOnly
\begin{exercise}[Factorisations]
    51, 55 et~62 p.~94
\end{exercise}

\begin{exercise}[La bonne forme]
    99 p.~97
\end{exercise}
\EndTeacherOnly

\subsection{Équations quotient nul}

\begin{proposition}
    Une fraction est nulle si et seulement si:
    \begin{itemize}
        \item \UGHOST{son numérateur est nul, \emph{et}}
        \item \UGHOST{son dénominateur n'est pas nul}
    \end{itemize}
\end{proposition}

\begin{remark}
    Si le dénominateur d'une fraction est nul, alors la fraction
    \textbf{n'a pas de résultat} du tout. En effet:
    \begin{itemize}
        \small
        \item Calculer $\frac{8}{2} = 8 \div 2$ revient à trouver le nombre
            manquant dans la multiplication $2 \times ? = 8$. La réponse
            est~$4$.
        \item Calculer $\frac{8}{0} = 8 \div 0$ revient à trouver le nombre
            manquant dans la multiplication $0 \times ? = 8$. Il n'y a
            pas de réponse possible, car multiplier n'importe quel nombre
            par~$0$ donne toujours~$0$, pas~$8$: $\frac{8}{0}$~n'existe pas.
        \item Calculer $\frac{0}{0} = 0 \div 0$ revient à trouver le nombre
            manquant dans la multiplication $0 \times ? = 0$. Ici tous les
            résultats sont possibles; on décide que $\frac{0}{0}$~n'existe
            pas\footnote{Si $\frac{0}{0}=3$ et $\frac{0}{0}=-8$ alors par
            \emph{transitivité} $3 = -8$, ce qui pose quelques problèmes\ldots}.
    \end{itemize}
\end{remark}

\begin{definition}
    Une équation de la forme $\dfrac{A}{B} = 0$ est appelée \define{équation
    quotient nul}.

    Les solutions de~$A=0$ sont appelées \UGHOST{\define{valeurs d'annulation}}
    de~$\frac{A}{B}$; ce sont les solutions potentielles de $\frac{A}{B}=0$ ---
    les candidats.

    Les solutions de~$B=0$ sont appelées \UGHOST{\define{valeurs interdites}}
    de~$\frac{A}{B}$; ce sont les valeurs de l'inconnue qui font que
    $\frac{A}{B}$~n'a même pas de résultat --- et donc ne donne certainement
    pas~$0$ !
\end{definition}

\begin{method}[Résolution d'équations quotient]
    \begin{enumerate}
        \item En retranchant le deuxième membre, on bascule tout dans le membre
            de gauche pour obtenir une équation de la forme $\textit{<formule>}
            = 0$.
        \item On \UGHOST{met le membre de gauche au même dénominateur}, puis on
            regroupe pour obtenir une équation de la forme $\dfrac{A}{B} = 0$
            --- c'est une équation quotient nul.
        \item On remplace par «\UGHOST{$A = 0 \text{ et } B \ne 0$}».
        \item On résout $A = 0$ --- le plus souvent par la méthode du produit
            nul --- pour obtenir les valeurs d'annulation. On résoud $B \ne 0$
            --- ou $B = 0$ ce qui revient au même --- pour obtenir les valeurs
            interdites.
        \item L'ensemble des solutions de l'équation s'obtient en prenant
            \UGHOST{les valeurs d'annulation \emph{auxquelles on retire} les
            valeurs interdites.}
    \end{enumerate}
\end{method}

\TeacherOnly
\begin{exercise}
    109 p.~98
\end{exercise}
\EndTeacherOnly

\section{Systèmes d'équations}

\begin{definition}
    Un \define{système d'équations} est un ensemble d'équations qu'on cherche à
    résoudre simultanément. Les solutions d'un système sont les solutions
    \emph{communes} à toutes les équations du système --- c'est-à-dire les
    groupes de valeurs à donner aux inconnues pour que toutes les équations
    soient vérifiées \emph{simultanément}.
\end{definition}

On s'intéressera ici aux systèmes linéaires de deux équations à deux inconnues.
Quelque soit la méthode, on conserve un système d'équations tout au long de la
résolution: alors qu'on modifie une équation on continue à écrire l'autre sans
la changer.

\begin{method}[résolution par substitution]
    \begin{enumerate}
        \item Dans une des équations, isoler une des inconnues pour l'exprimer
            en fonction de l'autre.
        \item Dans l'autre équation, remplacer \emph{chaque occurence} de
            l'inconnue isolée par l'expression trouvée à l'étape précédente: on
            obtient alors une équation qui n'a plus qu'une seule inconnue.
        \item Résoudre l'équation obtenue, en utilisant les techniques du
            premier degré pour isoler l'inconnue.
        \item Dans la première équation, remplacer la deuxième inconnue par sa
            valeur juste trouvée ce qui permet de calculer la valeur de
            l'inconnue manquante.
        \item Conclure avec l'ensemble des solutions --- il n'y en a qu'une,
            mais c'est un couple de nombres.
    \end{enumerate}
\end{method}

\TeacherOnly
\begin{example}
    Résoudre le système $(S) \left\{ \begin{aligned}
            &x = y + 3 \\
            &y + x + 9 = 0
        \end{aligned}
    \right.$
\end{example}
\EndTeacherOnly

\begin{method}[résolution par combinaison]
    \begin{enumerate}
        \item Dans une des équations, multiplier les deux membres par un même
            coefficient non nul, et faire de même pour l'autre équation. Le
            but est d'obtenir un nouveau système, équivalent au premier, dans
            lequel l'une des inconnues a un coefficient opposé dans chaque
            équation.
        \item Remplacer la deuxième équation par la somme des deux équations
            --- sans modifier la première bien entendu. Réduire.
        \item La deuxième équation devrait n'avoir plus qu'une inconnue. La
            résoudre.
        \item Dans la première équation, remplacer la deuxième inconnue par sa
            valeur juste trouvée ce qui permet de calculer la valeur de
            l'inconnue manquante.
        \item Conclure.
    \end{enumerate}
\end{method}

\TeacherOnly
\begin{example}
    Résoudre le système $(S) \left\{ \begin{aligned}
            &x - y = 8 \\
            &2y + x + 1 = 0
        \end{aligned}
    \right.$
\end{example}
\EndTeacherOnly


\end{document}
