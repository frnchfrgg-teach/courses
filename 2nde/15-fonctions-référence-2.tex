% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\usetikzlibrary{matrix}

\rivmathlib{geometry}

\StudentMode
\begin{document}

\chapter{D'autres fonctions de référence}

\section{Trinômes de degré~$2$}

\subsection{Les différentes formes}

\begin{definition}
Soit~$P$~une fonction. On dit que $P$~est une \define{fonction polynomiale de
degré~$2$} si on peut la mettre sous la forme~$P(x) = ax^2 + bx + c$ où
$a$,~$b$, et~$c$ sont des nombres réels ($a\neq 0$). Cette forme est appelée
forme \emph{développée, réduite et ordonnée} de~$P$.
\end{definition}

\begin{example}
    Les fonctions suivantes sont-elles polynomiales du second degré ?
    \begin{enumerate}[gathered, label={$\Alph*(x)={}$},labelsep=0pt,start=16,
                       centered]
        \item $6x^2-5x+3$
        \item $2 - 3x^2$
        \item $x + 7$
        \item $\sqrt2 x + \sqrt3 x^2 - 1$
        \item $\sqrt{2x} + \sqrt3 x^2 - 1$
        \item $(x+3)(1-x)$
    \end{enumerate}
\end{example}

Toute écriture de~$P(x)$ sous forme de produit de deux facteurs du premier degré
est appelée forme \emph{factorisée} de~$P$.

\begin{definition}
Soit~$P$~une fonction polynomiale de degré~$2$. On appelle \define{forme
canonique} de~$P$ toute écriture de~$P(x)$ dans laquelle la variable~$x$
n'apparait qu'une seule fois. On utilise souvent la forme~$P(x) = a(x-\alpha)^2
+ \beta$, avec $\beta = P(\alpha)$.
\end{definition}

\begin{example}
    Soit $P(x) = 2x^2 + 8x - 10$.
    \begin{enumerate}
        \item Montrer que $P(x) = 2(x+2)^2 - 18$.
        \item Montrer que $P(x) = 2(x-1)(x+5)$.
        \item Laquelle des trois formes est la plus adaptée pour résoudre
            $P(x) = 0$ ?
        \item Et pour résoudre $P(x) = -18$ ?
        \item Et pour démontrer quel est le minimum de~$P$ sur~$\mdR$ ?
    \end{enumerate}
\end{example}

\subsection{Représentation graphique}

\begin{definition}
    Soit $f:x\mapsto ax^2+bx+c$ une fonction polynomiale de degré~$2$. La courbe
    représentative de~$f$ est une \define{parabole}.
\end{definition}

\begin{proposition}
    Soit $f:x\mapsto a(x-\alpha)^2 + \beta$. Le point $\point S(\alpha;\beta)$
    est le \emph{sommet} de la parabole représentant~$f$ ---
    voir~\vref{fig:var-alpha,fig:var-beta,fig:var-a}.
\end{proposition}

\def\parab#1#2#3#4{%
    \begin{tikzpicture}
        \def\a{#1} \def\alp{#2} \def\bet{#3}
        \pgfmathsetmacro\yend{\a * pow(#4, 2) + (\bet)}
        \pgfmathsetmacro\ymin{min(0, \bet, \yend) - 0.4}
        \pgfmathsetmacro\ymax{max(0, \bet, \yend) + 0.4}
        \pgfmathsetmacro\vpos{ifthenelse(\a > 0, "below", "above")}
        \edef\Spos{\vpos\space left}
        \pgfmathsetmacro\Bpos{ifthenelse(\alp > 0, "left", "right")}
        \pgfmathsetmacro\Apos{%
            ifthenelse(\bet == 0, "\vpos\space right", "\Spos")%
        }
        \pgfmathsetmacro\Bpos{%
            ifthenelse(\alp == 0 || \bet == 0, "\vpos\space\Bpos", "\Bpos")%
        }
        \pgfmathsetmacro\Bstyle{%
            ifthenelse(\bet == 0, "", "draw")%
        }
        \datavisualization [school book axes,
                            all axes={ticks={major at={1}},
                                      unit length=.5cm},
                            visualize as scatter/.list={points,sym},
                            visualize as smooth line=P,
                            sym={style={mark=none}},
                        ]
        data [set=P, format=function] {
            var x : interval[(\alp-#4):(\alp+#4)];
            func y = \a * pow(\value x - \alp, 2) + (\bet);
        }
        data point [set=points, x=\alp, y=\bet,  name=S]
        data point [set=sym,    x=\alp, y=\ymin, name=min]
        data point [set=sym,    x=\alp, y=\ymax, name=max]
        data point [set=sym,    x=\alp, y=\yend, name=med]
        data point [set=sym,    x=\alp, y=0,     name=alp]
        info {
            \path [every node/.style={inner sep=.5ex, black},
                   \Bstyle, help lines]
                (S)  node[\Spos]  {$S$}
                -- (S -| 0,0) node [\Bpos] {$\beta$};
            \draw [dashed] (min) -- (max);
            \path (alp) node [\Apos] {$\alpha$};
        }
        ;
    \end{tikzpicture}%
}

\begin{figure}[p]
    \ffigbox{}{
        \begin{subfloatrow}[3]%
            \ffigbox{}{\caption{$\alpha<0$}
                \parab{0.8}{-1}{-2}{2.5}
            }
            \ffigbox{}{\caption{$\alpha=0$}
                \parab{0.8}{0}{-2}{2.5}
            }
            \ffigbox{}{\caption{$\alpha>0$}
                \parab{0.8}{2}{-2}{2.5}
            }
        \end{subfloatrow}
        \caption{Allure et sommet d'une parabole avec $\beta=-2$~et~$a=\<0.8>$.}
        \label{fig:var-alpha}
    }
\end{figure}

\begin{figure}[p]
    \ffigbox{}{
        \begin{subfloatrow}[3]%
            \ffigbox{}{\caption{$\beta<0$}
                \parab{0.8}{-1}{-2}{2.5}
            }
            \ffigbox{}{\caption{$\beta=0$}
                \parab{0.8}{-1}{0}{2.5}
            }
            \ffigbox{}{\caption{$\beta>0$}
                \parab{0.8}{-1}{1.5}{2.5}
            }
        \end{subfloatrow}
        \caption{Allure et sommet d'une parabole avec
            $\alpha=-1$~et~$a=\<0.8>$.}
        \label{fig:var-beta}
    }
\end{figure}

\begin{figure}[p]
    \ffigbox{}{
        \begin{subfloatrow}[3]%
            \ffigbox{}{\caption{$a=\<0.8>$}
                \parab{0.8}{-1}{2}{2.5}
            }
            \ffigbox[\FBwidth]{}{\caption{$a=\<0.2>$}
                \parab{0.2}{-1}{2}{2.8}
            }
            \ffigbox{}{\caption{$a=-1$}
                \parab{-1}{-1}{2}{2.5}
            }
        \end{subfloatrow}
        \caption{Allure et sommet d'une parabole avec
            $\alpha=-1$~et~$\beta=2$.}
        \label{fig:var-a}
    }
\end{figure}

\subsection{Sens de variation}

\begin{proposition}
    Soit $f:x\mapsto a(x-\alpha)^2 + \beta$ une fonction polynomiale du second
    degré. Alors:
    \begin{enumerate}[gathered, label=]
        \item \begin{tikzpicture}
                \begin{scope}[TAB]
                    \tkzTabInit [lgt=1.5,espcl=2.5]
                                {$x$/1,$P(x)$/2}
                                {$\;-\infty$,$\alpha$,$+\infty\;$}
                    \tkzTabVar {+/,-/$\beta$,+/}
                \end{scope}
                \node[above] at (current bounding box.north) {si $a>0$};
            \end{tikzpicture}
        \item \begin{tikzpicture}
                \begin{scope}[TAB]
                    \tkzTabInit [lgt=1.5,espcl=2.5]
                                {$x$/1,$P(x)$/2}
                                {$\;-\infty$,$\alpha$,$+\infty\;$}
                    \tkzTabVar {-/,+/$\beta$,-/}
                \end{scope}
                \node[above] at (current bounding box.north) {si $a<0$};
            \end{tikzpicture}
    \end{enumerate}

    %Ces résultats sont illustrés par \vref{fig:parabole}.
\end{proposition}


\end{document}
