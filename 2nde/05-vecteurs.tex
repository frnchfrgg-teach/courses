% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}


\begin{document}

\iffalse
\ExplSyntaxOn
\use:x {
    \exp_not:n {\pdfvariable pageattr}
    {
        /MediaBox [
            \dim_to_decimal_in_bp:n { 0in } ~
            \dim_to_decimal_in_bp:n { 0in } ~
            \dim_to_decimal_in_bp:n { \pagewidth - 0in} ~
            \dim_to_decimal_in_bp:n { \pageheight - 0in}
        ]
        /CropBox [
            \dim_to_decimal_in_bp:n { 1in } ~
            \dim_to_decimal_in_bp:n { 1in } ~
            \dim_to_decimal_in_bp:n { \pagewidth - 1in} ~
            \dim_to_decimal_in_bp:n { \pageheight - 1in}
        ]
    }
}
\ExplSyntaxOff
\fi

\StudentMode

\chapter{Vecteurs}

\section{Translations et vecteurs}

\begin{activity}
    Dans un repère orthonormé~$(O;I;J)$, placer les points $A(1;1)$, $B(-3;-1)$
    et~$C(2;-1)$.
    \begin{enumerate}
        \item Où et comment placer~$D$ de sorte à ce que $ABCD$~soit un
            parallélogramme ? 
            \IfStudentF{(essayer d'obtenir: symmétrique du milieu,
            parallèles des côtés, report des longueurs au compas, reproduction
        du décalage en carreaux)}
        \item Pour chacune des constructions, indiquer quel(s) théorème(s)
            permet(tent) de s'assurer que le quadrilatère~$ABCD$
            ainsi défini est bien un parallélogramme.
        \item En notant $D'(-2;-3)$ et $D''(-4;1)$, quelles hypothèses ne sont
            plus vérifiées par $D'$~et~$D''$ dans les démonstrations ?
        \item Énoncer des conditions sur~$[CD]$ par rapport à~$[BA]$ pour que
            $ABCD$~soit un parallélogramme.
            \IfStudentF{(même longueur, parallélisme, même
            sens pour éliminer~$D'$)}
    \end{enumerate}
\end{activity}

\clearpage

\begin{definition}
    La translation de $A$~vers~$B$, ou \define{translation de
    vecteur~$\vect{AB}$}, est la transformation du plan définie de la façon
    suivante: \UGHOST{l'image du point~$M$ est le symétrique~$M'$ de~$A$ par
    rapport au milieu de~$[BM]$.}

    Autrement dit, l'image de~$M$ est le point~$M'$ tel que
    \UGHOST{$[AM']$~et~$[BM]$ aient le même milieu.} Si $M$~n'est pas aligné
    avec $A$~et~$B$ cela revient à placer~$M$ de sorte à ce que $ABM'M$ soit un
    parallélogramme (voir \vref{fig:def-translate}).
\end{definition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}
        \ffigbox{}{\caption{}\label{fig:def-translate-1}
        \begin{tikzpicture}
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (2,0.4)   coordinate[label=above:$B$] (B);
            \path (1.5,-1)  coordinate[label=below:$M$] (M);
            \path ($(B)!0.5!(M)$) coordinate[label={[ghost]above left:$I$}] (I);
            \path ($(I)!-1!(A)$)  coordinate[label={[ghost]below:$M'$}] (M');

            \draw[->] (A) -- (B);
            \IfStudentTF{
                \draw (M) pic[point marker] {};
            }{
                \foreach[remember=\y as \x (initially B)] \y in {M',M,A} {
                    \draw[dashed] (\x) -- (\y);
                }
                \draw   (I) edge pic[dist marker=1] {} (A)
                            edge pic[dist marker=1] {} (M')
                            edge pic[dist marker=2] {} (B)
                            edge pic[dist marker=2] {} (M);
            }
        \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:def-translate-2}
        \begin{tikzpicture}
            \path (0,0)     coordinate[label=below:$A$] (A);
            \path (2,0.4)   coordinate[label=below:$B$] (B);
            \path ($ (A)!1.5!(B) $) coordinate[label=below:$N$]  (N);
            \path ($(B)!0.5!(N)$) coordinate[label={[ghost]below:$J$}] (J);
            \path ($(J)!-1!(A)$)  coordinate[label={[ghost]below:$N'$}] (N');

            \draw[->] (A) -- pic[tick marker] {} (B) pic[tick marker] {};
            \IfStudentTF{
                \draw (N) pic[point marker] {};
            }{
            \draw   (B) -- (J) pic[tick marker] {}
                    -- (N) pic[tick marker] {}
                    -- (N') pic[tick marker] {};
            \path[every edge/.style={draw=none}]
                    (J) edge pic[dist marker=1] {} (A)
                        edge pic[dist marker=1] {} (N')
                        edge pic[dist marker=2] {} (B)
                        edge pic[dist marker=2] {} (N);
            }
        \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Construction de deux translatés par la translation de
        vecteur~$\vect{AB}$.
        \subref{fig:def-translate-1} $M'$~est l'image de~$M$: $[AM']$~et~$[BM]$
        ont le même milieu~$I$.
        \subref{fig:def-translate-2} $N'$~est l'image de~$N$: $[AN']$~et~$[BN]$
        ont le même milieu~$J$.}
    \label{fig:def-translate}
    }
\end{figure}

\begin{remark}
    Un vecteur est donc la quantification d'un \emph{décalage}, c'est-à-dire la
    donnée des informations suivantes:
    \begin{enumerate}
        \item \UGHOST{une direction}: avec quelle inclinaison s'opère la
            translation;
        \item \UGHOST{une longueur}: sur quelle distance on déplace les points;
        \item \UGHOST{un sens}: vers où le déplacement est effectué le long du
            vecteur.
    \end{enumerate}
\end{remark}

\begin{definition}
    Les vecteurs $\vect{AB}$~et~$\vect{CD}$ sont égaux si \UGHOST{la translation
    de $A$~vers~$B$ est la même que celle de $C$~vers~$D$} --- c'est-à-dire si
    ces deux translations déplacent les points de la même façon\footnote{Deux
    fonctions ou transformations sont égales quand elles associent la même
    image à tous les éléments de leur ensemble de définition.}.
\end{definition}


\begin{proposition}
    \label{prop:vector-equality}
    $\vect{AB} = \vect{CD}$ si et seulement si $[AD]$~et~$[BC]$ ont le même
    milieu. Autrement dit $\vect{AB} = \vect{CD}$ si et seulement si $ABDC$ est
    un parallélogramme --- éventuellement plat (voir
    \vref{fig:prop-vector-equality}).
\end{proposition}

\begin{figure}
    \centering
    \caption{Condition d'égalité: $\vect{AB} = \vect{CD}$ si et seulement si
        $ABDC$ est un parallélogramme.}
    \label{fig:prop-vector-equality}
    \begin{tikzpicture}
        \path (0,0)     coordinate[label=above:$A$] (A);
        \path (4,-1)    coordinate[label=above:$B$] (B);
        \path (1,-1.5)  coordinate[label=below:$C$] (C);
        \path ($ (B)!0.5!(C) $) coordinate (I);
        \path ($ (I)!-1!(A) $)  coordinate[label=below:$D$] (D);

        \draw[dashed] (A) -- (C) (D) -- (B);
        \draw[->] (A) -- (B);
        \draw[->] (C) -- (D);
        \draw   (I) edge pic[dist marker=1] {} (A)
                    edge pic[dist marker=1] {} (D)
                    edge pic[dist marker=2] {} (B)
                    edge pic[dist marker=2] {} (C);
    \end{tikzpicture}
\end{figure}

\begin{remark}
    Les vecteurs $\vect{AB}$~et~$\vect{CD}$ sont égaux si et seulement si ils
    ont:
    \begin{enumerate}
        \item la même direction: \UGHOST{$(AB)$~et~$(CD)$ sont parallèles};
        \item la même longueur: \UGHOST{$AB = CD$};
        \item le même sens: le quadrilatère $ABDC$ \UGHOST{n'est pas croisé.}
    \end{enumerate}
    La position effective des points n'importe pas: un vecteur n'est pas «fixé»
    et peut «glisser» dans le plan sans rotation ni agrandissement pour obtenir
    un autre \UGHOST{\emph{représentant}} du vecteur. Le
    représentant~$\vect{AB}$ est dessiné dans le plan comme le segment~$[AB]$
    avec une flèche en~$B$ qui indique le sens de $A$~vers~$B$.
\end{remark}

\IfStudentF{
\begin{exercise}
    21 p.~325
\end{exercise}
}

\begin{definition}
    Le \define{vecteur nul} est le vecteur~$\vect{0}$ dont la translation ne
    déplace aucun point du plan. On a \UGHOST{$\vect{AA} = \vect{BB} = \vect{CC}
    = \vect{0}$} pour tous $A$,~$B$ et~$C$.
\end{definition}

\section{Sommes de vecteurs}

\begin{activity}
    \label{act:sum}
    \def\tu{{t_{\vect{u}}}}\def\tv{{t_{\vect{v}}}}
    On considère deux vecteurs $\vect{u} = \vect{AB}$ et $\vect{v} = \vect{CD}$
    (voir \vref{fig:sum}).
    On note~$\tu$ la translation de vecteur~$\vect{u}$ et $\tv$~celle de
    vecteur~$\vect{v}$.

    \begin{figure}
        \begin{tikzpicture}
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (4,2)     coordinate[label=above:$B$] (B);
            \path (6,1)     coordinate[label=above:$C$] (C);
            \path (8,-0.5)    coordinate[label=above:$D$] (D);

            \draw[->] (A) -- node[sloped,auto] {$\vect{u}$} (B);
            \draw[->] (C) -- node[sloped,auto] {$\vect{v}$} (D);

            \draw (1,-0.3) coordinate[label=above:$M$] (M)
                            pic[point marker] {};
            \draw (0.5,-2) coordinate[label=above:$N$] (N)
                            pic[point marker] {};
            \path ($ (M) + (B) - (A) $) coordinate[label=above:$M'$] (M');
            \draw[->] (M) -- (M');
            \path ($ (M') + (D) - (C) $) coordinate[label=below:$M''$] (M'');
            \draw[->] (M') -- (M'');

            \path ($ (N) + (B) - (A) $) coordinate[label=above:$N'$] (N');
            \draw[->] (N) -- (N');
            \path ($ (N') + (D) - (C) $) coordinate[label=below:$N''$] (N'');
            \draw[->] (N') -- (N'');

            \draw[green] (M) -- (N) -- (N'') -- (M'') -- cycle;

            \path ($ (B) + (D) - (C) $) coordinate[label=below:$A''$] (A'');
            \draw[->] (B) -- (A'');
            \draw[->,red] (A) -- (A'');

        \end{tikzpicture}
        \caption{Translations de \cref{act:sum}.}
        \label{fig:sum}
    \end{figure}
    \begin{enumerate}
        \item Quelle est l'image de~$A$ par la translation~$\tu$ ?
            \UGHOST{$t_{\vect{v}}(A)=B$}
        \item Si l'on effectue à la suite les translations $\tu$~et~$\tv$ dans
            cet ordre, tous les points du plan sont déplacés deux fois de suite.
            $A$~est-il envoyé sur~$D$ ? Justifier.
            \UGHOST{Non, car $t_{\vect{v}}$ n'envoie pas $B$~sur~$D$}
        \item On prend un point~$M$ quelconque. Construire l'image~$M'$ de~$M$
            par~$\tu$, puis l'image~$M''$ de~$M'$ par~$\tv$. Faire de même pour
            un point~$N$.
        \item Que dire de $MM''N''N$ ?
            \UGHOST{Cela semble être un $\#$}
        \item On note~$t$ l'opération qui consiste à effectuer $\tu$~puis~$\tv$.
            Quelle est l'image de~$M$ par~$t$ ? De~$N$ ?
            \UGHOST{$t(M) = M''$ et $t(N) = N''$}
        \item Quelle est la nature de la transformation~$t$ ?
            \UGHOST{$t$~est une translation}
    \end{enumerate}
\end{activity}

\begin{definition}
    Soient $\vect{u}$~et~$\vect{v}$ deux vecteurs. En effectuant \emph{à la
    suite} les translations de vecteurs $\vect{u}$~puis~$\vect{v}$, l'opération
    obtenue est encore une translation. Le vecteur de cette translation est noté
    $\vect{u} + \vect{v}$ et est appelé \define{somme de
    $\vect{u}$~et~$\vect{v}$}.
\end{definition}

\begin{remark}
    En d'autres termes, sommer deux vecteurs, c'est cumuler les décalages.
\end{remark}

\begin{remark}
    $\vect{u} + \vect{0} = \vect{u}$ car on cumule avec la translation qui ne
    fait rien.
\end{remark}

\begin{proposition}[Relation de \bsc{Chasles}]
    \label{prop:chasles}
    Soient $A$,~$B$ et~$C$ trois points. Alors $\vect{AB} + \vect{BC} =
    \vect{AC}$ (voir \vref{fig:chasles}).
\end{proposition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}%\useFCwidth
    \ffigbox{}{
        \caption{}
        \label{fig:chasles}
        \begin{tikzpicture}
            \path (0,0)         coordinate[label=above:$A$] (A);
            \path (2,1)         coordinate[label=above:$B$] (B);
            \path (3.5,-0.5)    coordinate[label=below:$C$] (C);
            \draw[->] (A) -- (B);
            \draw[->] (B) -- (C);
            \draw[->,thick] (A) -- (C);
        \end{tikzpicture}
    }
    \ffigbox{}{
        \caption{}
        \label{fig:parallelogram}
        \begin{tikzpicture}
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (1,1.5)   coordinate[label=above:$B$] (B);
            \path (3,-0.5)  coordinate[label=below:$C$] (C);
            \path ($ (B)+(C)-(A) $) coordinate[label=right:$M$] (M);
            \draw[->]        (A) -- (B);
            \draw[->]        (A) -- (C);
            \draw[dashed]    (B) -- (M) -- (C);
            \draw[->,thick]  (A) -- (M);
        \end{tikzpicture}
    }
    \end{subfloatrow}
    \caption{\subref{fig:chasles} $\vect{AB} + \vect{BC} = \vect{AC}$ (relation
        de \bsc{Chasles}). \subref{fig:parallelogram} La règle du
        parallélogramme s'obtient par $\vect{AB} + \vect{AC} = \vect{AB} +
        \vect{BM} = \vect{AM}$.}
    }
\end{figure}

On utilise cette propriété pour construire géométriquement la somme $\vect{u} +
\vect{v}$ en choisissant un représentant de~$\vect{v}$ dont l'origine est
l'extrémité de celui de~$\vect{u}$ --- en les mettant «à la queue-leu-leu».

\begin{proof}
    \UGHOST{%
    L'image de~$A$ par la translation de vecteur~$\vect{AB}$ est~$B$, et l'image
    de~$B$ par la translation de vecteur~$\vect{BC}$ est~$C$. La translation
    cumulée envoie donc $A$~vers~$C$ et son vecteur est ainsi égal
    à~$\vect{AC}$.  C'est dire $\vect{AB} + \vect{BC} = \vect{AC}$.%
    }[{\normalsize\qedhere}\par]
\end{proof}

\begin{remark}[Règle du parallélogramme]
    Soient $A$,~$B$ et~$C$ trois points non alignés. Alors \UGHOST{$\vect{AB} +
    \vect{AC} = \vect{AM}$} où $[AM]$~est la diagonale du parallélogramme de
    côtés $[AB]$~et~$[AC]$ (voir \vref{fig:parallelogram}).
\end{remark}

\IfStudentF{
\begin{exercise}[\bsc{Chasles} ou pas]
    58 p.~328
\end{exercise}
}

\begin{definition}
    L'\define{opposé de~$\vect{u}$} est l'unique vecteur~$\vect{v}$ tel que
    $\vect{u} + \vect{v} = \vect{0}$. On a $-\vect{AB} = \vect{BA}$ quels que
    soient les points $A$~et~$B$.
\end{definition}

\IfStudentF{
\begin{exercise}[Simplifier]
    61 p.~329
\end{exercise}

\begin{exercise}
    62 p.~329
\end{exercise}
}

\section{Coordonnées de vecteurs}

\subsection{Définition}

\begin{definition}
    Les coordonnées de~$\vect{u}$ sont \UGHOST{celles du point~$M$ tel que
    $\vect{OM} = \vect{u}$} (voir \vref{fig:coords}). On les note souvent en
    colonnes: $\vect{u}(x;y)$.
\end{definition}

\begin{figure}
    \begin{tikzpicture}[scale=0.8,baseline=(current bounding box.center)]
        \draw[help lines] (-1.5,-2.5) grid (8.5,5.5);
        \draw[->]   (current bounding box.west |- 0,0)
                    -- (current bounding box.east |- 0,0);
        \draw[->]   (current bounding box.south -| 0,0)
                    -- (current bounding box.north -| 0,0);
        \path (0,0) node[below left] {$O$}
              (1,0) node[below] {$I$}
              (0,1) node[left] {$J$};
        \path (2,-2)  coordinate[label=below left:$A$] (A) pic[point marker] {}
              +(4,3) coordinate[label=above right:$B$] (B) pic[point marker] {};
        \draw[->,semithick] (A) -- node[auto] {$\vect{u}$} (B);
    \end{tikzpicture}
    \caption{Coordonnées d'un vecteur}
    \label{fig:coords}
\end{figure}

\begin{remark}
    Pour lire des coordonnées graphiquement, on ne déplace en général pas le
    vecteur jusqu'à l'origine, mais on lit les coordonnées de son extrémité en
    imaginant l'origine du repère au point de départ du vecteur.
\end{remark}

\begin{proposition}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Alors
    $\vect{u} = \vect{v} \iff
            \UGHOST{
            \left\{
            \begin{aligned}
                x &= x' \\
                y &= y'
            \end{aligned}
            \right.
            }$
\end{proposition}

\begin{proof}
    \UGHOST{%
    $\vect{u} = \vect{v}$ si et seulement si leurs représentants partant de
    l'origine sont confondus, c'est-à-dire s'ils ont la même extrémité:
    $x = x'$ et $y = y'$.%
    }
\end{proof}

\subsection{Calculs de coordonnées}

\begin{proposition}
    Si $A(x_A;y_A)$~et~$B(x_B;y_B)$ alors $\vect{AB}(x_B - x_A; y_B - y_A)$.
\end{proposition}

\begin{example}
    Dans \vref{fig:coords}, lire les coordonnées de $A$~et~$B$. En déduire par
    le calcul les coordonnées de~$\vect{AB}$.

    \UGHOST{$\point A(2;-2)$ et $\point B(6;1)$ \pause
    donc $\vect{AB}(6-2;1-(-2))$ soit $\vect{AB}(4;3)$}
\end{example}

\begin{example}
    Soient $A(8;3)$~et~$\vect{u}(1;5)$. Déterminer les coordonnées de
    l'image~$B$ de~$A$ par la translation de vecteur~$\vect{u}$.

    \UGHOST{Puisque $\vect{AB} = \vect{u}$, $x_{\vect{u}} = x_B - x_A$ soit $1 =
    x_B - 8$ \pause et donc $x_B = 1 + 8 = 9$. \pause De même,
    $y_{\vect{u}} = y_B - y_A$, et ainsi $y_B = 3 + 5 = 8$. En définitive
    $\point B(9;8)$.}
\end{example}

\IfStudentF{
\begin{exercise}
    13 p.~324
\end{exercise}

\begin{exercise}
    33 p.~326
\end{exercise}

\begin{exercise}
    35 p.~326
\end{exercise}
}

\subsection{Coordonnées d'une somme}

\begin{proposition}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Alors $(\vect{u} +
    \vect{v})\UGHOST{\vect{}(x+x';y+y')}$.
\end{proposition}

\IfStudentF{
\begin{exercise}
    45 p.~327
\end{exercise}

\begin{exercise}
    47 p.~327
\end{exercise}

\begin{exercise}
    31 p.~326
\end{exercise}

}

\end{document}
