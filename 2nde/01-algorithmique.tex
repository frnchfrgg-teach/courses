% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{rivalgo}
\usepackage{graphicx}
\def\Python{\raisebox{-0.5ex}{\includegraphics[height=2ex]{python-logo}}}

\usepackage{tikz}
\usetikzlibrary{calc, shapes.geometric}

\begin{document}

\chapter{Bases de l'algorithmique}

\chapterquote{Comme la Hongrie, le monde informatique a une langue qui lui est
propre. Mais il y a une différence. Si vous restez assez longtemps avec des
Hongrois, vous finirez bien par comprendre de quoi ils parlent.}{Dave Barry}

\section{Algorithmes et langages impératifs}

\subsection{Algorithmes et instructions}

Un algorithme est comme une méthode, une recette qui va permettre d'obtenir un
résultat --- sans le savoir nous en utilisons souvent ! Quelques exemples:

\begin{description}
    \item[Une règle de grammaire] \emph{Si} l'auxiliaire est “avoir”
        et \emph{si} le C.O.D. est placé avant le verbe alors on accorde le
        participe passé avec le C.O.D.; \emph{sinon} on ne l'accorde pas.
    \item[Une recette de cuisine] Dans un saladier, mélanger \SI{250}{g} de
        farine et $4$~œufs, puis verser progressivement \SI{50}{cL} de lait en
        battant avec un fouet; enfin, ajouter une pincée de sel et \SI{4}{cL} de
        rhum.
    \item[Poser une addition] À vous de décrire la méthode\ldots
\end{description}

\begin{definition}
    Un \define{algorithme} (impératif) est un enchainement d'opérations
    élémentaires ou \define{instructions} à effectuer dans un certain ordre
    pour permettre la résolution d'un problème.
\end{definition}

\begin{definition}
    Pour écrire un algorithme, on a besoin d'utiliser un \define{langage} qui
    décrit les instructions.
\end{definition}

Un même algorithme peut s'écrire dans plusieurs langages. Au lycée, on utilise
principalement trois langages: le langage naturel ou pseudo-code, le langage
\Python, et le langage de la calculatrice.

\begin{example}
\label{ex:algofr}
\begin{enumerate}
    \item
        On considère le programme suivant: « Choisir un nombre. Lui
        ajouter~$1$. Multiplier le résultat par~$2$. Soustraire~$3$ au résultat.
        Afficher le résultat.»
        \begin{enumerate}
            \item Appliquer ce programme à $3$,~$-4$, $0$ et~$\frac{1}{3}$.
            \item Peut-on choisir un nombre pour lequel le programme
                affiche~$0$?
            \item Écrire un deuxième programme qui, à partir d'un résultat
                affiché par le programme précédent, retrouve le nombre choisi
                initialement.
        \end{enumerate}
    \item Que fait le programme \Python\ suivant ?
        \begin{lstlisting}[language=Python,gobble=8]
        print("Bonjour")
        print("3+4")
        print(3+4)
        \end{lstlisting}
\end{enumerate}
\end{example}

\subsection{Notion de variable}

Les données du problème ainsi que les résultats intermédiaires doivent être
mémorisés quelque part: c'est à ça que servent les \emph{variables}.

\begin{definition}
    Une \define{variable} est une boite\footnote{C'est en réalité un emplacement
    dans la mémoire vive de l'ordinateur.} désignée par un nom, un peu comme un
    fichier qui ne contiendrait qu'une seule donnée. On peut effectuer les
    opérations suivantes sur une variable:
\begin{description}
    \item[évaluation] Lorsqu'on écrit le nom d'une variable au milieu d'un
        calcul, le programme va regarder la valeur qu'a la variable à ce moment
        là --- c'est-à-dire le contenu du fichier --- et agit comme si on avait
        écrit cette valeur à la place du nom de variable.
    \item[affectation] Lorsqu'on affecte le résultat d'un calcul --- par exemple
        $5\var{b}$ --- à une variable~\var{a}, le programme commence en premier
        lieu par effectuer le calcul --- en évaluant certaines variables si
        besoin. Ensuite, il oublie le contenu de la variable~\var{a} et le
        remplace par le résultat du calcul.
\end{description}
\end{definition}

\begin{remark}
    Dans la plupart des langages, les noms de variables peuvent être aussi longs
    que l'on veut, et contenir n'importe quels caractères parmi les lettres
    latines minuscules et majuscules, les chiffres et le tiret du bas (mais ne
    peuvent pas commencer par un chiffre). En général la \emph{casse} est
    importante, donc \var{I}~n'est pas la même variable que~\var{i}.
\end{remark}

\begin{remark}
    Les calculatrices Casio et TI d'entrée de gamme que l'on utilise au lycée
    n'autorisent que 26 variables nommées \texttt{A}, \texttt{B},
    \dots,~\texttt{Z}.
\end{remark}

\begin{example}
    \begin{enumerate}
        \item En langage naturel, l'affectation s'écrit «\var{a} \textbf{prend
            pour valeur} $42$» ou encore «$\var{a} \leftarrow 42$».
        \item Dans le langage de la calculatrice, on écrit
            \code[language=casio]{42->A}. En \Python\ on écrit
            \pycode{a = 42}.
        \item Décrire ce qu'il se passe dans le programme suivant. Quelle est la
            valeur finale de la variable~\var{num} ?
            \begin{lstlisting}[language=Python,gobble=12]
            num = 15
            num = num + 4
            B = 2 * (num + 1)
            num = num + B * B
            \end{lstlisting}
    \end{enumerate}
\end{example}

\begin{remark}
    Les données contenues dans une variable ont un \emph{type}: nombre entier,
    nombre décimal, texte\footnote{On dit plutôt «chaine de caractères».}\dots

    Certains langages obligent à déclarer à l'avance le type de données qu'une
    variable pourra contenir (exemples: C++, Algobox). D'autres, comme \Python,
    permettent de changer le type à chaque affectation.

    Il faut cependant faire attention, on ne peut pas ajouter un nombre à un
    texte, ou diviser un texte par~$3$ ! Pour ces raisons on utilise des
    \emph{conversions}.
\end{remark}

\subsection{Entrées et sorties}

Un cas particulier d'affectation est \emph{l'entrée}, dont le principe est de
demander la valeur à mettre dans la variable lorsqu'on exécute le programme.
Cette valeur n'est ainsi plus connue au moment de la conception du programme qui
doit donc être capable de gérer des entrées arbitraires.

\begin{example}
    \begin{enumerate}
        \item En langage naturel, on écrit souvent «\natkwd{lire}~\var{a}» ou
            «\natkwd{saisir}~\var{a}».
        \item Sur une Casio, on écrira
            \code[language=casio]{"Entrez X"?->X}; sur une
            TI la commande est \code[language=ti]{Prompt X}.
        \item En \Python\ on utilise la \emph{fonction}
            \pycode{input()}. Entre les parenthèses, on peut
            indiquer un texte à afficher lorsque le programme attend la valeur.
            Le \emph{résultat} de \pycode{input()} est toujours
            de type \pycode{str}, le type du texte. Pour affecter
            ce résultat à une variable, on utilise souvent des conversions:
            \begin{lstlisting}[language=Python,gobble=12]
            num = int(input("Choisissez un nombre entier:"))
            x = float(input("Entrez un nombre décimal:"))
            prenom = input("Quel est ton prénom ?")
            \end{lstlisting}
            \begin{enumerate}
                \item À quoi sert la fonction \pycode{int()} ? La fonction
                    \pycode{float()} ?
                \item Pourquoi y a-t-il seulement \pycode{input()} pour la
                    troisième ligne ?
                \item Que se passe-t-il si l'utilisateur entre son prénom quand
                    on lui demande un nombre entier ? Peut-on entrer~\<5.3> ?
                    Et~\<5.0> ?
                \item Que se passe-t-il si l'on rentre un nombre entier
                    lorsque le programme demande un nombre décimal ?
            \end{enumerate}
    \end{enumerate}
\end{example}

Pour \emph{afficher} un texte ou un résultat on utilise la fonction
\pycode{print()}. Elle accepte entre les parenthèses des valeurs ou des
variables séparées par des virgules, qu'elle affichera sur une même ligne.

\begin{example}
    \begin{enumerate}
        \item Que fait le programme suivant ?
            \begin{lstlisting}[language=Python,gobble=12]
            prenom = input("Quel est ton prénom ?")
            age = int(input("Quel est ton âge ?"))
            print("Eh bien", prenom, "tu ne fais pas tes", age, "ans.")
            \end{lstlisting}
        \item Attention: \pycode{print("X")} et \pycode{print(X)} ne font pas du
            tout la même chose ! Expliquer pourquoi.
        \item Traduire en \Python\ le programme de calcul de \cref{ex:algofr}.
    \end{enumerate}
\end{example}

En langage naturel on écrira simplement «\natkwd{afficher} “Texte”» ou
«\natkwd{afficher} $\var{x}+3$».
Sur calculatrice les possibilités sont limitées:
\begin{example}
\begin{enumerate}
    \def\casiodisp{\tikz{\fill (0,0) -- (1ex,0) -- (1ex,1ex) -- cycle;}}
    \item Sur Casio écrire \code{"Texte"} sur une ligne demande d'afficher ce
        texte. Pour afficher le résultat d'un calcul on utilise «\casiodisp»,
        par exemple \code{(N+3)*2\casiodisp}.
    \item Sur TI on utilise la commande \code{Disp}, comme dans \code{Disp
        "texte"} ou \code{Disp X} ou encore \code{Disp "texte", A}.
\end{enumerate}
\end{example}

\section{Structures de contrôle}

Les structures de contrôle sont des instructions \emph{composées} qui peuvent
contenir d'autres instructions.

\subsection{Instruction conditionnelle (Si\dots Alors\dots Sinon)}

\begin{definition}
    L'\define{instruction conditionnelle} \natcode{\natkwd{si} \ldots\
    \natkwd{alors} \ldots\ \natkwd{sinon} \ldots} permet d'effectuer des
    instructions différentes en fonction d'une condition. Cela correspond à une
    bifurcation --- un aiguillage --- dans l'organigramme de l'algorithme (voir
    \vref{fig:orga}).

    \setlength\multicolsep{0pt}
    \begin{multicols}{2}
        En langage naturel:
        \begin{natlisting}[gobble=8]
        \natkwd{si} \textsl{condition} \natkwd{alors}
            \textsl{instructions si oui}
        \natkwd{sinon}
            \textsl{instructions si non}
        \natkwd{fin}
        \end{natlisting}

        \columnbreak
        En \Python:
        \begin{lstlisting}[language=Python,gobble=8]
        if condition:
            # instructions si oui
        else:
            # instructions si non
        \end{lstlisting}
    \end{multicols}
    \begin{multicols}{2}
        En langage Casio:
        \begin{lstlisting}[gobble=8]
        If condition Then
        instructions si oui
        Else
        instructions si non
        IfEnd
        \end{lstlisting}

        \columnbreak
        En langage TI:
        \begin{lstlisting}[gobble=8]
        If condition Then
        instructions si oui
        Else
        instructions si non
        End
        \end{lstlisting}
    \end{multicols}
\end{definition}

\begin{figure}
    \tikzset{
        command/.style={draw,shape=rectangle},
        test/.style={draw,shape=diamond,aspect=2},
    }
    \ffigbox{}{
    \begin{subfloatrow}
        \ffigbox[\FBwidth]{}{\caption{}\label{fig:orga-1}
        \begin{tikzpicture}
            \node[command,below=1em of {0,0}] (LIRE) {\natkwd{lire} $a$};
            \node[command,below=1.5em of LIRE] (TEXTE)
                    {\natkwd{afficher} “Résultat”};
            \node[command,below=1.5em of TEXTE] (VAR)
                    {\natkwd{afficher} $2a$};
            \draw[->] (0,0)--(LIRE);
            \draw[->] (LIRE)--(TEXTE);
            \draw[->] (TEXTE)--(VAR);
        \end{tikzpicture}
        }
        \ffigbox[\Xhsize]{}{\caption{}\label{fig:orga-2}
        \begin{tikzpicture}
            \node[test,below=1em of {0,0}] (SI)
                    {\textit{condition}\textbf{?}};
            \node[command,below left=1.5em and 0pt of SI.west,
                    align=center,font=\itshape] (OUI)
                    {instructions\\si oui};
            \node[command,below right=1.5em and 0pt of SI.east,
                    align=center,font=\itshape] (NON)
                    {instructions\\si non};
            \draw[->] (0,0) -- (SI);
            \draw[->] (SI.west) -| node[above,near start] {oui} (OUI);
            \draw[->] (SI.east) -| node[above,near start] {non} (NON);
            \coordinate (END) at
                ($ (OUI.south)!1/2!(NON.south) - (0,1.5em) $);
            \draw[->] (OUI.south) |- (END);
            \draw[->] (NON.south) |- (END);
            \draw (END) -- +(0,-1em);
        \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Organigrammes de quelques algorithmes. \subref{fig:orga-1} Un
    algorithme sans structure conditionnelle. \subref{fig:orga-2} Le test de la
    structure conditionnelle se représente avec un losange.}
    \label{fig:orga}
    }
\end{figure}


\begin{example}
Un site internet de développement de photos propose le tirage sur papier des
photos au tarif de \SI{0,11}{\euro} l’unité.
Le tarif passe à \SI{0,08}{\euro} pour une commande d’au moins 200 photos.
On veut créer un algorithme donnant le montant à faire payer pour un nombre de tirages donné.
\begin{enumerate}
    \item Combien le client doit-il payer pour une commande de $75$~photos ? Et
        pour $340$~photos ?
    \item \begin{enumerate}
            \item Indiquer une formule permettant de calculer le montant d'une
                commande de $n$~photos avec $n < 200$.
            \item Même question pour $n \ge 200$.
        \end{enumerate}
    \item Compléter le programme suivant:
    \begin{lstlisting}[language=Python,gobble=4]
    n = int(input("Nombre de photos: "))
    if

    else:
        prix =
    print("Le montant de la commande est", prix)
    \end{lstlisting}
\end{enumerate}
\end{example}

\subsection{Boucle bornée (Pour)}

\begin{definition}
    La \define{structure de boucle bornée} permet de répéter un même groupe
    d'instructions, une fois pour chaque entier entre deux bornes. À chaque
    \define{itération} de la boucle, le groupe d'instructions est effectué après
    avoir affecté (automatiquement) le nombre suivant à la variable de boucle.

    \setlength\multicolsep{0pt}
    \begin{multicols}{2}
        En langage naturel:

    \begin{natlisting}[gobble=4]
    \natkwd{pour} \var{variable} \natkwd{de} 5 \natkwd{à} 50 \natkwd{faire}
        \textsl{instructions à répéter}
    \natkwd{fin pour}
    \end{natlisting}

        \columnbreak
        En \Python:
        \begin{lstlisting}[language=Python,gobble=8]
        # attention la fin est exclue
        for variable in range(5,51):
            # instructions à répéter
        \end{lstlisting}
    \end{multicols}
    \begin{multicols}{2}
        En langage Casio:
        \begin{lstlisting}[language=casio,gobble=8]
        For 5->I To 50
        instructions à répéter
        Next
        \end{lstlisting}

        \columnbreak
        En langage TI:
        \begin{lstlisting}[gobble=8]
        For(I,5,50)
        instructions à répéter
        End
        \end{lstlisting}
    \end{multicols}
\end{definition}

\begin{example}
    On désire calculer la somme des nombres entiers de $1$~à~$100$ (inclus).
    \begin{enumerate}
        \item De combien de variables va-t-on avoir besoin ?
        \item Écrire l'algorithme en langage naturel puis en \Python.

            \noindent
            \begin{minipage}{\linewidth}
            \begin{lstlisting}[language=Python,gobble=12]

            for i in range(1,101):
                S =
            print("La somme est", S)
            \end{lstlisting}
            \end{minipage}
        \item Que modifier pour calculer la somme des entiers de $1$~à~$n$, où
            $n$~est demandé à l'utilisateur ?
    \end{enumerate}
\end{example}

\subsection{Boucle non bornée (Tant que)}

\begin{definition}
    Lorsqu'on veut répéter un même groupe d'instructions jusqu'à ce qu'une
    condition soit fausse, on ne connait plus à l'avance le nombre d'itérations
    ni même parfois quelles valeurs donner à une variable de boucle. On utilise
    alors la \define{structure de boucle non bornée}, ou «boucle tant que».

    \setlength\multicolsep{0pt}
    \begin{multicols}{2}
        En langage naturel:
        \begin{natlisting}[gobble=8]
        \natkwd{tant que} \textsl{condition} \natkwd{faire}
            \textsl{instructions à répéter}
        \natkwd{fin tant que}
        \end{natlisting}

        \columnbreak
        En \Python:
        \begin{lstlisting}[language=Python,gobble=8]
        while condition:
            # instructions à répéter
        \end{lstlisting}
    \end{multicols}
    \begin{multicols}{2}
        En langage Casio:
        \begin{lstlisting}[gobble=8,literate={->}{$\rightarrow$}{2}]
        While condition
        instructions à répéter
        WhileEnd
        \end{lstlisting}

        \columnbreak
        En langage TI:
        \begin{lstlisting}[gobble=8]
        While condition
        instructions à répéter
        End
        \end{lstlisting}
    \end{multicols}
\end{definition}

\begin{example}
    En 2017, je place \SI{3000}{\euro} sur un livret. Chaque année, la somme
    disponible sur ce livret augmente de \SI{3}{\percent}. Compléter
    l’algorithme ci-dessous afin qu’il affiche l’année à partir de laquelle je
    disposerai d’au moins \SI{3500}{\euro}.
    \begin{lstlisting}[language=Python,gobble=4]
    somme = 3000
    annee = 2017
    while somme <
        somme = somme + somme *
        annee = annee +
    print("En", annee, "j'aurai", somme, "€")
    \end{lstlisting}
\end{example}

\section{Fonctions}

\begin{definition}
    Une \define{fonction} est une sorte d'algorithme à part, un bloc
    d'instructions qui a un nom, dont le fonctionnement dépend d'un certain
    nombre de paramètres appelés \define{arguments} de la fonction, et qui peut
    renvoyer un résultat.

    Pour \define{appeler} une fonction, on écrit son nom suivi de parenthèses
    entre lesquelles on indique les arguments dans le bon ordre. L'algorithme
    exécute les instructions de la fonction, et remplace tout l'appel (nom de la
    fonction et arguments entre parenthèses) par le résultat de la fonction.

    \setlength\multicolsep{0pt}
    \begin{multicols}{2}
        En langage naturel:
        \begin{natlisting}[gobble=8]
        \natkwd{fonction} $f(x,y)$
            \textsl{instructions}
            \natkwd{retourner} $z$
        \natkwd{fin fonction}
        \end{natlisting}

        \columnbreak
        En \Python:
        \begin{lstlisting}[language=Python,gobble=8]
        def f(x, y):
            # instructions
            return z
        \end{lstlisting}
    \end{multicols}
\end{definition}

\begin{example}
    On considère l'algorithme suivant:
    \begin{lstlisting}[language=Python,gobble=4]
    def mystere(x, y):
        if x > y:
            return x * (y + 1)
        return (x + 2) * y
    print(mystere(8,3), mystere(3,8))
    \end{lstlisting}
    \begin{enumerate}
        \item Quel est le nom de la fonction ? Donner le nom de ses arguments.
        \item Dans \pycode{mystere(8,3)} quelles sont les valeurs des arguments?
        \item Que va afficher ce programme ?
    \end{enumerate}
\end{example}

\begin{remark}
    Avant même le début du programme, \Python\ définit des fonctions
    indispensables comme \pycode{input}, \pycode{print}, \pycode{int}\dots
\end{remark}


\end{document}
