% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\usetikzlibrary{matrix, decorations.pathreplacing}

\rivmathlib{geometry}

\StudentMode
\begin{document}

\chapter{Variations de fonctions}

\section{Variations}

\subsection{Sens de variation}

\begin{activity}
    
\end{activity}

\begin{definition}
    Soit~$f$ définie sur un intervalle~$I$. On dit que $f$~est
    \define{strictement croissante} sur~$I$ si (et seulement~si) la propriété
    suivante est vraie:
    \[ \text{pour tous } a < b, \text{on a } f(a) < f(b) \]

    On dit qu'une fonction strictement croissante \emph{conserve l'ordre}: les
    images par~$f$ sont toujours dans le même ordre que les antécédents.
    \Cref{fig:increasing} donne une interprétation graphique de cette propriété.
\end{definition}

\begin{figure}
    TODO
    \caption{$f$~est strictement croissante. Si $\point A(a;f(a))$ et $\point
    B(b;f(b))$, alors $a<b$ décrit le fait que de $A$~à~$B$ on va vers la droite
    tandis que $f(a) < f(b)$ indique que l'on monte pour aller de $A$~à~$B$.}
    \label{fig:increasing}
\end{figure}

\begin{definition}
    Soit~$f$ définie sur un intervalle~$I$. On dit que $f$~est
    \define{strictement décroissante} sur~$I$ si (et seulement~si) la propriété
    suivante est vraie:
    \[ \text{pour tous } a < b, \text{on a } f(a) > f(b) \]

    On dit qu'une fonction strictement décroissante \emph{change l'ordre}: les
    images par~$f$ sont toujours dans l'ordre contraire des antécédents.
    %\Cref{fig:increasing} donne une interprétation graphique de cette propriété.
\end{definition}

\begin{definition}
    Pour croissant et décroissant sans strictement\dots
\end{definition}

\clearpage

\subsection{Tableaux de variations}

Un tableau de variation sert à indiquer les sens de variation d'une fonction sur
les intervalles correspondants. Sa structure est la suivante:

\begin{center}
    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,\var$f(x)$/2}
                        {$\;-\infty$,$-3$,$2$,$8$}
            \tkzTabVar {+/,-/,+/,-/}
        \end{scope}
        \path ($ (N10)!1/2!(N40) $) node[above=1em,align=center] (changes)
            {abscisses où la\\ fonction change de sens};
        \path (changes.north) node[above=1em] (bounds)
            {bornes de l'intervalle de définition};
        \draw[->] (bounds) to[bend right=30] (L1);
        \draw[->] (bounds) to[bend left=30] (L4);
        \draw[->] (changes) to[bend right=20] (L2);
        \draw[->] (changes) to[bend left=20] (L3);
        \draw[decorate,decoration=brace]
            ($ (T20) +(1ex,0) $) -- ($ (T21) +(1ex,0) $)
            node[right=1ex,pos=1/2,align=left]
            {découpage des abscisses\\ en intervalles};
        \draw[decorate,decoration=brace]
            ($ (T21) +(1ex,0) $) -- ($ (T22) +(1ex,0) $)
            node[right=1ex,pos=1/2,align=left]
            {indication de variation\\ dans chaque intervalle};
    \end{tikzpicture}
\end{center}

Les cases de la ligne du bas sont \emph{entre} les abscisses de la ligne du
haut, même si l'on ne trace pas les traits verticaux qui sont sous les nombres.

Dans chaque case, on place une flèche qui monte d'un coin à l'autre si la
fonction est strictement croissante sur l'intervalle correspondant; on place une
flèche qui descend si la fonction y est strictement décroissante.
Dans l'exemple, $f$~est strictement décroissante sur~$\IN]-\infty;-3;]$.

\subsection{Extremums}

\begin{definition}
    Le \define{maximum} de~$f$ sur~$I$, s'il existe, est la plus grande des
    images par~$f$ sur~$I$. En d'autres termes, $M$~est le maximum de~$f$
    sur~$I$ si et seulement s'il existe $m \in I$ tel que $M = f(m) \ge f(x)$
    pour tous les $x \in I$.

    Dans ce cas on dit que le maximum~$M$ est \emph{atteint en $x = m$}.
\end{definition}

\begin{remark}
    Attention: le maximum~$M$ est la \emph{valeur} de l'image, il est atteint
    en~$m$ qui est un antécédent. Le maximum est unique (une seule valeur
    maximum) mais peut être atteint plusieurs fois.
\end{remark}

\begin{definition}
    Le \define{minimum} de~$f$ sur~$I$, s'il existe, est la plus petite des
    images par~$f$ sur~$I$. En d'autres termes, $M$~est le minimum de~$f$
    sur~$I$ si et seulement s'il existe $m \in I$ tel que $M = f(m) \le f(x)$
    pour tous les $x \in I$.

    Dans ce cas on dit que le minimum~$M$ est \emph{atteint en $x = m$}.
\end{definition}

\begin{definition}
    Un \define{maximum local} (resp. \define{minimum local}) de~$f$ est une
    image qui est plus grande (resp. plus petite) que les autres images dans un
    intervalle d'antécédents autour.
\end{definition}

\begin{remark}
    Le maximum (global) est un maximum local. C'est en fait le plus grand des
    maximums locaux.
\end{remark}

\begin{definition}
    Un \define{extremum} (global ou local) est un maximum ou un minimum.
\end{definition}

\begin{remark}
    On peut indiquer les extremums locaux dans un tableau de variations, en les
    plaçant au bout les flèches. Par exemple:\\
    \begin{tikzpicture}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,\var$f(x)$/2}
                        {$\;-\infty$,$-3$,$2$,$8$}
            \tkzTabVar {+/,-/$0$,+/$3$,-/$-5$}
    \end{tikzpicture}
\end{remark}

\section{Fonctions affines}

\subsection{Définition}

\begin{definition}
    On appelle \define{fonction affine} toute fonction pouvant s'écrire sous la
    forme $f(x) = mx+p$ avec $m$~et~$p$ deux réels.
\end{definition}

\begin{example}
    \label{exa:fonctions-affines}
    Les fonctions suivantes sont-elles affines ?
    \begin{enumerate}[gathered, label={$\falph*(x)={}$},labelsep=0pt]
        \item $2x+3$;
        \item $-3-3x$;
        \item $x^2 + 1$;
        \item $-\sqrt{2}x-1$;
        \item $-\sqrt{2x}-1$;
        \item $\dfrac{1-x}{2}$.
    \end{enumerate}
    \IfStudentT{
        \bigfiller{6}
    }
\end{example}


\subsection{Représentation graphique}

\begin{proposition}
    Soit $f:x\mapsto mx+p$ une fonction affine. La courbe représentative de~$f$
    est la droite \emph{non verticale} d'équation $y = mx+p$.
\end{proposition}

\begin{proposition}[Corrolaire]
    Soit $f:x\mapsto mx+p$ une fonction affine. Si $x_1$~et~$x_2$ sont deux
    réels distincts, alors $m = \GHOST{\dfrac{f(x_2) - f(x_1)}{x_2 -
    x_1}}$.
\end{proposition}

\begin{example}[Recherche de formule]
    Soit $f$~une fonction affine telle que $f(8) = 11$~et $f(10) = 10$.
    Déterminer l'expression de~$f$.
    \IfStudentT{
        \bigfiller{8}
    }
\end{example}

\begin{exercise}
    71 p.~72
\end{exercise}

\subsection{Sens de variation}

\begin{example}
    Soit $f:x\mapsto -3x+2$. Démontrer que $f$~est strictement décroissante
    sur~$\mdR$.

    \IfStudentT{
        \bigfiller{5}
    }
\end{example}


\begin{proposition}
    \label{thm:var-affine}
    Soit $f:x\mapsto mx+p$ une fonction affine. Alors:
    \begin{enumerate}
        \item Si $m<0$, $f$~est strictement décroissante sur~$\mdR$;
        \item si $m>0$, $f$~est strictement croissante sur~$\mdR$;
        \item si $m=0$, $f$~est constante sur~$\mdR$.
    \end{enumerate}
    La figure~\ref{fig:var-affine} permet de visualiser graphiquement ce
    théorème.
\end{proposition}



\begin{example}
    Donner les sens de variation des fonctions affines définies à
    l'exemple~\ref{exa:fonctions-affines}.
    \IfStudentT{
        \bigfiller{4}
    }
\end{example}

\begin{exercise}
    56 p.~72
\end{exercise}

\begin{figure}
    TODO
    \caption{Représentations graphiques du théorème~\ref{thm:var-affine}.}
    \label{fig:var-affine}
\end{figure}

\section{Fonction carré}

\subsection{Représentation graphique}

\subsection{Propriétés}

\begin{proposition}
    \begin{enumerate}[gathered]
        \item \label{itm:sgn-square}
            \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=1.5,espcl=2.5]
                            {$x$/1,\sgn$x^2$/1}
                            {$\;-\infty$,$0$,$+\infty\;$}
                \tkzTabLine{,+,z,+,}
            \end{tikzpicture}
        \item \label{itm:var-square}
            \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=1.5,espcl=2.5]
                            {$x$/1,\var$x^2$/2}
                            {$\;-\infty$,$0$,$+\infty\;$}
                \tkzTabVar {+/,-/$0$,+/}
            \end{tikzpicture}
    \end{enumerate}
\end{proposition}

\begin{proof}
    Par lecture graphique. En fait, on démontrera~\ref{itm:sgn-square} dans le
    prochain chapitre, et \ref{itm:var-square}~a été démontré en exercice dans
    le chapitre VIII.%\cref{chap:fonctions-2}
\end{proof}

\begin{proposition}
    symétrie
\end{proposition}

\clearpage

\section{Fonction inverse}

\subsection{Représentation graphique}

On dresse un tableau de valeurs:
\[\begin{array}{L*{11}{cL}}
    \hline
    x & -2 & -1 & -\frac{1}{2} & 0 & \frac{1}{3} & \frac{1}{2} &
        1 & 2 & 3 & 5\NL
    \frac{1}{x} & \GHOST{-\frac{1}{2}} & \GHOST{-1} & \GHOST{-2} &
                  \GHOST{\tikz \draw (0,0)--(1em,1em) (1em,0)--(0,1em);} &
                  \GHOST{3} & \GHOST{2} & \GHOST{1} &
                  \GHOST{\frac{1}{2}} & \GHOST{\frac{1}{3}} &
                  \GHOST{\frac{1}{5}} \\
    \hline
\end{array}\]

On peut alors construire la courbe représentative de $x \mapsto \frac{1}{x}$:
c'est \vref{fig:inv-repr}.

\begin{figure}
    \begin{tikzpicture}
        \def\xmax{8}\def\ymax{6.1}
        \datavisualization [school book axes,
                            all axes={grid={step=1},ticks={major at=1},
                                      unit length=0.45cm},
                            visualize as smooth line=inv,
                        ]
        data [set=inv, format=function] {
            var x : interval[-\xmax:-1];
            func y = 1 / (\value x);
        }
        data [set=inv, format=function] {
            var x : interval[-1:(-1/\ymax)];
            func y = 1 / (\value x);
        }
        data point[set=inv, outlier=true]
        data [set=inv, format=function] {
            var x : interval[(1/\ymax):1];
            func y = 1 / (\value x);
        }
        data [set=inv, format=function] {
            var x : interval[1:\xmax];
            func y = 1 / (\value x);
        }
        ;
    \end{tikzpicture}%
    \caption{Représentation graphique de la fonction inverse.}
    \label{fig:inv-repr}
\end{figure}

\subsection{Propriétés}

La fonction inverse n'est pas définie en~$0$. On utilise une nouvelle notation:
dans un tableau de signes ou de variations, on place une double barre verticale
sous les \emph{valeurs interdites}. On a ainsi:

\begin{proposition}
    Soit $f(x) = \dfrac{1}{x}$ définie sur~$\mdR^*$.
    \begin{enumerate}[gathered]
        \item \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=1.5,espcl=2.5]
                            {$x$/1,\sgn$f(x)$/1}
                            {$\;-\infty$,$0$,$+\infty\;$}
                \tkzTabLine{,-,d,+,}
            \end{tikzpicture}
        \item \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=1.5,espcl=2.5]
                            {$x$/1,\var$f(x)$/2}
                            {$\;-\infty$,$0$,$+\infty\;$}
                \tkzTabVar {+/,-D+//,-/}
            \end{tikzpicture}
    \end{enumerate}
\end{proposition}

\begin{remark}
    Danger: on ne peut pas dire que la fonction inverse est décroissante sur
    $\IN]-\infty;0;[ \cup \IN]0;+\infty;[$, mais seulement sur
    $\IN]-\infty;0;[$~\emph{et}~$\IN]0;+\infty;[$. En effet, $-5 < 3$ et
    pourtant $\dfrac{1}{-5} < \dfrac{1}{3}$\dots
\end{remark}

\begin{proposition}
    La courbe de la fonction inverse est symétrique par rapport à l'origine: si
    $x \in \mdR, \dfrac{1}{-x} = -\dfrac{1}{x}$.
\end{proposition}

\end{document}
