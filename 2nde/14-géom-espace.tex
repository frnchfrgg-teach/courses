% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc, patterns}

\rivmathlib{geometry}

\usepackage{sesacompat}
\usepackage{multicol}

\begin{document}
\StudentMode

\chapter{Géométrie dans l'espace}

\section{Objets dans l'espace}

\subsection{Représentation des solides}

\begin{definition}
    Un \define{solide} est un objet en relief. On ne peut pas le tracer en vraie
    grandeur sur une feuille de papier plane.
\end{definition}

\begin{definition}
    On peut \emph{fabriquer} le solide en construisant un \define{patron}, que
    l'on découpe, plie et colle.
\end{definition}

\begin{definition}
    On peut \emph{représenter} le solide sur une feuille plane en donnant
    l'impression de la 3D en utilisant par exemple la \define{perspective
    cavalière} --- voir \vref{fig:perspective}.
\end{definition}

\begin{figure}
    \begin{tikzpicture}[x=2cm, y=2cm, z=(45:1cm)]
        \fill[color=black,fill opacity=0.1]
            (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
        \draw (1,0,1) -- (1,1,1) -- (0,1,1);
        \draw[dashed] (1,0,1) -- (0,0,1) -- (0,1,1);
        \draw (1,0,1) -- (1,0,0)    (1,1,1) -- (1,1,0)    (0,1,1) -- (0,1,0);
        \draw[dashed] (0,0,1) -- (0,0,0);
        \filldraw[color=black,fill opacity=0.1]
            (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
        \draw[very thin]
            (1,0,0) -- +(0.5,0)
            +(0:0.2) arc[start angle=0, end angle=45, radius=0.2]
                node[midway,base right] {angle de fuite};
        \path (0,0,0) +(-3em,-2em) coordinate (SW);
        \path (1,1,1) +(3em,2em) coordinate (NE);
        \begin{scope}[semithick,->,
                    every node/.style={node font=\def\baselinestretch{0.8}}]
            \draw (0.5,0,0 |- SW)
                node[below,align=center]
                    {face frontale\\en vraie grandeur}
                to[bend left] (0.6,0.25,0);
            \draw (0,-0.15,0.5 -| SW)
                node[left,align=right]
                    {arête portée\\par une fuyante\\en grandeur\\réduite}
                to[bend left] (0,0,0.5);
            \draw (0,0.5,1 -| SW)
                node[left,align=right]
                    {arête\\non visible}
                to[bend left] (0,0.25,1);
            \draw (0,1.1,0.5 -| SW)
                node[left,align=right]
                    {arête\\visible}
                to[bend left] (0,1,0.5);
            \draw (0.5,1,0.5 |- NE)
                node[above]
                    {face arrière}
                to[bend left] (0.6,1,0.5);
            \coordinate (P) at (1,1,0 -| NE);
            \path (P)
                node[right,align=left,text width=7em]
                    {deux droites parallèles dans la réalité sont représentées
                     par des parallèles en perspective cavalière};
            \draw (P) +(0,-1em) to[bend left] (1,0,0.5);
            \draw (P) +(0,1em) to[bend right] (1,1,0.5);
        \end{scope}
    \end{tikzpicture}
    \caption{Représentation d'un cube en perspective cavalière}
    \label{fig:perspective}
\end{figure}

\subsection{Qu'est-ce qu'un plan?}

\begin{activity}
    \begin{enumerate}
        \item \label{q:figplan}
            Dans \bsc{Geogebra}, réaliser la figure suivante: placer trois
            points $A$, $B$, et~$C$ non alignés; construire la droite~$(BC)$, et
            y~placer un point~$D$; construire la droite~$(AD)$, activer sa
            trace, puis déplacer~$D$.\\
            Que remarque-t-on ?
            \IfStudentT{\hfiller}
        \item Obtient-t-on tous les points du plan? Est-ce dû à une restriction
            imposée par la taille de l'écran?
            \IfStudentT{\bigfiller{3}}
        \item Rajouter l'élément manquant sur la figure.
        \item Refaire la figure de \cref{q:figplan} dans le mode graphique~3D.
        \item Faire varier les angles de vue de la figure. Que remarque-t-on?
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{activity}


\begin{definition}
    Soient $A$, $B$ et~$C$ trois points de l'espace distincts et non alignés. Il
    y a un unique \define{plan} qui contient les trois points $A$, $B$ et~$C$:
    on le note~$(ABC)$. C'est l'union des droites
    \begin{itemize}
        \item passant par~$A$, et
        \item parallèles ou sécantes à la droite~$(BC)$.
    \end{itemize}
    \Vref{fig:plan} illustre cette construction du plan~$(ABC)$.
\end{definition}

\begin{figure}
    \begin{tikzpicture}[general, xscale=0.2, yscale=0.1]
        \draw[color=D4, fill=D4] (14,0)--(15,2)--(12,14)
                            --(0,26.5)--(5.5,6)--cycle;
        \draw (5.5,0)--(0,4.5)--(0,26.5)--(5.5,22);
        \draw (0,26.5)--(12,26.5)--(17.5,22);
        \draw[dashed] (12,26.5)--(12,14);
        \draw[dashed, color=C1] (12,4.5)--(12,14);
        \draw[dashed] (17.5,0)--(15,2);
        \draw[dashed, color=C1] (15,2)--(12,4.5);
        \draw[dashed] (0,4.5)--(7.5,4.5);
        \draw[dashed, color=C1] (7.5,4.5)--(12,4.5);

        \foreach \a/\b/\c/\d in {1.5/21/9.6/3.1,2.3/18/10.1/2.8,2.7/16.2/11.1/2.1,3.1/15/12.3/1.2,3.3/14.1/14/0,3.5/13.5/14.4/0.7,3.7/12.7/14.9/1.7,3.8/12/14.7/3.2,4/11.6/14.3/4.7,4.1/11.2/14.1/5.8,4.2/11/13.9/6.6,4.2/10.6/13.5/7.8,4.4/10.3/13.6/7.8,4.5/9.9/13.3/8.8,4.6/9.6/13.2/9.6,5.1/7.5/10.7/15.4}
        \draw[color=F1, line cap=butt] (\a,\b)--(\c,\d);
        \draw[color=D1, dashed, epais] (14,0)--(15,2)--(12,14)--(0,26.5);
        \draw[color=D1, epais] (14,0)--(5.5,6)--(0,26.5);
        \draw[color=A1] (6.7,9.8) node[above] {$A$} pic[point marker] {};
        \draw[color=A1] (10.5,6) node[below] {$B$} pic[point marker] {};
        \draw[color=A1] (12.7,9.5) node[right] {$C$} pic[point marker] {};
            \draw[color=A1, line width=0.75pt] (9,3.5)--(13,10);
        \draw (5.5,0)--(17.5,0)--(17.5,22)--(5.5,22)--cycle;
    \end{tikzpicture}
    \caption{Un plan de l'espace constitué d'une infinité de droites.}
    \label{fig:plan}
\end{figure}

\begin{remark}
    Pour déterminer un plan, il suffit de donner trois points non alignés ou
    deux droites sécantes ou encore deux droites strictement parallèles.
\end{remark}

\begin{remark}
    Dans chaque plan de l'espace, on peut appliquer tous les théorèmes de
    géométrie plane.
\end{remark}

\begin{example}
    $ABCDEFGH$ est un parallélépipède rectangle tel que:

    \setlength\multicolsep{0pt}%
    \nointerlineskip\vskip\lineskip
    \begin{multicols}{2}
        \begin{itemize}
            \item $AB=\SI{12}{\cm}$;
            \item $AD=AE=\SI{8}{\cm}$;
            \item $I$~est le milieu de~$[AB]$;
            \item $J$~est le milieu de~$[AD]$.
        \end{itemize}
        \begin{enumerate}
            \item Nommer le plan colorié.
            \item Calculer la longueur~$IJ$.
            \item Calculer la longueur~$JL$.
        \end{enumerate}

        \begin{tikzpicture}[x=1cm, y=1cm, z=(40:5mm), scale=1/5]
            \coordinate[label=below:$A$] (A) at (0,0,0);
            \coordinate[label=below:$B$] (B) at (12,0,0);
            \coordinate[label=left:$D$] (D) at (0,8,0);
            \coordinate[label=above right:$E$] (E) at (0,0,8);
            \coordinate[label=below right:$C$] (C) at ($ (D)+(B)-(A) $);
            \coordinate[label=below right:$F$] (F) at ($ (B)+(E)-(A) $);
            \coordinate[label=above right:$G$] (G) at ($ (C)+(E)-(A) $);
            \coordinate[label=above left:$H$] (H) at ($ (D)+(E)-(A) $);
            \coordinate[label=below:$I$] (I) at ($ (A)!1/2!(B) $);
            \coordinate[label=left:$J$] (J) at ($ (A)!1/2!(D) $);
            \coordinate[label={[base right]$K$}] (K) at ($ (J)+(E)-(A) $);
            \coordinate[label=above:$L$] (L) at ($ (I)+(E)-(A) $);
            \draw (F) -- (G) -- (H);
            \draw[dashed] (F) -- (E) -- (H);
            \draw[dashed] (A) -- (E);
            \draw (B) -- (F)    (C) -- (G)  (D) -- (H);
            \draw (A) -- (B) -- (C) -- (D) -- cycle;
            \fill[color=black,fill opacity=0.1] (I) -- (J) -- (K) -- (L);
            \draw[dashed] (I) -- (L) -- (K) -- (J);
            \draw (I) -- (J);
        \end{tikzpicture}
    \end{multicols}
\end{example}

\section{Positions relatives}

\subsection{Position relative de deux droites}

\begin{definition}
    Deux droites qu'on peut inclure dans un même plan sont dites
    \define{coplanaires}.
\end{definition}

\begin{remark}
    Deux droites sécantes sont coplanaires, de même que deux droites parallèles
    --- strictement ou non. Deux droites non coplanaires ne peuvent donc être ni
    sécantes, ni parallèles. \Vref{fig:droites-coplanaires} présente ces
    différents cas.
\end{remark}

\begin{figure}
    \ffigbox{}{
        \tikzset{x=12mm, y=12mm, z=(45:8mm)}
        \begin{subfloatrow}[4]
        \ffigbox{}{\caption{}\label{fig:droites-secantes}
            \begin{tikzpicture}
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick] (0.05,1,0.2) -- (0.9,1,0.8);
                \draw[thick] (0.05,1,0.8) -- (0.9,1,0.2);
            \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:droites-paralleles}
            \begin{tikzpicture}
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick] (0.05,1,0.2) -- (0.9,1,0.6);
                \draw[thick] (0.05,1,0.4) -- (0.9,1,0.8);
            \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:droites-confondues}
            \begin{tikzpicture}
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick] (0.05,1,0.8) -- (0.9,1,0.2);
            \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:droites-noncoplan}
            \begin{tikzpicture}
                \fill[fill opacity=0.1]
                    (0,0,0) -- (0,0,1) -- (1,0,1) -- (1,0,0) -- cycle;
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[thick,dashed] (0.1,0,0.2) -- (0.9,0,0.8);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick] (0.05,1,0.8) -- (0.9,1,0.2);
            \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Positions relatives de diverses droites.
        \subref{fig:droites-secantes} $\mcD$~et~$\mcD'$ sont coplanaires et
        sécantes; \subref{fig:droites-paralleles} $\mcD$~et~$\mcD'$ sont
        coplanaires et strictement parallèles; \subref{fig:droites-confondues}
        $\mcD$~et~$\mcD'$ sont confondues; \subref{fig:droites-noncoplan}
        $\mcD$~et~$\mcD'$ ne sont pas coplanaires.
    }
    \label{fig:droites-coplanaires}
    }
\end{figure}

\subsection{Position relative de deux plans}

\begin{definition}
    Deux plans sont \define{parallèles} si et seulement si deux droites sécantes
    de l'un sont respectivement parallèles à deux droites sécantes de l'autre.
    Deux plans non parallèles sont \define{sécants} suivant une droite --- voir
    \vref{fig:position-plans}.
\end{definition}

\begin{figure}
    \ffigbox{}{
        \tikzset{x=10mm, y=10mm, z=(45:6mm)}
        \begin{subfloatrow}[3]
        \ffigbox[\FBwidth]{}{\caption{}\label{fig:plans-paralleles}
            \begin{tikzpicture}[baseline={(0,0,0)}]
                \draw[fill=black!16,fill opacity=0.625]
                    (-0.6,0,-0.3)
                    node[above right=0.1em and 0.7em,inner sep=0em] {$\mcP'$}
                    -- (-0.6,0,1.2) -- (1.2,0,1.2) -- (1.2,0,-0.3) -- cycle;
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill=white,fill opacity=0.5]
                    (1,0,0) -- (1,1,0) -- (1,1,1) -- (1,0,1) -- cycle;
                \draw[fill=white,fill opacity=0.5]
                    (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (0,1,0) -- (0,1,1) -- (1,1,1);
                \draw[fill=black!16,fill opacity=0.625]
                    (-0.6,1,-0.3)
                    node[above right=0.1em and 0.7em,inner sep=0em] {$\mcP$}
                    -- (-0.6,1,1.2) -- (1.2,1,1.2) -- (1.2,1,-0.3) -- cycle;
            \end{tikzpicture}
        }
        \ffigbox[\FBwidth]{}{\caption{}\label{fig:plans-confondus}
            \begin{tikzpicture}[baseline={(0,0,0)}]
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill=white,fill opacity=0.5]
                    (1,0,0) -- (1,1,0) -- (1,1,1) -- (1,0,1) -- cycle;
                \draw[fill=white,fill opacity=0.5]
                    (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (0,1,0) -- (0,1,1) -- (1,1,1);
                \draw[fill=black!16,fill opacity=0.625]
                    (-0.6,1,-0.3)
                    node[above right=0.1em and 0.7em,inner sep=0em] {$\mcP$}
                    -- (-0.6,1,1.2) -- (1.2,1,1.2) -- (1.2,1,-0.3) -- cycle
                    (1,1,1)
                    node[below left=0.1em and 0.7em,inner sep=0em] {$\mcP'$};
            \end{tikzpicture}
        }
        \ffigbox[\Xhsize]{}{\caption{}\label{fig:plans-secants}
            \begin{tikzpicture}[baseline={(0,0,0)}]
                \draw[fill=black!16,fill opacity=0.625]
                    (-0.6,0,-1)
                    node[above right=0.1em and 0.7em,inner sep=0em] {$\mcP'$}
                    -- (-0.6,0,1) -- (2,0,1) -- (2,0,-1) -- cycle;
                \draw[dashed]
                    (1,1,0) -- (1,1,1)
                    (0,0,0) -- (0,0,1);
                \draw[dashed]
                    (0,1,1) -- (0,0,1) -- (1,0,1) --
                    (1,1,1) -- (0,1,1) -- (0,1,0);
                \draw[fill=black!16,fill opacity=0.625]
                    (-0.6,2.1,-1.1)
                    node[below right=0.1em and 0.7em,inner sep=0em] {$\mcP$}
                    -- (-0.6,0,1) -- (2,0,1) -- (2,2.1,-1.2) -- cycle;
                \draw[thick] (-1,0,1) node[left] {$\mcD$} -- (2.1,0,1);
                \draw[dashed,thick] (0,1,0) -- (0,0,1);
                \fill[white,fill opacity=0.5]
                    (0,0,0) -- (0,1,0) -- (1,1,0) --
                    (1,0,1) -- (1,0,0) -- cycle;
                \draw
                    (0,1,0) -- (0,0,0) -- (1,0,0) -- (1,1,0)
                    (1,0,0) -- (1,0,1);
                \draw[thick] (0,1,0) -- (1,1,0) -- (1,0,1);
            \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Positions relatives de plans.
        \subref{fig:plans-paralleles} $\mcP$~et~$\mcP'$ sont strictement
        parallèles; \subref{fig:plans-confondus} $\mcP$~et~$\mcP'$ sont
        confondus; \subref{fig:plans-secants} $\mcP$~et~$\mcP'$ sont sécants
        en~$\mcD$.
    }
    \label{fig:position-plans}
    }
\end{figure}

\begin{remark}
    Deux plans confondus sont considérés comme parallèles.
\end{remark}

\begin{proposition}
    Un plan coupe deux plans parallèles suivant deux droites parallèles.
\end{proposition}

\subsection{Positions relatives d'une droite et d'un plan}

\begin{definition}
 Une droite est \define{parallèle} à un plan si elle n'est pas sécante avec le
 plan. Une droite est \define{sécante} avec un plan~$\mcP$ si elle a un unique
 point d'intersection avec~$\mcP$ --- voir \cref{fig:droite-plan}.
\end{definition}

\begin{figure}
    \ffigbox{}{
        \tikzset{x=12mm, y=12mm, z=(45:8mm)}
        \begin{subfloatrow}[4]
        \ffigbox{}{\caption{}\label{fig:droite-parallele}
            \begin{tikzpicture}
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick] (0.05,0,0.8) -- (0.9,0,0.2);
            \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:droite-incluse}
            \begin{tikzpicture}
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick] (0.05,1,0.8) -- (0.9,1,0.2);
            \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:droite-secante}
            \begin{tikzpicture}
                \draw[dashed] (0,0,0) -- (0,0,1);
                \draw[dashed] (0,1,1) -- (0,0,1) -- (1,0,1);
                \draw[thick,dashed]
                    (0.25,0,0.25) -- (0.5,1,0.5);
                \draw[fill,fill opacity=0.1]
                    (0,1,0) -- (0,1,1) -- (1,1,1) -- (1,1,0) -- cycle;
                \draw (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
                \draw (1,1,1) -- (1,0,1) -- (1,0,0);
                \draw[thick]
                    (0.5,1,0.5) pic[point marker] {} -- (0.6,1.4,0.6);
            \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Positions relatives d'un plan et d'une droite.
        \subref{fig:droite-parallele} $\mcD$~est strictement parallèle au
        plan~$\mcP$; \subref{fig:droite-incluse} $\mcD$~est incluse dans le
        plan~$\mcP$; \subref{fig:droite-secante} $\mcD$~et~$\mcP$ sont sécants.
    }
    \label{fig:droite-plan}
    }
\end{figure}


\begin{proposition}
    Une droite est parallèle à un plan si et seulement si elle est parallèle à
    une droite du plan.
\end{proposition}


\end{document}
