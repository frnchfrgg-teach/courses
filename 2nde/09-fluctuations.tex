% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt,
                %oneside,
            ]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}
\StudentMode

\usetikzlibrary{datavisualization}

\rivmathlib{stats}

\def\rawpiles{
    2,2=9,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
}
\StatsSortData \piles = \rawpiles

\def\simulpiles{
4=2,
5=6,
6=24,
7=38,
8=62,
9=75,
10=85,
11=78,
12=60,
13=47,
14=17,
15=4,
16=1,
17=0,
18=1,
}

\def\classname{seconde~13}

\begin{document}

\chapter{Fluctuation d'échantillonnage}

\chapterquote{Selon les statistiques, il y a une personne sur cinq qui est
    déséquilibrée. S'il y a quatre personnes autour de toi et qu'elles te
    semblent normales, c'est pas bon.}{J.-C. \bsc{Van Damme}}


\begin{activity}
    \label{act:pileface}
    Chaque élève de la \classname\ a lancé $20$~fois une pièce et a compté le
    nombre de fois que «pile» a été obtenu.
    \begin{enumerate}
        \item Quelle proportion de pile s'attend-on à trouver ? Combien de
            «pile» devrait faire un élève en théorie ?
            \IfStudentT{\hfiller}
        \item Tous les élèves vont-ils obtenir exactement ce nombre de «pile» ?
            Pourquoi ?
            \IfStudentT{\hfiller}
        \iffalse
        \item On a rassemblé les résultats dans \vref{tab:piles}. Construire sur
            \vref{fig:comb-counts-piles} le diagramme en bâtons représentant
            cette série statistique.
            \begin{table}
                \centering
                \StatsTable\piles[
                        values=Nb. de pile,
                        ghosts={counts,icc},
                        icc,
                        maxcols=9,
                    ]
                \caption{Répartition des nombres de «pile» obtenus par les
                    élèves de \classname.}
                \label{tab:piles}
            \end{table}
            \begin{figure}
                \centering
                \StatsGraph\piles[values=Nombre de pile, width=8cm,
                                             comb/style=ghost
                                         ]
                \caption{Diagramme en bâtons de la répartition des «pile» dans
                    la \classname.}
                \label{fig:comb-counts-piles}
            \end{figure}
        \item \begin{enumerate}
                \item Déterminer la médiane et les quartiles de cette série.
                    \IfStudentT{\bigfiller{1}}
                \item Interpréter.
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
        \fi
        \item On a simulé par ordinateur $500$~élèves tirant chacun $20$~pièces;
            les résultats sont dans \cref{tab:simulpiles} et
            \cref{fig:comb-freq-simulpiles}.
            \begin{table}
                \centering
                \StatsTable\simulpiles[
                        values=Nombre de pile,
                        counts,
                        frequencies,
                        icf,
                        maxcols=8,
                    ]
                \caption{Répartition des nombres de «pile» obtenus par les
                    simulations.}
                \label{tab:simulpiles}
            \end{table}
            \begin{figure}
                \centering
                \StatsGraph\simulpiles[values=Nombre de pile, width=8cm,
                                        frequencies]
                \caption{Diagramme en bâtons de la répartition des «pile» pour
                    la simulation.}
                \label{fig:comb-freq-simulpiles}
            \end{figure}
            \begin{enumerate}
                \item Déterminer la médiane et les quartiles de la simulation.
                    Interpréter
                    \IfStudentT{\bigfiller{3}}
                \item Trouver le plus petit nombre~$a$ tel qu'au
                    moins~\SI{2.5}{\percent} des élèves simulés ont obtenu
                    $a$~fois «pile» ou moins.
                    \IfStudentT{\hfiller}
                \item Trouver de même le plus petit~$b$ tel qu'au
                    moins~\SI{97.5}{\percent} des élèves simulés ont obtenu
                    $b$~«pile» ou moins --- c'est-à-dire environ
                    \SI{2.5}{\percent} ayant obtenu plus de $b$~fois «pile».
                    \IfStudentT{\hfiller}
            \end{enumerate}
    \end{enumerate}
\end{activity}

\section{Échantillon et fluctuation}

\subsection{Expérience et échantillon}

\begin{definition}
    Une \define{expérience aléatoire} est une expérience renouvelable dont les
    résultats possibles sont connus sans qu'on puisse déterminer lequel sera
    réalisé.
\end{definition}

\begin{example}
    Les situations suivantes sont-elles des expériences aléatoires ?
    \begin{enumerate}
        \item un lancer de dé;
        \item un sondage d'opinion avant une élection;
        \item interroger une personne lors d'un sondage d'opinion;
        \item le tirage d'un jeton dans une urne ou d'une carte dans un jeu.
    \end{enumerate}
\end{example}

\begin{definition}
    Un \define{échantillon}{} de taille~$n$ est constitué des résultats
    de $n$~répétitions indépendantes de la même expérience.
\end{definition}

\begin{example} Les résultats suivants sont des échantillons:
    \begin{itemize}
        \item on lance une pièce $50$~fois et on regarde si on obtient pile;
        \item on tire $20$~fois une carte d'un jeu de $32$~cartes en la
            remettant et on regarde si c'est un cœur;
        \item on choisit \<1000>~fois un(e) français(e) au hasard et on lui
            demande si il (elle) votera aux prochaines élections
            présidentielles.
    \end{itemize}
\end{example}

\begin{remark}
    Le tirage du loto bien qu'un tirage de sept boules n'est pas un échantillon:
    les expériences ne sont pas réalisées de manière indépendantes. En effet, il
    n'est pas possible de tirer une deuxième fois une boule déjà tirée; à mesure
    que le tirage avance la probabilité de prendre une boule toujours en jeu
    \emph{augmente}.
\end{remark}

\begin{remark}
    Un sondage sur \<1000>~personnes choisis au hasard parmi l'ensemble des
    inscrits sur les listes électorales a le même problème en théorie que le
    tirage du loto. Cependant, même si on retire \<1000>~personne de l'ensemble
    des inscrits, la probabilité de choisir un inscrit changera très peu
    puisqu'il y a beaucoup beaucoup plus de \<1000>~inscrits. Dans ce cas on
    considère que les expériences sont indépendantes et que le sondage est bien
    un \emph{échantillon}.
\end{remark}

\subsection{Intervalle de fluctuation}

\begin{definition}
    Deux échantillons de même taille issus de la même expérience aléatoire ne
    sont généralement pas identiques. Les variations des fréquences relevées
    sont appelées \define{fluctuation d'échantillonnage}.
\end{definition}

Dans la suite on utilisera les notations suivantes:
\begin{itemize}
    \item $n$~est le nombre d'éléments de l'échantillon. On l'appelle
        effectif ou taille de l'échantillon.
    \item $f_o$~est la fréquence du caractère observé dans l'échantillon.
    \item $p$~est la proportion effective du caractère observé dans toute la
        population, y compris en dehors de l'échantillon.
\end{itemize}

\begin{definition}
    Un \define{intervalle de fluctuation} \emph{au seuil de~\SI{95}{\percent}},
    relatif aux échantillons de taille~$n$, est un intervalle \UGHOST{centré
    autour de~$p$ qui contient la fréquence observée~$f_o$ dans un échantillon
    de taille~$n$ avec une probabilité égale à~\<0.95>.}
\end{definition}

En d'autres termes c'est un intervalle centré autour de~$p$ qui représente les
fréquences observées des \SI{95}{\percent}~médians des échantillons théoriques.

\begin{remark}
    Il n'existe pas d'intervalle dans lequel on trouverait $f_o$ avec certitude
    --- à moins de prendre l'intervalle~$[0;1]$ --- à cause de la fluctuation
    d'échantillonnage.
\end{remark}

On a obtenu un intervalle de fluctuation grâce à la simulation dans
\cref{act:pileface}.

\begin{proposition}
    \label{thm:fluctuation}
    Si $p \in \IN[\<0.2>;\<0.8>;]$ et $n \ge 25$, alors $f_o$~appartient à
    l'intervalle $I_n =
    \GHOST{\IN[p-\dfrac{1}{\sqrt{n}};p+\dfrac{1}{\sqrt{n}};]}$ avec
    une probabilité d'environ~\UGHOST{\<0,95>}.

    Autrement dit, $I_n$ est un intervalle de fluctuation au seuil
    de~\SI{95}{\percent} relatif aux échantillons de taille~\UGHOST{$n \ge 25$}
    pour une proportion~\UGHOST{$p \in \IN[\<0.2>;\<0.8>;]$}.
\end{proposition}

\begin{method}[Prendre une décision]
    \begin{enumerate}
        \item On vérifie que la situation correspond bien à un
            \emph{échantillon}.
        \item On émet une hypothèse sur la proportion~$p$ du caractère dans la
            population.
        \item \UGHOST{On vérifie que $p$~et~$n$ sont dans les conditions de
            \cref{thm:fluctuation}.}
        \item On détermine l'intervalle de fluctuation au seuil
            de~\SI{95}{\percent} de la proportion~$p$ dans des échantillons de
            taille~$n$.
        \item On teste l'adéquation entre l'hypothèse et l'échantillon:
            \begin{itemize}
                \item Si $f_o \notin I_n$, \UGHOST{on rejette l'hypothèse faite
                    sur~$p$} \emph{avec un risque d'erreur de~\SI{5}{\percent}}.
                \item Si $f_o \in I_n$, on considère l'échantillon et
                    l'hypothèse \UGHOST{cohérents avec une probabilité
                    de~\<0.95>.}
            \end{itemize}
    \end{enumerate}
\end{method}

\begin{example}
    Entre 1999~et~2003 dans la réserve indienne d'\bsc{Aamjiwnaag}, située au
    Canada à proximité d'industries chimiques, il est né \<132>~enfants dont
    \<46>~garçons. Est ce normal?
    \IfStudentT{\bigfiller{6}}
\end{example}

\section{Estimation et confiance}

\begin{activity}[Loterie]
    Lors d'une kermesse d'école, des billets de loterie sont vendus avec
    l'annonce : «Un billet sur quatre est gagnant». Le papa de Cunégonde est
    très joueur et en achète \<28>.
    \begin{enumerate}
        \item Va-t-il gagner \<7> lots? \IfStudentT{\hfiller}
        \item En fait, il en a obtenu \<4>.
            \begin{enumerate}
                \item Déterminer l'intervalle de fluctuation associé à cet
                    échantillon.
                    \IfStudentT{\bigfiller{3}}
                \item Ce papa peut-il crier au scandale?
                    \IfStudentT{\bigfiller{1}}
            \end{enumerate}
        \item Pour quels résultats pourrait-il crier au scandale?
            \IfStudentT{\bigfiller{3}}
        \item Certain de ce qu'il avance, il achète de plus en plus de tickets
            et vérifie qu'il a toujours $\frac{1}{7}$~des billets gagnants pour
            $\frac{6}{7}$~perdants. Combien de billets devra-t-il acheter pour
            prouver l'injustice ?
            \IfStudentT{\bigfiller{6}}
    \end{enumerate}
\end{activity}

Cette fois, on connait toujours~$f_o$ par un échantillon, mais on ne connait
pas~$p$. L'intervalle de fluctuation permet d'obtenir un intervalle où se situe
la proportion inconnue~$p$ avec une probabilité de~\<0.95>: c'est
l'intervalle des proportions~$p$ qui sont cohérentes avec~$f_o$ au seuil
de~\SI{95}{\percent} --- autrement dit l'intervalle des~$p$ qu'on ne rejette
pas.

\begin{definition}
    Un \define{intervalle de confiance} au seuil de~\SI{95}{\percent}, relatif à
    un échantillon de taille~$n$ et de fréquence observée~$f_o$, est un
    intervalle centré autour de~$f_o$ où se situe la proportion~$p$ du caractère
    dans la population avec une probabilité égale à \<0.95>.
\end{definition}


\begin{proposition}
    \label{thm:confidence}
    Si $f_o \in \UGHOST{\IN[\<0.2>;\<0.8>;]}$ et \UGHOST{$n \ge 25$}, alors
    $p$~appartient à l'intervalle
    $\GHOST{\IN[f_o-\dfrac{1}{\sqrt{n}};f_o+\dfrac{1}{\sqrt{n}};]}$ avec une
    probabilité d'environ~\<0,95>.

    Autrement dit, l'intervalle précédent est un intervalle de confiance au
    seuil de~\SI{95}{\percent} relatif à un échantillon de taille~$n\ge25$ et de
    fréquence observée~$f_o \in \IN[\<0.2>;\<0.8>;]$.
\end{proposition}

\iffalse
\begin{proof}
    Cette symétrie dans les définitions d'intervalles de confiance et de
    fluctuation provient des inégalités suivantes:

    \GHOST{%
    \begin{align*}
        f_o \in \IN[p-\dfrac{1}{\sqrt{n}};p+\dfrac{1}{\sqrt{n}};]
            &\iff p-\dfrac{1}{\sqrt{n}} \le f_o \text { et }
                    f_o \le p+\dfrac{1}{\sqrt{n}} \\
            &\iff p \le f_o + \dfrac{1}{\sqrt{n}} \text { et }
                    f_o - \dfrac{1}{\sqrt{n}} \le p \\
            &\iff f_o - \dfrac{1}{\sqrt{n}} \le p \text { et }
                    p \le f_o + \dfrac{1}{\sqrt{n}} \\
            &\iff p \in \IN[f_o-\dfrac{1}{\sqrt{n}};f_o+\dfrac{1}{\sqrt{n}};]
            \qedhere
    \end{align*}%
    }
\end{proof}
\fi

\begin{method}[Estimer la proportion d'un caractère]
    \begin{enumerate}
        \item On réalise un échantillon de taille~$n$ et on obtient une
            fréquence observée~$f_o$.
        \item \UGHOST{On vérifie que $f_o$~et~$n$ sont dans les conditions de
            \cref{thm:confidence}.}
        \item On détermine l'intervalle de confiance au seuil
            de~\SI{95}{\percent} associée à l'échantillon réalisé, à partir de
            $f_o$~et~$n$.

            La proportion réelle~$p$ dans la population se situe dans cet
            intervalle \emph{avec une probabilité d'environ \<0.95>}.
    \end{enumerate}
\end{method}

\end{document}

\begin{example}
    Le 4~mai~2007, soit deux jours avant le second tour des élections
    présidentielles, on publie le sondage suivant réalisé auprès de
    \<992>~personnes :
    \begin{center}
        \begin{tabular}{lc}
            \hline
            Candidat & Intentions \\
            \hline
            S.~\bsc{Royal} & \SI{45}{\percent} \\
            N.~\bsc{Sarkozy} & \SI{55}{\percent} \\
            \hline
        \end{tabular}
    \end{center}

    Interpréter ce sondage.
    \IfStudentT{\bigfiller{7}}
\end{example}

