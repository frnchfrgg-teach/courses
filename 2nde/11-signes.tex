% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}
\geometry{margin=1.5cm,bindingoffset=0pt}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\usetikzlibrary{matrix}

\rivmathlib{geometry}

\StudentMode
\begin{document}

\iffalse

\chapter{Tableaux de signes et inéquations}

\section{Résolution graphique et tableaux de signes}

\begin{activity}[Où est l'eau ?]
    % Source: Sesamath 2F1 acti 1
    \Vref{tab:maree} contient les hauteurs d'eau, en mètres, relevées par le
    marégraphe de Saint-Malo le 30 aout 2012.

    \begin{table}
        \ttabbox[\hsize]{
        \begin{tabular}{l*{9}{c}}
            \firsthline
            Heure & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
            Hauteur (m) & 2,45 & 3,65 & 5,94 & 8,48 & 10,49 & 11,27 & 10,82 &
                        9,50 & 7,66 \\
            \lasthline
        \end{tabular}

        \begin{tabular}{l*{9}{c}}
            \firsthline
            Heure & 9 & 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 \\
            Hauteur (m) & 5,72 & 3,99 & 2,74 & 2,23 & 2,99 & 5,00 & 7,77 & 10,26
                        & 11,70 \\
            \lasthline
        \end{tabular}

        \begin{tabular}{l*{7}{c}}
            \firsthline
            Heure & 18 & 19 & 20 & 21 & 22 & 23 & 24 \\
            Hauteur (m) & 11,69 & 10,49 & 8,58 & 6,39 & 4,34 & 2,78 & 1,82 \\
            \lasthline
        \end{tabular}
        \caption{Relevés des hauteurs d'eau à Saint-Malo}
        \label{tab:maree}
        }
    \end{table}

    \begin{enumerate}
        \item À quelle heure la marée est-elle haute? Basse?
            \IfStudentT{\bigfiller{1}}
        \item Quand la mer a-t-elle dépassé une hauteur de \SI{10}{m} ?
            \IfStudentT{\bigfiller{1}}
        \item Quand la mer a-t-elle une hauteur inférieure à \SI{4}{m} ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{activity}

\section{Signe d'une fonction affine}

\begin{activity}
    \begin{enumerate}
        \item Soit $f:x\mapsto -\<0.5>x+3$.
            \begin{enumerate}
                \item Résoudre $f(x) > 0$ et $f(x) < 0$.
                \item Tracer dans un repère la courbe~$\mcC_f$.
                \item Dresser le tableau de signes de~$f$.
            \end{enumerate}
        \item Mêmes questions avec $g:x\mapsto x + 2$.
        \item Que remarque-t-on ?
    \end{enumerate}
\end{activity}

\clearpage

\begin{proposition}
    \label{thm:signe-affine}
    Soit $f:x\mapsto mx+p$ une fonction affine. Alors:

    \centering
    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1}
                        {{$\;-\infty$},$\GHOST{\frac{-p}{m}}$,$+\infty\;$}
            \IfStudentF{\tkzTabLine{,-,z,+,}}
        \end{scope}
        \node[above] at (current bounding box.north) {si $m>0$};
    \end{tikzpicture}
    \hfil\hfil
    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1}
                        {{$\;-\infty$},$\GHOST{\frac{-p}{m}}$,$+\infty\;$}
            \IfStudentF{\tkzTabLine{,+,z,-,}}
        \end{scope}
        \node[above] at (current bounding box.north) {si $m<0$};
    \end{tikzpicture}
\end{proposition}

\begin{proof}
    \IfStudentTF{
        \bigfiller{3}
    }{
        \begin{itemize}
            \item Cas où $m>0$: $mx+p > 0 \iff mx > -p \iff x > \frac{-p}{m}$.
            \item Cas où $m<0$: $mx+p > 0 \iff mx > -p \iff x < \frac{-p}{m}$
                (changement de sens).
            \item Autre méthode (rédaction pour $m<0$):
                $f(x) = 0 \iff mx+p = 0 \iff mx = -p \iff x = \frac{-p}{m}$. Si
                $x > \frac{-p}{m}$ alors $f(x) < \frac{-p}{m}$ car $f$~est
                strictement décroissante par la
                proposition~\ref{thm:var-affine}; c'est dire $f(x) < 0$. De même
                si $x < \frac{-p}{m}$ alors $f(x) > 0$.  \qedhere
        \end{itemize}
    }
\end{proof}

\begin{example}
    Dresser les tableaux de \emph{signe} des fonctions affines suivantes:
    \begin{enumerate}[gathered, label={$\falph*(x)={}$},labelsep=0pt]
        \item $2x+3$;
        \item $-3-3x$;
        \item $-\sqrt{2}x-1$;
        \item $\dfrac{1-x}{2}$.
    \end{enumerate}
    \IfStudentT{
        \bigfiller{8}
    }
\end{example}

\section{Signe d'un produit, d'un quotient}

\subsection{Tableau de signes d'un produit}

\begin{activity}
    Soit~$f$ définie sur~$\mdR$ par $f(x) = (x+3)(2x+1)$.
    \begin{enumerate}
        \item Si $x=-2$, quel est le signe de~$x+3$ ? Et de $2x+1$ ?
            \IfStudentT{\bigfiller{1}}
        \item En déduire le signe de~$f(x)$ quand $x=-2$.
            \IfStudentT{\bigfiller{1}}
        \item On suppose $x \in \IN]-3;-\frac{1}{2};[$. Déterminer le signe
            de~$x+3$, de~$2x+1$, puis de~$f(x)$.
            \IfStudentT{\bigfiller{2}}
        \item Dresser les tableaux de signes de $x+3$~et $2x+1$.
            \IfStudentT{\bigfiller{6}}
        \item Comment construire le tableau de signes de~$f$ ?
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\fi

\begin{method}[Tableau de signe d'un produit]
    \IfStudentTF{\def\p{}\def\m{}}{\def\p{+}\def\m{-}}
    \begin{enumerate}
        \item Mettre l’expression sous forme factorisée:
            $P(x) = \smash{\overset{F_1}{\overbrace{(x+3)}}
                    \overset{F_2}{\overbrace{(1-x)}}
                    \overset{F_3}{\overbrace{(2x+1)}}}$
        \item\label{step:tableau-signes-racines}
            Calculer les solutions de chaque équation $F_i = 0$, en utilisant la
            formule $\dfrac{-p}{m}$.
        \item Préparer le tableau de signes avec une première ligne pour~$x$,
            une ligne pour chaque facteur~$F_i$, et une dernière ligne pour le
            produit~$P(x)$. Dans la première ligne, placer en ordre croissant
            les bornes de l'ensemble de définition et toutes les solutions
            trouvées à l'étape~\ref{step:tableau-signes-racines}. Tracer une
            verticale sur toute la hauteur sous chaque solution.

            \begin{center}
            \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=2,espcl=2]
                            {$x$/1,$x+3$/1,$1-x$/1,$2x+1$/1,$P(x)$/1}
                            {$\;-\infty$,$-3$,$-\frac{1}{2}$,$1$,$+\infty\;$}
                \tkzTabLine {, ,t, ,t, ,t, ,}
                \tkzTabLine {, ,t, ,t, ,t, ,}
                \tkzTabLine {, ,t, ,t, ,t, ,}
                \tkzTabLine {, ,t, ,t, ,t, ,}
            \end{tikzpicture}
            \end{center}
        \item Compléter la ligne de chaque facteur~$F_i$ comme suit:
            \begin{enumerate}
                \item Tracer un~\tikz[baseline=(n.base)]0
                    \draw (0,-1.5ex) -- node (n) {$0$} (0,1.5ex);
                    sur la verticale de la solution de~$F_i = 0$.
                \item On place les signes d'après le théorème du tableau de
                    signe d'une fonction affine, selon si $m>0$ ou $m<0$.
            \end{enumerate}
            \begin{center}
            \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=2,espcl=2]
                            {$x$/1,$x+3$/1,$1-x$/1,$2x+1$/1,$P(x)$/1}
                            {$\;-\infty$,$-3$,$-\frac{1}{2}$,$1$,$+\infty\;$}
                \tkzTabLine {,\m,z,\p,t,\p,t,\p,}
                \tkzTabLine {,\p,t,\p,t,\p,z,\m,}
                \tkzTabLine {,\m,t,\m,z,\p,t,\p,}
                \tkzTabLine {, ,t, ,t, ,t, ,}
            \end{tikzpicture}
            \end{center}
        \item Compléter la dernière ligne --- celle de~$P(x)$:
            \begin{enumerate}
                \item\label{step:zero} Reporter les zéros des facteurs depuis leurs lignes.
                \item Compléter les signes avec la règle du signe d'un
                    produit:\\
                    «$-$» par «$-$» égale «$-$» par «$-$» égale «$+$»,\\
                    «$-$» par «$+$» égale «$+$» par «$-$» égale «$-$».
            \end{enumerate}
            \begin{center}
            \begin{tikzpicture}[TAB]
                \tkzTabInit [lgt=2,espcl=2]
                            {$x$/1,$x+3$/1,$1-x$/1,$2x+1$/1,$P(x)$/1}
                            {$\;-\infty$,$-3$,$-\frac{1}{2}$,$1$,$+\infty\;$}
                \tkzTabLine {,-,z,+,t,+,t,+,}
                \tkzTabLine {,+,t,+,t,+,z,-,}
                \tkzTabLine {,-,t,-,z,+,t,+,}
                \tkzTabLine {,\p,z,\m,z,\p,z,\m,}
            \end{tikzpicture}
            \end{center}
    \end{enumerate}
\end{method}

\subsection{Tableau de signes d'un quotient}

La méthode est exactement la même, à ceci près que les réels qui annulent les
facteurs du dénominateur donnent une valeur interdite pour~$f$. On change donc
l'étape~\ref{step:zero}:
\begin{itemize}
    \item en face des zéros \emph{du dénominateur}, placer une double barre,
        puis
    \item reporter les zéros \emph{du numérateur} qui ne sont pas des valeurs
        interdites.
\end{itemize}

\begin{example}
    Dresser le tableau de signes de $Q(x) = \dfrac{1-x}{(x+3)(2x+1)}$
\end{example}

\end{document}
