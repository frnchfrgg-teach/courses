% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}
\usetikzlibrary{decorations.markings}

\rivmathlib{geometry}

\begin{document}
\StudentMode

\chapter{Trigonométrie}

\section{Repérage sur le cercle trigonométrique}

\subsection{Enroulement de la droite des réels}

\begin{definition}
    On munit le plan d'un repère orthonormé~$(O;I;J)$.  Le \define{cercle
    trigonométrique}~$\mcC$ est le cercle de centre~$O$ et de rayon~$1$, sur
    lequel on choisit une orientation:
    \begin{itemize}
        \item le \define{sens direct} --- ou positif, ou encore
            \define{trigonométrique} --- est \emph{contraire} au sens de
            rotation des aiguilles d'une montre;
        \item le \define{sens indirect} --- ou négatif --- est le sens des
            aiguilles d'une montre.
    \end{itemize}
\end{definition}

On peut «enrouler» la droite d'équation~$x=1$ sur le cercle~$\mcC$ de la façon
suivante: si $M(1;t)$~est un point de cette droite on imagine que le
segment~$[IM]$ est un mètre à ruban qu'on peut faire tourner autour du cercle
sans l'étirer: la distance entre $I$~et~$M$ \emph{en suivant} le mètre à ruban
ne change jamais quelque soit la forme qu'on donne à ce dernier. Quand on
enroule une longueur de ruban de plus en plus grande, il forme une spirale de
plus en plus serrée qui se confond avec le cercle. Chaque point sur la droite
verticale est ainsi transformé en un point sur le cercle --- voir la
figure~\ref{fig:enroul}.

\begin{figure}
    \centering
    \caption{Enroulement de la demi-droite d'équation~$x=1$ pour $y \ge 0$. En
        haut à gauche, le ruban est le long de la demi-droite. En haut à droite,
        on a commencé à enrouler le ruban sur le cercle. En bas à gauche, le
        ruban est partiellement enroulé et le reste forme une spirale. En bas à
        droite on a totalement enroulé le ruban et l'on voit que $A$~et~$C$ se
        superposent.
    }
    \label{fig:enroul}
    \def\sqsize{2.3}
    % Found with Maxima to have a C^1 piecewise-defined function
    % - from 0 to \mp: 3rd order polynomial with value=0, derivative=1 at 0
    % - from \mp to +\infty: acos(1/t)
    \pgfmathsetmacro\mp{2*pi/3}
    \pgfmathsetmacro\malpha{0.05352575414691953}
    \pgfmathsetmacro\mbeta{-0.3449475860352801}
    \foreach \angle [evaluate=\angle using \angle*pi/12] in {0, 5, 18, 24}{
        \begin{tikzpicture}[x=1cm,y=1cm,
                            grid/.style={help lines, black!20},
                            axes/.style=->,
                            circle/.style=semithick
                            ]
            \path[clip]
                (-\sqsize, -\sqsize) rectangle (\sqsize, \sqsize);
            \draw[grid]
                (-\sqsize, -\sqsize) grid (\sqsize, \sqsize);
            \draw[axes] (-\sqsize, 0) -- (\sqsize, 0);
            \draw[axes] (0, -\sqsize) -- (0, \sqsize);
            \draw[circle] (0,0) circle[radius=1];
            \node at (0,0) [below left]  {$O$};
            \node at (1,0) [below right] {$I$};
            \node at (0,1) [above left]  {$J$};
            \draw[thick] (1, 0) -- (1, -3);
            \pgfmathsetmacro\mq{1-\angle/2/pi}
            \gdef\thepoints{}
            \foreach \t in {0, 0.05, ..., 14.5} {
                \pgfmathtruncatemacro\test{\t < \angle}
                \ifnum\test=1\relax
                    \def\mr{1}
                \else
                    \pgfmathsetmacro\mt{(\t-\angle) * \mq}
                    \pgfmathtruncatemacro\test{\mt > \mp}
                    \ifnum\test=1\relax
                        \pgfmathsetmacro\mc{rad(acos(1/\mt))}
                    \else
                        \pgfmathsetmacro\mc{\malpha * pow(\mt,3) +
                                            \mbeta * pow(\mt,2) + \mt}
                    \fi
                    \pgfmathsetmacro\me{pow(\mq,2)*\mt + (1-pow(\mq,2))*\mc}
                    \pgfmathsetmacro\mr{1/cos(\me r)}
                    \pgfmathtruncatemacro\test{\mr > 4}
                    \ifnum\test=1\relax
                        \breakforeach % at end of iteration
                    \fi
                \fi
                \xappto\thepoints{({\t r}:\mr)}
            }
            \draw[thick,
                  postaction=decorate,
                  decoration={
                            markings,
                            mark=at position 1 * 1cm with {
                                \pic {point};
                                \node at (0, 2ex) {$A$};
                            },
                            mark=at position pi * 1cm with {
                                \pic {point};
                                \node at (0, -2ex) {$B$};
                            },
                            mark=at position (2*pi + 1) * 1cm with {
                                \pic {point};
                                \node at (0, -2ex) {$C$};
                            },
                  }]
                plot coordinates{\thepoints};
        \end{tikzpicture}%
    }
\end{figure}

\begin{remark}
    Lorsqu'on enroule l’axe dans le sens \emph{direct}, ce sont les points
    d'\emph{abscisses positives} qui se superposent au cercle~$\mcC$, dans le
    sens \emph{indirect}, ce sont des points d'\emph{abscisses négatives}.
\end{remark}

\subsection{Point repéré par un réel}

\begin{definition}
    On dit que $M\in\mcC$ est \define{repéré} par~$t$ si le point $N(1;t)$ se
    retrouve en~$M$ après enroulement de la droite d'équation $x = 1$.
\end{definition}

\begin{remark}
    Un réel donné ne repère qu'un seul point, mais un point~$M$ du cercle est
    toujours repéré par une infinité de réels: si $M$~est repéré par~$t>0$ alors
    le point repéré par~$t + 2\pi$ est le même --- la longueur supplémentaire de
    ruban sert exactement à faire un tour complet en plus. De même pour
    $t-2\pi$.
\end{remark}

\begin{example}[Lire des réels qui repèrent un point donné]
    \leavevmode\\
    \begin{minipage}{\textwidth-4.5cm}
        \parskip=2ex
        Dans la figure ci-contre:

        $J$ est repéré par \UGHOST{$\dfrac{\pi}{2}$ ou $\dfrac{-3\pi}{2}$ ou
        $\dfrac{5\pi}{2}$;}

        $K$ est repéré par \UGHOST{$\dfrac{5\pi}{6}$ ou $\dfrac{-19\pi}{6}$;}

        $L$ est repéré par \UGHOST{$\dfrac{5\pi}{4}$ ou $\dfrac{-3\pi}{4}$.}
    \end{minipage}
    \hfill
    \begin{tikzpicture}[scale=1.5, baseline=0]
        \draw[help lines, black!20] (-1.2, -1.2) grid[step=0.5] (1.2, 1.2);
        \draw[->] (-1.2, 0) -- (1.2, 0);
        \draw[->] (0, -1.2) -- (0, 1.2);
        \draw (0, 0) circle [radius=1];
        \coordinate[label=below left:$O$] (O) at (0,0);
        \node at (1,0) [below right] {$I$};
        \draw (0,1)    coordinate[label=above right:$J$] (J) pic {point};
        \draw (150:1)  coordinate[label=above left:$K$]  (K) pic {point};
        \draw (-135:1) coordinate[label=below left:$L$]  (L) pic {point};
        \draw[dashed] (J) -- pic[dist marker] {}
                      (K) -- pic[dist marker] {}
                      (O);
        \path (O) -- pic[dist marker] {} (J);
    \end{tikzpicture}
    \parfillskip=0pt\par
\end{example}

\begin{example}[Placer le point répéré par un réel donné]
    Tracer un cercle trigonométrique et placer les points associés aux réels
    $\pi$;~$-\frac{\pi}{2}$; $\frac{\pi}{3}$ et~$-\frac{\pi}{6}$.

    \IfStudentT{\bigfiller{8}}
\end{example}


On a donc la correspondance suivante --- si $0 \le x \le 360$ et $0 \le y \le
2\pi$:
\[  \everymath{\displaystyle}
    \begin{array}{LlL*{11}{cL}}
    \firsthline
    \text{Degrés} & 0 & 30 & 45 & 60 & 90 & 120 & 180 & 270 & 360 &
                    x & \GHOST{\frac{180}{\pi}y} \NL
    \text{Radians} & \GHOST{0} & \GHOST{\frac{\pi}{6}} & \GHOST{\frac{\pi}{4}} &
                    \GHOST{\frac{\pi}{3}} & \GHOST{\frac{\pi}{2}} &
                    \GHOST{\frac{2\pi}{3}} & \GHOST{\pi} &
                    \GHOST{\frac{3\pi}{2}} & \GHOST{2\pi} &
                    \GHOST{\frac{\pi}{180}x} & y \\
    \lasthline
\end{array} \]

\end{document}
