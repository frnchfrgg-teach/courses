% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usetikzlibrary{datavisualization,
                datavisualization.formats.functions,
                fit, shapes.geometric}

\newdimen\mywidth

\begin{document}

\let\vref\cref
\StudentMode

\chapter{Généralités sur les fonctions}

\section{Ensembles de nombres}

\subsection{Naturels, rationnels, réels et compagnie}

\begin{center}
\setlength\mywidth{\linewidth-20em-5ex}%
\begin{tabular}{lMr>{\centering}m{8em}>{\small}m{\mywidth}}
    \hline
    Ensemble & \llap{Notation} & Exemples & Remarques
    \\\hline
    Entiers naturels & \mdN & $0; 1; 2; 5; \dotsc$ &
    \\
    Entiers relatifs & \mdZ & $\dotsc; -8; 0; 1; 5; \dotsc$ &
    \\
    Nombres décimaux & \mdD &
        les précédents, et $-\frac{11}{4}; 1,334; \dotsc$ &
        Ce sont les nombres dont l'écriture décimale s'arrête\footnote{La
        notation~$\mdD$ n'est utilisée qu'en France.}.
    \\
    Nombres rationnels & \mdQ &
        les précédents, et $\frac{4}{3}; \frac{9}{7};\dotsc$ &
        Ce sont les nombres pouvant s'écrire~$\frac{p}{q}$ avec $p$~et~$q$
        entiers relatifs\footnote{On peut démontrer que ce sont les nombres
        dont l'écriture décimale est finie ou infinie~\emph{périodique}.}.
    \\
    Nombres réels & \mdR &
        les précédents, et $\sqrt{2}; \pi;\dotsc$ &
        Les nombres de $\mdR\setminus\mdQ$ sont appelés nombres irrationnels.
    \\\hline
\end{tabular}
\end{center}

\begin{remark}
    $\mdN \subset \mdZ \subset \mdD \subset \mdQ \subset \mdR$; en d'autres
    termes, les entiers naturels sont aussi des entiers relatifs, les relatifs
    sont décimaux, etc: c'est une hiérarchie d'ensembles de nombres (voir
    \vref{fig:number-sets}).
\end{remark}

\begin{example}
    Compléter avec $\in$, $\notin$, ou le plus petit ensemble qui convient :
    \begin{align*}
        -\frac{5}{3} &\in \dotsc     &  10^{-3} &\in \dotsc      &
        \pi &\dotsc \mdR  &  -10^4 &\in \dotsc &
        \sqrt{9} &\in \dotsc \\
        5^0 &\dotsc \mdN            &  \sqrt{5} &\dotsc \mdQ    &
        \frac{13}{2} &\in \dotsc     &  \<-3.4444>... &\in \dotsc  &
        \<12.35> &\dotsc \mdD
    \end{align*}
\end{example}

\begin{figure}
    \caption{Représentation des ensembles de nombres imbriqués.}
    \label{fig:number-sets}
    \centering
    \begin{tikzpicture}[scale=0.7,absolute]
        \node (n1) at (0,0) {$0$};
        \node (n2) at (1,1) {$3$};
        \node (z1) at (1.5,-0.5) {$-4$};
        \node (z2) at (2.5,1.5) {$-1$};
        \node (d1) at (2,-2) {$\<5.63>$};
        \node (d2) at (-0.5,2.8) {$\<-3.2>$};
        \node (q1) at (-2.5,-1.5) {$\dfrac{4}{3}$};
        \node (q2) at (-3.5,0) {$-\dfrac{3}{11}$};
        \node (r1) at (5.5,2.5) {$\pi$};
        \node (r2) at (4,4) {$\sqrt{5}$};
        \node [draw, ellipse, rotate fit=40,
                fit=(n1) (n2),
                pin=0:$\mdN$] (N) {};
        \node [draw, ellipse, rotate fit=0,
                fit=(n1) (n2) (z1) (z2),
                pin=160:$\mdZ$] (Z) {};
        \node [draw, ellipse, rotate fit=-30,
                fit=(n1) (n2) (z1) (z2) (d1) (d2),
                pin=170:$\mdD$] (D) {};
        \node [draw, ellipse, rotate fit=-10,
                fit=(n1) (n2) (z1) (z2) (d1) (d2) (q1) (q2)
                    (D.80) (D.-20),
                pin=0:$\mdQ$] (Q) {};
        \node [draw, ellipse, rotate fit=0,
                fit=(n1) (n2) (z1) (z2) (d1) (d2) (q1) (q2) (r1) (r2)
                    (Q.100) (Q.-90), inner ysep=-0.5cm,
                pin=150:$\mdR$] (R) {};
    \end{tikzpicture}
\end{figure}

\subsection{Intervalles}

\begin{activity}
    \begin{enumerate}
        \item Représenter sur l'axe des réels les ensembles correspondant aux
            conditions suivantes:
            \begin{enumerate}[gathered]
                \item $x \geq 2$;
                \item $x < 5$;
                \item $x \geq 2$~et~$x < 5$;
                \item $x \leq -1$;
                \item $x \geq 2$~et~$x \leq -1$;
                \item $x \geq 2$~ou~$x \leq -1$;
                \item $x \geq 2$~ou~$x < 5$.
            \end{enumerate}
        \item Si possible, écrire ces ensembles sous forme d'intervalles.
    \end{enumerate}
    \IfStudentT{\bigfiller{8}}
\end{activity}

\begin{definition}
    Un intervalle de~$\mdR$, ou \define{intervalle}, est un ensemble~$I$ de
    nombres réels qui n'a pas de «trou»: on peut aller de n'importe quel nombre
    de~$I$ à n'importe quel autre sans devoir sortir de~$I$.

    On obtient un intervalle en sélectionnant tous les nombres
    \begin{itemize}
        \item \emph{entre} deux nombres réels $\alpha \le \beta$: on note $I =
            \begin{array}[t]{|*{5}{c|}}
                \hline
                \text{$]$ ou $[$} & \alpha & ; & \beta & \text{$]$ ou $[$} \\
                \hline
            \end{array}$
        \item plus petits qu'un réel $\beta$: on note $I =
            \begin{array}[t]{|*{3}{c|}}
                \hline
                \GHOST{]-\infty \mathrel;} & \GHOST{\beta} &
                    \GHOST{\text{$]$ ou $[$}} \\
                \hline
            \end{array}$
        \item plus grands qu'un réel $\alpha$: on note $I =
            \begin{array}[t]{|*{3}{c|}}
                \hline
                \GHOST{\text{$]$ ou $[$}} & \GHOST{\alpha} &
                    \GHOST{\mathrel; +\infty[} \\
                \hline
            \end{array}$
    \end{itemize}
    Les nombres $\alpha$~et~$\beta$ sont appelés \emph{bornes} de l'intervalle.
    Les embouts d'un crochet sont dirigés vers la borne correspondante si elle
    fait partie de l'intervalle, ou vers l'extérieur si la borne n'est pas dans
    l'intervalle. Les notations $\pm\infty$ doivent toujours avoir leur signe,
    et ne sont jamais \emph{incluses} puisque ce ne sont pas des nombres --- le
    crochet est toujours vers l'extérieur.
\end{definition}

\begin{example}
    \begin{enumerate}
        \item $x \in \IN[\alpha;\beta;]$ correspond~à $\alpha \le x \le \beta$;
        \item $x \in \IN]\alpha;\beta;]$ correspond~à $\alpha < x \le \beta$;
        \item $x \in \IN]\alpha;+\infty;[$ correspond~à $\alpha < x$
                c'est-à-dire $x > \alpha$.
    \end{enumerate}
\end{example}

\begin{example}
    Indiquer les intervalles correspondant aux conditions suivantes:
            \begin{enumerate}[gathered]
                \item $x \geq 3$;
                \item $2 \leq x < 8$;
                \item $x \leq 5$~et~$x > -5$;
                \item $x \leq 12$;
                \item $x \geq 5$~et~$x \leq 5$.
            \end{enumerate}
    \IfStudentT{\bigfiller{5}}
\end{example}

\begin{remark}
    Lorsqu'on \emph{cumule} des conditions avec le mot «et», il est de plus en
    plus difficile pour un nombre d'être accepté dans l'ensemble puisqu'il faut
    pour cela remplir \emph{toutes} les conditions; chaque condition
    \emph{retire} des nombres à l'ensemble. C'est la notion
    d'\emph{intersection}, notée $\cap$.
\end{remark}

\begin{remark}
    Lorsqu'on \emph{associe} des conditions avec le mot «ou», il est de plus en
    plus facile pour un nombre d'être accepté dans l'ensemble puisqu'il suffit
    pour cela de remplir \emph{une} des conditions; chaque condition
    \emph{ajoute} des nouveaux nombres à l'ensemble. C'est la notion
    d'\emph{union}, notée $\cup$.
\end{remark}

\begin{example}
    \begin{enumerate}
        \item $2 \leq x$ correspond à \UGHOST{$\IN[2;+\infty;[$} et $x < 8$
            correspond à \UGHOST{$\IN]-\infty;8;[$}. Ainsi $2 \leq x < 8$
            correspond à $\IN[2;8;[$ mais aussi à
            \UGHOST{$\IN[2;+\infty;[ \cap \IN]-\infty;8;[$}.
        \item $x < 0 \text{ ou } x > 1$ correspond à
            \UGHOST{$\IN]-\infty;0;[ \cup \IN[1;+\infty;[$}.
        \item $\IN[0;25;] \cup \IN[11;30;]$ peut se simplifier en
            \UGHOST{$\IN[0;30;]$}. En effet:
            \bigfiller{1}
        \item $\IN[0;11;] \cup \IN[25;30;]$ \UGHOST{ne peut pas se simplifier}.
            \bigfiller{1}
        \item $\IN]-\infty;0;[ \cup \IN]0;+\infty;[$ se simplifie-t-il ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\section{Fonctions}

\subsection{Ensemble de définition, images, antécédents}

\begin{activity}[Inspirée par \textit{La Fille du puisatier}, Marcel Pagnol]
    % Source: Sesamath 2F1 acti 2
    \label{act:seaux}\leavevmode\\
    Pascal \bsc{Amoretti}, puisatier, possède plusieurs seaux pour transporter
    de l'eau. Il voudrait connaitre le volume d'eau transporté en mesurant juste
    la hauteur d'eau grâce à une jauge, c'est-à-dire une réglette graduée.
    Jacques \bsc{Mazel}, son beau-fils, lui construit un schéma et une courbe:
    voir \vref{fig:seaux}.

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}
        \ffigbox{}{\caption{}
        \begin{tikzpicture}[scale=0.7]
            \coordinate (A) at (-1,0); 
            \coordinate (C) at (1,0); 
            \coordinate (F) at (-2.5,3); 
            \coordinate (G) at (2.5,3); 
            \coordinate (B) at (0,0); 
            \coordinate (H) at (0,3); 
            \coordinate (D) at (-2,2); 
            \coordinate (E) at (2,2); 
            \draw[fill,fill opacity=0.25]
                (A)..controls +(0,0.25) and +(0,0.25)..
                (C)..controls +(0,-0.25) and +(0,-0.25)..
                (A)--
                (D)..controls +(0,0.5) and +(0,0.5)..
                (E)--
                (C);
            \draw (D)..controls +(0,0.5) and +(0,0.5)..(E)..controls +(0,-0.5) and +(0,-0.5)..(D);
            \draw (F)..controls +(0,0.5) and +(0,0.5)..(G)..controls +(0,-0.5) and +(0,-0.5)..(F);
            \draw (A)-- (F);
            \draw(C)-- (G);
            \draw (B)--(H);
        \end{tikzpicture}
        }
        \ffigbox{}{\caption{}
        \begin{tikzpicture}
            \datavisualization [school book axes,
                                x axis={unit length=2cm,
                                    grid={step=1,minor steps between steps=3}},
                                y axis={unit length=0.25cm,
                                    grid={step=1,minor steps between steps=1}},
                                visualize as smooth line=C,
                            ]
            data [set=C, format=function] {
                var x : interval [0:2.8];
                func y = 0.03 * pow(\value x, 3) + 0.56 * pow(\value x, 2)
                            + 3.14 * \value x;
            }
            ;
        \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Schéma et courbe de \cref{act:seaux}.}
    \label{fig:seaux}
    }
\end{figure}

À l'aide du graphique, compléter les tableaux de correspondance suivants:

\begin{center}
    \def\F{\GHOST{10}}
    \begin{tabular}{LlL*{6}{cL}}
        \firsthline
        Hauteur en dm & 0 & 0,5 & 1 & 1,5 & 2 & 2,5\NL
        Volume d'eau en L & \F & \F & \F & \F & \F & \F\\
        \lasthline
    \end{tabular}

    \begin{tabular}{LlL*{6}{cL}}
        \firsthline
        Volume d'eau en L & 1 &2 & 3 & 4 & 5 & 10\NL
        Hauteur en dm & \F & \F & \F & \F & \F & \F\\
        \lasthline
    \end{tabular}
\end{center}
\end{activity}

\begin{activity}[Somme de chiffres]
    \label{act:sumdigits}
    % Source: Sesamath 2F1 acti 3
    On considère un processus qui, à tout nombre entier naturel, associe la
    somme de ses chiffres.
    \begin{enumerate}
        \item Qu'obtient-on à partir du nombre \<13717> ?
            \IfStudentT{\hfiller}
        \item Proposer un nombre dont le résultat de ce processus est $22$.
            \IfStudentT{\hfiller}
        \item Combien de nombres de l'intervalle $\IN]0;\<10000>;]$ permettent
            d'obtenir $3$? Expliquer.
            \IfStudentT{\bigfiller{3}}
        \item Est-ce que tout entier naturel peut être le résultat de ce
            processus?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{activity}

\begin{definition}
    Une \define{fonction} est un objet mathématique\footnote{On ne détaillera
    pas plus au niveau du lycée.} qui associe une \UGHOST{unique
        \hbox{\define{image}}
    notée~$f(x)$ à chaque élément~$x$ de son \hbox{\define{ensemble de
    définition}}}.
\end{definition}

Autrement dit:
\begin{itemize}
    \item l'ensemble de définition de~$f$ est \UGHOST{l'ensemble des nombres
            qui ont une image;}
    \item \UGHOST{\emph{tous}} les nombres de l'ensemble de définition
        \UGHOST{ont une image;}
    \item chacun de ces nombres a \UGHOST{\emph{une seule} image;}
    \item deux nombres peuvent avoir la même image.
\end{itemize}

\begin{definition}
    Soit $f$~une fonction définie sur~$\mcD$. On dit que $x\in\mcD$ est un
    \define{antécédent} de~$y$ par~$f$ si \UGHOST{$f(x)=y$}. Les antécédents
    de~$y$ par~$f$ sont \UGHOST{tous les $x\in\mcD$ dont l'image est~$y$}.
\end{definition}

\begin{example}
    \begin{enumerate}
        \item On considère la fonction~$f$ de \vref{act:sumdigits}.
            \begin{enumerate}
                \item Quel est l'ensemble de définition de~$f$ ?
                    \hfiller
                \item Déterminer $f(\<13717>)$.
                    \hfiller
                \item Indiquer un antécédent de~$22$.
                    \hfiller
            \end{enumerate}
         \item Soit $g$ définie sur~$\IN]-5;30;[$ par $g(x) = 3x - 1$.
            \begin{enumerate}
                \item $-3$~a-t-il une image ? Si oui, laquelle ?
                    \hfiller
                \item $-8$~a-t-il une image ? Si oui, laquelle ?
                    \hfiller
                \item $3 \times 20 - 1 = 59 \notin \IN]-5;30;[$. Le nombre~$20$
                    a-t-il une image ?
                    \IfStudentT{\bigfiller{1}}
                \item Combien $23$~a-t-il d'antécédents ?
                    \IfStudentT{\bigfiller{3}}
                \item Même question pour~$-37$.
                    \IfStudentT{\bigfiller{4}}
            \end{enumerate}
    \end{enumerate}
\end{example}

\subsection{Courbe représentative}

\begin{activity}
    \begin{enumerate}
        \item Construire la courbe de~$f:\mdR\to\mdR,x\mapsto x^2$ sur la figure
            ci-contre.
        \item Comment sait-on qu'un point est sur la courbe ?
    \end{enumerate}
\end{activity}

\begin{definition}
    La \define{courbe représentative} de~$f$ est l'ensemble~$\mcC_f$ des
    points~$M(x;y)$ avec $x\in\mcD$~et~$y = f(x)$. Autrement dit, $M \in \mcC_f
    \iff x_M \in \mcD \text{ et } f(x_M) = y_M$.
\end{definition}

\begin{example}
    On définit~$f$ sur~$\IN[-5;5;]$ par $f(x) = 2x+3$. Parmi les points
    suivants, lesquels appartiennent à la courbe~$\mcC_f$ représentant~$f$ ?
    \begin{enumerate}[gathered, label=$\Alph*$]
        \item $(5;13)$;
        \item $(4;13)$;
        \item $(-3;-3)$;
        \item $(-5;-8)$;
        \item $(-10;-17)$;
        \item $(8;19)$.
    \end{enumerate}
\end{example}

\end{document}
