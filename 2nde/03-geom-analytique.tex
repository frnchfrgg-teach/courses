% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}
\usepackage{tikz}
\usetikzlibrary{datavisualization}

\newsavebox\mybox

\begin{document}
%\StudentMode

\iffalse
    \begin{lrbox}\mybox
        \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={grid, unit length=.5cm,
                                ticks={tick typesetter/.code=}},
                            x axis={include value/.list={-6,6}},
                            y axis={unit vector={(.5pt,1pt)},
                                    include value/.list={-3,5}},
                            visualize as scatter=basis,
                            basis={style={mark=none}},
                        ]
        data [set=basis] {
            x, y, name
            0, 0, O
            1, 0, I
            0, 1, J
        }
        ;
    \end{tikzpicture}
    \end{lrbox}
    \null
    \vskip -1.5cm
    \noindent\usebox\mybox\par
    \vskip 0.25cm
    \noindent\usebox\mybox\par
\fi

\chapter{Géométrie plane et analytique}

\section{Repérage dans le plan}

\begin{definition}
    Définir un \define{repère du plan} c'est donner trois points distincts et
    non alignés, dans un ordre précis.

    En d'autres termes, un repère est la donnée de
    \begin{itemize}
        \item une origine~$O$;
        \item deux axes distincts $(OI)$~et~$(OJ)$;
        \item un sens (orienté de $O$~à~$I$ ou de $O$~à~$J$) et une échelle sur
            chaque axe (donnée par $OI$ ou $OJ$).
    \end{itemize}
\end{definition}

\emph{A priori} il n'y a pas d'obligation pour que l'axe~$(OI)$ soit horizontal,
ni l'axe~$(OJ)$ vertical.

\begin{definition}
    Soit~$M$ un point du plan. La droite passant par~$M$ et parallèle à~$(OJ)$
    coupe $(OI)$~en~$P$. La droite passant par~$M$ et parallèle à~$(OI)$ coupe
    $(OJ)$~en~$Q$ --- voir \vref{fig:def-coords}.
    \begin{itemize}
        \item L'\define{abscisse} de~$M$ est l'abscisse de~$P$ sur
            l'axe~$(O,I)$, c'est-à-dire le nombre~$\frac{OP}{OI}$ avec un signe
            «+» si $I$~et~$P$ sont du même côté de~$O$ et un «-» sinon.
        \item L'\define{ordonnée} de~$M$ est le nombre~$\frac{OQ}{OJ}$ avec un
            signe «+» si $J$~et~$Q$ sont du même côté de~$O$ et un «-» sinon.
    \end{itemize}
\end{definition}

\begin{figure}
    \centering
    \caption{Définition des coordonnées d'un point dans le plan par projection
        parallèlement aux axes.}
    \label{fig:def-coords}
\begin{tikzpicture}
    \datavisualization [school book axes,
                        all axes={unit length=.5cm,
                            ticks={tick typesetter/.code=}},
                        x axis={include value/.list={-4,5},
                                ticks={major also at=1 as $I$}},
                        y axis={unit vector={(.5pt,1pt)},
                                include value/.list={-2,4},
                                ticks={major also at=0 as $O\;$,
                                       major also at=1 as $J$}},
                        visualize as scatter/.list={point,proj},
                        proj={style={mark=none}},
                    ]
    data point [set=point, x=-3, y=3, name=M]
    data point [set=proj, x=-3, y=0, name=P]
    data point [set=proj, x=0, y=3, name=Q]
    info {
        \draw[densely dotted]
                (P) node[below] {$P$}
             -- (M) node[left] {$M$}
             -- (Q) node[right] {$Q$};
    }
    ;
\end{tikzpicture}
\end{figure}

\section{Géométrie et coordonnées}

\subsection{Milieu d'un segment}

\begin{activity}
\begin{enumerate}
\item Lire les coordonnées de tous les points dans le repère~$(O,I,J)$
    ci-dessous:

    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={unit length=.5cm, grid,
                                    ticks={major at=0 as $O$}},
                            x axis={include value/.list={-6,6},
                                    ticks={major also at=1 as $I$}},
                            y axis={unit vector={(-.5pt,1pt)},
                                    include value/.list={-2,4},
                                    ticks={major also at=1 as $J$}},
                            visualize as scatter=points,
                        ]
        data [set=points] {
            x,  y, name
            -3, 3,  A
            5, -1,  B
    %        5, 3,   C
            -5, -2, D
    %        -1, -2, E
        }
        ;
        \tikzset{every node/.style={
                    anchor=base west,
                    inner sep=.5ex, yshift=.5ex,
        }}
        \path foreach \x in {A, B, D} { (\x) node {$\x$} };
    \end{tikzpicture}

\item Placer les points $C(5;3)$~et~$E(-1;-2)$.
\item Construire les $P$,~$Q$, et~$R$ les milieux respectifs de $[BC]$,~$[DE]$
    et~$[AB]$. Lire leurs coordonnées.
\item Que remarque-t-on ? Vérifier la conjecture avec les trois milieux.
\end{enumerate}
\end{activity}


\begin{theorem}
    \label{thm:geom-coord-milieu}
    Soient $P$~et~$Q$ deux points du plan muni d'un repère~$(O, I, J)$. On
    note~$M$ le milieu du segment~$[PQ]$. Alors
    \[ x_M = \frac{x_P + x_Q}{2} \quad\text{et}\quad
       y_M = \frac{y_P + y_Q}{2} \]
\end{theorem}

\begin{example}
    Soit~$(O,I,J)$ un repère. On note $A(2;3)$,~$B(5;7)$, $C(1;4)$
    et~$D(-2;0)$.
    \begin{enumerate}
        \item Déterminer les coordonnées de~$M$ le milieu de~$[AC]$.
        \item Montrer que~$M$ est aussi le milieu de~$[BD]$ (on pourra
            appeler $N$~le milieu de~$[BD]$).
        \item Qu'en déduit-t-on ?
    \end{enumerate}
\end{example}


\begin{exercise}[Calculs de milieux dans un repère oblique]
    (54 p.~254)
\end{exercise}

\begin{exercise}[Parallélogramme ou pas ?]
    (28 p.~251)
\end{exercise}

\begin{example}
    \label{exo:parallelogramme}
    Dans un repère~$(O;I;J)$ quelconque, on fixe $A(2;3)$,~$B(5;7)$, $C(1;4)$
    et~$D(-2;0)$.
    \begin{enumerate}
        \item Quelle est la nature de~$ABCD$ ? Montrer que c'est un
            parallélogramme.
        \item Déterminer le point~$E$ de sorte que~$ABEC$ soit un
            parallélogramme.
    \end{enumerate}
\end{example}

\begin{exercise}[Dernier point d'un parallélogramme]
    32 p.~252
\end{exercise}

\subsection{Calculs de longueurs}

\begin{activity}
    \begin{enumerate}
        \item Pythagore dans triangle $3$, $4$, $5$, angle droit bas gauche.
        \item Soient $A(2;2)$~et~$B(6,-1)$ dans un repère orthonormé. Calculer
            $AB$. Indice: rajouter $C(2;-1)$.
        \item A-t-on besoin d'orthonormé ?
    \end{enumerate}
\end{activity}

\begin{definition}
    repère orthogonal, repère orthonormé.
\end{definition}

\begin{theorem}
    \label{thm:geom-longueur-segment}
    Soient $A$~et~$B$ deux points dans le plan muni d'un repère orthonormé.
    Alors:
    \[ AB = \sqrt{\left( x_B - x_A \right)^2 + \left( y_B - y_A \right)^2} \]
\end{theorem}

\begin{proof}
    On pose $C(x_B;y_A)$. Suite à écrire\ldots
\end{proof}

\begin{example}
    Dans un repère orthonormé~$(O;I;J)$ on fixe $A(5;3)$~et~$B(-1;11)$.
    Calculer~$AB$.
\end{example}

\begin{example}
    Reprendre \vref{exo:parallelogramme} mais cette fois dans un
    repère orthonormé. Quelle est la nature de~$ABCD$ ? Le démontrer.
\end{example}

\begin{exercise}[Calculs de longueurs]
    34 p.~252
\end{exercise}

\begin{exercise}[Point sur cercle ?]
    38 p.~252
\end{exercise}

\begin{exercise}[Repère adapté]
    45 p.~253
\end{exercise}

\begin{exercise}[Quadrilatère de Varignon]
    Soit~$ABCD$ un parallélogramme. On note $I$,~$J$, $K$ et~$L$ les milieux
    respectifs de $[AB]$,~$[BC]$, $[CD]$, et~$[DA]$.
    \begin{enumerate}
        \item Quelle est la nature de~$IJKL$ ?
        \item Démontrer votre affirmation (indice: on utilisera un repère adapté
            à la figure).
        \item Pourquoi ne peut-on pas utiliser \vref{thm:geom-longueur-segment}
            pour obtenir $AB = CD$~et~$AD = BC$ ?
    \end{enumerate}
\end{exercise}

\end{document}
