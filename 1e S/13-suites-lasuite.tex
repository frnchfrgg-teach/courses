% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\def\genericform#1{<\textsl{#1}>}

\begin{document}

\StudentMode

\chapter{Suites, la suite}

\section{Limite d'une suite}

\begin{activity}
    \begin{enumerate}
        \item On considère la suite définie par $u_0 = 0$ et $u_{n+1} =
            -u_n^2+2u_n+1$. On pose $f(x) = -x^2+2x+1$. A-t-on $u_n = f(n)$ ?
            \IfStudentT{\bigfiller{2}}
        \item Tracer le plus soigneusement possible la courbe représentative
            de~$f$ dans un repère. Peut-on lui superposer la représentation
            de~$u$ ?
            \StudentOnly
            \par
            \begin{tikzpicture}
                \datavisualization [school book axes,
                                    all axes={ticks and grid={step=1,
                                                minor steps between steps=1
                                        },
                                        unit length=1.5cm},
                                    visualize as smooth line=f,
                                    f={style={ghost}},
                                ]
                data [set=f, format=function] {
                    var x : interval[-0.6:2.6];
                    func y = -pow(\value x,2) + 2 * \value x + 1;
                }
                ;
            \end{tikzpicture}%

            \bigfiller{1}
            \EndStudentOnly
        \item \begin{enumerate}
                \item Placer~$u_0$ sur l'axe des abscisses. Comment lire
                    graphiquement~$f(u_0)$ ?
                    \IfStudentT{\bigfiller{1}}
                \item Proposer une construction pour placer~$u_1$ sur l'axe des
                    abscisses. On pourra tracer la droite~$y=x$.
                    \IfStudentT{\bigfiller{2}}
                \item Reproduire la construction pour~$u_2$, puis~$u_3$.
            \end{enumerate}
        \item On considère maintenant $v_0=0$ et $v_{n+1} = -\frac{3}{4}v_n^2 +
            \frac{9}{4}v_n + \frac{1}{2}$.\\
            On a représenté la fonction
            $g:x\mapsto -\frac{3}{4}x^2 + \frac{9}{4}x + \frac{1}{2}$.
            \begin{enumerate}
                \item Construire graphiquement les $10$~premiers termes.
                    Vérifier à la calculatrice.
                    \StudentOnly
                    \par
                    \begin{tikzpicture}
                        \datavisualization [school book axes,
                                    all axes={ticks and grid={step=1,
                                                minor steps between steps=1
                                        },
                                        unit length=1cm},
                                            visualize as smooth line=f,
                                            %u={style={ghost}},
                                        ]
                        data [set=f, format=function] {
                            var x : interval[-0.7:3.7];
                            func y = -3/4*pow(\value x,2)
                                        + 9/4 * \value x + 1/2;
                        }
                        ;
                    \end{tikzpicture}%
                    \EndStudentOnly
                \item Que semblent faire les termes de~$v$ ? Et si $v_0$~était
                    un autre nombre entre $0$~et~$3$ ?
                    \IfStudentT{\bigfiller{3}}
                \item Recommencer avec $v_0 \in \IN[-1;-\frac{1}{2};]$. Que
                    remarque-t-on ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
    \end{enumerate}
\end{activity}

\section{Sens de variation}

\subsection{Première approche}

\begin{recall}
    Si $f$~est une fonction, $f$~est strictement croissante (ou
    décroissante) si et seulement si \UGHOST{$\forall a < b, f(a) < f(b)$}
    (respectivement \UGHOST{$\forall a < b, f(a) > f(b)$}).
\end{recall}

\begin{definition}
    Une suite~$u$ est strictement croissante (ou strictement décroissante,
    croissante, décroissante) si et seulement si
    \UGHOST{$\forall i<j, u_i < u_j$}
    (respectivement \UGHOST{$u_i > u_j$, $u_i \le u_j$, $u_i \ge u_j$}).
\end{definition}

\begin{example}
    Soit $u$~définie par $\forall n \ge 1, u_n = 5n + 2$.
    \IfStudentT{\bigfiller{3}}
\end{example}

\begin{exercise}
    24 p.~151
\end{exercise}

\begin{proposition}
    Soit $u$~une suite définie sur~$\mdN$ par $\forall n \in \mdN, u_n = f(n)$
    où $f$~est une fonction définie sur~$\IN[0;+\infty;[$. Si $f$~est
    strictement croissante (ou strictement décroissante, \ldots), alors
    \UGHOST{$u$ l'est aussi.}
\end{proposition}

\begin{proof}
    \UGHOST{Soient $0\le i<j$. Puisque $f$~est strictement décroissante
    sur~$\IN[0;+\infty;[$, $f(i) > f(j)$. C'est dire $u_i > u_j$ et ce pour tous
    $i<j$: $u$~est strictement décroissante.}
\end{proof}

\begin{example}
    \begin{enumerate}
        \item $\forall n \in \mdN, u_n = n^2$.
            \IfStudentT{\bigfiller{1}}
        \item $\forall n \in \mdN, v_n = \sqrt{n}$.
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{remark}
    Attention: la réciproque est fausse ! La proposition «Si $u$~est croissante,
    $f$~aussi» n'est pas toujours vérifiée.

    Par exemple, prenons $u_n = n
    \sin(2\pi \times n)$. Alors $f:x\mapsto x \sin(2\pi \times x)$ n'est pas
    croissante --- voir sa représentation graphique --- mais $u$ l'est ! En
    fait, on peut même montrer que $u_n = n$ pour tout~$n$\ldots
\end{remark}

\begin{exercise}
    28 p.~151
\end{exercise}

\subsection{Par récurrence}

\begin{activity}
    $u_{n+1} = u_n + n^2$
\end{activity}

\begin{theorem}
    \label{thm:increasing-seq}
    $u$~est strictement croissante si et seulement si $\forall n\in\mdN, u_n <
    u_{n+1}$. De même pour les autres.
\end{theorem}

\begin{example}
    Suite précédente
\end{example}

\begin{proof}
    Pas cette année\ldots Un petit teaser quand même: blblbl
\end{proof}

\begin{exercise}
    26 p.~151

    Remarque: en ignorant l'énoncé, il était plus rapide de comparer
    $u_i$~et~$u_j$ directement ici\ldots C'est plutôt rare.
\end{exercise}

\begin{exercise}
    23 p.~151
\end{exercise}

\end{document}

