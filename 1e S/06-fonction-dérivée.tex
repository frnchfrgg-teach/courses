% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}

\def\Tx#1{\Delta_{#1,h}}

\begin{document}

\StudentMode

\chapter{Fonction dérivée}

\begin{activity}[Avec \bsc{Geogebra}]
    On considère la fonction $f:\mdR\to\mdR,x\mapsto x^2+1$.
    \begin{enumerate}
        \item \begin{enumerate}
                \item Tracer la fonction~$f$ dans \bsc{Geogebra}.
                \item Placer un point~$A$ sur~$\mcC_f$ --- dans un premier temps
                    le positionner à l'abscisse~$2$.
            \end{enumerate}
        \item \begin{enumerate}
                \item Construire la tangente~$\mcT$ à~$\mcC_f$ en~$A$.
                \item À l'aide du graphique, déterminer une équation réduite
                    de~$\mcT$.
                    \IfStudentT{\bigfiller{1}}
                \item Comparer avec le résultat trouvé au chapitre sur la
                    dérivation.%à \vref{ex:tangent-eq}.
                    \IfStudentT{\bigfiller{1}}
            \end{enumerate}
        \item Ajouter un objet «pente» à la tangente~$\mcT$ --- on
            l'appellera~$m$. Retrouve-t-on bien le nombre dérivé de~$f$ en~$2$ ?
            \IfStudentT{\bigfiller{1}}
        \item \begin{enumerate}
                \item Créer un point~$B$ de même abscisse que~$A$ et
                    d'ordonnée~$m$.
                \item Déplacer~$A$. Que se passe-t-il ? Confirmer en activant la
                    trace de~$B$.
                    \IfStudentT{\bigfiller{2}}
                \item De quelle fonction le point~$B$ semble-t-il décrire la
                    courbe ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
        \item Refaire les questions avec $g:\mdR\to\mdR,x\mapsto x^3$ en lieu et
            place de~$f$ --- hormis les calculs de nombre dérivé et d'équation
            de tangente.
            \IfStudentT{\bigfiller{4}}
    \end{enumerate}
\end{activity}

\section{Dériver sur un intervalle}

\begin{definition}
    Soit~$f$ une fonction définie sur un intervalle~$I$. On dit que $f$~est
    \define{dérivable sur~$I$} si $f$~est dérivable \UGHOST{en tout~$a\in I$.}
    Dans ce cas, la fonction définie sur~$I$ qui à chaque~$a\in I$ associe
    \UGHOST{le nombre dérivé de~$f$ en~$a$ est appelée \define{fonction dérivée
    de~$f$} et est notée~$f'$. En d'autres termes $f' : a \mapsto f'(a)$.}
\end{definition}

\begin{remark}
    $f'$~est une fonction définie de manière compliquée, presque algorithmique:
    pour calculer l'image de~$1$ par~$f'$ on calcule le taux
    d'accroissement~$\Tx{1}$ puis on cherche la limite quand~$h\to0$. Et on
    répète la procédure pour déterminer chaque image !
\end{remark}

\begin{example}
    Soient $f$~définie sur~$\mdR$ par $f(x) = 42$ et $a\in\mdR$.
    \begin{enumerate}
        \item Calculer $\Tx{a}$. Que remarque-t-on ?
            \IfStudentT{\bigfiller{1}}
        \item $f$~est-elle dérivable en~$a$ ? Si oui, déterminer~$f'(a)$.
            \IfStudentT{\bigfiller{2}}
        \item Quelle formule pourrait-on proposer pour~$f'$ --- sans passer par
            le taux d'accroissement de~$f$ ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\begin{example}
    Soit $g$~une fonction affine définie sur $\mdR$: $g(x) = mx + p$. Montrer
    que $g$~est dérivable sur~$\mdR$ et que $g'(a) = m$ pour tout~$a \in \mdR$.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{theorem}[Dérivée de la fonction carré]
    \label{thm:diff-x2}
    Soit $f:\mdR\to\mdR, x \mapsto x^2$. La fonction~$f$ est dérivable
    sur~$\mdR$ et pour tout~$a\in\mdR$, $f'(a) = 2a$.
\end{theorem}

\begin{proof}
    \UGHOST{%
    Soit $a\in\mdR$. $f(a) = a^2$ et $f(a+h) = (a+h)^2 = a^2 + 2ah + h^2$. Ainsi
    $f(a+h) - f(a) = 2ah + h^2 = h(2a + h)$. Mais alors $\Tx{a} =
    \frac{f(a+h) - f(a)}{h} = 2a + h$. En définitive, $\lim_{h\to 0} \Tx{a} =
    2a$: la fonction~$f$ est dérivable en~$a$ et~$f'(a) = 2a$. C'est vrai pour
    tout~$a\in\mdR$ et on a notre résultat.%
    }
\end{proof}

\section{Dérivées usuelles}

\begin{theorem}
    \label{thm:common-derivatives}
    \leavevmode
    \begin{center}
    $\begin{array}{L*{3}{cL}}
        \firsthline
        \text{Fonction} & \text{Dérivable sur} & \text{Fonction dérivée} \\
        \hline
        f(x) = k \text{ avec $k \in \mdR$} & \mdR & f'(x) = 0 \NL
        f(x) = x & \mdR & f'(x) = 1 \NL
        f(x) = mx + p \text{ avec $m,p \in \mdR$} & \GHOST{\mdR} &
                f'(x) = \GHOST{m} \NL
        f(x) = x^2 & \GHOST{\mdR} & f'(x) = \GHOST{2x} \NL
        f(x) = x^n \text{ avec $n \ge 0$ } & \GHOST{\mdR} &
                f'(x) = \GHOST{nx^{n-1}} \NL
        f(x) = \dfrac{1}{x} & \GHOST{\begin{gathered}
                    \IN]-\infty;0;[ \text{ ou } \\
                    \IN]0;+\infty;[ \end{gathered}} &
                f'(x) = \GHOST{-\dfrac{1}{x^2}} \NL
        f(x) = \sqrt{x} & \GHOST{\IN]0;+\infty;[} &
                f'(x) = \GHOST{\dfrac{1}{2\sqrt{x}}} \\
        \lasthline
    \end{array}$
    \end{center}
\end{theorem}

\begin{proof}
    On a déjà démontré les quatre premières lignes; les autres seront admises ou
    vues en exercice.
\end{proof}

\begin{example}
    La fonction $f:x\mapsto x^5$ est dérivable sur~$\mdR$ et pour
    tout~$x\in\mdR$, $f'(x) = 5 x^{5-1} = 5x^4$.
\end{example}

\begin{exercise}[Dérivée et tangente de la fonction inverse]
    52 p.~79
\end{exercise}

\section{Théorèmes opératoires}

\begin{theorem}
    Si $u$ est une fonction dérivable sur~$I$ et $k \in \mdR$ est une constante,
    alors $f = k u$~est dérivable sur~$I$ et $f' = k u'$.
\end{theorem}

\begin{remark}
    $f = k u$ veut dire $f(x) = k \times u(x)$ pour tout~$x \in I$. De même
    $f' = k u'$ signifie $\forall x\in I, f'(x) = k u'(x)$.
\end{remark}

\begin{proof}
    Soit $a \in I$. On a $\frac{f(a+h) - f(a)}{h} = \frac{ku(a+h) - ku(a)}{h} =
    k \frac{u(a+h)-u(a)}{h}$. Or $\lim_{h\to0} \frac{u(a+h) - u(a)}{h} = u'(a)$
    donc $\lim_{h\to0} \frac{f(a+h) - f(a)}{h} = k u'(a)$. En définitive
    $f$~est dérivable en~$a$ et $f'(a) = ku'(a)$.
\end{proof}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f = u + v$~est
    dérivable sur~$I$ et $f' = u' + v'$.
\end{theorem}

\begin{remark}
    $f = u + v$ veut dire $f(x) = u(x) + v(x)$ pour tout~$x \in I$.
\end{remark}

\begin{proof}
    \GHOST{%
    Soit $a \in I$.
    \begin{align*}
        \frac{f(a+h) - f(a)}{h} &= \frac{\left( u(a+h) + v(a+h) \right) -
                                         \left( u(a) + v(a) \right)}{h} \\
                                &= \frac{u(a+h) - u(a) + v(a+h) - v(a)}{h} \\
                                &= \frac{u(a+h) - u(a)}{h} +
                                   \frac{v(a+h) - v(a)}{h}
    \end{align*}
    $\frac{u(a+h) - u(a)}{h}$~tend vers~$u'(a)$ tandis que $\frac{v(a+h) -
    v(a)}{h}$~tend vers~$v'(a)$. La somme tend ainsi vers $u'(a) + v'(a)$: la
    fonction~$f$ est dérivable en~$a$ et $f'(a) = u'(a) + v'(a)$.
    }
\end{proof}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = x^3 + \frac{4}{x} +
    2\sqrt{x}$. Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa
    fonction dérivée.

    \UGHOST{%
    Rédaction type: $f$ est dérivable car $x\mapsto x^3$, $x\mapsto\frac{1}{x}$
    et $x\mapsto\sqrt{x}$ le sont et $f'(x) = 2x^3 + 4 \times \frac{-1}{x^2} + 2
    \times \frac{1}{2\sqrt{x}} = 2x^3 - \frac{4}{x^2} + \frac{1}{\sqrt{x}}$.%
    }
\end{example}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, alors $f=uv$~est
    dérivable sur~$I$ et $f' = u'v + uv'$.
\end{theorem}

\begin{example}
    Soit $f$~définie sur~$\IN[0;+\infty;[$ par $f(x) = (3x + 2)(\sqrt{x} + 1)$.
    Indiquer sur quel intervalle $f$~est dérivable, et déterminer sa fonction
    dérivée.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{exercise}
    65 p.~80
\end{exercise}

\begin{theorem}
    Si $u$~et~$v$ sont deux fonctions dérivables sur~$I$, et \emph{si $v(x) \neq
    0$ pour tout~$x\in I$}, alors $f=\dfrac{u}{v}$~est dérivable sur~$I$ et $f'
    = \dfrac{u'v - uv'}{v^2}$.
\end{theorem}

\IfStudentT{\bigfiller{7}}

\end{document}
