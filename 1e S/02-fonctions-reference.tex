% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}


\rivmathlib{geometry}

\begin{document}

\StudentMode


\chapter{Fonctions de référence}

\chapterquote{Une citation sans références est à peu près aussi utile qu'une
horloge sans aiguilles.}{Paul \bsc{Desalmand}}

\section{Les basiques}

\subsection{Fonctions affines}

\begin{definition}
    On appelle \define{fonction affine} toute fonction pouvant s'écrire sous la
    forme $f(x) = mx+p$ avec $m$~et~$p$ deux réels.
\end{definition}

\begin{example}
    Les fonctions suivantes sont-elles affines ?
    \begin{enumerate}[gathered,label={$\falph*(x)={}$},labelsep=0pt]
        \item $2x+3$;
        \item $-3-3x$;
        \item $x^2 + 1$;
        \item $-\sqrt{2}x-1$;
        \item $-\sqrt{2x}-1$;
        \item $\dfrac{1-x}{2}$.
    \end{enumerate}
    \IfStudentT{\bigfiller{2}}
\end{example}

\begin{example}[Recherche de formule]
    Soit $f$~une fonction affine telle que $f(8) = 11$~et $f(10) = 10$.
    Déterminer l'expression de~$f$.
    \IfStudentT{\bigfiller{5}}
\end{example}

\begin{proposition}
    Soit $f:x\mapsto mx+p$. Alors:
    \begin{sidebyside}{2}
    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1,$f(x)$/2}
                        {{$\;-\infty$},$\frac{-p}{m}$,$+\infty\;$}
            \tkzTabLine{,-,z,+,}
            \tkzTabVar {-/,R,+/}
        \end{scope}
        \node[above] at (current bounding box.north) {si $m>0$};
    \end{tikzpicture}

    \begin{tikzpicture}
        \begin{scope}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1,$f(x)$/2}
                        {{$\;-\infty$},$\frac{-p}{m}$,$+\infty\;$}
            \tkzTabLine{,+,z,-,}
            \tkzTabVar {+/,R,-/}
        \end{scope}
        \node[above] at (current bounding box.north) {si $m<0$};
    \end{tikzpicture}
    \end{sidebyside}
\end{proposition}

\begin{proof}
    \begin{itemize}
        \item Pour le signe: $mx+p > 0 \iff mx > -p \iff x > \frac{-p}{m}$
            si~$m>0$ tandis que $mx+p > 0 \iff mx > -p \iff x < \frac{-p}{m}$
            si~$m<0$ (changement de sens).
        \item Pour les variations, c'est similaire: on prend $a < b$ deux réels.
            Si $m>0$, on a $ma < mb$ donc $ma+p < mb+p$ pour~tous~$a<b$ et ainsi
            $f$~est strictement croissante. Si $m<0$, on a $ma > mb$ donc $ma+p
            > mb+p$ pour~tous~$a<b$ et ainsi $f$~est strictement décroissante.
            \qedhere
    \end{itemize}
\end{proof}

\subsection{Fonction inverse}


\begin{figure}
    \caption{Représentation graphique de la fonction inverse.}
    \label{fig:inv-repr}
    \centering
    \begin{tikzpicture}
        \def\xmax{8}\def\ymax{6.1}
        \datavisualization [school book axes,
                            all axes={grid={step=1},ticks={major at=1},
                                      unit length=0.45cm},
                            visualize as smooth line=inv,
                        ]
        data [set=inv, format=function] {
            var x : interval[-\xmax:-1];
            func y = 1 / (\value x);
        }
        data [set=inv, format=function] {
            var x : interval[-1:(-1/\ymax)];
            func y = 1 / (\value x);
        }
        data point[set=inv, outlier=true]
        data [set=inv, format=function] {
            var x : interval[(1/\ymax):1];
            func y = 1 / (\value x);
        }
        data [set=inv, format=function] {
            var x : interval[1:\xmax];
            func y = 1 / (\value x);
        }
        ;
    \end{tikzpicture}%
\end{figure}


\begin{proposition}
    Soit $f:x\mapsto \dfrac{1}{x}$. On a le tableau suivant (voir
    aussi \vref{fig:inv-repr}):
    \begin{center}
        \begin{tikzpicture}[TAB]
            \tkzTabInit [lgt=1.5,espcl=2.5]
                        {$x$/1,$f(x)$/1,$f(x)$/2}
                        {$\;-\infty$,$0$,$+\infty\;$}
            \tkzTabLine{,-,d,+,}
            \tkzTabVar {+/,-D+//,-/}
        \end{tikzpicture}
    \end{center}
\end{proposition}

\begin{proof}
    \UGHOST{
    Soient $a<b<0$ deux réels. On a $\displaystyle \frac{1}{a} - \frac{1}{b} =
    \frac{b - a}{ab}$. Puisque $b>a$, $b-a>0$. De plus~$ab>0$. Ainsi
    $\displaystyle \frac{1}{a} - \frac{1}{b} > 0$ c'est-à-dire $\displaystyle
    \frac{1}{a} > \frac{1}{b}$. En définitive $f$~change l'ordre et est donc
    strictement décroissante sur~$\mdR_-^*$. On procède de manière identique
    pour montrer que $f$~est strictement décroissante sur~$\mdR_+^*$.
    }
\end{proof}

\begin{remark}
    Howard dit « Si~$a < b$ alors~$\dfrac{1}{a} > \dfrac{1}{b}$ ».
    Sheldon répond « C'est faux ! ».
    Qui a raison ? Quelle partie du raisonnement tombe à l'eau ?
    \IfStudentT{\bigfiller{1}}

    Conclusion, danger: on ne peut pas dire que la fonction inverse est
    décroissante sur $\IN]-\infty;0;[ \cup \IN]0;+\infty;[$, mais seulement sur
    $\IN]-\infty;0;[$~\emph{et}~$\IN]0;+\infty;[$ \emph{séparément}.
\end{remark}


\section{Fonction racine carrée}

\subsection{Définition}

\TeacherOnly
\begin{activity}
    \begin{enumerate}
        \item Soit $f:\mdR\to\mdR, x\mapsto 5x^2 - 2x + 7$. Parmi les points
            suivants, lesquels sont sur la courbe~$\mcC$ représentant~$f$ ?
            \begin{enumerate}[gathered, label=$\Alph*$,ref=\Alph*,labelsep=0pt]
                \item $(0;7)$;\label{item:activ:is-on-curve:same-xy-source}
                \item $(3;46)$;
                \item $(2;45)$;
                \item $(-10;7)$;\label{item:activ:is-on-curve:same-y}
                \item $(0;4)$;\label{item:activ:is-on-curve:same-x}
                \item $\left( \frac{2}{5};7 \right)$.
            \end{enumerate}
        \item Peut-on dire: « $\ref{item:activ:is-on-curve:same-x} \notin
            \mcC$~car $\ref{item:activ:is-on-curve:same-xy-source} \in \mcC$~a
            déjà cette abscisse » ?
        \item Peut-on dire: « $\ref{item:activ:is-on-curve:same-y} \notin
            \mcC$~car $\ref{item:activ:is-on-curve:same-xy-source} \in \mcC$~a
            déjà cette ordonnée » ?
        \item Proposer une fonction~$g$ simple telle que~$g(0) = g(-10) = 7$.
        \item Construire une fonction~$h$ du second degré telle que~$h(0) =
            h(-10) = 7$. Indice: commencer par $j$~telle que~$j(0) = j(-10) =
            0$, en se demandant quelles sont les racines de~$j$.
        \item Proposer deux autres fonctions du second degré qui valent~$7$ en
            $0$~et~$-10$.
    \end{enumerate}
\end{activity}


\begin{activity}[Découverte fonction réciproque]
    \label{activ:reciprocate}
    Soit~$f$ définie par~$f(x) = \dfrac{1}{10}x^2 + \dfrac{1}{2}x$ sur~$\left[
    0; +\infty \right[$. On note~$\mcC_f$ sa courbe représentative.
    \begin{enumerate}
        \item Déterminer le sens de variation de~$f$, puis son signe.
        \item Vérifier que $\mcC_f$ passe par $O(0;0)$,~$A(5;5)$
            et~$B(2;\frac{7}{5})$.
        \item Expliquer pourquoi quand~$y\ge0$ alors $y$~a un seul
            antécédent par~$f$.
    \end{enumerate}
    On cherche à décrire la fonction~$g$ définie sur~$\mdR_+$ qui associe à
    chaque nombre son antécédent par~$f$. Soit~$\mcC_g$ sa courbe
    représentative.
    \begin{enumerate}[resume]
        \item Si $f(x) = y$, que peut-on dire de~$g(y)$ ?
        \item Déterminer $g(0)$~puis~$g(5)$. En déduire deux points appartenant
            à la courbe~$\mcC_g$. Construire un troisième point appartenant
            à~$\mcC_g$.
        \item Soit $M(x;y) \in \mcC_f$. Déterminer les coordonnées d'un
            point~$M' \in \mcC_g$ dépendant de~$M$.
        \item Construire ces points dans \bsc{GeoGebra}, activer le mode trace
            sur~$M'$ et déplacer~$M$ sur~$\mcC_f$. Que remarque-t-on ?
    \end{enumerate}
\end{activity}
\EndTeacherOnly

\begin{definition}
    Soit~$f:\mdR_+\to\mdR_+, x \mapsto x^2$. Si~$y\ge0$ alors $y$~a un unique
    antécédent par~$f$. On définit ainsi une fonction~$g$ telle que l'image
    de~$y\ge0$ est son unique antécédent par~$f$, noté~$\sqrt{y}$. La
    fonction~$g$ est appelée \define{fonction racine carrée}.

    Autrement dit, $\sqrt{y}$ est l'unique antécédent positif de~$y$ par la
    fonction carrée, c'est-à-dire l'unique réel~$x\ge0$ tel que~$x^2 = y$.
\end{definition}

Comme on l'a vu dans \vref{activ:reciprocate}, la courbe représentant
la racine carrée est constituée des points~$M'(y;x)$ quand $M(x;y)$~décrit la
courbe de la fonction carré. Or de tels $M$~et~$M'$ sont symétriques par rapport
à la diagonale~$\mcD$ d'équation~$y=x$, puisque c'est la médiatrice de~$[MM']$.
En effet, il est évident que $M$~et~$M'$ sont à égale distance de
l'origine~$O\in\mcD$; de plus le milieu de~$[MM']$ a son abscisse et son
ordonnée toutes deux égales à $\frac{x+y}{2}$ et est ainsi sur~$\mcD$.

\begin{figure}
    \caption{Construction de la courbe de la racine carrée par symétrie.}
    \label{fig:sqrt-repr}
    \centering
    \begin{tikzpicture}
        \def\PT#1#2{visualization cs: x=#1,y=#2}
        \datavisualization [school book axes,
                            all axes={grid,
                                      ticks={minor steps between steps=1},
                                      unit length=1cm},
                            visualize as smooth line/.list={sq,sqrt},
                            style sheet=vary dashing,
                        ]
        data [set=sq, format=function] {
            var x : interval[0:2.5];
            func y = pow(\value x, 2);
        }
        data [set=sqrt, format=function] {
            var y : interval[0:3.1];
            func x = pow(\value y, 2);
        }
        info {
            \draw[dash dot] (\PT{0}{0}) -- (\PT{6}{6});
            \path let \n1 = {2.2},
                      \n2 = {\n1 * \n1},
                      \n3 = {(\n1 + \n2)/2}
                in
                    (\PT{\n1}{\n2}) coordinate (M)
                    (\PT{\n2}{\n1}) coordinate (M')
                    (\PT{\n3}{\n3}) coordinate (mid);
            \draw (M) -- pic[dist marker] {} (mid) pic[perp marker]
                      -- pic[dist marker] {} (M');
            \path (M) node[left] {$M$} pic {point};
            \path (M') node[below] {$M'$} pic {point};
        }
        ;
    \end{tikzpicture}%
\end{figure}

En définitive, la courbe représentative de la fonction racine carrée est
symétrique dans son ensemble de la courbe de la fonction carré (voir
\vref{fig:sqrt-repr}).


\subsection{Propriétés}

\begin{proposition}
    Pour tout~$x\in\mdR_+$, $\sqrt{x} \ge 0$. Mieux: $\forall x > 0, \sqrt{x}
    > 0$.
\end{proposition}

\begin{proof}
    $\sqrt{x} \ge 0$ par définition, et si $\sqrt{x} = 0$
    alors $x = (\sqrt{x})^2 = 0$.
\end{proof}

\begin{activity}
    \begin{enumerate}
        \item Calculer $(\sqrt{5} + \sqrt{3})(\sqrt{5} - \sqrt{3})$.
        \item En déduire que
            $\displaystyle \frac{1}{\sqrt{5} + \sqrt{3}} =
            \frac{\sqrt{5} - \sqrt{3}}{2}$
    \end{enumerate}
\end{activity}

\begin{theorem}
    \label{thm:sqrt:increasing}
    La fonction racine carrée est strictement croissante sur~$\mdR_+$.
\end{theorem}

\begin{proof}
    Soient $0 \le a < b$ deux réels.

    On remarque tout d'abord que $u - v = \dfrac{u^2 - v^2}{u+v}$ car
    $u^2 - v^2 = (u - v)(u + v)$. En appliquant cette formule avec
    $u = \sqrt{a}$~et $v = \sqrt{b}$ on obtient
    $\sqrt{a} - \sqrt{b} = \dfrac{a - b}{\sqrt{a} + \sqrt{b}}$.

    Bien entendu $a - b < 0$ puisque~$a<b$. Or $\sqrt{a} + \sqrt{b} > 0$ donc
    $\dfrac{a - b}{\sqrt{a} + \sqrt{b}} < 0$, c'est-à-dire $\sqrt{a} - \sqrt{b}
    < 0$ ou encore $\sqrt{a} < \sqrt{b}$, et ce pour tous~$a<b$: la fonction
    racine est strictement croissante.
\end{proof}

\begin{exercise}[Fonction monotone ou pas ?]
    Exercice 101~p.~44
\end{exercise}

\begin{exercise}
    \begin{enumerate}
        \item Montrer que $f:x\mapsto \dfrac{1}{\sqrt{x}+3} - 3$ est strictement
            décroissante sur~$\IN[0;+\infty;[$.
        \item Soit $g:x\mapsto \dfrac{2}{x^2-1}$.
            \begin{enumerate}
                \item Quel est l'ensemble de définition de~$g$ ?
                \item Montrer que $g$~est strictement croissante
                    sur~$\IN]-\infty;-1;[$.
                \item Montrer que $g$~est strictement décroissante
                    sur~$\IN[0;1;[$.
            \end{enumerate}
        \item Montrer que $h:x\mapsto \dfrac{3}{\sqrt{x}-4} + 1$ est
            strictement décroissante sur~$\IN]0;16;[$.
    \end{enumerate}
\end{exercise}

\begin{exercise}[Variations]
    85~p.~38 questions 1~et~2; 86~p.~38 questions 1~et~2.
\end{exercise}

\subsection{Positions relatives de \texorpdfstring{$\sqrt{x}$}{√x},~$x$ et~$x^2$}

On cherche à ordonner les valeurs prises par $\sqrt{x}$,~$x$ et~$x^2$. Dans le
pire des cas on peut être amené à les comparer deux à deux: si l'on commence par
comparer le plus grand aux deux autres, il faudra encore comparer ceux-ci pour
les départager.

Cependant, on remarque sur \vref{fig:sqrt-repr} que la diagonale semble
toujours être entre les courbes du carré et de la racine carrée\ldots\ Ainsi on
va comparer d'une part $x$~et~$x^2$, et d'autre part $x$~et~$\sqrt{x}$.

\paragraph{$x$ \emph{v.s.} $x^2$}

Pour comparer $x$~et~$x^2$ on va déterminer le signe de $x^2 - x = x(x-1)$.
C'est le travail du tableau de signes suivant:
\begin{center}
\begin{tikzpicture}[TAB]
    \tkzTabInit [lgt=2,espcl=2.5]
                {$x$/1,$x$/1,$x-1$/1,$x^2-x$/1}
                {{$\;-\infty$},$0$,$1$,$+\infty\;$}
    \tkzTabLine{,-,z,+,t,+,}
    \tkzTabLine{,-,t,-,z,+,}
    \tkzTabLine{,+,z,-,z,+,}
\end{tikzpicture}
\end{center}

En définitive $x^2 > x$ sur $\IN]-\infty;0;[$~et~$\IN]1;+\infty;[$ tandis que
$x^2 < x$ sur~$\IN]0;1;[$.

\paragraph{$x$ \emph{v.s.} $\sqrt{x}$}

On utilise le résultat précédent: si $x \in \IN]0;1;[$ alors $x^2 < x$ et
puisque la fonction racine est strictement croissante $\sqrt{x^2} < \sqrt{x}$.
Or $x>0$ donc $\sqrt{x^2} = x$ est l'antécédent positif de $x^2$. Ainsi $x <
\sqrt{x}$.

Si $x \in \IN]1;+\infty;[$ alors $x^2 > x$ et comme la fonction racine est
strictement croissante $x = \sqrt{x^2} > \sqrt{x}$. En définitive $x > \sqrt{x}$
sur~$\IN]1;+\infty;[$ tandis que $x < \sqrt{x}$ sur~$\IN]0;1;[$.

\paragraph{Récapitulatif}

Sur~$\IN]0;1;[$, on a $x^2 < x < \sqrt{x}$; sur~$\IN]1;+\infty;[$ c'est le
contraire: $\sqrt{x} < x < x^2$. Enfin $\sqrt{0} = 0 = 0^2$ et $\sqrt{1} = 1 =
1^2$.

\begin{exercise}
    Comparer les nombres suivants:
    \begin{enumerate}[gathered]
        \item $5$~et~$\sqrt{5}$;
        \item $\<0.2>$~et~$\sqrt{\<0.2>}$;
        \item $\sqrt{5}$~et~$\sqrt{\<0.2>}$;
        \item $3$~et~$\sqrt{5}$;
        \item $\dfrac{1}{\sqrt{3}+1}$~et~$\dfrac{1}{\sqrt{6}+1}$;
        \item $\dfrac{1}{\sqrt{3}-2}$~et~$\dfrac{1}{\sqrt{6}-2}$
    \end{enumerate}
\end{exercise}

\let\SB\sectionbreak
\def\sectionbreak{\let\sectionbreak\SB\clearpage}

\section{Fonction valeur absolue}

\subsection{Définition}

\begin{activity}
    Soit~$f:x\mapsto \sqrt{(x+3)^2}$.
    \begin{enumerate}
        \item Quel est l'ensemble de définition de~$f$ ?
        \item Montrer que $f$~est strictement croissante sur~$\left[ -3;+\infty
            \right[$.
        \item Calculer $f(-8)$,~$f(-3)$, $f(0)$, $f(2)$ et~$f(8)$. A-t-on $f(x)
            = x+3$ ?
        \item Déterminer le sens de variation de~$f$ sur~$\left] -\infty; -3
            \right]$.
        \item Quels sont les antécédents de~$9$ par~$x\mapsto x^2$ ? De~$-9$ ?
            De~$3^2$ ? De~$-3^2$ ? De~$(x+3)^2$ ?
        \item Montrer que $f(x) = \begin{cases}
                                        x+3 & \text{si $x\ge-3$} \\
                                        -x-3 & \text{si $x\le-3$}
                                \end{cases}$
    \end{enumerate}
\end{activity}

\begin{definition}
    On appelle \define{fonction valeur absolue} la fonction définie sur~$\mdR$
    par $\abs{x} = \sqrt{x^2}$.
\end{definition}

\begin{remark}
    Dans un repère orthonormé, on prend $A(x;0)$. Comment calcule-t-on la
    distance~$OA$ ? Ainsi, $\abs{x}$~est la distance entre $x$~et~$0$ quand ils
    sont représentés sur un axe gradué. Autrement dit c'est la valeur de~$x$ en
    ne tenant pas compte de son signe.
\end{remark}

\begin{proposition}
    \label{prop:sqrt-expr}
    Soit~$x\in\mdR$. Alors $\abs{x} = \begin{cases}
                                         x & \text{si $x\ge 0$} \\
                                        -x & \text{si $x\le 0$}
                                \end{cases}$
\end{proposition}

\begin{example}
    Soit $f:x\mapsto\abs{x}$. Calculer $f(0)$,~$f(10)$, $f(5)$, $f(2)$,
    $f(-2)$, $f(-10)$ et~$f(-42)$.
\end{example}

\begin{figure}
    \caption{Représentation graphique de la valeur absolue.}
    \label{fig:abs-repr}
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={grid={step=1},
                                      ticks={major at/.list={0,1}},
                                      unit length=0.45cm},
                            visualize as line=abs,
                        ]
        data [set=abs, format=function] {
            var x : interval[-8:8] samples 3;
            func y = abs(\value x);
        }
        ;
    \end{tikzpicture}%
\end{figure}

\subsection{Propriétés}

\begin{proposition}
    Pour~tout~$x\in\mdR$, $\abs{x} \ge 0$. Mieux, $x \ne 0 \iff \abs{x} \ne
    0$.
\end{proposition}

\begin{proof}
    C'est évident en utilisant l'expression donnée à \vref{prop:sqrt-expr}.
\end{proof}

\begin{proposition}
    On a le tableau suivant:
    \begin{tikzpicture}[TAB]
        \tkzTabInit [lgt=1,espcl=2]
                    {$x$/1,$\abs{x}$/2}
                    {$\;-\infty$,$0$,$+\infty\;$}
        \tkzTabVar {+/,-/$0$,+/}
    \end{tikzpicture}
\end{proposition}

\begin{exercise}[Variations avec $\abs{\cdot}$]
    85~p.~38 questions 3~et~4; 86~p.~38 questions 3~et~4.
\end{exercise}


\section{Fonctions associées}

\iffalse
\begin{TP}[Avec \bsc{GeoGebra}]
    Soit~$f$ la fonction définie sur~$\mdR$ par $f(x) = \frac{1}{4}x^2 + x -
    8$.

    \begin{enumerate}
        \item \begin{enumerate}
            \item Construire la fonction~$f$ dans \bsc{GeoGebra}.
            \item Dresser le tableau de variations de~$f$.
        \end{enumerate}
        \item On définit la fonction~$g$ par $g(x) = f(x) + 3$.
            \begin{enumerate}
                \item Construire la courbe de~$g$ dans \bsc{GeoGebra}.
                \item Quelle transformation géométrique permet de passer de
                    $\mcC_f$~à~$\mcC_g$ ? Vérifier en construisant cette
                    transformation dans \bsc{GeoGebra}.
                \item Dresser le tableau de variations de~$g$. Que
                    remarque-t-on?
                \item Refaire la procédure avec $x\mapsto f(x) + a$ où $a$~est
                    un curseur de \bsc{GeoGebra}.
            \end{enumerate}
        \item On définit la fonction~$h$ par $h(x) = f(x+3)$.
            \begin{enumerate}
                \item Construire la courbe de~$h$ dans \bsc{GeoGebra}.
                \item Quelle transformation géométrique permet de passer de
                    $\mcC_f$~à~$\mcC_h$ ? Vérifier en construisant cette
                    transformation dans \bsc{GeoGebra}.
                \item Calculer une formule de~$h$ et vérifier sa cohérence en
                    traçant cette courbe dans \bsc{GeoGebra} pour tester si elle
                    se superpose avec la courbe de~$h$.
                \item Dresser le tableau de variations de~$h$. Que
                    remarque-t-on?
                \item Refaire la procédure avec $x\mapsto f(x - b)$ où $b$~est
                    un curseur de \bsc{GeoGebra}.
            \end{enumerate}
        \item On définit la fonction~$i$ par $i(x) = 2 \times f(x)$.
            \begin{enumerate}
                \item Construire la courbe de~$i$ dans \bsc{GeoGebra}.
                \item Dresser le tableau de variations de~$i$. Que
                    remarque-t-on?
                \item Refaire la procédure avec $x\mapsto c \times f(x)$ où
                    $c$~est un curseur de \bsc{GeoGebra}.
            \end{enumerate}
        \item On définit la fonction~$j$ par $j(x) = \dfrac{1}{f(x)}$.
            \begin{enumerate}
                \item Quel est l'ensemble de définition de~$j$ ?
                \item Construire la courbe de~$j$ dans \bsc{GeoGebra}.
                \item Dresser le tableau de variations de~$j$. Que
                    remarque-t-on?
            \end{enumerate}
        \item On définit la fonction~$k$ par $k(x) = \sqrt{f(x)}$.\\
            Répondre à la question~5 en remplaçant $j$~par~$k$.
        \item On définit la fonction~$l$ par $l(x) = \abs{f(x)}$.
            \begin{enumerate}
                \item Construire la courbe de~$l$ dans \bsc{GeoGebra}.
                \item Quelle transformation géométrique permet de passer de
                    $\mcC_f$~à~$\mcC_g$ sur~$\IN[-5;8;]$ ? Partout ailleurs ?
            \end{enumerate}
    \end{enumerate}
\end{TP}
\fi

\begin{proposition}
    Soit $f$~une fonction définie sur un intervalle~$I$.
    \begin{enumerate}
        \item Pour tout~$k\in\mdR$, $f+k:x\mapsto f(x)+k$ a le même sens de
            variation que~$f$.
        \item Pour tout~$\alpha > 0$, $\alpha f : x\mapsto \alpha f(x)$ a le
            même sens de variation que~$f$.\\
            Pour tout~$\alpha < 0$, $\alpha f$ varie en sens contraire de~$f$.
        \item Si $f \ge 0$ sur~$I$, c'est-à-dire $f(x) \ge 0$ pour tout~$x\in
            I$, alors $\sqrt{f}: x \mapsto \sqrt{f(x)}$ a le même sens de
            variation que~$f$.
        \item Si $f$~est de signe constant et ne s'annule pas sur~$I$, alors
            $\dfrac{1}{f}: x \mapsto \dfrac{1}{f(x)}$ varie en sens contraire
            de~$f$.
    \end{enumerate}
\end{proposition}

\begin{DM}[Variations de fonctions associées]
    DM \no 2.
\end{DM}

\end{document}
