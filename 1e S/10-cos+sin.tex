% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}
\usetikzlibrary{decorations.markings}

\rivmathlib{geometry}

\begin{document}
\StudentMode

\chapter{Fonctions trigonométriques}

% TODO Activité trigo de 2nde
% Existe-t-il triangle avec 2 angles droits
% Et si \theta > \pi/2 ?

\bigfiller{11}

\section{Cosinus et sinus}

\subsection{Définition et propriétés}

\begin{definition}
    Soient $x$~un réel et $M$~son point associé sur le cercle
    trigonométrique~$\mcC$ (voir \vref{fig:cos-sin}).

    L’abscisse de M est appelée \define{cosinus} de~$x$, noté $\cos(x)$ ou
    $\cos x$.

    L’ordonnée de M est appelée \define{sinus} de~$x$, noté $\sin(x)$ ou
    $\sin x$.
\end{definition}

\begin{figure}
\ffigbox{}{
    %\TeacherMode
    \begin{subfloatrow}[2]
        \ffigbox{}{\caption{}\label{fig:cos-sin}%
            \begin{tikzpicture}[scale=2.4]
                \draw (-1.1, 0) -- (1.1, 0);
                \draw (0, -1.1) -- (0, 1.1);
                \draw (0, 0) circle [radius=1];
                \path (0,0) coordinate[label=below right:$O$] (O);
                \node at (1,0) [below right] {$I$};
                \node at (0,1) [above left]  {$J$};
                \draw[->] (0,0) -- node[sloped,above,pos=1/2] {$\vect{u}$}
                        (35:1) coordinate[label=right:$M$] (M);
                \IfStudentF{
                    \draw[help lines] (M |- O) -- (M) -- (M -| O);
                    \node[below] at (M |- O) {$\cos x$};
                    \node[left]  at (M -| O) {$\sin x$};
                    % phantom
                    \begin{scope}[transparent]
                        \node[above right] at (90:1) {$\frac{\pi}{2}$};
                        \node[below right] at (-90:1) {$\frac{-\pi}{2}$};
                    \end{scope}
                }
            \end{tikzpicture}%
        }%
        \ffigbox{}{\caption{}\label{fig:cos-sin-values}%
            \begin{tikzpicture}[scale=2.4]
                \draw (-1.1, 0) -- (1.1, 0);
                \draw (0, -1.1) -- (0, 1.1);
                \draw (0, 0) circle [radius=1];
                \path (0,0) coordinate[label=below right:$O$] (O);
                \node at (1,0) [below right] {$I$};
                \node at (0,1) [above left]  {$J$};
                \IfStudentF{
                    \foreach \n/\d in {1/6,1/4,1/3,2/3,3/4,5/6} {
                        \ifnum\n=1\def\nx{}\else\let\nx\n\fi
                        \foreach \s in {,-} {
                            \path[every label/.style={inner sep=0pt}]
                                ({\s180*\n/\d}:1) coordinate
                                [label={\s180*\n/\d}:$\frac{\s\nx\pi}{\d}$]
                                (M);
                            \draw[help lines] (M |- O) -- (M) -- (M -| O);
                        }
                    }
                    \node[above left] at (180:1) {$\pi$};
                    \node[above right] at (90:1) {$\frac{\pi}{2}$};
                    \node[below right] at (-90:1) {$\frac{-\pi}{2}$};
                }
            \end{tikzpicture}%
        }%
    \end{subfloatrow}
    \caption{%
        \subref{fig:cos-sin}~Définition de $\cos x$~et~$\sin x$ avec le
        point~$M$ repéré par~$x$.
        \subref{fig:cos-sin-values}~Construction de valeurs remarquables pour
        $\cos$~et~$\sin$.
    }
}
\end{figure}


\begin{proposition}
    Pour tout~$x\in\mdR$,
    \begin{enumerate}
        \item $\UGHOST{-1} \le \cos x \le \UGHOST{1}$ et
            $\UGHOST{-1} \le \sin x \le \UGHOST{1}$;
        \item $\cos(x+2\pi) = \UGHOST{\cos x}$ et
            $\sin(x+2\pi) = \UGHOST{\sin x}$;
        \item $\cos^2 x + \sin^2 x = \UGHOST{1}$.
    \end{enumerate}
\end{proposition}

\begin{remark}
    Attention: la notation traditionnelle $\cos^2 x$ signifie $(\cos x)^2$, à ne
    pas confondre avec $\cos(x^2)$ !
\end{remark}

\begin{proof}
    Soit $M$ le point repéré par le nombre réel $x$.
    \begin{enumerate}
        \item Le point~$M$ est sur le cercle trigonométrique, et en particulier
            $-1 \le x_M \le 1$ ainsi que $-1 \le y_M \le 1$.
        \item Soit $M'$ le point repéré par le nombre réel
            $x+k\times{}2\pi{}$. Les points $M$ et $M'$ sont confondus. Ils
            ont donc même abscisse et même ordonnée.
        \item Le point~$M$ est sur le cercle trigonométrique, c'est-à-dire
            $OM = 1$, soit $\sqrt{(x_M - 0)^2 + (y_M - 0)^2} = 1$. En mettant
            au carré, on obtient $x_M^2 + y_M^2 = 1$ soit
            $\left(\cos x\right)^2 + \left(\sin x\right)^2= 1$.\qedhere
    \end{enumerate}
\end{proof}

\begin{example}
  \begin{itemize}
  \item Le nombre réel 0 repère le point~$\point I(1 ; 0)$ donc
    $\cos 0 = 1$ et $\sin 0 = 0$.
  \item Le nombre réel $\dfrac{\pi{}}{2}$ repère le point~$\point J(0 ; 1)$
    donc $\cos\dfrac{\pi{}}{2}=0$ et $\sin\dfrac{\pi{}}{2}=1$.
  \end{itemize}
\end{example}

\begin{method}[Calculer $\sin x$ quand on connaît $\cos x$]
  \begin{enumerate}
  \item On utilise l'égalité ${\left(\cos x\right)}^2+{\left(\sin
        x\right)}^2=1$ pour calculer ($\sin x){^2}$.

  \item On détermine le signe de~$\sin x$:
    \begin{itemize}
    \item si la mesure principale de $x$ appartient à
      $\left]0\ ;\ \pi{}\right[$, alors $\sin x>0$\ ;\ 

    \item sinon si la mesure principale de $\left]-\pi{}\ ;\
        0\right[$, alors $\sin x<0$.
    \end{itemize}
  \item On conclut sur la valeur exacte de sin $x$.
  \end{enumerate}
\end{method} % cf exo 43 p 296

\subsection{Valeurs remarquables}

\begin{example}
    Calculer $\sin\dfrac{\pi{}}{3}$ sachant que 
    $\cos\dfrac{\pi{}}{3}=\dfrac{1}{2}$.
    \IfStudentT{\bigfiller{4}}
\end{example}

De la même façon on peut déduire quelques valeurs de $\cos x$~et~$\sin x$ pour
des angles~$x$ particuliers. On en indique dans \vref{tbl:cos-sin-values}, mais
aussi sur \vref{fig:cos-sin-values}.

\begin{table}
\ttabbox{}{
    \caption{Valeurs remarquables de $\cos$~et~$\sin$}
    \label{tbl:cos-sin-values}
    \everymath{\displaystyle}
    \let\studentsize\Large
    \begin{tabular}{LlL*{7}{McL}}
    \firsthline
    Degrés & 0 & 30 & 45 & 60 & 90 & 120 & 270 \NL
    Radians & \GHOST{0} & \GHOST{\frac{\pi}{6}} & \GHOST{\frac{\pi}{4}} &
                    \GHOST{\frac{\pi}{3}} & \GHOST{\frac{\pi}{2}} &
                    \GHOST{\frac{2\pi}{3}} & \GHOST{\frac{3\pi}{2}} \NL
    $\cos x$ & \,\GHOST{1}\, & \GHOST{\frac{\sqrt{3}}{2}} &
            \GHOST{\frac{\sqrt{2}}{2}} & \GHOST{\frac{1}{2}} &
            \quad\GHOST{0}\quad & \GHOST{-\frac{1}{2}} & \GHOST{0} \NL
    $\sin x$ & \GHOST{0} & \GHOST{\frac{1}{2}} &
            \GHOST{\frac{\sqrt{2}}{2}} & \GHOST{\frac{\sqrt{3}}{2}} & \GHOST{1}&
            \GHOST{\frac{\sqrt{3}}{2}} & \GHOST{-1} \\
    \lasthline
\end{tabular}
}
\end{table}

\subsection{Les fonctions \texorpdfstring{$\cos$~et~$\sin$}{cos et sin}}

À partir de ces valeurs, on peut tracer les fonctions
$\cos : x \mapsto \cos x$ et $\sin : x \mapsto \sin x$.
Voir \vref{fig:cos+sin-repr}.

\begin{figure}
    \caption{Représentation graphique des fonctions $\cos$~et~$\sin$.}
    \label{fig:cos+sin-repr}
    \leavevmode\hbox to 0pt{\hss
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={ticks={tick typesetter/.code=}},
                            x axis={ticks and grid={
                                step=(pi/4),minor steps between steps=2}},
                            y axis={label=$\cos x$, ticks and grid={
                                major at={-1,(-1/2*sqrt(3)),(-1/2*sqrt(2)),
                                    (-1/2),1,(1/2*sqrt(3)),(1/2*sqrt(2)),
                                    (1/2)}
                            }},
                            visualize as smooth line=cos,
                            %cos = {style={ghost}},
                        ]
        data [set=cos, format=function] {
            var x : interval[-4:8] samples 100;
            func y = cos(\value x r);
        }
        ;
    \end{tikzpicture}\hss}%
    \\[1cm]
    \leavevmode\hbox to 0pt{\hss
        \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={ticks={tick typesetter/.code=}},
                            x axis={ticks and grid={
                                step=(pi/4),minor steps between steps=2}},
                            y axis={label=$\sin x$, ticks and grid={
                                major at={-1,(-1/2*sqrt(3)),(-1/2*sqrt(2)),
                                    (-1/2),1,(1/2*sqrt(3)),(1/2*sqrt(2)),
                                    (1/2)}
                            }},
                            visualize as smooth line=sin,
                            sin = {style={ghost}},
                        ]
        data [set=sin, format=function] {
            var x : interval[-4:8] samples 100;
            func y = sin(\value x r);
        }
        ;
    \end{tikzpicture}\hss}%
\end{figure}

\iffalse
\begin{remark}
    \begin{enumerate}
        \item On peut voir la périodicité des courbes.
        \item La courbe de~$\cos$ semble symétrique par rapport \UGHOST{à
            l'axe~$(Ox)$}: on dit que $\cos$~est \UGHOST{\emph{paire}}.
        \item La courbe de~$\sin$ semble symétrique par rapport \UGHOST{au
            point~$O$}: on dit que $\sin$~est \UGHOST{\emph{impaire}}.
    \end{enumerate}
\end{remark}
\fi

\begin{theorem}
    \begin{itemize}
        \item $\cos$ est dérivable sur~$\mdR$ et
            $\forall x\in\mdR \cos'(x) = -\sin(x)$.
        \item $\sin$ est dérivable sur~$\mdR$ et
            $\forall x\in\mdR \sin'(x) = \cos(x)$.
    \end{itemize}
\end{theorem}

\begin{remark}
    Lorsqu'on compare le signe de~$\cos(x)$ avec le sens de variation de~$\sin$,
    c'est cohérent avec le théorème précédent.
\end{remark}

\section{Angles associés}

\begin{figure}
\ffigbox{}{
    \begin{tikzpicture}[scale=3]
        \draw (-1.2, 0) -- (1.2, 0);
        \draw (0, -1.2) -- (0, 1.2);
        \draw (0, 0) circle [radius=1];
        \node at (0,0) [below right]  {$O$};
        \node at (1,0) [below right] {$I$};
        \node at (0,1) [above left]  {$J$};
    \end{tikzpicture}%
    \caption{%
        Cosinus et sinus des angles associés.
    }
    \label{fig:assoc}
}
\end{figure}

\begin{proposition}[Angles opposés et supplémentaires]
    \leavevmode\\
    \UGHOST{$\cos\left( \pi + x \right)= -\cos x$ \pause et
        $\sin\left( \pi + x \right) = -\sin x$} \\
    \UGHOST{$\cos\left( -x \right)= \cos x$ \pause et
        $\sin\left( -x \right) = -\sin x$} \\
    \UGHOST{$\cos\left( \pi - x \right)= -\cos x$ \pause et
        $\sin\left( \pi - x \right) = \sin x$}
\end{proposition}

\begin{proposition}[Angles complémentaires]
    \leavevmode\\
    \UGHOST{$\cos\left( \frac{\pi}{2}-x \right)= \sin x$ \pause et
        $\sin\left( \frac{\pi}{2} -x \right) = \cos x$} \\
    \UGHOST{$\cos\left( \frac{\pi}{2}+x \right)= -\sin x$ \pause et
        $\sin\left( \frac{\pi}{2} +x \right) = \cos x$}
\end{proposition}

\begin{example}
  On sait que $\cos\dfrac{\pi{}}{3}=\dfrac{1}{2}$. 
  En déduire $\cos\left(-\dfrac{\pi{}}{3}\right)$ et
  $\cos\dfrac{4\pi{}}{3}$.
\end{example}

\section{(In)équations trigonométriques}

\subsection{Équations \texorpdfstring{$\cos x=a$}{cos x = a} ou
\texorpdfstring{$\sin x=a$}{sin x = a}}

\begin{method}[Résoudre $\cos x=a$ avec $x\in{}\mdR$]
    Cela revient à chercher quels réels repèrent les points du cercle dont
    l'abscisse est égale à $a$.
    \begin{enumerate}
        \item Pour résoudre l'équation $\cos x=a$ pour $x\in{}\mdR$, on résout
            d'abord cette équation dans $\IN]-\pi;\pi;]$. Dans le cas général,
            $a\in\IN]-1;1;[$.

            Il existe un unique nombre $b$ dans~$\IN]0;\pi;[$ tel que
            $a = \cos b$. L'équation est donc équivalente à $\cos x = \cos b$
            dont les solutions sont $b$ et $-b$. Une valeur approchée de $b$
            peut être obtenue à l'aide de la calculatrice.
        \item L'ensemble des solutions dans $\mdR$ est obtenu en soustrayant ou
            en ajoutant un nombre entier de fois $2\pi$ :
            $b+k\times2\pi$ et $-b+k\times 2\pi$, $k\in\mdZ$.
\end{enumerate}
\end{method}

\begin{example}
    Résoudre dans $\IN]-\pi;\pi;]$ puis dans $\mdR$ l'équation
    $\cos x=\dfrac{1}{2}$.
\end{example}

\begin{method}[Résoudre $\sin x=a$ avec $x\in{}\mdR$]
    Cela revient à chercher quels réels repèrent les points du cercle dont
    l'abscisse est égale à $a$.
    \begin{enumerate}
        \item Pour résoudre l'équation $\cos x=a$ pour $x\in{}\mdR$, on résout
            d'abord cette équation dans $\IN]-\pi;\pi;]$. Dans le cas général,
            $a\in\IN]-1;1;[$.

            Il existe un unique nombre $b$
            dans~$\IN]-\dfrac{\pi}{2};\dfrac{\pi}{2};[$ tel que $a = \sin b$.
            L'équation est donc équivalente à $\cos x = \cos b$
            dont les solutions sont $b$ et $\pi-b$.
        \item L'ensemble des solutions dans $\mdR$ est obtenu en soustrayant ou
            en ajoutant un nombre entier de fois $2\pi$ :
            $b+k\times2\pi$ et $\pi-b+k\times 2\pi$, $k\in\mdZ$.
\end{enumerate}
\end{method}

\begin{example}
    Résoudre l'équation $\sin x=\dfrac{\sqrt{2}}{2}$ dans~$\mdR$.
\end{example}

\subsection{Inéquation du type \texorpdfstring{$\cos x\ge a$}{cos x ≥ a}
    ou \texorpdfstring{$\sin x \ge a$}{sin x ≥ a}}

\begin{example}
  Résoudre l'inéquation  $\cos x>\dfrac{\sqrt{3}}{2}$ dans 
  $\left]-\pi \ ;\ \pi \right]$. 
\end{example}

\begin{remark}
    \begin{enumerate}
        \item Des cas particuliers peuvent se présenter, il faut faire attention
            à traduire $\cos x\ge a$ (resp. $\sin x\ge a$) par~: on cherche les
            nombres réels qui repèrent un point sur le cercle dont l'abscisse
            (resp. ordonnée) est supérieure ou égale à~$a$.
        \item L'intervalle de résolution $I$ n'est pas toujours
            $\IN]-\pi;pi;]$. L'intervalle $\IN[0;2\pi;[$ est un autre
            intervalle possible pour décrire le cercle trigonométrique.
    \end{enumerate}
\end{remark}



\end{document}
