% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}

\begin{document}

\StudentMode

\chapter{Géométrie plane}

\section{Rappels sur les vecteurs}

\subsection{Translations et vecteurs}

\begin{activity}
    \begin{enumerate}
        \item Quelles sont les données qui définissent un vecteur ?
            \IfStudentT{\bigfiller{3}}
        \item Comment vérifie-t-on que $\vect{AB}$~et~$\vect{CD}$ ont la même
            longueur ?
            \IfStudentT{\bigfiller{1}}
        \item Comment vérifie-t-on que $\vect{AB}$~et~$\vect{CD}$ ont la même
            direction ?
            \IfStudentT{\bigfiller{1}}
        \item Comment vérifie-t-on que $\vect{AB}$~et~$\vect{CD}$ ont le même
            sens ?
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{activity}

\begin{definition}
    La translation de $A$~vers~$B$, ou \define{translation de
    vecteur~$\vect{AB}$}, est la transformation du plan définie de la façon
    suivante: l'image du point~$M$ est le symétrique~$M'$ de~$A$ par rapport au
    milieu de~$[BM]$. Autrement dit, l'image de~$M$ est le point~$M'$ tel que
    $[AM']$~et~$[BM]$ aient le même milieu. Si $M$~n'est pas aligné avec
    $A$~et~$B$ cela revient à placer~$M$ de sorte à ce que $ABM'M$ soit un
    parallélogramme (voir \vref{fig:def-translate}).
\end{definition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}
        \ffigbox{}{\caption{}\label{fig:def-translate-1}
        \begin{tikzpicture}[baseline=(I)]
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (2,0.4)   coordinate[label=above:$B$] (B);
            \path (1.5,-1)  coordinate[label=below:$M$] (M);
            \path ($ (B)!0.5!(M) $) coordinate[label=above left:$I$] (I);
            \path ($ (I)!-1!(A) $)  coordinate[label=below:$M'$] (M');

            \foreach[remember=\y as \x (initially B)] \y in {M',M,A} {
                \draw[dashed] (\x) -- (\y);
            }
            \draw[->] (A) -- (B);
            \draw   (I) edge pic[dist marker=1] {} (A)
                        edge pic[dist marker=1] {} (M')
                        edge pic[dist marker=2] {} (B)
                        edge pic[dist marker=2] {} (M);
        \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:def-translate-2}
        \begin{tikzpicture}[baseline=(J)]
            \path (0,0)     coordinate[label=below:$A$] (A);
            \path (2,0.4)   coordinate[label=below:$B$] (B);
            \path ($ (A)!1.5!(B) $) coordinate[label=below:$N$]  (N);
            \path ($ (B)!0.5!(N) $) coordinate[label=below:$J$]  (J);
            \path ($ (J)!-1!(A) $)  coordinate[label=below:$N'$] (N');

            \draw[->] (A) -- pic[tick marker] {} (B) pic[tick marker] {};
            \draw   (B) -- (J) pic[tick marker] {}
                    -- (N) pic[tick marker] {}
                    -- (N') pic[tick marker] {};
            \path[every edge/.style={draw=none}]
                    (J) edge pic[dist marker=1] {} (A)
                        edge pic[dist marker=1] {} (N')
                        edge pic[dist marker=2] {} (B)
                        edge pic[dist marker=2] {} (N);
        \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Construction de deux translatés par la translation de
        vecteur~$\vect{AB}$.
        \subref{fig:def-translate-1} $M'$~est l'image de~$M$: $[AM']$~et~$[BM]$
        ont le même milieu~$I$.
        \subref{fig:def-translate-2} $N'$~est l'image de~$N$: $[AN']$~et~$[BN]$
        ont le même milieu~$J$.}
    \label{fig:def-translate}
    }
\end{figure}

\begin{definition}
    Les vecteurs $\vect{AB}$~et~$\vect{CD}$ sont égaux si la translation de
    $A$~vers~$B$ est la même que celle de $C$~vers~$D$ --- c'est-à-dire si ces
    deux translations déplacent les points de la même façon\footnote{Deux
    fonctions ou transformations sont égales quand elles associent la même
    image à tous les éléments de leur ensemble de définition.}.
\end{definition}

\begin{proposition}
    \label{prop:vector-equality}
    \UGHOST{%
    $\vect{AB} = \vect{CD}$ si et seulement si $[AD]$~et $[BC]$ ont le même
    milieu. Autrement dit $\vect{AB} = \vect{CD}$ si et seulement si $ABDC$ est
    un parallélogramme --- éventuellement plat (voir
    \vref{fig:prop-vector-equality}).}
\end{proposition}

\begin{figure}
    \begin{tikzpicture}
        \path (0,0)     coordinate[label=above:$A$] (A);
        \path (4,-1)    coordinate[label=above:$B$] (B);
        \path (1,-1.5)  coordinate[label=below:$C$] (C);
        \path ($ (B)!0.5!(C) $) coordinate (I);
        \path ($ (I)!-1!(A) $)  coordinate[label=below:$D$] (D);

        \draw[dashed] (A) -- (C) (D) -- (B);
        \draw[->] (A) -- (B);
        \draw[->] (C) -- (D);
        \draw   (I) edge pic[dist marker=1] {} (A)
                    edge pic[dist marker=1] {} (D)
                    edge pic[dist marker=2] {} (B)
                    edge pic[dist marker=2] {} (C);
    \end{tikzpicture}
    \caption{Condition d'égalité: $\vect{AB} = \vect{CD}$ si et seulement si
        $ABDC$ est un parallélogramme.}
    \label{fig:prop-vector-equality}
\end{figure}

\iffalse
\begin{proof}
    On note~$t_{\vect{AB}}$ (resp.~$t_{\vect{CD}}$) la translation de
    vecteur~$\vect{AB}$ (resp.~$\vect{CD}$).
    \begin{itemize}
        \item Si $\vect{AB} = \vect{CD}$, c'est que les translations
            $t_{\vect{AB}}$~et~$t_{\vect{CD}}$ sont égales. Or l'image de~$C$
            par~$t_{\vect{CD}}$ est~$D$, donc l'image de~$C$ par~$t_{\vect{AB}}$
            est~$D$ puisque c'est la même translation. Mais alors $ABDC$~est un
            parallélogramme.
        \item Réciproquement, supposons que $ABDC$~est un parallélogramme. Alors
            $(AB) \parallel (CD)$ et $AB = CD$. Soient $M$~un point du plan et
            $M'$~son image par~$t_{\vect{AB}}$: $ABM'M$ est un parallélogramme.
            On a ainsi $(AB) \parallel (MM')$ et $AB = MM'$. Mais alors $(CD)
            \parallel (MM')$ et $CD = MM'$. En définitive $CDM'M$ est un
            parallélogramme donc $M'$~est l'image de~$M$ par~$t_{\vect{CD}}$.
            $M$~a la même image par $t_{\vect{AB}}$~et~$t_{\vect{CD}}$, et ce
            pour tout~$M$: les translations sont égales et les vecteurs aussi.

            Attention: on a admis que $CDM'M$ n'était pas croisé; c'est très
            --- trop --- fastidieux à montrer.\qedhere
    \end{itemize}
\end{proof}
\fi

\begin{definition}
    Le \define{vecteur nul} est le vecteur~$\vect{0}$ dont la translation ne
    déplace aucun point du plan. On a $\vect{AA} = \vect{BB} = \vect{CC} =
    \vect{0}$ pour tous $A$,~$B$ et~$C$.
\end{definition}

\subsection{Sommes de vecteurs}

\begin{definition}
    Soient $\vect{u}$~et~$\vect{v}$ deux vecteurs. En effectuant \emph{à la
    suite} les translations de vecteurs $\vect{u}$~puis~$\vect{v}$, l'opération
    obtenue est \UGHOST{encore une translation. Le vecteur de cette translation
    est noté $\vect{u} + \vect{v}$ et est appelé \define{somme de
    $\vect{u}$~et~$\vect{v}$}.}
\end{definition}

\begin{remark}
    $\vect{u} + \vect{0} = \vect{u}$ car on cumule avec la translation qui ne
    fait rien.
\end{remark}

\begin{proposition}[Relation de Chasles]
    \label{prop:chasles}
    Soient $A$,~$B$ et~$C$ trois points. Alors \UGHOST{$\vect{AB} + \vect{BC} =
    \vect{AC}$} (voir \vref{fig:chasles}).
\end{proposition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}%\useFCwidth
    \ffigbox{}{
        \caption{}
        \label{fig:chasles}
        \begin{tikzpicture}
            \path (0,0)         coordinate[label=above:$A$] (A);
            \path (2,1)         coordinate[label=above:$B$] (B);
            \path (3.5,-0.5)    coordinate[label=below:$C$] (C);
            \draw[->] (A) -- (B);
            \draw[->] (B) -- (C);
            \draw[->,thick] (A) -- (C);
        \end{tikzpicture}
    }
    \ffigbox{}{
        \caption{}
        \label{fig:parallelogram}
        \begin{tikzpicture}
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (1,1.5)   coordinate[label=above:$B$] (B);
            \path (3,-0.5)  coordinate[label=below:$C$] (C);
            \path ($ (B)+(C)-(A) $) coordinate[label=right:$M$] (M);
            \draw[->]        (A) -- (B);
            \draw[->]        (A) -- (C);
            \draw[dashed]    (B) -- (M) -- (C);
            \draw[->,thick]  (A) -- (M);
        \end{tikzpicture}
    }
    \end{subfloatrow}
    \caption{\subref{fig:chasles} $\vect{AB} + \vect{BC} = \vect{AC}$ (relation
        de Chasles). \subref{fig:parallelogram} La règle du parallélogramme
        s'obtient par $\vect{AB} + \vect{AC} = \vect{AB} + \vect{BM} =
        \vect{AM}$.}
    }
\end{figure}

\begin{proof}
    L'image de~$A$ par la translation de vecteur~$\vect{AB}$ est~$B$, et l'image
    de~$B$ par la translation de vecteur~$\vect{BC}$ est~$C$. La translation
    cumulée envoie donc $A$~vers~$C$ et son vecteur est ainsi égal
    à~$\vect{AC}$.  C'est dire $\vect{AB} + \vect{BC} = \vect{AC}$.
\end{proof}

On utilise cette propriété pour construire géométriquement la somme $\vect{u} +
\vect{v}$ en choisissant un représentant de~$\vect{v}$ dont l'origine est
l'extrémité de celui de~$\vect{u}$ --- en les mettant «à la queue-leu-leu».

\begin{remark}[Règle du parallélogramme]
    Soient $A$,~$B$ et~$C$ trois points non alignés. Alors $\vect{AB} +
    \vect{AC} = \vect{AM}$ où $[AM]$~est la diagonale du parallélogramme de
    côtés $[AB]$~et~$[AC]$ (voir \vref{fig:parallelogram}).
\end{remark}

\begin{definition}
    L'\define{opposé de~$\vect{u}$} est l'unique vecteur~$\vect{v}$ tel que
    $\vect{u} + \vect{v} = \vect{0}$. On a $-\vect{AB} = \vect{BA}$ quels que
    soient les points $A$~et~$B$.
\end{definition}


\subsection{Coordonnées de vecteurs; multiplication par un réel}

\begin{definition}
    Les coordonnées de~$\vect{u}$ sont celles du point~$A$ tel que $\vect{OA} =
    \vect{u}$. On les note souvent en colonnes: $\vect{u}(x;y)$.
\end{definition}

\begin{proposition}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Alors
    $\vect{u} = \vect{v} \iff
            \left\{
            \begin{aligned}
                x &= x' \\
                y &= y'
            \end{aligned}
            \right.$
\end{proposition}

\begin{proposition}
    Si $A(x_A;y_A)$~et~$B(x_B;y_B)$ alors
    $\vect{AB}(\GHOST{x_B - x_A}; \GHOST{y_B - y_A})$.
\end{proposition}

\begin{proposition}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Alors $\vect{u} +
    \vect{v} = \vect{w}(\GHOST{x+x'};\GHOST{y+y'})$.
\end{proposition}

\begin{definition}
    Soient $\vect{u}(x;y)$~et~$\alpha\in\mdR$. On définit le
    vecteur~$\alpha\cdot\vect{u}$ par
    $(\alpha\cdot\vect{u})\vect{}(\GHOST{\alpha x};\GHOST{\alpha y})$.
\end{definition}

\begin{example}
    Dans un repère, on place $A(1;3)$~et~$B(3;4)$.

    \StudentOnly
    \begin{tikzpicture}
        \datavisualization [school book axes,
                            all axes={grid, unit length=.5cm,
                                ticks={tick typesetter/.code=}},
                                x axis={include value/.list={-4,9}},
                                y axis={include value/.list={-2,7}},
                                visualize as scatter=points,
                        ]
            data[set=labels] {
                x, y, label, style
                0, 0, $O$, {anchor=30,circle}
                1, 0, $I$, anchor=110
                0, 1, $J$, {anchor=-50,inner sep=2pt}
            }
            ;
    \end{tikzpicture}%
    \EndStudentOnly

    \begin{enumerate}
        \item Placer~$C$ tel que $\vect{BC}+\vect{BA} = \vect{0}$.
        \item Placer~$D$ tel que $\vect{DB} = \vect{AC}$.
        \item Compléter les égalités suivantes:
            \begin{enumerate}[gathered]
                \item $\vect{DA} + \vect{BC} = \vect{\UGHOST*{DB}}$;
                \item $\vect{AB} = \UGHOST*{\frac{1}{2}}\vect{AC}$;
                \item $\vect{AB} = \UGHOST*{-\frac{1}{2}}\vect{BD}$;
                \item $\vect{CA} = \UGHOST*{\frac{2}{3}}\vect{CD}$;
                \item $\vect{AB} + \vect{AC} = \vect{\UGHOST*{D}C}$.
            \end{enumerate}
            \IfStudentT{\bigfiller{2}}
        \item Déterminer par le calcul les coordonnées de
            $\vect{AB}$,~$\vect{AC}$, $\vect{BC}$ et~$\vect{CD}$.
            \IfStudentT{\bigfiller{6}}
        \item On pose~$E(3;1)$. Placer~$F$ tel que $\vect{CF} = \vect{AE}$.
            Quelles sont ses coordonnées ?
            \IfStudentT{\bigfiller{2}}
        \item Quelle est la nature du quadrilatère~$BDEF$ ? Le démontrer.
            \IfStudentT{\bigfiller{5}}
        \item Déterminer les coordonnées de~$G$ tel que $\vect{BG} = \vect{DE} -
            \vect{AD}$.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{example}




\section{Vecteurs colinéaires}

\subsection{Propriété caractéristique}

\begin{definition}
    Les vecteurs $\vect{u}$~et~$\vect{v}$ sont \define{colinéaires} si (et
    seulement si) \UGHOST{il existe~$\alpha\in\mdR$ tel que $\vect{u} =
    \alpha\vect{v}$ ou tel que $\vect{v} = \alpha \vect{u}$.}
\end{definition}

\begin{remark}
    Le vecteur nul est colinéaire à tous les vecteurs.
\end{remark}

\begin{remark}
    Deux vecteurs non nuls sont colinéaires s'ils ont la même direction.
\end{remark}

\begin{activity}
    Dans chacun des cas suivants, indiquer si $\vect{u}(a;b)$~et~$\vect{v}(c;d)$
    sont colinéaires, puis calculer~$ad-bc$:
    \begin{enumerate}[gathered]
        \item $\vect{u}(3;4)$~et~$\vect{v}(8;9)$;
        \item $\vect{u}(-4;8)$~et~$\vect{v}(6;-12)$;
        \item $\vect{u}(0;4)$~et~$\vect{v}(0;11)$;
        \item $\vect{u}(0;4)$~et~$\vect{v}(-11;0)$;
        \item $\vect{u}(0;0)$~et~$\vect{v}(5;-4)$.
    \end{enumerate}
    \IfStudentT{\bigfiller{8}}
\end{activity}

\begin{theorem}
    \label{thm:determinant}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Les vecteurs
    $\vect{u}$~et~$\vect{v}$ sont colinéaires si et seulement si
    \UGHOST{$xy' - x'y = 0$.}
\end{theorem}

\begin{remark}[Culture uniquement]
    Le nombre~$xy' - x'y$ est appelé \emph{déterminant} de
    $\vect{u}$~et~$\vect{v}$ et est noté~$\det(\vect{u},\vect{v})$.
\end{remark}

\begin{proof}
    \begin{enumerate}
        \item \UGHOST{Supposons que $\vect{u}$~et~$\vect{v}$ sont colinéaires et
            calculons $xy' - x'y$. Si $\vect{u} = \vect{0}$, alors $x = y = 0$
            et le résultat est évident. Sinon, $\vect{u} \ne \vect{0}$.  Alors
            il existe~$\alpha\in\mdR$ tel que $\vect{v} = \alpha \vect{u}$.
            Ainsi $x' = \alpha x$~et$y'=\alpha y$, et clairement $xy' - x'y =
            \alpha x y - \alpha x y = 0$.}
        \item \UGHOST{Réciproquement, supposons $xy' - x'y = 0$. On veut montrer
            que $\vect{u}$~et~$\vect{v}$ sont colinéaires. Si $x \ne 0$, on pose
            $\alpha = \frac{x'}{x}$: bien sûr $x' = \alpha x$. Or $xy' = x'y$
            donc $y' = \alpha y$ en divisant par~$x$. Ainsi~$\vect{v} = \alpha
            \vect{u}$. Si $x = 0$ mais $y \ne 0$, on pose $\alpha =
            \frac{y'}{y}$ et on obtient de même~$\vect{v} = \alpha \vect{u}$.
            Enfin, si $x = y = 0$, $\vect{u} = \vect{0} = 0 \vect{v}$.}\qedhere
    \end{enumerate}
\end{proof}


\subsection{Alignement et parallélisme}


\begin{proposition}
    Soient $A$,~$B$, $C$, et~$D$ quatre points du plan. Alors
    $\vect{AB}$~et~$\vect{CD}$ sont colinéaires si et seulement si
    $(AB)$~et~$(CD)$ sont parallèles.
\end{proposition}

\begin{corollary}
    Soient $A$,~$B$, et~$C$ trois points du plan. Alors
    $\vect{AB}$~et~$\vect{AC}$ sont colinéaires si et seulement si
    $A$,~$B$, et~$C$ sont alignés.
\end{corollary}

\begin{example}
    Dans les cas suivants, indiquer si $A$,~$B$ et~$C$ sont alignés:
    \begin{enumerate}[gathered]
        \item $A(8;3)$, $B(11;4)$ et $C(\<3.5>;\<1.5>)$;
        \item $A(1;1)$, $B(3;2)$ et $C(4;3)$.
    \end{enumerate}
    \IfStudentT{\bigfiller{6}}
\end{example}

\begin{example}
    Soient $x\in\mdR$, $A(1;1)$, $B(x;2)$ et~$C(5;x)$. Peut-on trouver~$x$ de
    sorte que $A$,~$B$ et~$C$ soient alignés ?
    \IfStudentT{\bigfiller{5}}
\end{example}

\section{Équations cartésiennes}

\subsection{Vecteur directeur}

\begin{definition}
    Soit~$\mcD$ une droite du plan. On appelle \define{vecteur directeur}
    de~$\mcD$ tout vecteur~$\vect{u} = \vect{AB}$ où $A$~et~$B$ sont deux points
    distincts appartenant à~$\mcD$ (voir \cref{fig:vect-dir}).
\end{definition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}
    \ffigbox{}{\caption{}\label{fig:vect-dir}
    \begin{tikzpicture}[every node/.style=above]
        \path (0,0) coordinate (A);
        \path (1,0.25) coordinate (B);
        \path (-1,1) coordinate (C);
        \draw[thin] ($ (A)!-1!(B) $) -- ($ (A)!4!(B) $)
                        node[pos=0.1] {$\mcD$};
        \begin{scope}[->, thick]
            \draw (A) -- node {$\vect{u}$} (B);
            \draw ($ (A)!3!(B) $) -- node {$\vect{v}$} ($ (A)!2.5!(B) $);
            \draw (C) -- node {$\vect{w}$} ($ (C) + (A)!2.5!(B) - (A) $);
        \end{scope}
    \end{tikzpicture}
    }
    \ffigbox{}{\caption{}\label{fig:vect-cond}
    \begin{tikzpicture}[every node/.style=above]
        \path (0,0)     coordinate[label=below:$A$] (A);
        \path (1,0.25)  coordinate[label=below:$B$] (B);
        \path (0,1)     coordinate (C);
        \path ($ (A)!2.5!(B) $) coordinate[label=below right:$M_1$] (M1);
        \path ($ (C) + (A)!2!(B) - (A) $)
                        coordinate[label=right:$M_2$] (M2) pic {point};
        \draw ($ (A)!-1!(B) $) -- (A) node[pos=0.4] {$\mcD$}
            pic[tick marker] {} -- (B) pic[tick marker] {}
            -- (M1) pic[tick marker] {} -- ($ (A)!4!(B) $);
        \begin{scope}[->, thick]
            \draw (C) -- node {$\vect{u}$} ($ (C) + (B) - (A) $);
            \draw (A) -- (M1);
            \draw (A) -- (M2);
        \end{scope}
    \end{tikzpicture}
    }
    \end{subfloatrow}
    \caption{\subref{fig:vect-dir} $\vect{u}$,~$\vect{v}$ et~$\vect{w}$ sont
        trois vecteurs directeurs de~$\mcD$. \subref{fig:vect-cond} $M_1 \in
        \mcD$ et~$\vect{AM_1}$ est colinéaire à~$\vect{u}$; $M_2 \notin \mcD$
        et~$\vect{AM_2}$ n'est pas colinéaire à~$\vect{u}$.}
    }
\end{figure}

\begin{remark}
    $\vect{u}$ est un vecteur directeur de~$\mcD$ s'il a la même direction
    que~$\mcD$.
\end{remark}

\begin{remark}
    Une droite a une infinité de vecteurs directeurs.
\end{remark}

\begin{remark}
    Deux vecteurs directeurs de~$\mcD$ sont \UGHOST{colinéaires}.
\end{remark}

\begin{remark}
    Si $A$~et~$B$ sont deux points, et $\mcD$~est une droite, alors $\vect{AB}$
    est un vecteur directeur de~$\mcD$ si et seulement si
    \UGHOST{$(AB) \parallel \mcD$.}
\end{remark}

\begin{remark}
    Une droite peut être définie par:
    \begin{itemize}
        \item deux points $A$~et~$B$;
        \item un point~$A$ et un vecteur~$\vect{u}$.
    \end{itemize}
\end{remark}

\begin{proposition}
    Soit $\mcD$~la droite de vecteur directeur~$\vect{u}$ qui passe par~$A$.
    Soit $M$~un point quelconque. Alors $M \in \mcD$ si et seulement si
    \UGHOST{$\vect{AM}$~et~$\vect{u}$ sont colinéaires (voir
    \cref{fig:vect-cond}).}
\end{proposition}

\begin{proof}
    \UGHOST{%
    On fixe le point~$B$ tel que $\vect{AB} = \vect{u}$: c'est l'extrémité du
    représentant de~$\vect{u}$ qui part de~$A$. Mais alors $M \in \mcD$ si et
    seulement si $A$,~$B$ et~$M$ sont alignés c'est-à-dire si
    $\vect{AB}$~et~$\vect{AM}$ sont colinéaires.%
    }
\end{proof}


\subsection{Équations cartésiennes de droites}

\begin{definition}
    Soit $\mcE$~un ensemble de points dans un repère du plan. Une \emph{équation
    cartésienne de~$\mcE$} est une équation~$E$ d'inconnues $x$~et~$y$ telle que
    $M(x;y) \in \mcE$ si et seulement si $(x;y)$~est une solution de~$E$.
\end{definition}

\begin{example}
    \begin{enumerate}
        \item $y = 2x + 1$~est une équation cartésienne \UGHOST{d'une droite
            oblique.}
        \item $x = 3$~est une équation cartésienne \UGHOST{d'une droite
            verticale.}
    \end{enumerate}
\end{example}

\begin{example}
    De quel ensemble de points $x^2 + y^2 = 1$ est-elle une équation ?
    \IfStudentT{\bigfiller{2}}
\end{example}

\begin{remark}
    Si $\mcE$~admet une équation cartésienne, alors il en admet une infinité: il
    suffit de multiplier les membres de l'équation par un même réel non nul.
\end{remark}

\begin{theorem}
    \label{thm:line-has-cartesian-equation}
    Toute droite du plan admet une équation cartésienne de la forme
    \UGHOST{$ax + by + c = 0$ avec $(a;b) \ne (0;0)$.}
\end{theorem}

\begin{proof}
    \GHOST{%
    Soient $\mcD$~une droite, $A(s;t)\in\mcD$ et $\vect{u}(\alpha;\beta)$~un
    vecteur directeur de~$\mcD$. Alors
    \begin{align*}
        M(x;y)\in\mcD &\iff \text{$\vect{AM}(x-s;y-t)$~et~$\vect{u}$
                                    sont colinéaires} \\
                      &\iff (x-s)\times\beta - (y-t)\times\alpha = 0 \\
                      &\iff \beta x - \alpha y - s\beta + t\alpha = 0 \\
                      &\iff ax + by + c = 0
    \end{align*}
    avec $a = \beta$,~$b=-\alpha$ et~$c=t\alpha-s\beta$. Puisque $\vect{u}$~est
    un vecteur directeur, $\vect{u}\ne\vect{0}$: c'est dire
    $(\alpha;\beta)\ne(0;0)$ et ainsi $(a;b)\ne(0;0)$.%
    }
\end{proof}

\begin{example}
    Soit~$\mcD$ passant par~$A(1;1)$ et de vecteur directeur~$\vect{u}(-3;2)$.
    Déterminer une équation cartésienne de~$\mcD$.
    \IfStudentT{\bigfiller{7}}
\end{example}

\begin{remark}
    Selon quel vecteur directeur on a choisi --- voire même selon l'ordre des
    deux vecteurs dans la condition de colinéarité --- on obtient une équation
    cartésienne différente. \emph{Toutes sont correctes}: une droite admet une
    infinité d'équations cartésiennes.
\end{remark}

\begin{theorem}
    \label{thm:cartesian-equation-is-line}
    Tout ensemble d'équation cartésienne $ax + by + c = 0$ avec $(a;b) \ne 0$
    est \UGHOST{une droite, de vecteur directeur~$\vect{u}(-b;a)$.}
\end{theorem}

\begin{proof}
    \GHOST{%
    On pose $\vect{u}(-b;a)$. Si $b\ne0$, on pose $A\left(0\mathrel;-\frac{c}{b}
    \right)$. Alors
    \begin{align*}
        M(x;y)\in\mcD &\iff \text{$\vect{AM}(x;y+\frac{c}{b})$~et~$\vect{u}$
                                    sont colinéaires} \\
                      &\iff x\times a - \left( y + \frac{c}{b} \right)
                                            \times (-b) = 0 \\
                      &\iff ax + by + \frac{c}{b} \times b = 0 \\
                      &\iff ax + by + c = 0
    \end{align*}
    L'ensemble d'équation cartésienne~$ax+by+c=0$ est bien la droite~$\mcD$
    passant par~$A$ et de vecteur directeur~$\vect{u}(-b;a)$. Si $b=0$ alors
    $a\ne0$ et on choisit~$A\left(-\frac{c}{a} \mathrel; 0\right)$ pour finir
    exactement de la même façon.%
    }
\end{proof}

\begin{remark}
    Bien entendu, on peut choisir $\vect{v}(b;-a)$ ou $\vect{w}(2b;-2a)$ comme
    vecteurs directeurs\ldots
\end{remark}

\subsection{Parallélisme}

\begin{proposition}
    \label{prop:parallel-cartesian}
    Soient $\mcD: ax + by + c$ et $\mcD': a'x + b'y + c'$ deux droites du plan.
    $\mcD$~et~$\mcD'$ sont parallèles si et seulement si
    \UGHOST{$a'b-ab' = 0$.}
\end{proposition}

\begin{proof}
    \UGHOST{%
    $\vect{u}(-b;a)$ et $\vect{u'}(-b';a')$ sont des vecteurs directeurs
    respectifs de $\mcD$~et~$\mcD'$. Alors $\mcD \parallel \mcD'$ si et
    seulement si $\vect{u}$~est un vecteur directeur de~$\mcD'$, c'est-à-dire si
    $\vect{u}$~et~$\vect{u'}$ sont colinéaires, ou encore $-ba'+ab' = 0$.%
    }
\end{proof}

\begin{example}
    Les droites d'équation $3x-y+5=0$ et $-6x+2y+7=0$ sont-elles parallèles ?
    \IfStudentT{\bigfiller{4}}
\end{example}

\subsection{Équations réduites}

\begin{proposition}
    \label{prop:reduced-equation-nonvert}
    Toute droite non verticale admet une équation du type $y = mx + p$.
    Réciproquement, $y = mx + p$ définit une droite de vecteur
    directeur~$\vect{u}(1;m)$.
\end{proposition}

\begin{proof}%
    \begin{enumerate}
        \item $y = mx + p \iff -mx + 1y -p = 0$ est une équation cartésienne de
            droite de vecteur directeur~$\vect{u}(-1;-m)$ ou encore
            $\vect{v}(-1;-m)$.
        \item Réciproquement, soit $\mcD$ est une droite non verticale. Elle
            admet une équation cartésienne $ax + by + c = 0$ où $\vect{w}(-b;a)$
            est un vecteur directeur. Puisque $\mcD$ n'est pas verticale,
            $\vect{w}$ n'est pas colinéaire à $\vect{\jmath}(0;1)$ donc $b\ne0$.
            Ainsi l'équation s'écrit $y = \frac{-a}{b}x - \frac{c}{b}$.\qedhere
    \end{enumerate}
\end{proof}

\begin{proposition}
    \label{prop:reduced-equation-vert}
    Toute droite verticale admet une équation du type~$x = k$. Réciproquement,
    $x = k$ définit une droite verticale, de vecteur directeur~$\vect{v}(0;1)$.
\end{proposition}

\begin{proof}
    \begin{enumerate}
        \item $x = k \iff 1x + 0y - k = 0$ est une équation cartésienne de
            droite de vecteur directeur~$\vect{u}(0;1)$.
        \item Réciproquement, si $\mcD$ est une droite verticale, alors elle
            admet une équation cartésienne $ax + by + c = 0$ où $\vect{v}(-b;a)$
            est un vecteur directeur colinéaire à $\vect{\jmath}(0;1)$. Ainsi
            $b = 0$, et puisque $\vect{v}\ne\vect{0}$ on est sûr que $a\ne0$:
            l'équation s'écrit $x = \frac{-c}{a}$.\qedhere
    \end{enumerate}
\end{proof}

\begin{example}
    Soit $\mcD$~d'équation $y = \frac{2}{3}x + 4$. Un vecteur directeur
    de~$\mcD$ est \UGHOST{$\vect{u}(1;2/3)$; un autre est $\vect{v}(3;2)$
    ($\vect{v}=3\vect{u}$).}
\end{example}

\section{Décomposition de vecteurs}

\TeacherMode

\begin{theorem}
    \label{thm:vector-basis}
    Soient $\vect{u}$~et~$\vect{v}$ deux vecteurs non colinéaires. Alors tout
    vecteur~$\vect{w}$ peut se décomposer en \emph{combinaison linéaire} de
    $\vect{u}$~et~$\vect{v}$: \UGHOST{il existe $\alpha$~et~$\beta$ tels que
    $\vect{w} = \alpha\vect{u} + \beta\vect{v}$. Une telle écriture est
    \emph{unique}.}%
\end{theorem}

\begin{remark}
    Si $\vect{u}$~et~$\vect{v}$ ne sont pas colinéaires, alors en particulier
    $\vect{u} \ne \vect{0}$ et $\vect{v} \ne \vect{0}$.
\end{remark}

\begin{remark}
    On dit alors que $(\vect{u};\vect{v})$ est une \emph{base} vectorielle.
\end{remark}

\begin{example}[Demi-somme]
    Soit $ABC$~un triangle quelconque. On note $I$,~$J$ et~$K$ les milieux
    respectifs de $[AB]$, $[AC]$ et~$[BC]$. Décomposer les vecteurs $\vect{AI}$,
    $\vect{AJ}$ et~$\vect{AK}$ en fonction de $\vect{AB}$~et~$\vect{AC}$.
\end{example}

\IfStudentF{%
\begin{proof}
    \begin{enumerate}
        \item Pour l'existence: on fixe un point~$O$ et on place $I$~et~$J$,
            tels que $\vect{OI} = \vect{u}$ et $\vect{OJ} = \vect{v}$. Comme
            $\vect{u}$~et~$\vect{v}$ ne sont pas colinéaires, $(O;I;J)$~est un
            repère du plan; dans ce repère $\vect{w}$~a des coordonnées:
            $\vect{w}(\alpha;\beta)$. Alors $\vect{w} = \alpha \vect{OI} + \beta
            \vect{OJ} = \alpha\vect{u} + \beta\vect{v}$.
        \item Pour l'unicité: on suppose qu'il existe
            $(\alpha;\beta)$~et$(\alpha';\beta')$ tels que $\vect{w} =
            \alpha\vect{u} + \beta\vect{v} = \alpha'\vect{u} + \beta'\vect{v}$.
            Alors $\alpha\vect{u} - \alpha'\vect{u} = \beta'\vect{v} -
            \beta\vect{v}$, donc $(\alpha-\alpha')\vect{u} =
            (\beta'-\beta)\vect{v}$. Si l'on avait $\alpha-\alpha' \ne 0$ ou
            $\beta'-\beta \ne 0$ l'on pourrait exprimer l'un de
            $\vect{u}$~et~$\vect{v}$ proportionnellement à l'autre, ce qui n'est
            pas possible puisqu'ils ne sont pas colinéaires. Ainsi $\alpha -
            \alpha' = \beta' - \beta = 0$: en définitive $(\alpha;\beta) =
            (\alpha';\beta')$.\qedhere
    \end{enumerate}
\end{proof}
}

\end{document}
