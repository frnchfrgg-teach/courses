% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usetikzlibrary{datavisualization}

\rivmathlib{stats}

\rivmathsetup{stats/table/values/format=\rivmathsetup{IN-format=\<##1>}#1}

\DeclareMathOperator\cov{cov}

\begin{document}

\StudentMode

\chapter{Statistiques}

\section{Série statistique à deux variables}

\subsection{Définition}

On considère maintenant une population à laquelle on associe \emph{deux}
caractères mesurés simultanément. Si l'on numérote les individus de la
population étudiée, on associe donc à l'individu numéro~$i$ deux valeurs
$x_i$~et~$y_i$.

Bien sûr, on peut toujours étudier la série des~$x_i$ et la série des~$y_i$
séparément, en utilisant les statistiques à une seule variable. L'intérêt de ce
chapitre est d'étudier conjointement les $x_i$~et~$y_i$.

\begin{example}
    On a demandé à $20$~personnes leur âge et leur temps approximatif
    d'utilisation d'Internet par jour, en heures. On obtient les résultats
    suivants:

    \begin{tabular}{l*{10}{Mc}}
        \firsthline
        Âge (années)             & 10 & 12     & 16 & 17     & 18 & 21 &
                                   23     & 25 & 28 & 29 \\
        Temps d'utilisation (h)  & 0  & \<0.5> & 2  & \<2.5> & 2  & 3  &
                                   \<2.5> & 2  & 2  & \<1.5> \\
        \lasthline
    \end{tabular}

    \begin{tabular}{l*{10}{Mc}}
        \firsthline
        Âge (années)             & 31     & 35 & 38 & 41     & 44 & 48 &
                                   51     & 60 & 66 & 70 \\
        Temps d'utilisation (h)  & \<1.5> & 1  & 2  & \<1.5> & 1  & \<0.5> &
                                   \<0.5> & 3  & 0  & 0 \\
        \lasthline
    \end{tabular}

    \begin{enumerate}
        \item \begin{enumerate}
                \item Calculer l'âge médian des personnes interrogées, l'écart
                    interquartile, ainsi que l'âge moyen et l'écart-type.
                    \IfStudentT{\bigfiller{2}}
                \item Les âges des personnes interrogées sont-ils homogènes ou
                    très divers ? Sont ils bien répartis ?
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
        \item \begin{enumerate}
                \item Calculer le temps médian d'utilisation d'internet, l'écart
                    interquartile, le temps moyen et l'écart-type.
                    \IfStudentT{\bigfiller{2}}
                \item Interpréter.
                    \IfStudentT{\bigfiller{4}}
            \end{enumerate}
    \end{enumerate}
\end{example}

\begin{remark}
    On n'utilise presque jamais les tableaux d'effectifs dans le cadre de séries
    à deux variables. Pourquoi ?
    \IfStudentT{\bigfiller{3}}
\end{remark}

\subsection{Représentation graphique}

On choisit une représentation graphique qui explicite le lien entre
$x_i$~et~$y_i$ qu'est l'individu numéro~$i$.

\begin{definition}
    On appelle \define{nuage de points} de la série $(x, y)$ l'ensemble des
    points $M_i(x_i; y_i)$.
\end{definition}

\begin{example}
    Représenter la série précédente avec un nuage de points.\\
    \begin{tikzpicture}[x=0.1cm]
        \draw[help lines] (0,0) grid[xstep=5, ystep=0.5] (100,7);
    \end{tikzpicture}
\end{example}

\subsection{Point moyen}

On note $\bar{x}$~et~$\bar{y}$ les moyennes respectives des séries $x = (x_i)$
et $y = (y_i)$ considérées séparément comme des séries à une variable.

\begin{definition}
    Le \define{point moyen} de la série~$(x,y)$ est le
    point~$M(\bar{x}; \bar{y})$.
\end{definition}

\begin{example}
    Placer le point moyen de la série précédente sur le graphique.
\end{example}

\bigfiller{6}

\TeacherMode

\section{Corrélation}

\subsection{Covariance}

\begin{recall}
    La \emph{variance} d'une série statistique est définie par
    \[ v(x) =
        \GHOST{\frac{n_1 (x_1 - \bar{x})^2 + \dots + n_k (x_k - \bar{x})^2}{N}}
    \]
    c'est-à-dire
    \[ v(x) =
        \GHOST{\frac{1}{N} \sum_{i=1}^k n_i (x_i - \bar{x}) (x_i - \bar{x})}
    \]
\end{recall}

\begin{definition}
    On appelle \define{covariance} de $x$~et~$y$ le nombre réel
    \[ \cov(x,y) =
        \GHOST{\frac{1}{N} \sum_{i=1}^k n_i (x_i - \bar{x}) (y_i - \bar{y})}
    \]
\end{definition}

\begin{remark}
    En particulier, $\cov(x,x) = \GHOST{v(x) = \sigma(x)^2}$.
\end{remark}

\subsection{Coefficient de corrélation}

\begin{definition}
    Le \define{coefficient de corrélation} de $x$~et~$y$ est le nombre réel
    $R = \dfrac{\cov(x,y)}{\sigma(x) \sigma(y)}$.
\end{definition}

\begin{remark}
    Soit $(x,y)$ une série statistique à deux variables.
    \begin{enumerate}
        \item Dans quel cas $x$~et~$y$ ont-ils un lien le plus étroit possible ?
            \IfStudentT{\bigfiller{2}}
        \item Calculer le coefficient de corrélation dans ce cas.
            \IfStudentT{\bigfiller{2}}
        \item D'après le TP, le signe de~$R$ dépend il de l'importance de la
            corrélation entre $x$~et~$y$ ? À quoi peut on relier ce signe ?
            \IfStudentT{\bigfiller{2}}
        \item \begin{enumerate}
                \item D'après le TP, de quel réel $R$~est-il proche quand les
                    données n'ont aucun lien entre elles ?
                    \IfStudentT{\bigfiller{1}}
                \item Quand peut on considérer que les données ont un lien de
                    corrélation ?
                    \IfStudentT{\bigfiller{2}}
            \end{enumerate}
    \end{enumerate}
\end{remark}

\subsection{Régression linéaire}

On parle aussi d'ajustement affine par la méthode des moindres carrés.

\bigfiller{2}




\end{document}
