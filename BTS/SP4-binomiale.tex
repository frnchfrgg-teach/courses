% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{graphs,graphdrawing}
\usegdlibrary{trees}

\rivmathlib{geometry}

\usepackage{sesacompat}
\usepackage{multicol}

\begin{document}
\StudentMode

\chapter{Loi binomiale}

\section{Arbres de probabilité}

\begin{activity}
    \label{activ:qcm}
    À un devoir surveillé de Mathématiques, il y a un questionnaire à choix
    multiples. Chacune des quatre questions a une unique bonne réponse parmi
    trois propositions. Choisir la bonne réponse rapporte un point, tandis que
    choisir la mauvaise, plusieurs, ou ne pas répondre ne rapportent ni
    n'enlèvent aucun point.

    Un élève, n'ayant pas appris sa leçon, décide de répondre totalement au
    hasard à toutes les questions. On note $X$~la variable aléatoire donnant le
    nombre de points obtenus au Q.C.M.

    \begin{enumerate}
        \item Représenter la situation par un arbre de probabilités --- on
            pourra considérer les évènements $J$~(l'élève a juste à la question)
            et $F$~(l'élève a faux ou n'a pas répondu).
        \item Par quoi a-t-on représenté les issues ? Combien y en a-t-il ?
        \item \begin{enumerate}
                \item Déterminer la probabilité d'avoir tout juste.
                \item Déterminer la probabilité d'avoir juste aux trois
                    premières questions mais faux à la dernière.
            \end{enumerate}
        \item \begin{enumerate}
                \item Déterminer $p(X=3)$.
                \item Déterminer la loi de probabilités de~$X$.
            \end{enumerate}
        \item Calculer puis interpréter l'espérance de~$X$.
        \item Et si le Q.C.M. comporte dix questions ?
    \end{enumerate}
\end{activity}

\begin{remark}[Rappels]
    On peut représenter une situation aléatoire sous forme d'arbre. Dans ce cas:
    \begin{itemize}
        \item Les issues sont représentées par les branches de l'arbre.
        \item Si l'on parcourt une branche en partie seulement en rencontrant
            les évènements $A_1$, $A_2$, \dots, le sous-arbre qui est accroché à
            cette branche représente les évènements qui peuvent se produire
            quand on sait avec certitude que $A_1$, $A_2$, \dots\ se sont
            produits.
        \item La probabilité d'une issue est le produit des
            probabilités\footnote{Ce sont en fait des probabilités
            conditionnelles que l'on reverra en Terminale.} rencontrées le long
            de la branche.
        \item La probabilité d'un évènement est la somme des probabilités des
            issues qui le réalisent.
    \end{itemize}
\end{remark}

\section{Loi binomiale}

\subsection{Loi de \bsc{Bernoulli}}

\begin{definition}
    On dit qu'une expérience aléatoire est une \define{épreuve de
    \bsc{Bernoulli}} si elle a deux issues, l'une appelée «succès» par
    convention et notée~$S$, l'autre appelée «échec» et notée~$\bar{S}$.

    Plus généralement, on considère qu'une expérience est une épreuve de
    \bsc{Bernoulli} si on peut lui associer un évènement~$S$ de probabilité~$p$.
\end{definition}

\begin{definition}
    Soit une épreuve de \bsc{Bernoulli} avec $p(S) = p$. Alors $p(\bar{S}) = 1
    -p$. La variable aléatoire~$X$ qui vaut~$1$ en cas de succès et $0$~en cas
    d'échec admet la loi de probabilité suivante:
    \[ \begin{array}{ccc}
            \firsthline
            x & 0 & 1 \\
            p(X\mathop{=}x) & p & 1-p \\
            \lasthline
    \end{array} \]
    Cette loi de probabilité est appelée \define{loi de \bsc{Bernoulli} de
    paramètre~$p$}.
\end{definition}

\begin{example}
    Répondre au hasard à \emph{une} question du Q.C.M. de \cref{activ:qcm}
    constitue \UGHOST{une épreuve de \bsc{Bernoulli} où $p=\frac{1}{3}$.}
\end{example}

\subsection{Schéma de \bsc{Bernoulli}}

\begin{definition}
    Répéter $n$~fois de façon \emph{indépendante} de la \emph{même} épreuve de
    \bsc{Bernoulli} construit une nouvelle expérience aléatoire, appelée
    \define{schéma de \bsc{Bernoulli}} de paramètres $n$~et~$p$.
\end{definition}

\begin{remark}
    Puisque les répétitions sont identiques et indépendantes, la structure de
    l'arbre correspondant au schéma est la même à tous les étages --- voir
    \vref{fig:tree}.
\end{remark}

\begin{figure}
    \ffigbox{}{
        \begin{subfloatrow}%
        \ffigbox{}{\caption{}\label{fig:tree3}%
            \begin{tikzpicture}
                \graph[tree layout,grow'=right,
                        %sibling distance=0pt,sibling sep=0pt,
                        level distance=4em,
                    ] {
                    r/ -- {
                        s/$S$ -- {
                            ss/$S$ -- {
                                sss/$S$,
                                sse/$\bar{S}$
                            },
                            se/$\bar{S}$ -- {
                                ses/$S$,
                                see/$\bar{S}$
                            }
                        },
                        e/$S$ -- {
                            es/$S$ -- {
                                ess/$S$,
                                ese/$\bar{S}$
                            },
                            ee/$\bar{S}$ -- {
                                ees/$S$,
                                eee/$\bar{S}$
                            }
                        }
                    }
                };
            \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:tree4}
            \begin{tikzpicture}
                \graph[tree layout,grow'=right,
                        sibling distance=0pt,sibling sep=0pt,
                        level distance=3em,
                    ] {
                    r/ -- {
                        s/$S$ -- {
                            ss/$S$ -- {
                                sss/$S$ -- {
                                    ssss/$S$,
                                    ssse/$\bar{S}$
                                },
                                sse/$\bar{S}$ -- {
                                    sses/$S$,
                                    ssee/$\bar{S}$
                                }
                            },
                            se/$S$ -- {
                                ses/$S$ -- {
                                    sess/$S$,
                                    sese/$\bar{S}$
                                },
                                see/$\bar{S}$ -- {
                                    sees/$S$,
                                    seee/$\bar{S}$
                                }
                            }
                        },
                        e/$S$ -- {
                            es/$S$ -- {
                                ess/$S$ -- {
                                    esss/$S$,
                                    esse/$\bar{S}$
                                },
                                ese/$\bar{S}$ -- {
                                    eses/$S$,
                                    esee/$\bar{S}$
                                }
                            },
                            ee/$S$ -- {
                                ees/$S$ -- {
                                    eess/$S$,
                                    eese/$\bar{S}$
                                },
                                eee/$\bar{S}$ -- {
                                    eees/$S$,
                                    eeee/$\bar{S}$
                                }
                            }
                        }
                    }
                };
            \end{tikzpicture}
        }
    \end{subfloatrow}%
    \caption{Représentation d'un schéma de \bsc{Bernoulli} avec un arbre de
        probabilités:
        \subref{fig:tree3} avec $n = 3$; \subref{fig:tree4} avec $n=4$.
    }
    \label{fig:tree}
    }
\end{figure}

\begin{example}
    Répondre au hasard au Q.C.M. de \cref{activ:qcm} constitue \UGHOST{un
    schéma de \bsc{Bernoulli} de paramètres $n=4$ et $p=\frac{1}{3}$. En
    effet les répétitions sont identiques et indépendantes puisque l'élève
    répond totalement au hasard.}
\end{example}

\begin{definition}
    La variable aléatoire~$X$ qui donne le nombre de succès obtenus lors d'un
    schéma de \bsc{Bernoulli} de paramètres $n$~et~$p$ suit une loi de
    probabilité appelée \define{loi binomiale} avec les mêmes paramètres. On
    note cette loi $\mcB(n;p)$.
\end{definition}

\begin{example}
    Le nombre de points obtenus dans \cref{activ:qcm} suit \UGHOST{une loi
    binomiale de paramètres $n=4$ et $p=\frac{1}{3}$.}
\end{example}

\begin{remark}
    Comme on a vu dans \cref{activ:qcm}, toutes les branches avec $k$~succès ont
    la même probabilité. Ainsi:
    \[
        p(X\mathop{=}k) = p^k \times (1-p)^{n-k}
                            \times \text{nombre de branches}
    \]
    Reste à compter les branches\dots
\end{remark}

\end{document}
