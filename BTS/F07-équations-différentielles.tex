% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\begin{document}

\chapter{Équations différentielles}

\begin{activity}
    \begin{questions}
        \item Comment trouver une fonction $f$ définie sur $\mdR$ telle que\\
            $\forall x \in \mdR, f'(x) = 5x^3 + 2$ ?
            \bigfiller{2}
        \item \begin{enumerate}
                \item Existe-t-il une fonction $g:\mdR\to\mdR$ telle que
                    $\forall x \in \mdR, g'(x) = g(x)$ ?
                    \bigfiller{1}
                \item Y en a-t-il plusieurs ?
                    \bigfiller{1}
                \item Et $\forall x \in \mdR, h'(x) = 2 h(x)$ ?
                    \bigfiller{1}
            \end{enumerate}
        \item Soit $k:\mdR\to\mdR,x\mapsto \cos x$.
            \begin{enumerate}
                \item Déterminer la fonction $k'$ dérivée de~$k$.
                    \hfiller
                \item Déterminer la fonction $k''$ dérivée de~$k'$.
                    \hfiller
                \item Vérifier que $\forall x \in \mdR, k''(x) + k(x) = 0$
                    \bigfiller{1}
                \item Trouver d'autres fonctions qui vérifient cette égalité.
                    \bigfiller{2}
            \end{enumerate}
    \end{questions}
\end{activity}

\section{Équations différentielles}

\begin{definition}
    Une \define{équation différentielle} est une équation dont l'inconnue est
    une \emph{fonction}~$f$ qui fait intervenir $f(x)$ mais aussi $f'(x)$ ou
    $f''(x)$ ou même des dérivées d'ordre supérieur.

    Une fonction~$f$ est \define{solution} de l'équation différentielle si:
    \begin{enumerate}
        \item elle est dérivable, sa dérivée l'est aussi, et ainsi de suite
            suffisamment de fois pour que la dérivée $f^{(k)}$ existe bien
            jusqu'à l'ordre maximum de dérivée dans l'équation;
        \item l'équation est vérifiée pour tous les~$x$ de l'ensemble de
            définition de~$f$.
    \end{enumerate}
\end{definition}

\begin{remark}
    Par abus de notation, on omet souvent les «$(x)$» dans la formule de
    l'équation différentielle quand ça ne produit pas d'ambigüité.
\end{remark}

\begin{example}
    \begin{enumerate}
        \item Une solution de l'équation différentielle $f' - 5x^3 - 2 = 0$ est
            \bigfiller{1}
        \item Une solution de l'équation différentielle $f' - f = 0$ est
            \bigfiller{1}
        \item Une solution de $f' - 2f = 0$ est
            \bigfiller{1}
        \item Une solution de $f'' + f = 0$ est
            \bigfiller{1}
    \end{enumerate}
\end{example}

\clearpage

\section[Éq. diff. linéaires du 1\ier ordre]
    {Équations différentielles linéaires du premier ordre}

\begin{definition}
    Une \define{équation différentielle linéaire du premier ordre} est une
    équation de la forme $a f' + b f = c(x)$ avec $a \in \mdR^{*}$, $b \in \mdR$
    et $c:\mdR\to\mdR$.
\end{definition}

\subsection{Équation homogène}

\begin{definition}
    L'\define{équation homogène} associée à $a f' + b f = c(x)$ est l'équation
    $a f' + b f = 0$.
\end{definition}

\begin{theorem}
    \label{thm:first-order-homogeneous}
    L'ensemble des solutions de $(E): a f' + b f = 0$ est l'ensemble des
    fonctions définies sur~$\mdR$ par
    $\displaystyle f(x) = k \e^{\frac{-b}{a}x}$ avec $k\in\mdR$.
\end{theorem}

\begin{proof}
    Bien entendu, si $\displaystyle f(x) = k \e^{\frac{-b}{a}x}$, alors $f$~est
    solution de~$(E)$, puisque
    $\displaystyle f'(x) = -\frac{-b}{a} k \e^{\frac{-b}{a}x}$

    Réciproquement, soit $f$~une solution de~$(E)$. On pose
    $v(x) = \e^{\frac{b}{a}x}$ et $g(x) = f(x) \times v(x)$. Mais alors $v$~est
    dérivable sur~$\mdR$ et $v'(x) = \frac{b}{a}\e^{\frac{b}{a}x}$. Ainsi
    $g$~est dérivable, et
    \begin{align*}
        g'(x) &= f'(x) v(x) + f(x) v'(x) \\
              &= f'(x) \e^{\frac{b}{a}x} + f(x) \frac{b}{a}\e^{\frac{b}{a}x} \\
              &= \frac{\e^{\frac{b}{a}x}}{a}
                    \left( a f'(x) + b f(x) \right)
              &= 0
    \end{align*}
    En définitive, $g$~est une fonction constante, donc $\forall x, g(x) =
    g(0)$, c'est-à-dire $\forall x, f(x) \e^{\frac{b}{a}x} = f(0) \e^0$,
    ou encore $\forall x, f(x) = f(0) \e^{\frac{-b}{a}x}$.

    Nous avons ainsi montré que $f$~a bien la bonne forme, avec $k = f(0)$.
\end{proof}

\subsection{Équation avec second membre}

\begin{theorem}
    \label{thm:first-order}
    Soit $(E): a f' + b f = c(x)$, et $f_0$ une solution de~$(E)$. Alors
    l'ensemble des solutions de~$(E)$ est l'ensemble des fonctions définies par
    $f(x) = f_0(x) + u(x)$ où $u(x)$~est n'importe quelle solution de l'équation
    \emph{homogène} associée à~$(E)$. En d'autres termes, on obtient toutes les
    solutions de~$(E)$ avec les fonctions
    $f:x\mapsto f_0(x) + k \e^{\frac{-b}{a}x}$ avec $k\in\mdR$.
\end{theorem}

\begin{proof}
    TODO
\end{proof}

\subsection{Conditions aux bornes}

TODO

\section[Éq. diff. linéaires d'ordre~$2$]
    {Équations différentielles linéaires d'ordre~$2$}

\begin{definition}
    Une \define{équation différentielle linéaire du premier ordre} est une
    équation de la forme $a f'' + b f = c(x)$ avec $a \in \mdR^{*}$,
    $b \in \mdR$ et $c:\mdR\to\mdR$.
\end{definition}

\end{document}
