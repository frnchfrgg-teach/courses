% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\begin{document}

%\StudentMode

\chapter{Primitives}

\section{Primitives d'une fonction}

\begin{activity}
    \begin{enumerate}
        \item Devinette: «Je pense à une fonction dérivable. Si je dis~$f'(x) =
            2x$, peut-on retrouver la définition de~$f$ ?»
            \IfStudentT{\bigfiller{1}}
        \item Même question pour $g'(x) = 4x$ et $h'(x) = 3x^2 + 1$.
            \IfStudentT{\bigfiller{3}}
        \item Y avait-il d'autres réponses possibles ?
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\begin{definition}
    Soit $f$~une fonction définie sur un intervalle~$I$. On dit que $F$~est une
    \define{primitive} de~$f$ sur~$I$ si
    \begin{enumerate}
        \item $F$~est dérivable sur~$I$;
        \item $F' = f$, c'est-à-dire $F'(x) = f(x)$ pour tout~$x\in I$.
    \end{enumerate}
\end{definition}

\begin{example}
    Soit~$f:\mdR\to\mdR,x\mapsto 3x^2 + 1$. Alors $F:x \mapsto x^3 + x - 42$ est
    une primitive de~$f$ sur~$\mdR$.
\end{example}

\begin{proposition}
    \label{prop:all-antiderivatives}
    Soient $f$~une fonction définie sur~$I$ et $F$~une primitive de~$f$.
    Les primitives de~$f$ sont les fonctions définies sur~$I$
    par~\UGHOST{$x\mapsto F(x) + C$ avec $C\in\mdR$}.
\end{proposition}

\begin{remark}
    Si une fonction a une primitive, alors elle en a une infinité --- autant que
    de nombres réels.
\end{remark}

\begin{proof}
    \UGHOST{%
    Bien entendu, une fonction définie par~$x\mapsto F(x) + C$ est dérivable car
    $F$~l'est et sa dérivée est~$F' = f$. Réciproquement, prenons une
    primitive~$F_1$ de~$f$ et posons $c(x) = F_1(x) - F(x)$. $c$~est dérivable
    car $F_1$~et~$F$ le sont, et $c'(x) = F_1'(x) - F'(x) = f(x) - f(x) = 0$.
    Mais alors $c$~est une constante, c'est-à-dire $c(x) = C \in \mdR$. Ainsi
    $F_1(x) = F(x) + C$.
    }
\end{proof}

\begin{example}
    Les primitives de $f:\mdR\to\mdR,x\mapsto 3x^2 + 1$ sont les fonctions $x
    \mapsto x^3 + x + C$ où $C$~est une constante réelle. Peut-on choisir une
    primitive~$F$ de sorte que~$F(2) = 52$ ?
    \IfStudentT{\bigfiller{8}}
\end{example}

\begin{theorem}
    \label{thm:unique-antiderivative-through-point}
    Soient~$f$ une fonction admettant des primitives sur~$I$, $x_0 \in I$
    et~$y_0 \in \mdR$. Il existe une et une seule primitive de~$f$ telle que
    l'image de~$x_0$ soit~$y_0$.
\end{theorem}

\section{Primitives de référence}

\begin{theorem}
    \label{thm:common-antiderivatives}
    \leavevmode
    \begin{center}
    \everymath{\displaystyle}
    $\begin{array}{L*{3}{cL}}
        \firsthline
        \text{Fonction} & \text{Définie sur} & \text{Primitive} \\
        \hline
        f(x) = 0 & \mdR & F(x) = C \NL
        f(x) = a & \mdR & F(x) = \GHOST{ax + C} \NL
        f(x) = x & \mdR & F(x) = \GHOST{\frac{x^2}{2} + C} \NL
        f(x) = x^n \text{ avec $n \ge 0$} & \GHOST{\mdR} &
                F(x) = \GHOST{\frac{x^{n+1}}{n+1} + C} \NL
        f(x) = \dfrac{1}{x^2} & \GHOST{\begin{gathered}
                    \IN]-\infty;0;[ \text{ ou } \\
                    \IN]0;+\infty;[ \end{gathered}}  &
                F(x) = \GHOST{-\dfrac{1}{x} + C} \NL
        f(x) = x^n \text{ avec $n \le -2$} & \IN]-\infty;0;[
                    \text{ ou } \IN]0;+\infty;[ &
                F(x) = \GHOST{\frac{x^{n+1}}{n+1} + C} \NL
        f(x) = \cos x & \GHOST{\mdR} & F(x) = \GHOST{\sin x + C} \NL
        f(x) = \sin x & \GHOST{\mdR} & F(x) = \GHOST{-\cos x + C} \NL
        f(x) = \dfrac{1}{x} & \GHOST{\IN]0;+\infty;[} & F(x)
                                = \GHOST{\ln x + C} \NL
        f(x) = \e^x & \GHOST{\mdR} & F(x) = \GHOST{\e^x + C} \\
        \lasthline
    \end{array}$
    \end{center}
\end{theorem}

\clearpage

\section{Théorèmes opératoires}

\begin{proposition}
    Si $F$~est une primitive de~$f$ sur~$I$, alors quel que soit~$a\in\mdR$,
    \UGHOST{$aF$~est une primitive de~$af$ sur~$I$}.
\end{proposition}

\begin{proposition}
    Si $F$~et~$G$ sont deux primitives sur~$I$ de $f$~et~$g$ respectivement,
    alors \UGHOST{$F+G$~est une primitive de~$f+g$ sur~$I$}.
\end{proposition}

\begin{proof}
    Dériver.
\end{proof}

\begin{remark}
    Il n'y a pas de théorème opératoire général pour trouver une primitive de
    $f \times g$; la réponse n'est pas $F \times G$ sauf situation très
    particulière\footnote{Si $f = 0$ ou $g = 0$ par exemple.}. De même
    pour~$\frac{f}{g}$ dont $\frac{F}{G}$~n'est pas une primitive, sauf
    accident.
\end{remark}

\begin{example}
    Déterminer les primitives de la fonction~$f$ définie sur~$\mdR$ par $f(x) =
    16x^3 - 3x^2 + 3x - 5$.
\end{example}


\end{document}

