% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usetikzlibrary{datavisualization}

\rivmathlib{stats}
%\rivmathsetup{stats/table/frame=full}

\def\rawpointures{
    37, 43, 41, 36, 39, 36, 41, 45, 46, 43, 43, 42, 40, 37,
    42, 37, 38, 44, 40, 37, 43, 42, 45, 48, 38
}
\StatsSortData \pointures = \rawpointures
\StatsRangeData \pointuresclasses=\pointures
    (\IN[35.5;38.5;[, \IN[38.5;40.5;[, \IN[40.5;43.5;[, \IN[43.5;48.5;[)

\def\monnaie{0=3,0.5=4,1.2=6,2=11,5=6,10=7,15=2,200=1}
\def\classname{première~S\;2}


\rivmathsetup{stats/table/values/format=\rivmathsetup{IN-format=\<##1>}#1}

\begin{document}

\chapter{Statistiques}

\chapterquote{Il y a trois sortes de mensonges : les mensonges, les sacrés
mensonges et les statistiques.}{Mark \bsc{Twain}}

\section{Série statistique}

\subsection{Vocabulaire de base}

\begin{definition}
    Une \define{série statistique} est une liste\footnote{L'ordre n'importe pas,
    mais on tient compte des répétitions.} de résultats issus d'une étude.
    \begin{itemize}
        \item L'ensemble sur lequel porte l'étude est appelé
            \define{population}.
        \item Un élément de cet ensemble est appelé \define{individu}.
        \item Si la population est trop vaste, on restreint l'étude à une partie
            appelée \emph{échantillon}.
    \end{itemize}
\end{definition}

\begin{definition}
    On étudie un \define{caractère} sur les individus. Les valeurs prises par le
    caractère sont appelées \define{modalités} et sont souvent notées~$x_i$.
    L'\define{effectif} d'une modalité est le nombre d'individus dont le
    caractère prend cette valeur.
\end{definition}

\begin{remark}
    L'effectif \emph{total} est le nombre d'individus dans la population.
\end{remark}

\begin{remark}
    Les modalités peuvent être des nombres --- on parle de caractère quantitatif
    --- ou des informations qualitatives --- la couleur des yeux par exemple.
\end{remark}

\begin{define}
    La \define{classe} d'intervalle~$I$ est l'ensemble des individus dont le
    caractère appartient à~$I$. L'effectif d'une classe est le nombre
    d'individus qui la composent.
\end{define}

\begin{definition}
    La \define{fréquence} d'une modalité (ou d'une classe) est \UGHOST{la
    proportion des individus correspondants par rapport à la population
    entière.} C'est le nombre réel $f =
    \UGHOST{\dfrac{\text{effectif}}{\text{effectif total}}}$.
\end{definition}

\begin{remark}
     Une fréquence est toujours comprise entre $0$~et~$1$ (inclus).
\end{remark}

\begin{remark}
    La somme des fréquences des modalités est
    $f_1 + f_2 + \dots + f_k = \GHOST{1}$
\end{remark}

\subsection{Tableau d'effectifs, de fréquence}

On représente souvent une série statistique par son tableau des effectifs, mais
on peut aussi utiliser les fréquences:

\begin{center}
    $\begin{array}[c]{l*{4}{c}}
        \firsthline
        $Modalité$   & x_1 & x_2 & \dotsm & x_k \\
        $Effectif$  & n_1 & n_2 & \dotsm & n_k \\
        \lasthline
    \end{array}$
    \hfil
    $\begin{array}[c]{l*{4}{c}}
        \firsthline
        $Modalité$   & x_1 & x_2 & \dotsm & x_k \\
        $Fréquence$ & f_1 & f_2 & \dotsm & f_k \\
        \lasthline
    \end{array}$
\end{center}


\begin{example}[Pointures des élèves de \classname]
    \leavevmode
    \begin{center}
        \StatsTable \pointures[
            values=Pointure,
            frequencies,
            ghosts=frequencies,
            maxcols=6,
        ]
    \end{center}
\end{example}

Lorsque le nombre de modalités est très grand ou que les valeurs sont continues
--- par exemple le temps de trajet de chez vous au lycée --- on donne plutôt les
effectifs ou les fréquences de classes bien choisies.


\begin{example}[Pointures reloaded]
    \leavevmode
    \begin{center}
        \StatsTable\pointuresclasses[
            values=Pointure,
            ghosts={counts,frequencies},
        ]
    \end{center}
\end{example}


\subsection{Représentation graphique}

\begin{definition}
    Lorsque l'on dispose des effectifs ou fréquences de chaque modalité, on peut
    représenter la série statistique par un \define{diagramme en batons}. À
    chaque modalité correspond un segment vertical, de hauteur proportionnelle à
    l'effectif de la modalité --- ou à sa fréquence, c'est équivalent.
\end{definition}

\begin{example}[Diagramme en bâtons]
    Voir \vref{fig:comb-counts-footsize,fig:comb-freqs-footsize}.
\end{example}

\begin{figure}
    \centering
    \StatsGraph\pointures[values=Pointure, width=8cm,
                                 style=ghost]
    \caption{Diagramme en bâtons de la répartition des pointures dans la
    \classname. Les ordonnées sont les effectifs.}
    \label{fig:comb-counts-footsize}
\end{figure}

\begin{figure}
    \centering
    \StatsGraph\pointures[values=Pointure, width=8cm, frequencies]
    \caption{Diagramme en bâtons de la répartition des pointures dans la
    \classname. Les ordonnées sont les fréquences.}
    \label{fig:comb-freqs-footsize}
\end{figure}

\begin{remark}
    On peut même utiliser cette représentation si le caractère est
    \emph{qualitatif}.
\end{remark}

\begin{definition}
    Lorsque l'on dispose des effectifs ou fréquences par classes, on peut
    représenter la série statistique par un \define{histogramme}. À
    chaque classe d'intervalle~$I$ correspond un rectangle de base~$I$ et dont
    \emph{l'aire} est proportionnelle à l'effectif --- ou la fréquence --- de la
    classe.
\end{definition}

\begin{remark}
    Pour construire le rectangle d'une classe, on peut calculer sa hauteur en
    divisant son aire par la longueur de l'intervalle. En règle générale on
    comptera tout simplement les carreaux.
\end{remark}

\begin{example}[Petits pieds, moyens pieds et grands pieds]
    Voir \vref{fig:hist-counts-footsize}.
\end{example}

\begin{figure}
    \centering
    \StatsGraph\pointuresclasses[values=Pointure, width=8cm,
                                style=ghost,
                                histogram/areas/style=ghost,
                                x/axis = {
                                    grid = { phase=0.5 },
                                    ticks = { minor steps between steps=1 }
                                },
                                histogram/legend={
                                    x=45.5,y=2,label=[ghost]below:1 élève
                                },
                            ]
    \caption{Histogramme de la répartition des pointures dans la \classname. Les
    ordonnées ne correspondent ni aux effectifs, ni aux fréquences. }
    \label{fig:hist-counts-footsize}
\end{figure}

\subsection{Effectifs et fréquences cumulé(e)s}

\begin{definition}
    L'\define{effectif cumulé croissant} correspondant à une modalité~$x_i$ est
    le nombre d'individus dont le caractère est inférieur ou égal à~$x_i$. On le
    calcule en sommant les effectifs de toutes les modalités plus petites
    que~$x_i$ ($x_i$~comprise). La \define{fréquence cumulée croissante}
    s'obtient en divisant l'effectif cumulé croissant par l'effectif total.
\end{definition}

\begin{example}[ECC et FCC]
    Voir \vref{tab:ecc+fcc}.
\end{example}

\begin{table}
    \centering
    \StatsTable \pointures[
        values=Pointure,
        icc,
        frequencies,
        icf=FCC,
        ghosts={icc,icf},
        maxcols=6,
    ]
    \caption{Effectifs et fréquences cumulés croissants. Ici les erreurs
        d'arrondi ne se cumulent pas parce qu'on n'a pas toujours arrondi les
        fréquences au plus proche.}
    \label{tab:ecc+fcc}
\end{table}

Quand la série statistique est donnée par classes, on ne peut déterminer comment
se comportent les effectifs cumulés à au milieu des intervalles mais seulement à
leurs bornes. Cependant, en supposant qu'à l'intérieur d'une classe les
individus sont uniformément répartis\footnote{Cette supposition est fausse bien
entendu mais peut servir d'\emph{approximation}.}, alors les effectifs
cumulés augmentent régulièrement sur toute la classe. On peut ainsi placer sur
un graphique les effectifs (ou fréquences) cumulé(e)s qu'on peut déterminer, et
relier les points obtenus par des \emph{segments}.

\begin{example}[Avec des classes]
    Voir \vref{fig:cuml-counts-footsize}.
\end{example}

\begin{definition}
    L'\define{effectif cumulé décroissant} ou la \define{fréquence cumulée
    décroissante} se définit de la même manière mais en considérant les
    modalités plus grandes ou égales.
\end{definition}

\ExplSyntaxOn
\makeatletter
\def\autostep{\int_eval:n{\tikz@lib@dv@step-1}}
\ExplSyntaxOff
\makeatother

\begin{figure}
    \centering
    \StatsGraph\pointuresclasses[values=Pointure, width=8cm, cumulative,
                                 style=ghost,
                                x/axis={ grid={
                                    minor steps between steps=1} ,
                                },
                             ]
    \caption{Effectifs cumulés croissants des pointures dans la \classname.}
    \label{fig:cuml-counts-footsize}
\end{figure}

\begin{example}[ECD et FCD]
    Voir \vref{tab:ecd+fcd}.
\end{example}

\begin{table}
    \centering
    \StatsTable \pointures[
        values=Pointure,
        dcc,
        frequencies=Fréq. en~$\%$,
        dcf=FCD en~$\%$,
        allfreqs/format/scaled=100,
        ghosts=dcc,
    ]
    \caption{Effectifs et fréquences cumulés décroissants.}
    \label{tab:ecd+fcd}
\end{table}

\begin{example}[Classes toujours]
    Voir \vref{fig:dcuml-freqs-footsize}.
\end{example}

\begin{figure}
    \centering
    \StatsGraph\pointuresclasses[values=Pointure, width=8cm,
                                 frequencies, cumulative, decreasing]
    \caption{Fréquences cumulées décroissantes des pointures dans la \classname.}
    \label{fig:dcuml-freqs-footsize}
\end{figure}


\section{Quelques indicateurs}

\subsection{Indicateurs de position}

On rappelle qu'on utilise les notations suivantes:
    $\begin{array}[c]{l*{4}{c}}
        \firsthline
        $Modalité$   & x_1 & x_2 & \dotsm & x_k \\
        $Effectif$  & n_1 & n_2 & \dotsm & n_k \\
        $Fréquence$ & f_1 & f_2 & \dotsm & f_k \\
        \lasthline
    \end{array}$


\begin{definition}
    La \define{moyenne} de la série $x$ est le nombre réel
    \[ \bar{x}
        = \frac{n_1 \cdot x_1 + n_2 \cdot x_2 + \dots + n_k \cdot x_k}{N}
    \]
    où $N = n_1 + \dots + n_k$ est l'effectif total.
\end{definition}

\begin{remark}
    C'est la valeur qu'auraient toutes les modalités si elles étaient égales.
\end{remark}

\begin{proposition}
    $\displaystyle
        \bar{x} = f_1 \cdot x_1 + f_2 \cdot x_2 + \dotsb + f_k \cdot x_k
    $
\end{proposition}

\begin{proof}
    \GHOST{
    \begin{align*}
        \bar{x} &= \frac{n_1 \cdot x_1 + n_2 \cdot x_2 + \dots
                            + n_k \cdot x_k}{N} \\
                & = \frac{n_1}{N} \cdot x_1 + \frac{n_2}{N} \cdot x_2 + \dots
                            + \frac{n_k}{N} \cdot x_k \\
            & = f_1 \cdot x_1 + f_2 \cdot x_2 + \dots + f_k \cdot x_k
            \qedhere
    \end{align*}
    }
\end{proof}

\begin{remark}
    Et si la série est donnée par classes ? On fait comme si tous les individus
    d'une classe~$\IN[a_i;b_i;[$ ont pour caractère le \emph{centre de la
    classe} $c_i = \frac{a_i+b_i}{2}$.
\end{remark}


\begin{example}
    Déterminer la pointure moyenne des élèves de \classname, à partir des
    modalités puis à partir des classes. Comparer.
    \IfStudentT{\bigfiller{3}}
\end{example}

\begin{example}
    \label{ex:pocket-money}
    Un groupe de collègues voulant acheter des pizzas regarde combien de monnaie
    chacun a dans sa poche. Ils obtiennent les résultats suivants:
    \begin{center}
        \StatsTable\monnaie
            [values=Monnaie,values/format=\SI{#1}{\euro},nocounts,frequencies]
    \end{center}

    \begin{enumerate}
        \item Calculer le montant moyen de monnaie dans la poche d'un collègue.
            \IfStudentT{\bigfiller{1}}
        \item Il y a $40$~collègues. Déterminer les effectifs, puis la monnaie
            moyenne avec une deuxième méthode. Comparer.
            \IfStudentT{
                \par
                \StatsTable\monnaie
                    [values=Monnaie,values/format=\SI{#1}{\euro},
                     nofrequencies, ghosts=counts]
                \bigfiller{1}
            }
    \end{enumerate}
\end{example}

\begin{definition}
    Une \define{médiane} d'une série est un nombre~$M$ qui sépare la population
    en deux groupes de même effectif: d'une part les individus dont le caractère
    est au plus~$M$, et d'autre part ceux dont le caractère est au moins~$M$.

    Autrement dit, c'est la plus petite modalité dont l'effectif cumulé
    croissant est au moins la moitié de l'effectif total\footnote{Si l'effectif
    total~$N$ est \emph{pair} et que l'effectif cumulé de~$x_i$ est exactement
    égal à~$\frac{N}{2}$, alors on prend parfois $M = \frac{x_i + x_{i+1}}{2}$
    mais alors $M$~n'est plus une des modalités.}. C'est aussi la plus petite
    modalité dont la fréquence cumulée croissante est au
    moins~\SI{50}{\percent}.
\end{definition}

\begin{example}
    Déterminer la pointure médiane des élèves de \classname.
    \IfStudentT{\bigfiller{1}}
\end{example}

\begin{example}
    \begin{enumerate}
        \item Déterminer la médiane des montants de monnaie dans les poches des
            collègues de \vref{ex:pocket-money}.
            \IfStudentT{
                \par
                \StatsTable\monnaie
                    [values=Monnaie,values/format=\SI{#1}{\euro},
                     nocounts, icf, ghosts=icf,
                     allfreqs/format/scaled=100,
                    ]
                \bigfiller{1}
            }
        \item Comparer avec la moyenne. Qu'en pensez-vous ?
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{example}

\begin{remark}
    Un maire indique que le salaire moyen dans sa ville est de~\SI{4500}{\euro}.
    Ainsi il affirme que grâce à lui la moitié des administrés gagne
    \SI{4500}{\euro}~ou plus. Qu'en pensez-vous ?
    \IfStudentT{\bigfiller{2}}
\end{remark}

Quand la série statistique est donnée par classes, on ne peut déterminer avec
précision quelle modalité est la médiane, mais seulement dans quelle classe la
médiane se situe. En revanche, la médiane est l'antécédent de~$\<0.5>$ par la
fonction des fréquences cumulées croissantes; cet antécédent peut être lu
graphiquement sur le graphique tracé en supposant que dans chaque classe les
individus sont uniformément répartis. Même si dans la réalité ce n'est pas le
cas, on espère que la médiane ainsi obtenue est suffisamment correcte pour tirer
des conclusions sur la série.

\begin{example}
    Quelle pointure médiane obtient-on si l'on utilise les données par classe ?
    Est-elle loin du compte ? Comment expliquer la différence ?
    \IfStudentT{\bigfiller{3}}
\end{example}

\subsection{Quartiles}

\TeacherMode

\begin{definition}
    Le \define{premier quartile} d'une série statistique, noté~$Q_1$, est
    \UGHOST{la plus petite modalité dont l'effectif cumulé croissant est au
    moins le quart de l'effectif total}. C'est aussi la plus petite modalité
    dont la fréquence cumulée croissante est \UGHOST{au
    moins~\SI{25}{\percent}}.
\end{definition}

\begin{definition}
    Le \define{troisième quartile} d'une série statistique, noté~$Q_3$, est la
    plus petite modalité dont l'effectif cumulé croissant est au moins trois
    quarts de l'effectif total. C'est aussi la plus petite modalité dont la
    fréquence cumulée croissante est \UGHOST{au moins~\SI{75}{\percent}}.
\end{definition}

On les détermine de la même façon que la médiane.

\StudentMode

\begin{example}
    Déterminer les quartiles des pointures de la \classname.
    \IfStudentT{\bigfiller{2}}
\end{example}

\begin{example}
    Déterminer les quartiles des montants de \vref{ex:pocket-money}.
    \IfStudentT{\bigfiller{2}}
\end{example}

\subsection{Indicateurs de dispersion}

\begin{definition}
    L'\define{étendue} d'une série statistique est la différence entre la plus
    grande modalité et la plus petite\footnote{Attention, ce n'est pas la
    différence entre les effectifs extrêmes, ni entre les modalités
    correspondantes !}.
\end{definition}

\begin{definition}
    L'\define{écart inter-quartile} est la différence~$Q_3 - Q_1$.
\end{definition}

\begin{remark}
    Environ la moitié de la population a son caractère dans l'intervalle
    inter-quartile~$\IN[Q_1;Q_3;]$. L'écart inter-quartile est donc l'étendue
    d'environ la moitié de la population, obtenue en éliminant les quarts
    extrêmes.
\end{remark}

\begin{example}
    Déterminer l'étendue et l'écart inter-quartile des pointures de la
    \classname.
    \IfStudentT{\bigfiller{1}}
\end{example}

\begin{example}
    Déterminer l'étendue et l'écart inter-quartile des montants de
    \vref{ex:pocket-money}. L'étendue est-elle à peu près le double
    de~$Q_3 - Q_1$ ? Comment l'expliquer ?
    \IfStudentT{\bigfiller{2}}
\end{example}

\StudentMode

\section{Variance et écart-type}

\subsection{Score moyen et écarts à la moyenne}

\begin{activity}
    Un groupe de joueurs à League of Legends décide d'organiser un tournoi:
    ils comptent le nombre de victoires que chaque joueur a obtenu.
    Les résultats sont les suivants:
    \[\begin{array}{Tl*{6}{c}}
        \firsthline
        Nombre de victoires & 0 & 2 & 3 & 5 & 6 & 8 \\
        Nombre de joueurs   & 3 & 1 & 4 & 2 & 4 & 2 \\
        \lasthline
    \end{array}\]
    \begin{enumerate}
        \item Quels sont le caractère et la population étudiés ? Combien y
            a-t-il de joueurs dans le groupe ? Combien de parties ont-ils
            jouées ?
            \IfStudentT{\bigfiller{3}}
        \item Quel est le nombre moyen de victoires par joueur ?
            \IfStudentT{\bigfiller{2}}
        \item Les joueurs décident de définir un \emph{score} qui se calcule en
            effectuant la soustraction $x_i - \bar{x}$, c'est-à-dire en
            calculant la différence avec la moyenne.
            \begin{enumerate}
                \item Compléter les trois premières lignes du tableau suivant:
                    \[\cellprops{array td {min-width: 2em}}
                        \begin{array}{LTl*{6}{LGc}L}
                        \firsthline
                        Nombre de victoires & 0  & 2  & 3  & 5 & 6 & 8  \NL
                        Nombre de joueurs   & 3  & 1  & 4  & 2 & 4 & 2  \NL
                        Score obtenu        & -4 & -2 & -1 & 1 & 2 & 4  \NL
                        \GHOST{$\text{Score}^2$}
                                            & 16 & 4  & 1  & 1 & 4 & 16  \\
                        \lasthline
                    \end{array}\]
                \item Quel est le score moyen ? Comment l'expliquer ?
                    \IfStudentT{\bigfiller{3}}
            \end{enumerate}
        \item Les joueurs décident alors de mettre les scores au carré.
            Les inscrire dans la dernière ligne du tableau.
        \item Calculer la moyenne de ces carrés. A-t-on résolu le problème
            précédent ?
            \IfStudentT{\bigfiller{3}}
    \end{enumerate}
\end{activity}

\subsection{Variance}

On reprend les notations $x_i$ pour les modalités, $n_i$ pour les effectifs et
$f_i$ pour les fréquences ($i = 1, \dots, k$).

\begin{definition}
    On appelle \define{variance de~$x$} le nombre réel positif ou nul
    \[ v(x) =
        \GHOST{\frac{n_1 (x_1 - \bar{x})^2 + \dots + n_k (x_k - \bar{x})^2}{N}
            = \frac{1}{N} \sum_{i=1}^k n_i (x_i - \bar{x})^2}
    \]
\end{definition}

\begin{proposition}
    $\displaystyle
       v(x) = f_1 (x_1 - \bar{x})^2 + \dots + f_k (x_k - \bar{x})^2
            = \sum_{i=1}^k f_i (x_i - \bar{x})^2$.
\end{proposition}

\begin{remark}
    Autrement dit, $v(x)$~se calcule avec l'algorithme suivant:
    \begin{enumerate}
        \item Calculer la moyenne~$\bar{x}$.
        \item Pour chaque modalité, \UGHOST{calculer l'écart $x_i - \bar{x}$.}
        \item Pour chaque modalité, \UGHOST{mettre l'écart au carré:}\\
            \UGHOST{$(x_i - \bar{x})^2$.}
        \item Calculer \UGHOST{la moyenne de ces carrés} --- avec les mêmes
            effectifs ou fréquences que pour~$x$.
    \end{enumerate}
\end{remark}

\subsection{Écart-type}

\begin{definition}
    On appelle \define{écart-type de~$x$} le nombre réel $\sigma(x) =
    \sqrt{v(x)}$. L'écart-type~$\sigma(x)$ est toujours positif ou nul.
\end{definition}

\end{document}
