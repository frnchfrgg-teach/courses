% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\begin{document}

\chapter{Limites de fonctions}

\chapterquote{Only two things are infinite, the universe and human stupidity.
But I'm not sure about the former.}{Albert \bsc{Einstein}}

\section{Limite à l'infini}

\subsection{Limite finie en~$±∞$}

\begin{definition}
    Soit~$f$ définie sur $I=\IN[a;+\infty;[$ ou $I=\IN]a;+\infty;[$. On dit que
    $f$ admet une \define{limite~$\ell\in\mdR$ en $+\infty$}, et on écrit
    $\lim_{x \to +\infty} f(x) = \ell$ si (et seulement si) $f(x)$~devient et
    reste aussi près qu'on veut de~$\ell$ pourvu qu'on prenne $x$~assez
    grand.
\end{definition}

\begin{remark}
    $\lim_{x \to +\infty} f(x) = \ell$ se lit «la limite de~$f(x)$
    quand $x$~tend vers~$+\infty$ est~$\ell$». On écrit aussi $\displaystyle
    f(x) \tendsto[x \to +\infty] \ell$, qui se lit «$f(x)$~tend
    vers~$\ell$ quand $x$~tend vers~$+\infty$».
\end{remark}

\begin{definition}
    Soit~$f$ définie sur $I=\IN[-\infty;a;[$ ou $I=\IN]-\infty;a;[$. On dit que
    $f$ admet une \define{limite~$\ell\in\mdR$ en $-\infty$}, et on écrit
    $\lim_{x \to -\infty} f(x) = \ell$ si (et seulement si) $f(x)$~devient et
    reste aussi près qu'on veut de~$\ell$ pourvu qu'on prenne $x$~assez
    grand.
\end{definition}

\subsection{Limite infinie en $±∞$}

\begin{definition}
    On dit que $f$~admet une \define{limite $+\infty$ en $+\infty$} et on note
    $\lim_{x\to+\infty} f(x) = +\infty$ si $f(x)$~dépasse
    n'importe quel «plafond»~$A$ pourvu que~$x$ devienne assez grand.
\end{definition}

\begin{definition}
    On dit que $f$~admet une \define{limite $-\infty$ en $+\infty$} et on note
    $\lim_{x\to+\infty} f(x) = -\infty$ si $f(x)$~devient
    inférieure à n'importe quel seuil~$A$ pourvu que~$x$ devienne assez grand.
\end{definition}

On a les définitions équivalentes pour les limites en~$-\infty$.


\subsection{Limites de référence}

\begin{proposition}
    Soit $f$~définie par~$f(x) = A$ avec $A\in\mdR$. Alors
    $\lim_{x \to +\infty} f(x) = A$.
\end{proposition}

\begin{proposition}
    Soit un entier $n \ge 1$. Alors
    $\lim_{x \to +\infty} \frac{1}{x^n} = 0$ et
    $\lim_{x \to -\infty} \frac{1}{x^n} = 0$.
\end{proposition}

\begin{proposition}
    $\lim_{x \to +\infty} \frac{1}{\sqrt{x}} = 0$.
\end{proposition}

\begin{proposition}
    Plus généralement, $\displaystyle \begin{aligned}[t]
        \lim_{x\to+\infty} x^n &= +\infty \text{ tandis que} \\
        \lim_{x\to-\infty} x^n &= \begin{cases}
                                    +\infty &\text{si $n$ est pair} \\
                                    -\infty &\text{si $n$ est impair}
                                \end{cases}
    \end{aligned}$
\end{proposition}

\begin{example}[Recherche de seuil]
    Puisque $\lim_{x \to +\infty} \frac{1}{x} = 0$, alors $\dfrac{1}{x}$~sera
    aussi près qu'on veut de~$0$ dès que $x$~sera assez grand. À partir de
    quelle valeur de~$x$ a-t-on $\dfrac{1}{x} \leq \<0.001>$ ? À partir de quand
    a-t-on $\dfrac{1}{x} \leq 10^{-25}$ ?
\end{example}

\begin{remark}
    Ne Pas Confondre:\\
    $10^{-20} = \<0.00000000000000000001>$\\
    $-10^{20} = -\<100000000000000000000>$
\end{remark}


\subsection{Asymptote horizontale}

\begin{definition}
    Si $\lim_{x \to \alpha} f(x) = \ell$, alors quand $x$~tend
    vers~$\alpha$ la courbe de~$f$ est de plus en plus proche de la droite
    horizontale d'équation $y=\ell$. On dit que la droite d'équation~$y=\ell$
    est une \define{asymptote horizontale} de la courbe de~$f$.
\end{definition}

\begin{example}
    La droite d'équation~$y=0$ est une asymptote horizontale à la courbe
    représentative de la fonction inverse.
\end{example}

\iffalse
\begin{example}
    Reprenons la fonction de l'exemple~\ref{example:product}.
    La droite d'équation~$y=18$ est une asymptote horizontale à la courbe
    représentative de la fonction~$h$.
\end{example}
\fi

\begin{remark}
    Les fonctions $\sin$~et~$\cos$ ne ressemblent jamais à une droite: on admet
    qu'elles n'ont pas d'asymptote horizontale, et pas de limite
    en~$\pm\infty$.
\end{remark}


\subsection{Théorèmes opératoires}

Dans la suite de cette partie,, $\alpha$ indiquera $+\infty$~ou~$-\infty$ tandis
que $f_1$~et~$f_2$ seront deux fonctions définies sur un intervalle~$I$ dont
$\alpha$~est une borne (par exemple $I = \left] -\infty \mathrel; 5 \right[$
si $\alpha$~est~$-\infty$).


\begin{theorem}[Limite d'une somme]
    Si $\lim_{x \to \alpha} f_1(x) = \ell_1$ et $\lim_{x \to \alpha} f_2(x) =
    \ell_2$, alors $\lim_{x \to \alpha} \left( f_1(x) + f_2(x) \right)$ se lit
    dans le tableau suivant:
    \def\FI{\textit{\textbf{F.I.}}}
    \[\begin{array}{l*{3}{c}}
            \firsthline
                    & \ell_1 \in \mdR & \ell_1 = +\infty & \ell_1 = -\infty \\
            \hline
            \ell_2 \in \mdR & \ell_1+\ell_2 & +\infty & -\infty \\
            \ell_2 = +\infty &      +\infty & +\infty & \FI \\
            \ell_2 = -\infty &      -\infty & \FI     & -\infty \\
            \lasthline
    \end{array}\]
\end{theorem}


\begin{theorem}[Limite d'un produit]
    Si $\lim_{x \to \alpha} f_1(x) = \ell_1$ et $\lim_{x \to \alpha} f_2(x) =
    \ell_2$, alors $\lim_{x \to \alpha} \left( f_1(x) f_2(x) \right)$ se lit
    dans le tableau suivant --- dans les cas $\pm\infty$, le signe de la limite
    se détermine avec la règle du signe d'un produit:
    \def\FI{\textit{\textbf{F.I.}}}
    \[\begin{array}{l*{3}{c}}
            \firsthline
                             & \ell_1 \in \mdR\setminus\left\{ 0 \right\} &
                                  \ell_1 = 0 & \ell_1 = \pm\infty \\
            \hline
            \ell_2 \in \mdR\setminus\left\{ 0 \right\} & \ell_1\times\ell_2 &
                                    0 & \pm\infty \\
            \ell_2 = 0       &  0 & 0 & \FI \\
            \ell_2 = \pm\infty & \pm\infty & \FI & \pm\infty  \\
            \lasthline
    \end{array}\]
\end{theorem}

\begin{theorem}[Limite d'un quotient]
    Si $\lim_{x \to \alpha} f_1(x) = \ell_1$ et $\lim_{x \to \alpha} f_2(x) =
    \ell_2$, alors $\lim_{x \to \alpha} \dfrac{f_1(x)}{f_2(x)}$ se lit
    dans le tableau suivant --- dans les cas $\pm\infty$, le signe de la limite
    se détermine avec la méthode qui suit le théorème:
    \def\FI{\textit{\textbf{F.I.}}}
    \[\begin{array}{l*{3}{c}}
            \firsthline
                             & \ell_1 \in \mdR\setminus\left\{ 0 \right\} &
                                  \ell_1 = 0 & \ell_1 = \pm\infty \\
            \hline
            \ell_2 \in \mdR\setminus\left\{ 0 \right\} & \frac{\ell_1}{\ell_2}&
                                    0 & \pm\infty \\
            \ell_2 = 0       &  \pm\infty & \FI & \pm\infty \\
            \ell_2 = \pm\infty & 0 & 0 & \FI  \\
            \lasthline
    \end{array}\]
\end{theorem}



\end{document}

\subsection{Lever les formes indéterminées}

\begin{example}
    \label{example:product}
    Déterminer la limite en $+\infty$ de~$h(x) =
        \dfrac{(5+6x)(3\sqrt{x} - 1)}{x\sqrt{x}}$.
\end{example}


\section{Limite en $a \in\mdR$}


\begin{example}
    \begin{enumerate}[gathered,display]
        \item $\lim_{x\to 1}(x^2+3x+2)$
        \item $\lim_{x\to 1} \frac{x^2-2x+1}{x-1}$
        \item $\lim_{x\to 0} \frac{3x+1}{x}$
        \item $\lim_{x\to 2} \frac{1}{x-2}$
        \item $\lim_{x\to -8} \frac{3x^2+2x-5}{(x+8)^2}$
    \end{enumerate}
\end{example}

