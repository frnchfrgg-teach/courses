% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}
\usepackage{sesacompat}

\persocourssetup{qrcodes}

\begin{document}

\chapter{Calcul vectoriel}

\section{Définition, somme et coordonnées}

\subsection{Translations et vecteurs}

\begin{definition}
    La translation de $A$~vers~$B$, ou \define{translation de
    vecteur~$\vect{AB}$}, est la transformation du plan définie de la façon
    suivante: l'image du point~$M$ est le symétrique~$M'$ de~$A$ par rapport au
    milieu de~$[BM]$. Autrement dit, l'image de~$M$ est le point~$M'$ tel que
    $[AM']$~et~$[BM]$ aient le même milieu. Si $M$~n'est pas aligné avec
    $A$~et~$B$ cela revient à placer~$M$ de sorte à ce que $ABM'M$ soit un
    parallélogramme (voir \vref{fig:def-translate}).
\end{definition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}
        \ffigbox{}{\caption{}\label{fig:def-translate-1}
        \begin{tikzpicture}[baseline=(I)]
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (2,0.4)   coordinate[label=above:$B$] (B);
            \path (1.5,-1)  coordinate[label=below:$M$] (M);
            \path ($ (B)!0.5!(M) $) coordinate[label=above left:$I$] (I);
            \path ($ (I)!-1!(A) $)  coordinate[label=below:$M'$] (M');

            \foreach[remember=\y as \x (initially B)] \y in {M',M,A} {
                \draw[dashed] (\x) -- (\y);
            }
            \draw[->] (A) -- (B);
            \draw   (I) edge pic[dist marker=1] {} (A)
                        edge pic[dist marker=1] {} (M')
                        edge pic[dist marker=2] {} (B)
                        edge pic[dist marker=2] {} (M);
        \end{tikzpicture}
        }
        \ffigbox{}{\caption{}\label{fig:def-translate-2}
        \begin{tikzpicture}[baseline=(J)]
            \path (0,0)     coordinate[label=below:$A$] (A);
            \path (2,0.4)   coordinate[label=below:$B$] (B);
            \path ($ (A)!1.5!(B) $) coordinate[label=below:$N$]  (N);
            \path ($ (B)!0.5!(N) $) coordinate[label=below:$J$]  (J);
            \path ($ (J)!-1!(A) $)  coordinate[label=below:$N'$] (N');

            \draw[->] (A) -- pic[tick marker] {} (B) pic[tick marker] {};
            \draw   (B) -- (J) pic[tick marker] {}
                    -- (N) pic[tick marker] {}
                    -- (N') pic[tick marker] {};
            \path[every edge/.style={draw=none}]
                    (J) edge pic[dist marker=1] {} (A)
                        edge pic[dist marker=1] {} (N')
                        edge pic[dist marker=2] {} (B)
                        edge pic[dist marker=2] {} (N);
        \end{tikzpicture}
        }
    \end{subfloatrow}
    \caption{Construction de deux translatés par la translation de
        vecteur~$\vect{AB}$.
        \subref{fig:def-translate-1} $M'$~est l'image de~$M$: $[AM']$~et~$[BM]$
        ont le même milieu~$I$.
        \subref{fig:def-translate-2} $N'$~est l'image de~$N$: $[AN']$~et~$[BN]$
        ont le même milieu~$J$.}
    \label{fig:def-translate}
    }
\end{figure}

\begin{proposition}
    \label{prop:vector-equality}
    \UGHOST{%
    $\vect{AB} = \vect{CD}$ si et seulement si $[AD]$~et~$[BC]$ ont le même
    milieu. Autrement dit $\vect{AB} = \vect{CD}$ si et seulement si $ABDC$ est
    un parallélogramme --- éventuellement plat (voir
    \vref{fig:prop-vector-equality}).}
\end{proposition}

\begin{figure}
    \begin{tikzpicture}
        \path (0,0)     coordinate[label=above:$A$] (A);
        \path (4,-1)    coordinate[label=above:$B$] (B);
        \path (1,-1.5)  coordinate[label=below:$C$] (C);
        \path ($ (B)!0.5!(C) $) coordinate (I);
        \path ($ (I)!-1!(A) $)  coordinate[label=below:$D$] (D);

        \draw[dashed] (A) -- (C) (D) -- (B);
        \draw[->] (A) -- (B);
        \draw[->] (C) -- (D);
        \draw   (I) edge pic[dist marker=1] {} (A)
                    edge pic[dist marker=1] {} (D)
                    edge pic[dist marker=2] {} (B)
                    edge pic[dist marker=2] {} (C);
    \end{tikzpicture}
    \caption{Condition d'égalité: $\vect{AB} = \vect{CD}$ si et seulement si
        $ABDC$ est un parallélogramme.}
    \label{fig:prop-vector-equality}
\end{figure}

\begin{definition}
    Le \define{vecteur nul} est le vecteur~$\vect{0}$ dont la translation ne
    déplace aucun point du plan. On a $\vect{AA} = \vect{BB} = \vect{CC} =
    \vect{0}$ pour tous $A$,~$B$ et~$C$.
\end{definition}

\subsection{Sommes de vecteurs}

\begin{definition}
    Soient $\vect{u}$~et~$\vect{v}$ deux vecteurs. En effectuant \emph{à la
    suite} les translations de vecteurs $\vect{u}$~puis~$\vect{v}$, l'opération
    obtenue est \UGHOST{encore une translation. Le vecteur de cette translation
    est noté $\vect{u} + \vect{v}$ et est appelé \define{somme de
    $\vect{u}$~et~$\vect{v}$}.}
\end{definition}

\begin{remark}
    $\vect{u} + \vect{0} = \vect{u}$ car on cumule avec la translation qui ne
    fait rien.
\end{remark}

\begin{proposition}[Relation de Chasles]
    \label{prop:chasles}
    Soient $A$,~$B$ et~$C$ trois points. Alors \UGHOST{$\vect{AB} + \vect{BC} =
    \vect{AC}$} (voir \vref{fig:chasles}).
\end{proposition}

\begin{figure}
    \ffigbox{}{
    \begin{subfloatrow}%\useFCwidth
    \ffigbox{}{
        \caption{}
        \label{fig:chasles}
        \begin{tikzpicture}
            \path (0,0)         coordinate[label=above:$A$] (A);
            \path (2,1)         coordinate[label=above:$B$] (B);
            \path (3.5,-0.5)    coordinate[label=below:$C$] (C);
            \draw[->] (A) -- (B);
            \draw[->] (B) -- (C);
            \draw[->,thick] (A) -- (C);
        \end{tikzpicture}
    }
    \ffigbox{}{
        \caption{}
        \label{fig:parallelogram}
        \begin{tikzpicture}
            \path (0,0)     coordinate[label=above:$A$] (A);
            \path (1,1.5)   coordinate[label=above:$B$] (B);
            \path (3,-0.5)  coordinate[label=below:$C$] (C);
            \path ($ (B)+(C)-(A) $) coordinate[label=right:$M$] (M);
            \draw[->]        (A) -- (B);
            \draw[->]        (A) -- (C);
            \draw[dashed]    (B) -- (M) -- (C);
            \draw[->,thick]  (A) -- (M);
        \end{tikzpicture}
    }
    \end{subfloatrow}
    \caption{\subref{fig:chasles} $\vect{AB} + \vect{BC} = \vect{AC}$ (relation
        de Chasles). \subref{fig:parallelogram} La règle du parallélogramme
        s'obtient par $\vect{AB} + \vect{AC} = \vect{AB} + \vect{BM} =
        \vect{AM}$.}
    }
\end{figure}

\begin{remark}[Règle du parallélogramme]
    Soient $A$,~$B$ et~$C$ trois points non alignés. Alors $\vect{AB} +
    \vect{AC} = \vect{AM}$ où $[AM]$~est la diagonale du parallélogramme de
    côtés $[AB]$~et~$[AC]$ (voir \vref{fig:parallelogram}).
\end{remark}

\begin{definition}
    L'\define{opposé de~$\vect{u}$} est l'unique vecteur~$\vect{v}$ tel que
    $\vect{u} + \vect{v} = \vect{0}$. On a $-\vect{AB} = \vect{BA}$ quels que
    soient les points $A$~et~$B$.
\end{definition}

\subsection{Coordonnées de vecteurs; multiplication par un réel}

\begin{definition}
    Les coordonnées de~$\vect{u}$ sont celles du point~$A$ tel que $\vect{OA} =
    \vect{u}$. On les note souvent en colonnes: $\vect{u}(x;y)$.
\end{definition}

\begin{proposition}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Alors
    $\vect{u} = \vect{v} \iff
            \left\{
            \begin{aligned}
                x &= x' \\
                y &= y'
            \end{aligned}
            \right.$
\end{proposition}

\begin{proposition}
    Si $A(x_A;y_A)$~et~$B(x_B;y_B)$ alors
    $\vect{AB}(\GHOST{x_B - x_A}; \GHOST{y_B - y_A})$.
\end{proposition}

\begin{proposition}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Alors $(\vect{u} +
    \vect{v})\vect{}(\GHOST{x+x'};\GHOST{y+y'})$.
\end{proposition}

\begin{definition}
    Soient $\vect{u}(x;y)$~et~$\alpha\in\mdR$. On définit le
    vecteur~$\alpha\cdot\vect{u}$ par
    $(\alpha\cdot\vect{u})\vect{}(\GHOST{\alpha x};\GHOST{\alpha y})$.
\end{definition}

\StudentMode


\begin{example}
    Dans un repère, on place $A(1;3)$~et~$B(3;4)$.
    \begin{enumerate}
        \item Placer~$C$ tel que $\vect{BC}+\vect{BA} = \vect{0}$.
        \item Placer~$D$ tel que $\vect{DB} = \vect{AC}$.
        \item Compléter les égalités suivantes:
            \begin{enumerate}[gathered]
                \item $\vect{DA} + \vect{BC} = \vect{\UGHOST*{DB}}$;
                \item $\vect{AB} = \UGHOST*{\frac{1}{2}}\vect{AC}$;
                \item $\vect{AB} = \UGHOST*{-\frac{1}{2}}\vect{BD}$;
                \item $\vect{CA} = \UGHOST*{\frac{2}{3}}\vect{CD}$;
                \item $\vect{AB} + \vect{AC} = \vect{\UGHOST*{D}C}$.
            \end{enumerate}
            \IfStudentT{\bigfiller{2}}
        \item Déterminer par le calcul les coordonnées de
            $\vect{AB}$,~$\vect{AC}$, $\vect{BC}$ et~$\vect{CD}$.
            \IfStudentT{\bigfiller{6}}
        \item On pose~$E(3;1)$. Placer~$F$ tel que $\vect{CF} = \vect{AE}$.
            Quelles sont ses coordonnées ?
            \IfStudentT{\bigfiller{2}}
        \item Quelle est la nature du quadrilatère~$BDEF$ ? Le démontrer.
            \IfStudentT{\bigfiller{5}}
        \item Déterminer les coordonnées de~$G$ tel que $\vect{BG} = \vect{DE} -
            \vect{AD}$.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
    \StudentOnly
        \begin{tikzpicture}
    \datavisualization [school book axes,
                        all axes={grid, unit length=.6cm,
                            ticks={tick typesetter/.code=}},
                        x axis={include value/.list={-4,9}},
                        y axis={include value/.list={-2,7}},
                        visualize as scatter=points,
                    ]
    data[set=labels] {
        x, y, label, style
        0, 0, $O$, {anchor=30,circle}
        1, 0, $I$, anchor=110
        0, 1, $J$, {anchor=-50,inner sep=2pt}
    }
    ;
\end{tikzpicture}%
    \EndStudentOnly
\end{example}


\section{Vecteurs colinéaires}

\begin{definition}
    Les vecteurs $\vect{u}$~et~$\vect{v}$ sont \define{colinéaires} si (et
    seulement si) \UGHOST{il existe~$\alpha\in\mdR$ tel que $\vect{u} =
    \alpha\vect{v}$ ou tel que $\vect{v} = \alpha \vect{u}$.}
\end{definition}

\begin{remark}
    Le vecteur nul est colinéaire à tous les vecteurs.
\end{remark}

\begin{remark}
    Deux vecteurs non nuls sont colinéaires s'ils ont la même direction.
\end{remark}

\begin{theorem}
    \label{thm:determinant}
    Soient $\vect{u}(x;y)$~et~$\vect{v}(x';y')$. Les vecteurs
    $\vect{u}$~et~$\vect{v}$ sont colinéaires si et seulement si
    \UGHOST{$xy' - x'y = 0$.}
\end{theorem}

\begin{remark}[Culture uniquement]
    Le nombre~$xy' - x'y$ est appelé \emph{déterminant} de
    $\vect{u}$~et~$\vect{v}$ et est noté~$\det(\vect{u},\vect{v})$.
\end{remark}

\begin{proposition}
    Soient $A$,~$B$, $C$, et~$D$ quatre points du plan. Alors
    $\vect{AB}$~et~$\vect{CD}$ sont colinéaires si et seulement si
    $(AB)$~et~$(CD)$ sont parallèles.
\end{proposition}

\begin{corollary}
    Soient $A$,~$B$, et~$C$ trois points du plan. Alors
    $\vect{AB}$~et~$\vect{AC}$ sont colinéaires si et seulement si
    $A$,~$B$, et~$C$ sont alignés.
\end{corollary}

\begin{example}
    Dans les cas suivants, indiquer si $A$,~$B$ et~$C$ sont alignés:
    \begin{enumerate}[gathered]
        \item $A(8;3)$, $B(11;4)$ et $C(\<3.5>;\<1.5>)$;
        \item $A(1;1)$, $B(3;2)$ et $C(4;3)$.
    \end{enumerate}
    \StudentOnly
    \begin{enumerate}
        \item \strut\UGHOST{$\vect{AB}(11-8;4-3)$ soit $\vect{AB}(3;1)$}
            \\
            \UGHOST{$\vect{AC}(\<3.5>-8;\<1.5>-3)$ c'est à dire
            $\vect{AC}(\<-4.5>;\<-1.5>)$}
            \\
            \UGHOST{$3 \times (\<-1.5>) - 1 \times (\<-4.5>) = 0$ donc
            $\vect{AB}$~et~$\vect{AC}$ sont colinéaires.}
            \UGHOST{$A$, $B$ et $C$ sont alignés.}
        \item \bigfiller{5}
    \end{enumerate}
    \EndStudentOnly
\end{example}


\section{Produit scalaire}
\persocourssetup{qrcodes=prod-scal}


% Après la partie                           % Exercices
% -----------------------------------------------------------------
% 2. Première formule du produit scalaire   % 17, 19, 24 p 129
% 3. Orthogonalité                          % 28 p 130
% 4. Produit scalaire et coordonnées        % 16, 20, 22 p 129
%                                           % 26, 27, 29, 30 p 130
% 5. Autres expressions du produit scalaire % 18, 21, 23 p 129

\begin{activity}
    \makelink{work}
    On considère un objet en forme de pavé, de masse très importante, en
    déplacement rectiligne de gauche à droite sur un sol horizontal grâce à des
    roulettes.

    On s'intéresse à plusieurs forces qui peuvent s'appliquer sur l'objet
    pendant son déplacement, que l'on représente dans chaque cas par un vecteur
    partant du centre de masse~$O$.

    \thisfloatsetup{valign=b}
    \begin{figure}[b]
    \def\basepic{%
            \path[pattern = north east lines]
                (0,0) rectangle (4,-0.3);
            \draw (0,0) -- (4,0);
            \fill (1,0.1) circle[radius=0.1];
            \fill (2.5,0.1) circle[radius=0.1];
            \draw[fill=black,fill opacity=0.05]
                (0.5,0.2) coordinate (A)
                rectangle (3,1.5) coordinate (B);
            \path ($ (A)!1/2!(B) $) coordinate[label=below:$O$] (O)
                    pic[point marker] {};
    }%
    \ffigbox{}{
        \begin{subfloatrow}[2]
            \ffigbox{}{\caption{}\label{fig:force-work-neg}%
                \begin{tikzpicture}
                    \basepic
                    \draw[thick,->]
                        (O) -- node[auto,swap]
                        {$\vect{F}$} +(180:2);
                \end{tikzpicture}%
            }%
            \ffigbox{}{\caption{}\label{fig:force-work-null}%
                \begin{tikzpicture}
                    \basepic
                    \draw[thick,->]
                        (O) -- node[auto]
                        {$\vect{F}$} +(90:2);
                \end{tikzpicture}%
            }%
        \end{subfloatrow}
        \begin{subfloatrow}[2]
            \ffigbox{}{\caption{}\label{fig:force-work-small}%
                \begin{tikzpicture}
                    \basepic
                    \draw (O) -- +(0:0.6);
                    \draw (O) +(0:0.4)
                        arc[radius=0.4, start angle=0, end angle=60];
                    \path (O) +(30:0.6) node {$\frac{\pi}{3}$};
                    \draw[thick,->]
                        (O) -- node[auto]
                        {$\vect{F}$} +(60:2);
                \end{tikzpicture}%
            }%
            \ffigbox{}{\caption{}\label{fig:force-work-big}%
                \begin{tikzpicture}
                    \basepic
                    \draw[thick,->]
                        (O) -- node[auto]
                        {$\vect{F}$} +(0:2);
                \end{tikzpicture}%
            }%
        \end{subfloatrow}
        \caption{%
            Forces motrices ou résistives, avec un travail plus ou moins grand.
        }
        \label{fig:force-work}%
    }
    \end{figure}

    \begin{enumerate}
        \item Parmi tous les cas représentés dans \cref{fig:force-work},
            le(s)quel(s) correspondent à une force \emph{motrice} --- qui
            favorise le déplacement ? Dans quel(s) cas la force n'a-t-elle aucun
            effet sur le mouvement, et quand est-elle \emph{résistive}
            --- elle s'oppose au mouvement ?
            \IfStudentT{\bigfiller{2}}
        \item Classer les différents cas dans l'ordre « d'efficacité » si l'on
            veut entretenir le mouvement.
            \IfStudentT{\bigfiller{1}}
        \item Pour traduire cette efficacité les physiciens ont introduit la
            notion de \emph{travail} d'une force. Dans la situation présentée
            ici, la force est constante d'intensité~\SI{20}{\newton} et l'on
            s'intéresse au mouvement rectiligne de l'objet sur une longueur
            de~\SI{1}{\meter}. Alors le travail en \emph{joules} est égal à
            \[ W = F \times AB \times \cos\vectangle(F,AB) \]
            où $F$~est le vecteur force et $\vect{AB}$~le vecteur correspondant
            au déplacement.

            Dans chaque cas, calculer le travail de la force. Est-ce cohérent
            avec le classement intuitif ?
            \IfStudentT{\bigfiller{5}}
    \end{enumerate}
\end{activity}

\subsection{Norme d'un vecteur}

\begin{definition}
    \makelink{norme}
    Soient $A$~et~$B$ deux points du plan, et $\vect{u} = \vect{AB}$. La
    \define{norme de~$\vect{u}$} est la longueur du vecteur~$\vect{u}$:
    $ \norm{\vect{u}} = \norm{\vect{AB}} = AB $
\end{definition}

\begin{example}
    On considère le triangle~$ABC$ tel que $AB=6$, $AC=5$ et $BC=8$, et l'on
    pose $\vect{u} = \vect{AB}$ et $\vect{v} = \vect{AC}$.
    \begin{enumerate}
        \item Déterminer $\norm{\vect{u}}$, $\norm{\vect{v}}$ et $\norm{\vect{v}
            - \vect{u}}$.\\
            \UGHOST{$\norm{\vect{u}} = \norm{\vect{AB}} = 6$;}
            \UGHOST{$\norm{\vect{v}} = \norm{\vect{AC}} = 5$}

            \UGHOST{$\norm{\vect{v}-\vect{u}} =
                \norm{\vect{AC} - \vect{AB}}$}%
            \UGHOST{${} = \norm{\vect{AC} + \vect{BA}}$} \\
            \UGHOST{${} = \norm{\vect{BA} + \vect{AC}} = \norm{\vect{BC}} = 8$}
        \item A-t-on $\norm{\vect{v} - \vect{u}} = \norm{\vect{v}} -
            \norm{\vect{u}}$ ?

            \UGHOST{$\norm{\vect{v}} - \norm{\vect{u}} = 5 - 6 = -1 \ne 8$}
    \end{enumerate}
\end{example}

\begin{proposition}
    Si $\vect{u}$~est un vecteur et $k$~un nombre réel, alors
    $\norm{k\,\vect{u}}
    = \abs{k} \norm{\vect{u}}$.
\end{proposition}

\subsection{Première formule du produit scalaire}

\begin{definition}
    \makelink{uvcos}
    Le \define{produit scalaire} de $\vect{u}$~et~$\vect{v}$, noté
    $\vect{u}\cdot\vect{v}$ --- qui se lit «$\vect{u}$ scalaire $\vect{v}$» ---
    est défini par :
    \begin{itemize}
        \item $\vect{u}\cdot\vect{v} = \UGHOST{
            \norm{\vect{u}} \times \norm{\vect{u}} \times \cos\vectangle(u,v)
            }$\\
            \UGHOST{si $\vect{u} \ne \vect{0}$ et $\vect{v} \ne \vect{0}$;}
        \item $\vect{u}\cdot\vect{v} = \UGHOST{0}$
            si $\vect{u} = \UGHOST{\vect{0}}$ ou
            $\vect{v} = \UGHOST{\vect{0}}$.
    \end{itemize}
\end{definition}

\begin{remark}
    Le produit scalaire de deux vecteurs n'est pas un vecteur mais \emph{un
    nombre réel}.
\end{remark}

\begin{remark}
    \begin{itemize}
        \item Si $\vect{u}$~et~$\vect{v}$ sont colinéaires de même sens,
            $\vect{u}\cdot\vect{v} = \UGHOST{
                \norm{\vect{u}} \times \norm{\vect{v}}
            }$.
        \item Si $\vect{u}$~et~$\vect{v}$ sont colinéaires de sens contraire,
            $\vect{u}\cdot\vect{v} = \UGHOST{
                - \norm{\vect{u}} \times \norm{\vect{v}}
            }$.
    \end{itemize}
\end{remark}

\begin{remark}
    Si $A$, $B$ et~$C$ sont trois points distincts, alors:
    \[ \vect{AB} \cdot \vect{AC}
        = AB\times AC \times \cos\left(\widehat{BAC}\right) \]
\end{remark}

\begin{example}
    \makelink{ex-uvcos}
    On considère un carré $ABCD$ de côté $3$.
    Calculer $\vect{BC}\cdot \vect{AC}$.
    \IfStudentT{\bigfiller{4}}
\end{example}

\begin{definition}
    $\vect{u}\cdot\vect{u}$ est également noté~$\vect{u}^2$, appelé
    \define{carré scalaire} de~$\vect{u}$.
\end{definition}

\begin{proposition}
    On a les propriétés suivantes:
    \begin{enumerate}
        \item le produit scalaire est \emph{commutatif}, c'est-à-dire que
            $\vect{u}\cdot\vect{v}=\vect{v}\cdot\vect{u}$;
        \item $\vect{u}^2=\norm{\vect{u}}^2$.
    \end{enumerate}
\end{proposition}

\subsection{Orthogonalité}

\begin{definition}
    On dit que deux vecteurs $\vect{u}$ et $\vect{v}$ sont \define[vecteurs
    orthogonaux]{orthogonaux} --- ce que l'on note $\vect{u}\perp\vect{v}$ ---
    si et seulement si {$\vect{u}\cdot\vect{v}=0$}.
\end{definition}

\begin{remark}
  Le vecteur nul est orthogonal à tout vecteur du plan.
\end{remark}

\begin{proposition}
    Deux vecteurs non nuls $\vect{u}$~et~$\vect{v}$ sont orthogonaux si et
    seulement si $\left(\vect{u};\vect{v}\right)=\pm\dfrac{\pi}{2}\;[2\pi]$.
\end{proposition}

\begin{remark}
    Concrètement, cela veut dire que deux vecteurs non nuls sont orthogonaux
    quand ils «forment un angle droit».
\end{remark}

\begin{remark}
    Pour $k\neq 0$, on sait que
    $\left(\vect{u};k\vect{v}\right)=\left(\vect{u};\vect{v}\right)\;[2\pi]$ si
    $k>0$ et $\left(\vect{u};k\vect{v}\right)=\left(\vect{u};\vect{v}\right) +
    \pi\;[2\pi]$ si $k<0$, donc si $\vect{u}$ est orthogonal à $\vect{v}$, alors
    $\vect{u}$ est orthogonal à tout vecteur colinéaire à $\vect{v}$.
\end{remark}


\begin{corollary}
    Deux droites du plan sont perpendiculaires si et seulement si un vecteur
    directeur de l'une est orthogonal à un vecteur directeur de l'autre.
\end{corollary}

\subsection{Produit scalaire et coordonnées}

Dans toute la suite, on dispose d'un repère \emph{orthonormé} du plan.

\begin{proposition}
    Soient $\vect{u}(x;y)$ et $\vect{v}(x';y')$ dans un repère orthonormé. Alors
    $\vect{u}\cdot\vect{v} = xx' + yy'$.
\end{proposition}

\begin{corollary}
    Soit $\vect{u}(x;y)$ dans un repère orthonormé. Alors
    $\norm{\vect{u}} = \sqrt{x^2 + y^2}$.
\end{corollary}

\begin{example}
    \makelink{ex-orth}
    Soient $\point{A}(-1;3)$, $\point{B}(5;1)$, $\point{C}(3;5)$
    et~$\point{D}(6;14)$ dans un repère~$(O;I;J)$ orthonormé. Montrer que
    $(AB)$~et~$(CD)$ sont perpendiculaires.
    \IfStudentT{\bigfiller{3}}
\end{example}

\begin{example}
    \makelink{ex-angle}
    Dans un repère~$(O;I;J)$ orthonormé, on considère
    $\point{B}(5;1)$~et~$\point{L}(2;4)$.
    \begin{enumerate}
        \item Calculer $\vect{OB}\cdot\vect{OL}$, $OB$ et $OL$.
            \IfStudentT{\bigfiller{3}}
        \item En déduire une mesure de l'angle $\widehat{BOL}$, qu'on donnera
            en degrés arrondi à \<0.1>~près.
            \IfStudentT{\bigfiller{2}}
    \end{enumerate}
\end{example}

\subsection{Autres expressions du produit scalaire}

\begin{proposition}
    $\vect{u}\cdot\vect{v} = \GHOST{\dfrac{1}{2}\left(
        \norm{\vect{u}}^2 + \norm{\vect{v}}^2 - \norm{\vect{v}-\vect{u}}^2
        \right)}$
\end{proposition}

\begin{proposition}
    $\vect{u}\cdot\vect{v} = \GHOST{\dfrac{1}{2}\left(
    \norm{\vect{u}+\vect{v}}^2 - \norm{\vect{u}}^2 - \norm{\vect{v}}^2
        \right)}$
\end{proposition}

\begin{example}
    \makelink{ex-polar}
    Soit $ABC$ un triangle tel que $AB=6$, $AC=5$ et $BC=8$.
    \begin{enumerate}
        \item Calculer $\vect{BA}\cdot \vect{AC}$.
            \IfStudentT{\bigfiller{4}}
        \item $\vect{BA}$~et~$\vect{AC}$ sont-ils orthogonaux ?
            \IfStudentT{\bigfiller{1}}
    \end{enumerate}
\end{example}

\let\vv\vect

\begin{definition}
    Dans le plan, on considère une droite $(AB)$ et un point $C$ extérieur à
    cette droite. Le \define{projeté orthogonal} de $C$ sur la droite $(AB)$ est
    l'intersection~$H$ de $(AB)$ et de la perpendiculaire à $(AB)$ passant par
    $C$.
\end{definition}

\begin{proposition}
    Soient $A$, $B$ et $C$ trois points distincts du plan et $H$ le projeté
    orthogonal de $C$ sur $(AB)$.
    Alors $\vect{AB}\cdot \vect{AC}=\vect{AB}\cdot \vect{AH}$.

Plus précisément, si $H\neq A$, il y a deux configurations possibles :
\setlength\multicolsep{0pt}
\begin{multicols}{2}
    \begin{itemize}
        \item $\vv{AB}$ et $\vv{AH}$ ont même sens :
            \begin{center}
                \begin{tikzpicture}[general,scale=0.8]
                    \draw (0,0)--(5,0)     ;
                    \draw[dashed] (3,2)--(3,0) ;
                    \draw[->,color=A1, epais] (1,0)--(4,0);
                    \draw[->,color=A1, epais] (1,0)--(3,2);
                    \draw[->,color=A1, epais] (1,0)--(3,0);
                    \draw (1,0) node[below]{$A$} ;
                    \draw (4,0) node[below]{$B$} ;
                    \draw (3,2) node[above]{$C$} ;
                    \draw (3,0) node[below]{$H$} ;
                    \draw (3,0.3)--(3.3,0.3)--(3.3,0) ;
                \end{tikzpicture}	
            \end{center}
            $\vv{AB}\cdot \vv{AC}=\vv{AB}\cdot \vv{AH}=AB\times AH$
        \item $\vv{AB}$ et $\vv{AH}$ sont de sens opposés :
            \begin{center}
                \begin{tikzpicture}[general,scale=0.8]
                    \draw (0,0)--(5,0) ;
                    \draw[dashed] (0.5,2)--(0.5,0) ;
                    \draw[->,color=A1, epais] (2,0)--(4.5,0);
                    \draw[->,color=A1, epais] (2,0)--(0.5,2);
                    \draw[->,color=A1, epais] (2,0)--(0.5,0);
                    \draw (2,0) node[below]{$A$} ;
                    \draw (4.5,0) node[below]{$B$} ;
                    \draw (0.5,2) node[above]{$C$} ;
                    \draw (0.5,0) node[below]{$H$} ;
                    \draw (0.5,0.3)--(0.2,0.3)--(0.2,0) ;
                \end{tikzpicture}	
            \end{center}
            $\vv{AB}\cdot \vv{AC}=\vv{AB}\cdot \vv{AH}=-AB\times AH$
    \end{itemize}
\end{multicols}
\end{proposition}

\begin{example}
    \makelink{ex-proj}
    On considère le carré $ABCD$ de côté $2$ et $I$ le milieu de $[AB]$.
    \begin{enumerate}
        \item Faire une figure.
        \item Calculer $\vv{AB}\cdot \vv{AC}$ puis $\vv{IC}\cdot \vv{BI}$.
    \end{enumerate}
    \IfStudentT{\bigfiller{15}}
\end{example}

\subsection{Propriétés algébriques}

\begin{proposition}
    On a les propriétés suivantes:
    \begin{enumerate}
        \item Le produit scalaire est \define{distributif} par rapport
            à l'addition:
            \[\vect{u}\cdot(\vect{v}+\vect{w}) =
            \vect{u}\cdot\vect{v} + \vect{u}\cdot\vect{w}\]
        \item Pour deux réels $k$ et $k'$:
            \[ (k\vect{u})\cdot (k'\vect{v}) =
            (k\times k')\vect{u}\cdot \vect{v} \]

            En particulier $(-\vect{u})\cdot\vect{v} = \vect{u}\cdot(-\vect{v})
            = -\vect{u}\cdot\vect{v}$
    \end{enumerate}
\end{proposition}

\begin{proposition}[Identités remarquables]
    On a:
    \begin{enumerate}
        \item $\norm{\vect{u}+\vect{v}}^2 = \left(\vect{u}+\vect{v}\right)^2 =
            \vect{u}^2+2\vect{u}\cdot \vect{v}+\vect{v}^2 =
            \norm{\vect{u}}^2+2\vect{u}\cdot \vect{v}+\norm{\vect{v}}^2$
        \item $\norm{\vect{u}-\vect{v}}^2 = \left(\vect{u}+\vect{v}\right)^2 =
            \vect{u}^2-2\vect{u}\cdot \vect{v}+\vect{v}^2 =
            \norm{\vect{u}}^2+2\vect{u}\cdot \vect{v}+\norm{\vect{v}}^2$
        \item $(\vect{u}+\vect{v})\cdot(\vect{u}-\vect{v}) =
            \vect{u}^2-\vect{v}^2 =
            \norm{\vect{u}}^2-\norm{\vect{v}}^2$
    \end{enumerate}
\end{proposition}

\end{document}
