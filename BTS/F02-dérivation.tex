% vim: tw=80
% Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
\documentclass[a5paper,9pt]{rivbook}

\usepackage{persocours}
\setmainlanguage{french}

\usepackage{mdframed}

\usepackage{tikz}
\usetikzlibrary{calc,
                datavisualization,
                datavisualization.formats.functions}

\rivmathlib{geometry}

\def\Tx#1{\Delta_{#1,h}}


\begin{document}

%\StudentMode

\chapter{Rappels sur la dérivation}

\subsection{Nombre dérivé et tangente}

\begin{definition}
    Soient~$f$ une fonction définie sur un intervalle~$I$ et $a\in I$.
    On appelle \emph{nombre dérivé} de~$f$ en~$a$ le nombre
    \[ f'(a) = \lim_{h\tendsto 0} \frac{f(a+h) - f(a)}{h} \]
    quand la limite existe.
\end{definition}

\begin{remark}
    Si $f$~est dérivable en~$a$, alors le coefficient directeur de la
    \define{tangente} à~$\mcC_f$ au point d'abscisse~$a$ est~$f'(a)$.
    Voir la figure~\ref{fig:corde+tangente}.

    En particulier, la tangente est \emph{horizontale} si et seulement
    si~$f'(a) = 0$
\end{remark}

\begin{figure}
    \centering
    \caption{Interprétation graphique du taux d'accroissement et du nombre
    dérivé. $\Tx{a}$~est le coefficient directeur de la corde~$(AB)$. À gauche,
    $h$~est relativement grand et $(AB)$~n'est pas proche de la tangente. À
    droite, $h$~est plus petit et $(AB)$~ressemble nettement plus à la
    tangente.}
    \label{fig:corde+tangente}
    \def\myfigure#1{%
        \begin{tikzpicture}[
                declare function={
                    a = 1.5;
                    h = #1;
                    f(\x) = 1 / (\x + 1.5) + pow(\x, 2) / 8;
                    fp(\x) = -1 / pow(\x + 1.5, 2) + \x / 4;
                    t(\x) = fp(a) * (\x - a) + f(a);
                    d = (f(a+h) - f(a)) / h;
                    c(\x) = d * (\x - a) + f(a);
                }
            ]
            \datavisualization [school book axes,
                                all axes={unit length=0.65cm},
                                x axis={ticks and grid={
                                    major at={(a) as $a$, (a+h) as $a+h$}},
                                    label=$x$},
                                y axis={ticks and grid={none}, label=$y$},
                                visualize as smooth line=f,
                                f={style=thin, label in data={
                                        text=$\mcC_f$,when=x is -0.8}},
                                visualize as line=t,
                                visualize as line=c,
                                c={style=densely dashed},
            ]
            data[set=f, format=function] {
                var x : interval[-1:6] samples 70;
                func y = f(\value x);
            }
            data point[set=t, x=-0.5, y=(t(-0.5))]
            data point[set=t, x=6,    y=(t(6))]
            data point[set=c, x=0.5, y=(c(0.5))]
            data point[set=c, x=(a), y=(f(a)), name=A]
            data point[set=c, x=(a+h), y=(f(a+h)), name=B]
            data point[set=c, x=6, y=(c(6))]
            info {
                \foreach \x in {A,B} {
                    \path (\x) node[above left] {$\x$} pic{point};
                }
            }
            ;
        \end{tikzpicture}%
    }
    \myfigure{3.5}\hfill\myfigure{0.9}
\end{figure}

\begin{proposition}
    Si $f$~est dérivable en~$a$, alors la tangente à~$\mcC_f$ au point
    d'abscisse~$a$ a pour équation
    \[ y = f'(a)(x-a) + f(a) \]
\end{proposition}

\subsection{Dérivées de fonctions usuelles}


\begin{theorem}
    \label{thm:common-derivatives}
    \leavevmode
    \begin{center}
    \normalfont
    $\begin{array}{L*{3}{cL}}
        \firsthline
        \text{Fonction} & \text{Dérivable sur} & \text{Fonction dérivée} \\
        \hline
        f(x) = k \text{ avec $k \in \mdR$} & \mdR & f'(x) = 0 \NL
        f(x) = x & \mdR & f'(x) = 1 \NL
        f(x) = mx + p \text{ avec $m,p \in \mdR$} & \GHOST{\mdR} &
                f'(x) = \GHOST{m} \NL
        f(x) = x^2 & \GHOST{\mdR} & f'(x) = \GHOST{2x} \NL
        f(x) = x^n \text{ avec $n \ge 0$ } & \GHOST{\mdR} &
                f'(x) = \GHOST{nx^{n-1}} \NL
        f(x) = \dfrac{1}{x} & \GHOST{\IN]-\infty;0;[
                \text{ ou } \IN]0;+\infty;[} &
                f'(x) = \GHOST{-\dfrac{1}{x^2}} \NL
        f(x) = \sqrt{x} & \GHOST{\IN]0;+\infty;[} &
                f'(x) = \GHOST{\dfrac{1}{2\sqrt{x}}} \NL
        f(x) = \sin(x) & \GHOST{\mdR} & f'(x) = \GHOST{\cos(x)} \NL
        f(x) = \cos(x) & \GHOST{\mdR} & f'(x) = \GHOST{\sin(x)} \NL
        f(x) = \ln(x) & \GHOST{\IN]0;+\infty;[} &
                f'(x) = \GHOST{\dfrac{1}{x}} \NL
        f(x) = \e^x   & \GHOST{\mdR} &
                f'(x) = \GHOST{\e^x} \NL
        f(x) = x^n \text{ avec $n < -1$ } & \GHOST{\IN]-\infty;0;[
                \text{ ou } \IN]0;+\infty;[} &
                f'(x) = \GHOST{nx^{n-1}} \NL
        f(x) = x^a \text{ avec $a \in \mdR$ } & \GHOST{\IN]0;+\infty;[} &
                f'(x) = \GHOST{ax^{a-1}} \\
        \lasthline
    \end{array}$
    \end{center}
\end{theorem}

\subsection{Théorèmes opératoires}

\begin{center}
    \everymath{\displaystyle}
    \normalfont$
    \begin{array}{LcLcLcLcL}
    \firsthline
    \text{Fonction} & \text{Dérivée} &
    \text{Exemple} & \text{Solution} \\
    \hline
    f = u + v & f' = u' + v'
            & f(x) = \cos x + x^2 & f'(x) = \GHOST{-\sin{x} + 2x} \NL
    f = k u & f' = k u'
            & f(x) = 3 x^5 & f'(x) = \GHOST{3 \times 5x^4 = 15x^4} \NL
    f = u \times v & f' = u'v + uv'
            & f(x) = x \sin x & f'(x) = \GHOST{1\times\sin x + x\cos x} \NL
    f = \frac{u}{v} & f' = \frac{u'v - uv'}{v^2}
            & f(x) = \frac{\sin x}{x}
            & f'(x) = \GHOST{\frac{x\cos x - \sin x}{x^2}} \NL
    \lasthline
    \end{array}$%
\end{center}

\subsection{Sens de variation}

\begin{theorem}
    Soit~$f$ une fonction dérivable sur~$I$.
    \begin{enumerate}
        \item Si $f'(x)\geq 0$ pour~tout~$x \in I$ alors $f$~est croissante
            sur~$I$ (si $f'(x) > 0$ alors $f$~est strictement croissante);
        \item Si $f'(x)\leq 0$ pour~tout~$x \in I$ alors $f$~est décroissante
            sur~$I$ (si $f'(x) < 0$ alors $f$~est strictement décroissante);
        \item Si $f'(x) = 0$ pour~tout~$x \in I$ alors $f$~est constante.
    \end{enumerate}
    Réciproquement, si $f$~est croissante (resp. décroissante, resp. constante)
    alors $f'(x) \geq 0$ (resp. $f'(x) \leq 0$, $f'(x) = 0$).
\end{theorem}

\begin{example}
    Dresser le tableau de variations de $f$ définie par
    \[ f(x) = x^3 + \frac{3}{2}x^2-6x+1 \]
\end{example}

\subsection{Extremum local}

\begin{proposition}
    Soit~$f$ une fonction dérivable sur un intervalle \emph{ouvert}~$I$.
    Si~$f'$~s'annule en~$a\in I$ \emph{en changeant de signe}, alors $f(a)$~est
    un extremum local.
\end{proposition}

\begin{exercise}
    Soit $f$~définie sur~$\mdR$ par $f(x) = (100 - 2x)(x+10)$.
    \begin{enumerate}
        \item Calculer si possible $f'(x)$ (on pourra développer).
        \item Montrer que $f$~admet un maximum. En quelle abscisse est-il
            atteint ? Comment aurait-t-on pu le retrouver?
    \end{enumerate}
\end{exercise}


\end{document}
